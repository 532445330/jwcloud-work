// 百度地图API功能
//var map = new BMap.Map("map_demo");
var loginName=decodeURIComponent($.cookie('login_user'));
var token=$.cookie('login_token');
var requestData = JSON.parse(window.localStorage.getItem('mapData'));
var keyWord=requestData?requestData.data.text:'';
var alias=requestData?requestData.data.alias:'';
var key = $.cookie('mapPath')?JSON.parse($.cookie('mapPath')):'';
var dogId = $.cookie('dogId');
var wordArr='';
var sourceJson={};
var currentPage=1;
var allDataList=[];
var totalPages=0;  				//所有页数
var viewRangeList=[];
var map = new BMap.Map("map", {minZoom: 9, maxZoom: 16});
var b = new BMap.Bounds(new BMap.Point(119.806048, 38.585799), new BMap.Point(123.657982, 41.112183));
try {
    BMapLib.AreaRestriction.setBounds(map, b);
} catch (e) {
    alert(e);
}
var searchType='';
function showList(n){
    var oParent=$(n).parent();
    var ele=oParent.find('.map-list');
    var oS=$(n).find('span').eq(1);
    if(ele.css('display')=='none'){
        oS.text('收起');
        ele.show();
    }else{
        oS.text('展开');
        ele.hide();
    }
    if(oParent.hasClass('map-high')){
        // cleanInput();
    }
}
if(!requestData){
    showList('.map-title');
    $('.data-list').hide();
}
function cleanInput(){
    var oInput=$('.map-high').find('input[type=text]');
    for(var i=0;i<oInput.length;i++){
        oInput.eq(i).val('');
    }
}
if(!!key.word){
    $.ajax({
        url: 'http://' + lastIp + '/index.php',
        type: 'POST',
        async: false,
        data: JSON.stringify({
            interface: 'getField',
            act: 'getFieldByUnique',
            uniqueName: key.word,
            Token: token,
            username: loginName
        }),
        xhrFields:{
            withCredentials:true
        },
        success: function (data) {
            if (data.status == 0) {
                $.cookie('uniqueWord',$('.search-only input:radio:checked').next().text());
                if(data.Item.length===0){
                    wordArr = key.word;
                }else{
                    var arr=[];
                    for(var i=0;i<data.Item.length;i++){
                        arr.push(data.Item[i].fieldName);
                    }
                    wordArr=arr;
                }
            } else {
                wordArr = key.word;
            }
        },
        error: function (err) {
            console.log(err);
        }
    });
}

$.ajax({
    url:'http://21.26.96.110/index.php',
    type:'POST',
    async: false,
    data:JSON.stringify({
        "interface":'getSource',
        "act":'getSourceTypeField',
        "typeId":21,
        "Token":token,
        "username":loginName
    }),
    success:function(dataStr){
        var data=JSON.parse(dataStr);
        if(data.status==0){
            var sourceList=data.Item;
            for(var i=0;i<sourceList.length;i++){
                var fieldList=sourceList[i].field.field;
                var sourceNameS=sourceList[i].sourceName.replace(/[,.\s]/g,'');
                sourceJson[sourceNameS]={};
                allDataList=[];
                for(var j=0;j<fieldList.length;j++){
                    if(fieldList[j].isLatitude==0){
                        sourceJson[sourceNameS].latitude=fieldList[j].fieldName;
                    }
                    if(fieldList[j].isLongitude==0){
                        sourceJson[sourceNameS].longitude=fieldList[j].fieldName;
                    }
//                        sourceJson[sourceNameS][fieldList[j].fieldName]=fieldList[j].level;
                }
            }
            if(!!requestData){  //不是首页过来
                getPageData();
            }

        }
    }
});
function getArg(obj,system){
    var dataJson={};
    $('.map-list input[type=text]').removeClass('warn');
    for(var i=0;i<obj.length;i++){
        if(!!obj.eq(i).val()){
            if(system==16){
                dataJson[obj.eq(i).attr('name').toLowerCase()]=parseInt(obj.eq(i).val().trim(),16);
            }else{
                dataJson[obj.eq(i).attr('name').toLowerCase()]=obj.eq(i).val().trim();
            }
        }else{
            obj.eq(i).addClass('warn');
            dataJson=false;
            break;
        }
    }
    dataJson.alias=alias?alias:'地图地图';
    dataJson.dogId=dogId;
    return dataJson;
}
$('.map-high .map-list input[type=button]').on('click',function(e){
    e.stopPropagation();
});
$('.map-high').on('click',function(){
    $('.map-list input[type=text]').removeClass('warn');
});
function searchMovie(obj){
    var ele=$(obj).parent().find('input[type=text]');
    var system=$(obj).parent().parent().find('.system-box input:radio:checked').attr('id')=='system10'?10:16;
    var arg=getArg(ele,system);
    if(!!arg){
        getsearchPageData(0,arg);
    }
}
function searchCDMA(obj){
    var ele=$(obj).parent().parent().find('input[type=text]');
    var system=$(obj).parent().parent().find('.system-box input:radio:checked').attr('id')=='system-spe-10'?10:16;
    var arg=getArg(ele,system);
    console.log(arg);
    if(!!arg){
        getsearchPageData(1,arg);
    }

}
function searchLng(obj){
    var ele=$(obj).parent().find('input[type=text]');
    var arg=getArg(ele);
    if(arg){
        getsearchPageData(2,arg);
    }

}

function getsearchPageData(type,arg){
    var reqUrl='';
    switch(type){
        case 0:
            reqUrl='mobileUnicomBaseStationQuery';
            break;
        case 1:
            reqUrl='telecomBaseStationQuery';
            break;
        case 2:
            reqUrl='lngAndLatQuery';
            if(!!requestData){
                requestData.url='http://21.26.96.110:8080/jwcloud/map/'+reqUrl;
            }
            break;
    }
    if(type==2){
        currentPage=1;
        arg.page=currentPage;
    }
    if(!!requestData){
        requestData.data=arg;
    }
    function findRangSame(arr,obj){
        for(var i=0;i<arr.length;i++){
            if(arr[i].lat==obj.location.lat && arr[i].lng==obj.location.lon && arr[i].range==obj.range){
                return true;
            }
        }
        return false;
    }
    $.ajax({
        url:'http://21.26.96.110:8080/jwcloud/map/'+reqUrl,
        type:'POST',
        async:false,
        data:arg,
        xhrFields:{
            withCredentials:true
        },
        success:function(data){
            if(data.status==0){
                 if(type<=1){
                    var lat=data.location.lat;//纬度
                    var lng=data.location.lon; //经度
                    var range=data.range;
                    var fieldList=data._source;
                    if(!findRangSame(viewRangeList,data)){
                        viewRangeList.push({'lat':lat,'lng':lng,'range':range});
                        addRange(lng,lat,range);
                        addMarker(lng,lat,fieldList);
                    }
                }
            }else if(data.status==-1){
                alert('没有查询结果');
            }else{
                $('.data-list').show();
                type='searchLatLng';
                var dataList=data.hits;
                $('.map-list ul').html('');
                allDataList=[];
                for(var e=0;e<dataList.length;e++){
                    allDataList.push(dataList[e]._source);
                }
                console.log(allDataList);
                totalPages=data.totalPages;
                repeatData(dataList);
            }

        },
        error:function(err){
            console.log(err);
        }
    })
}

function addRange(lng,lat,range){
    var point = new BMap.Point(lng,lat);
    map.centerAndZoom(point, 16);
    var circle = new BMap.Circle(point,(range*1000).toFixed(2),{strokeColor:"red", strokeWeight:0, strokeOpacity:0.5}); //创建圆
    map.addOverlay(circle);
    map.enableScrollWheelZoom(true);
    var marker = new BMap.Marker(point);
    map.addOverlay(marker);
    map.addControl(new BMap.NavigationControl());			//添加左上角放大比例显示
    var b = new BMap.Bounds(new BMap.Point(119.806048, 38.585799), new BMap.Point(123.657982, 41.112183));
    try {
        BMapLib.AreaRestriction.setBounds(map, b);
    } catch (e) {
        alert(e);
    }
}
function getPageData(){
    requestData.data.page=currentPage;
    $.ajax({
        url:requestData.url,
        type:"POST",
        async: false,
        data:requestData.data,
        xhrFields:{
            withCredentials:true
        },
        success:function(data){
            var dataList=data.hits;
            for(var e=0;e<dataList.length;e++){
                allDataList.push(dataList[e]._source);
            }
            totalPages=data.totalPages;
            repeatData(dataList);
        },
        error:function(){

        }
    })
}
function repeatData(dataList){
    var oLi='';
    for(var i=0;i<dataList.length;i++){
        var longitudeNum='';
        var latitudeNum='';
        $.each(sourceJson,function(k,v){
            if(dataList[i]._type==k){
                var list=dataList[i]._source;
                $.each(list,function(key,value){
                    if(key==v.longitude){
                        longitudeNum=value;
                    }
                    if(key==v.latitude){
                        latitudeNum=value;
                    }
                })
            }
        });
        oLi+='<li data="'+((20*(currentPage-1))+i)+'" onclick="creatPosition(this)"><i>'+((20*(currentPage-1))+i+1)+'、</i><p>经度：<span class="lo">'+longitudeNum+'</span></p><p>纬度：<span class="la">'+latitudeNum+'</span></p></li>';
    }
    $('.map-list ul').append(oLi);
    limitScroll=true;
}
var limitScroll=true;
$('.map-list').on('scroll',function(){
    if(!limitScroll)return;
    var scrollTop = $('.data-list .map-list').scrollTop();
    console.log(scrollTop);
    var oH = $('.data-list .map-list').height();
    var oUh = $('.data-list .map-list ul').height();
    if(currentPage<totalPages){
        if(scrollTop+oH>oUh-20){
            limitScroll=false;
            currentPage++;
            getPageData();
        }
    }
});
function creatPosition(n){
    var longitude=$(n).find('.lo').text();
    var latitude=$(n).find('.la').text();
    var fieldList=allDataList[$(n).attr('data')];
    createMap(longitude,latitude,fieldList);
    $('.map-list ul li').removeClass('on');
    $(n).addClass('on');
}
if(!!requestData){
    $('.map-list ul li').eq(0).trigger('click');
}else{
    map.enableScrollWheelZoom(true);                        //启用滚轮放大缩小
    map.addControl(new BMap.NavigationControl());			//添加左上角放大比例显示
    var point = new BMap.Point(121.561193, 38.927656);
    map.centerAndZoom(point, 12);
}

function createMap(longitude,latitude,fieldList) {
    map.enableScrollWheelZoom(true);                        //启用滚轮放大缩小
    map.addControl(new BMap.NavigationControl());			//添加左上角放大比例显示
    if (Number(longitude) == 0 || Number(latitude) == 0) {
        var point = new BMap.Point(121.561193, 38.927656);
        map.centerAndZoom(point, 12);
    } else {
        var point = new BMap.Point(longitude, latitude);
        map.centerAndZoom(point, 16);
//        setTimeout(function(){
//            map.setZoom(18);
//        }, 2000);  //2秒后放大到14级
        addMarker(longitude, latitude,fieldList)

    }
}
function addMarker(longitude, latitude,fieldList){
    var opts = {
        width: 420,
        minHeight: 300,
        maxHeight:400,
        overflowY: 'auto'
    };

    var marker = new BMap.Marker(new BMap.Point(longitude, latitude));
    map.addOverlay(marker);
    addClickHandler(fieldList, marker);
    function addClickHandler(content, marker) {
        var list = '';
        var smallList = '';
        var longList = '';
        var specialData = '';
        var regT1 = /\t/g;
        var reg=new RegExp('('+keyWord+')','img');
        $.each(content,function(i,v){
            var field = i;
            var value = v;
            if(field == '展示时间' || field=='上传时间' || field == '采集时间'){

            }
            else if (field == '数据来源') {
                specialData = '<li class="clearfix"><p>' + field + ':</p><p>' + value + '</p></li>';
            }
            else if (field.length + value.length <= 12) {
                var viewValue=value.replace(reg,'<span class="highLight" style="background:#ea4844;color:#fff;">$1</span>');
                if (!!key.word) {
                    if (findArrSame(wordArr, field)) {

                        smallList += '<li class="clearfix"><p>' + field + ':</p><p><a href="/mixresult?wd=' + value.replace(regT1, '\\t') + '&word=' + (key.word ? key.word : '') + '&unison=' + key.unison + '&listCount=20&currentPage=1" target="_blank">' + viewValue + '</a></p></li>';
                    } else {
                        smallList += '<li class="clearfix"><p>' + field + ':</p><p>' + value + '</p></li>';
                    }
                } else {
                    smallList += '<li class="clearfix"><p>' + field + ':</p><p><a href="/mixresult?wd=' + value.toString().replace(regT1, '\\t') + '&word=' + (key.word ? key.word : '') + '&unison=' + key.unison + '&listCount=20&currentPage=1" target="_blank">' + viewValue + '</a></p></li>';
                }

            } else {
                var viewValue=value.replace(reg,'<span class="highLight" style="background:#ea4844;color:#fff;">$1</span>');
                if (!!key.word) {
                    if (findArrSame(wordArr, field)) {
                        longList += '<li class="clearfix"><p>' + field + ':</p><p><a href="/mixresult?wd=' + value.replace(regT1, '\\t') + '&word=' + (key.word ? key.word : '') + '&unison=' + key.unison + '&listCount=20&currentPage=1" target="_blank">' + viewValue + '</a></p></li>';
                    } else {
                        longList += '<li class="clearfix"><p>' + field + ':</p><p>' + viewValue + '</p></li>';
                    }
                } else {
                    longList += '<li class="clearfix"><p>' + field + ':</p><p><a href="/mixresult?wd=' + value.toString().replace(regT1, '\\t') + '&word=' + (key.word ? key.word : '') + '&unison=' + key.unison + '&listCount=20&currentPage=1" target="_blank">' + viewValue + '</a></p></li>';
                }
            }
        });
        var sContent = "<h4 style='margin:0 0 5px 0;padding:0.2em 0'>信息详情</h4><div class='listbox'><ul class='smallList clearfix'>" + smallList + "</ul><ol class='longList'>" + longList + specialData + "</ol></div>";

        marker.addEventListener('click', function (e) {
            openInfo(sContent, e);
        })
    }

    function openInfo(content, e) {
        var p = e.target;
        var point = new BMap.Point(p.getPosition().lng, p.getPosition().lat);
        var infoWindow = new BMap.InfoWindow(content, opts);
        map.openInfoWindow(infoWindow, point);
    }
}
function findArrSame(a,n){
    if(typeof(a)=='string'){
        if(a==n){
            return true;
        }else{
            return false;
        }
    }else{
        for(var i=0;i<a.length;i++){
            if(a[i]==n) {
                return true;
            }
        }
        return false;
    }
}
//	var marker = new BMap.Marker(point);     //创建标注
//	map.addOverlay(marker);						//添加标注
//
//	var infoWindow = new BMap.InfoWindow("地址：北京市东城区王府井大街88号乐天银泰百货八层", opts);
//	marker.addEventListener('click',function(){
//	    map.openInfoWindow(infoWindow,point);
//	})

/*
 var b = new BMap.Bounds(new BMap.Point(121.525,39.037),new BMap.Point(121.525,39.037));
 try {
 BMapLib.AreaRestriction.setBounds(map, b);
 } catch (e) {
 alert(e);
 }
 */
window.onload=function(){
    $('.BMap_cpyCtrl.BMap_noprint.anchorBL').remove();
};
