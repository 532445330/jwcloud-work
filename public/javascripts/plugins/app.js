var app = angular.module('app', ['ui.router']);

app.controller('my-controller',function($scope,$location,$http,$timeout,$rootScope,$state){
    $scope.query = $location.search();
    $scope.query['loginName'] = decodeURIComponent($.cookie('login_user'));
    $scope.query['dogId'] = $.cookie('dogId');
    $scope.switch=new Object();
    $scope.switch.isShowExplorer = false; //文档浏览窗口显示切换
    $scope.switch.isShowHistory = true; //展示历史记录
    $scope.data=new Object();
    $scope.path=new Object();
    $scope.data.formListTotalPageNum=1;
    $scope.data.formListCurrentPage=$location.search().page?$location.search().page:1;
    $scope.data.secondBtn=[{'name':'全部','value':'all'},{'name':'手机','value':'phone'},{'name':'介质','value':'media'}];
    $scope.data.currentScend='all';   //当前的全部、手机、介质类型切换
    $scope.data.wordFrequencyList='';  //词汇列表
    $scope.path.currentFormWordPath='';  //当前报表词汇路经
    $scope.data.currentWord='';        //当前词汇
    $scope.data.currentPageIndex=0;    //当前报表词汇路经索引
    $scope.data.searchWordFrequencyList=[];
    $scope.data.currentSearchForm='';    //搜索关键词
    $scope.data.keyWord='';   //请求使用的关机词
    $scope.data.noData=false;//没有结果提示
    $scope.switch.isShowFastSearchChangeBtn=false;  //快速搜索报表内容 切换单词 按钮显示  默认不显示
    $scope.data.viewWordList=[];
    $scope.data.currentSearchWord='';

    $scope.data.currentSearchForm='';  //查找报表输入框中内容

    $scope.path.currentFilePath='';  //展示页frame里的链接

    $scope.switch.isRuihai=true;
    $scope.switch.isWordFrequency=true;
    $scope.data.selectbar=0;   //数据挖掘默认类别索引
    $scope.data.floatDataList=[
        {
            name:'身份证',
            class:'idcard',
            onImg:'/images/data-icon/icon_idcard_1.png'
        },
        {
            name:'手机号',
            class:'phone',
            onImg:'/images/data-icon/icon_phone_1.png'
        },
        {
            name:'地址',
            class:'address',
            onImg:'/images/data-icon/icon_address_1.png'
        },
        {
            name:'ip',
            class:'ip',
            onImg:'/images/data-icon/icon_ip_1.png'
        },
        {
            name:'网址',
            class:'website',
            onImg:'/images/data-icon/icon_website_1.png'
        },
        {
            name:'银行卡号',
            class:'bankcard',
            onImg:'/images/data-icon/icon_bankcard_1.png'
        }
    ];
    $scope.data.dataMiningNum=0;      //数据挖掘共多少条信息
    $scope.data.dataMiningName=$scope.data.floatDataList[$scope.data.selectbar].name;   //数据挖掘类型
    $scope.path.itemImage=$scope.data.floatDataList[$scope.data.selectbar].onImg;

    $scope.switch.isShowReport=false;
    $scope.data.searchFormResult=[];  //关联列表搜索出的数据
    $scope.data.SelectList=[];     //插入右侧列表数据
    $scope.data.classList=['color1','color2','color3','color4','color5','color6','color7','color8','color9','color10'];  //class名称

    $scope.data.searchFormResultPage=1;   //搜索关联报表的当前页数
    $scope.data.searchFormResultTotalPage=1;   //搜索关联报表的全部页数

    $scope.data.currentFormName='';

    $scope.getSearchFormCurrentData=function(e){   //获取搜索报表的数据并展示
        if(!$scope.switch.isShowReport){
            $scope.data.formListCurrentPage=1;
            if(!!e){
                var keycode = window.event?e.keyCode:e.which;
                if(keycode==13){
                    $scope.searchForm();
                    if($scope.data.currentSearchForm.length>0){
                        $scope.setRecord(1);
                    }
                }
            }else{
                $scope.searchForm();
            }
            $scope.searchForm=function(){
                console.log($scope.data.currentSearchForm);
                // $location.path('word');

                $scope.data.keyWord=$scope.data.currentSearchForm;
                $state.go('word');
                console.log($scope.data.currentSearchForm);
                $scope.$broadcast('getSearchFormCurrentData');
            }
        }
    };
    // window.onpopstate=function(event){
    //     console.log('执行onpopstate');
    //     console.log(event.state);
    //     if(!!event.state){
    //         $scope.formList=event.state.html;
    //         $scope.data.formListTotalPageNum=event.state.totalPage;
    //         $scope.data.formListCurrentPage=event.state.currentPage;
    //         $scope.data.currentScend=event.state.currentScend;
    //     }
    // }
    $('.explorer-box').on('resize',function(){
        console.log($('.explorer-box').height());
        $scope.data.explorerTop=($window.innerHeight-$('.explorer-box').height())/2+'px';
    });
    $rootScope.layer=function(boolean,msg,auto){
        $('body').css({overflow:'hidden'});
        $('.append').html('');
        var oLayer='<div class="layer-bg-dim"></div>'+
            '<div class="layer-container">'+
            '<h2>提示</h2>'+
            '<div class="layer-body">'+
            '<span></span>'+
            '<p class="layer-msg">'+msg+'</p>'+
            '<a href="'+(!!userPage?"/wanganLogin":"/login")+'"><input type="button" value="确定" id="skip-login"/></a>'+
            '</div> </div>';
        $('.append').append(oLayer);
        $('.layer-bg-dim').css({'height':$(window).height()});
        $(window).on('resize',function(){
            $('.layer-bg-dim').css({'height':$(window).height()});
        });
        if(boolean){
            $('.layer-container input').remove();
            $('.layer-container').addClass('layer-right-container');
            layerErrorCount=0;
        }else{
            $('.layer-container').removeClass('layer-right-container');
            layerRightCount=0;
        }
        if(auto){
            setTimeout(function(){
                $('.layer-bg-dim').remove();
                $('.layer-container').remove();
                $('body').css({overflow:'auto'});
            },1000);
        }
        $('#skip-login').on('click',function(){
            $.cookie('login_user', null, { expires: 0, path: '/' });
            $.cookie('dogId', null, { expires: 0, path: '/' });
            $.cookie('editUser', null, { expires: 0, path: '/' });
            $.cookie('history', null, { expires: 0, path: '/' });
            $.cookie('admin',null,{ expires: 0, path: '/' });
            $.cookie('hurl',encodeURIComponent(window.location.href));
            $.cookie('formLink',null,{ expires: 0, path: '/' });
            $.cookie('linkArr',null,{ expires: 0, path: '/' });
        });
    }
});
app.controller('word-ctrl',function($scope,$http,$rootScope,$window,$location,$stateParams,$timeout){
    //加载历史记录
    console.log($scope.data.currentSearchForm);
    $scope.data.keyWord=$scope.data.currentSearchForm;
    console.log($scope.data.currentSearchForm);
    $scope.updataHistory=function(){
        $http({
            url:'http://'+newIp+'/jwcloud/user/getRecord',
            method:"POST",
            data:$.param({
                dogId:$scope.query.dogId,
                getDogId:$scope.query.dogId,
                action:'getWordFrequencyRecord'
            }),
            withCredentials:true
        }).success(function(data){
            if(data.status==0){
                $scope.data.historyList=data.recordList;
            }else{
                $rootScope.layer(false,'您还没有登录，请点击“确定”返回登录页');
            }
        });
    };
    $scope.updataHistory();
    $scope.isEmpty=function(arr){
        if(arr.length == 0){
            return true;
        }else{
            return false;
        }
    };
    //加载第一页
    $scope.getStatement=function(i){
        $http({
            url:'http://'+newIp+'/jwcloud/html/getStatements',
            method:'POST',
            async:false,
            data:$.param({dogId:$scope.query.dogId,"keyword":$scope.data.keyWord,"page":$scope.data.formListCurrentPage,"class":i?i:'all'}),
            withCredentials:true
        }).success(function(data){
            var data2= {
                status:0,
                hits: [
                    {
                        "statementName": "iphone_5c",
                        "uploadDate": "2018-03-08",
                        "htmlNum": "51",
                        "statementSource": 0,
                        "path": "iphone的报表/iphone的报表_2018-03-08-14-25_308/iphone_5c",
                        "uuid": "iphone_5c-1e026868-d98b-4d6f-9e05-30321ba161f8"
                    }, {
                        "statementName": "iphone_5c",
                        "uploadDate": "2018-03-08",
                        "htmlNum": "51",
                        "statementSource": 0,
                        "path": "iphone的报表/iphone的报表_2018-03-08-14-25_308/iphone_5c",
                        "uuid": "iphone_5c-1e026868-d98b-4d6f-9e05-30321ba161f8"
                    },
                    {
                        "statementName": "iphone_5c",
                        "uploadDate": "2018-03-08",
                        "htmlNum": "51",
                        "statementSource": 0,
                        "path": "iphone的报表/iphone的报表_2018-03-08-14-25_308/iphone_5c",
                        "uuid": "iphone_5c-1e026868-d98b-4d6f-9e05-30321ba161f8"
                    }, {
                        "statementName": "iphone_5c",
                        "uploadDate": "2018-03-08",
                        "htmlNum": "51",
                        "statementSource": 0,
                        "path": "iphone的报表/iphone的报表_2018-03-08-14-25_308/iphone_5c",
                        "uuid": "iphone_5c-1e026868-d98b-4d6f-9e05-30321ba161f8"
                    }
                ],
                totalPage:40
            };
            if(data.status==0){
                if(!data){
                    $scope.data.noData=true;
                    $scope.formList='';
                    return;
                }
                var list = data.hits;
                if(list.length==0){
                    $scope.data.noData=true;
                    $scope.formList='';
                }else{
                    angular.element('.word-frequency-page').html('');
                    $scope.data.noData=false;
                    $scope.formList=list;
                    $scope.data.formListTotalPageNum=data.totalPage;
                    // console.log(list);
                    // window.history.pushState({'html':list,'currentPage':$scope.data.formListCurrentPage,'currentScend':$scope.data.currentScend,'totalPage':$scope.data.formListTotalPageNum},null,location.href.split('?')[0]+'?page='+$scope.data.formListCurrentPage);

                    //生成页数
                    $('.word-frequency-page').createPage(function(n){

                    },{
                        pageCount:$scope.data.formListTotalPageNum,
                        current:Number($scope.data.formListCurrentPage),
                        showPrev:true,//是否显示上一页按钮
                        showNext:true,//是否显示下一页按钮
                        showIconBtn:true,
                        showFirst:true,
                        showLast:true,
                        align:'left',
                        pageSwap:false,
                        showNear:1,
                        frame:"angular"
                    },{
                        "color":"#268aee",
                        "height":32,
                        // "pageWidth":36,
                        "pagesMargin":4,
                        "borderColor":"#dcdcdc",
                        "currentColor":"#fff",
                        "currentBorderColor":"#4098f0",
                        "currentBgColor":"#4098f0",
                        "disableColor":'#bfd5ea',
                        "disableBackColor":"#fff",
                        "disableborderColor":"#dcdcdc",
                        "prevNextWidth":42,
                        "btnPadding":14
                    });
                }
                if($scope.data.keyWord.length!==0){
                    $scope.updataHistory();
                }
            }else{
                $rootScope.layer(false,'您还没有登录，请点击“确定”返回登录页');
            }
        });
    };
    $scope.getCurrentData=function(i){
        $scope.isSecend=i;
        $scope.data.currentScend=i;
        $scope.getStatement($scope.data.currentScend);
    };

    $scope.$on('getSearchFormCurrentData',function(){   //获取搜索报表的数据并展示
        console.log($scope.data.currentSearchForm);
        $scope.data.currentScend='all';
        $scope.getCurrentData($scope.data.currentScend);
    });

    $scope.loadCurrent = function(n){    //加载当前页
        $scope.data.formListCurrentPage=Number(n);
        $scope.getCurrentData($scope.data.currentScend);
    };
    $scope.nextPage = function(){
        $scope.data.formListCurrentPage++;
        $scope.getCurrentData($scope.data.currentScend);
    };
    $scope.prevPage=function(){
        $scope.data.formListCurrentPage--;
        $scope.getCurrentData($scope.data.currentScend);
    };
    $scope.skipPage=function(){
        var pageNum=$('.ma-page-input').val();
        var reg=/\d/g;
        if(!reg.test(pageNum) || pageNum> $scope.data.formListTotalPageNum|| pageNum<1 || !pageNum){

        }else{
            $scope.data.formListCurrentPage=pageNum;
            $scope.getCurrentData($scope.data.currentScend);
        }
    };
    $scope.lastPage = function(){
        $scope.data.formListCurrentPage=$scope.data.formListTotalPageNum;
        // $scope.getCurrentData();
    };
    $scope.firstPage = function(){
        $scope.data.formListCurrentPage=1;
        $scope.getCurrentData();
    };
    $scope.fileExplorer=function(path,n,type){
        $scope.data.currentFormName=n?n:$scope.data.currentFormName;
        if(!type){
            $scope.query['basepath']=path;
        }
        $scope.$broadcast('fileExplorer',path);
        $scope.switch.isShowExplorer=true;
    };
    if(!!$stateParams.path){
        // $scope.query['basepath']=$stateParams.path;
        // $scope.$broadcast('fileExplorer',$stateParams.path);
        // $scope.switch.isShowExplorer=true;
        $timeout(function(){
            console.log($stateParams);
            $scope.query['basepath']=$stateParams.basePath;
            $scope.query['path']=$stateParams.path;

            $scope.fileExplorer($stateParams.path,null,1);
            $scope.$broadcast('setPathList',$stateParams.path);


        },0);
    }
    if(!!$stateParams.page){
        console.log($stateParams.page);
        $scope.data.formListCurrentPage=$stateParams.page;

        $scope.getCurrentData($scope.data.currentScend);
    }else{
        console.log('没有页码');
        $scope.getCurrentData($scope.data.currentScend);  //进入页面展示所有报表列表
    }
    //添加瀑布流

    //词频统计页
    $scope.switch.lock1=true;
    $scope.dropWordFrequency=function(n,state){

    };
    $scope.getFullTextWordFrequencyStatistics=function(){   //得到全文搜
        var req='fullTextWordFrequencyStatistics';
        getFullWord(req);
    };
    $scope.getWordFrequencyStatistics=function(){    //得到聊天记录
        var req='wordFrequencyStatistics';
        getFullWord(req);
    };
    function getFullWord(req){
        $scope.switch.isLoading=true;  //加载中
        $scope.switch.isRuihai=true;  //是睿海
        $http({
            url:'http://'+newIp+'/jwcloud/'+req,
            method:'POST',
            data:$.param({dogId:$scope.query.dogId,"uuid":$scope.data.currentUuid}),
            withCredentials:true
        }).success(function(data){
            if(data.status==0){
                var list=data.hits;
                $scope.switch.isLoading=false;
                $scope.data.wordFrequencyList=list;
                if(list.length>0){
                    $scope.switch.isRuihai=true;
                    $scope.addScrolling($scope.data.wordFrequencyList);
                }else{
                    $scope.switch.isRuihai=false;
                }
            }else{
                if(!!data.message){
                    $scope.switch.isLoading=false;
                    $scope.switch.isRuihai=false;
                }else{
                    $rootScope.layer(false,'您还没有登录，请点击“确定”返回登录页');
                }

            }

        })
    }

    $scope.getRelation=function(n){
        $scope.data.currentFormName=n;
        $.cookie('currentFormName',$scope.data.currentFormName);
        localStorage.removeItem('reportList');
    };
    $rootScope.setRecord=function(n){
        var operate;
        var kw;
        var keyword=$scope.data.currentFormName;
        if(!!keyword){
            if(keyword.length>40){
                keyword=keyword.substring(0,40)+'...';
            }
            keyword=keyword.trim().replace(/"/g, '\\"').replace(/:/g, '\\:');
        }
        if(n==1||n==2||n==3){
            if(n==1){
                kw=$scope.data.currentSearchForm.trim().replace(/"/g, '\\"').replace(/:/g, '\\:');
                if(kw.length>40){
                    kw=kw.substring(0,40)+'...';
                }
                operate="搜索报表 '"+kw+"'";
            }
            else if(n==2){
                kw=$scope.data.currentSearchFormWord.trim().replace(/"/g, '\\"').replace(/:/g, '\\:');
                if(kw.length>20){
                    kw=kw.substring(0,20)+'...';
                }
                operate="全文搜索报表 '"+keyword+" '内容 '"+kw+" '";
            }
            else if(n==3){
                kw=$scope.data.currentSearchWord.trim().replace(/"/g, '\\"').replace(/:/g, '\\:');
                if(kw.length>20){
                    kw=kw.substring(0,20)+'...';
                }
                operate="搜索报表 '"+keyword+" '统计词汇内容 '"+kw+" '";
            }
            $http({
                url: 'http://' + newIp + '/jwcloud/user/addRecord',
                method:"POST",
                data:$.param({
                    dogId:$scope.query.dogId,
                    operateInfo: operate,
                    operateState: n,
                    keyword:kw,
                    url:''
                }),
                withCredentials:true
            }).success(function(data){
                if (data.status == 0) {

                }else{
                    $rootScope.layer(false,'您还没有登录，请点击“确定”返回登录页');
                }
            })
        }
        else{
            if(n==-1){
                operate="查看报表 '"+keyword+"' 的词频统计";
            }
            else if(n==0){
                operate="查看报表 '"+keyword+"' 的文件详情";
            }
            $http({
                url: 'http://' + newIp + '/jwcloud/user/addRecord',
                method: 'POST',
                data: $.param({
                    dogId:$scope.query.dogId,
                    operateInfo: operate,
                    operateState: -1,
                    url:'',
                    keyword:''
                }),
                withCredentials:true
            }).success(function(data){
                if (data.status == 0) {

                }else{
                    $rootScope.layer(false,'您还没有登录，请点击“确定”返回登录页');
                }
            })
        }

    };
    $scope.toDataMining=function(){
        $scope.switch.isWordFrequency=false;
    };
    $scope.toWordFrequency=function(){
        $scope.switch.isWordFrequency=true;
    };

    $rootScope.setReportBoxTop=function(){
        var top=($window.innerHeight-$('.report-box').height())/2;
        if(top>162){
            $scope.data.reportBoxTop='162px';
        }
        else if(top<0){
            $scope.data.reportBoxTop='0px';
        }else{
            $scope.data.reportBoxTop=top+'px';
        }
    };
    $rootScope.setReportBoxTop();
    $('.report-box').on('resize',function(){
        setReportBoxTop();
        $scope.$apply();
    });
    $(window).on('resize',function(){
        $rootScope.setReportBoxTop();
        $scope.$apply();
    });

});
app.controller('explorer-list', function($scope, $location, $http, $timeout,$interval,$window) {
    $scope.waitData = false;  //不在等待进程中
    $scope.query['apiadr'] = $scope.query['apiadr'] || "";
    $scope.query['apipwd'] = $scope.query['apipwd'] || "";
    $scope.query['basepath'] = $scope.query['basepath'] || "";
    $scope.query['path'] = $scope.query['path'] || $scope.query['basepath'];
    $scope.data.explorerTop=0;
    $scope.viewData={};
        //提供一个路径，分析父级目录

    var pathList = function(path){
        // var arr = [{
        //     name : '根目录',
        //     path : ''
        // }];
        var basepPathLength = $scope.query['basepath'].split('/').length;
        var arr=[];
        if( path ){
            var parr = path.split('/');
             parr.splice(0,basepPathLength-1);
            var ptmp =$scope.query['basepath'].split('/').splice(0,basepPathLength-1).join('/');
            for(var i = 0 ; i < parr.length ; i++ ){
                ptmp += '/' + parr[i];
                arr.push({
                    name : parr[i],
                    path : ptmp
                });
            }
        }
        arr[ arr.length - 1 ].active = true;
        return arr;
    };
    $scope.$on('setPathList',function(e,path){
        $scope.viewData.pathList = pathList(path);
        console.log($scope.viewData.pathList);
    });
    $scope.setExplorerBoxTop=function(){
        var top=($window.innerHeight-$('.explorer-box').height())/2;
        if(top>162){
            $scope.data.explorerTop='162px';
        }
        else if(top<0){
            $scope.data.explorerTop='0px';
        }
        else{
            $scope.data.explorerTop=($window.innerHeight-$('.explorer-box').height())/2+'px';
        }
    };
    $scope.setExplorerBoxTop();
    $(window).on('resize',function(){
        $scope.setExplorerBoxTop();
    });

    //测试
    $scope.demo = function(){
        $scope.query.apiadr = "http://www.miaoqiyuan.cn/products/web-explorer/demo.asp";
        $scope.openFolder("/");
    };
    $scope.$on('fileExplorer',function(e,path){
        $scope.openFolder(path);
        // setExplorerBoxTop();

    });
    //打开文件夹
    $scope.openFolder = function(path){
        $scope.waitData = true;
        $scope.connected = false;
        $scope.taskMsg = "打开目录" + path;
        $scope.query['path'] = path;
        $location.search($scope.query);
        if(path=='/'){
            $http.jsonp($scope.query['apiadr'] + '?pwd=' + $scope.query['apipwd'] + '&path=' + $scope.query['path'] + '&callback=JSON_CALLBACK')
                .success(function (result) {
                    if( !!result.folder || !!result.file ){
                        var _tmp_name;
                        $scope.connected = true;
                        for(var i = 0 ; i < result.file.length ; i++){
                            _tmp_name = result.file[i].name.split('.');
                            result.file[i].ext = _tmp_name[_tmp_name.length - 1];
                        }
                        $scope.data = result;
                        $scope.data.pathList = pathList(result.path);
                        $scope.waitData = false;
                        $scope.getFolderSize(0, result.path);
                    }else if( !!result.err ){
                        $scope.taskMsg = result.err + "，等待用户输入";
                    }else{
                        $scope.taskMsg = "未知错误，等待用户输入";
                    }
                });
        }else{
            $http({
                url:'/getExplorer',
                method:'POST',
                data:$.param({"path": $scope.query["path"]})
            })
            .success(function (result) {
                if( !!result.folder || !!result.file ){
                    var _tmp_name;
                    $scope.connected = true;
                    for(var i = 0 ; i < result.file.length ; i++){
                        _tmp_name = result.file[i].name.split('.');
                        result.file[i].ext = _tmp_name[_tmp_name.length - 1];
                    }
                    $scope.viewData = result;

                    // $scope.setExplorerBoxTop();
                    $scope.viewData.pathList = pathList(result.path);
                    $scope.waitData = false;

                    $scope.getFolderSize(0, result.path);
                    $timeout(function(){
                        $scope.setExplorerBoxTop();
                    },0);

                }else if( !!result.err ){
                    $scope.taskMsg = result.err + "，等待用户输入";
                }else{
                    $scope.taskMsg = "未知错误，等待用户输入";
                }
            });
        }
    };

    //获取文件夹大小
    $scope.getFolderSize = function(index, parent){
        if( index < $scope.viewData.folder.length && parent == $scope.viewData.path ){
            var _tmp_name = parent + "/" + $scope.viewData.folder[index].name;
            $scope.waitData = true;
            $scope.taskMsg = "计算" + _tmp_name + "大小";
            $http.post('/getExplorer',$.param({
                "path":_tmp_name,
               "action":"getsize",
                "index":index
            }))
            .success(function (result) {
                $scope.waitData = false;
                $scope.viewData.folder[result.index].size = result.size;
                $scope.getFolderSize(Number(result.index) + 1, result.parent); //获取下一个
            });
        }
    };
     
    //点击文件，打开文件
    $scope.openFile = function(file){
        $scope.$emit('setExplorerTop');
        $scope.switch.isShowExplorer=false;
        $scope.path.currentFilePath=$scope.viewData.path.replace(/^data\/backup\//g,'')+ '/' + file.name;
        $.cookie('VFURL',$scope.path.currentFilePath);
        $location.path('viewFile');
    };
 
    //页面打开时，尝试打开目录 
    //$scope.query['path'] = $scope.query['path'] || "";
    // $scope.openFolder($scope.query['path']);
     $scope.hideAll=function(){
         // $scope.$emit('hideAll');
         $scope.switch.isShowExplorer=false;
     };
});
app.controller('relationCtrl',function($scope,$http,$window,$compile,$state,$stateParams,$location,$rootScope){
    $scope.switch.isShowRelation=false;
    $scope.data.moreRelationList=[];
    $scope.data.relationType='';
    $scope.switch.isRelationShow = false;   //开始不显示关系图
    $scope.switch.isloadRelation=true;   //关系图加载动画显示，隐藏报错提示
    var reportList=localStorage.getItem('reportList');

    var name='';
    $scope.data.convasheight=$window.innerHeight-170+'px';

    if(!!$location.search().uuid){
        name= $scope.data.currentFormName?$scope.data.currentFormName:$.cookie('currentFormName')
    }else{
        console.log(JSON.parse(localStorage.getItem('reportList')));
    }
    console.log(name);
    $scope.hidePop=function(){
        $scope.switch.isShowRelation=false;
    };
    var objToString = Object.prototype.toString;
    $scope.back=function(){
        $state.go('/word',{'page':$.cookie().page});
    };
    function isArray(value) {
        return objToString.call(value) === '[object Array]';
    }
    function isObj(value) {
        return objToString.call(value) === '[object Object]';
    }
    function isString(value) {
        return objToString.call(value) === '[object String]';
    }
    if(!!name || !!reportList){
        var reqUrl='';
        var reqData={};
        if(!!$location.search().uuid){
            reqUrl='statementParse';
            reqData={dogId:$scope.query.dogId,"uuid":$location.search().uuid};
        }else{
            var reportListARR=JSON.parse(reportList);
            console.log(reportListARR);
            var uuids=[];
            for(var i=0;i<reportListARR.length;i++){
                uuids.push(reportListARR[i].uuid);
            }
            reqUrl='moreStatementParse';
            reqData={"dogId":$scope.query.dogId,"uuids":uuids};
        }
        $http({
            url: 'http://' + newIp + '/jwcloud/html/'+reqUrl,
            method:'POST',
            data:$.param(reqData,true),
            withCredentials:true
        }).success(function(res){
            // console.log(JSON.stringify(res));
            // console.log(res);
            var node = res.nodes;

            if(res.status==0){
                if(res.links.length === 0 || res.nodes.length===0){
                    $scope.switch.isloadRelation=false;
                }else{
                    var myChart = echarts.init(document.getElementById('main'));
                    myChart.showLoading();
                    var graph = res;
                    var graph2 ={
                        "status":0,
                        "nodes": [
                            {
                                "id": "1",
                                "name": "机主iphone5c",
                                "symbolSize": 80,
                                "attributes": {
                                    "modularity_class": 0
                                },
                                "dataList": {
                                    "手机":18910818429,
                                    "QQ":532440457,
                                    "微信": {
                                        "阿凡提": "wxid_p6c2womqqupg12",
                                        "刘明琛": "wxid_ljli6a68326w12",
                                        "-.-": "wxid_6x2jfk4pl5f051",
                                        "熊": "sunna198678",
                                        "夏洛特": "wxid_i45smemoqxkt22"
                                    },
                                    "新浪微博": {
                                        "小熊sunna": 1258752167,
                                        "null": 1004820520858
                                    },
                                    "腾讯微博": {
                                        "熊": 33607585
                                    }
                                }
                            },
                            {
                                "id": "0",
                                "name": '13998644433',
                                "symbolSize": 80,
                                "attributes": {
                                    "modularity_class": 0
                                },
                                label: {
                                    normal: {
                                        position: 'right',
                                        formatter: '{b}{star|}',
                                        rich:{
                                            star:{
                                                height:12,
                                                backgroundColor: {
                                                    image: '/images/icon_star_1.png'
                                                }
                                            }
                                        }
                                    }
                                },
                                "dataList": [
                                    {
                                        "手机号":18910818426,
                                        "本篇报表频次":"120次",
                                        "其他报表频次":"120次",
                                        "信息库频次":"120次"
                                    },
                                    {
                                        "手机号":18910818426,
                                        "本篇报表频次":"120次",
                                        "其他报表频次":"120次",
                                        "信息库频次":"120次"
                                    }
                                ]
                            },
                            {
                                "id": "2",
                                "name": 15899997520,
                                "symbolSize": 1,
                                "attributes": {
                                    "modularity_class": 0
                                },
                                "dataList": {}
                            },
                            {
                                "id": "3",
                                "name": 15899997527,
                                "symbolSize": 100,
                                "attributes": {
                                    "modularity_class": 0
                                },
                                "dataList": {}
                            },
                            {
                                "id": "4",
                                "name": "李丽山海",
                                "symbolSize": 28,
                                "attributes": {
                                    "modularity_class": 0
                                },
                                "dataList": {}
                            },
                            {
                                "id": "5",
                                "name": 10010,
                                "symbolSize": 15,
                                "attributes": {
                                    "modularity_class": 0
                                },
                                "dataList": {}
                            },
                            {
                                "id": "6",
                                "name": 106903297209,
                                "symbolSize": 4,
                                "attributes": {
                                    "modularity_class": 0
                                },
                                "dataList": {}
                            },
                            {
                                "id": "7",
                                "name": 15754076124,
                                "symbolSize": 3,
                                "attributes": {
                                    "modularity_class": 0
                                },
                                "dataList": {}
                            },
                            {
                                "id": "8",
                                "name": 1069070069,
                                "symbolSize": 2,
                                "attributes": {
                                    "modularity_class": 0
                                },
                                "dataList": {}
                            },
                            {
                                "id": "9",
                                "name": "阿凡提",
                                "symbolSize": 4,
                                "attributes": {
                                    "modularity_class": 1
                                },
                                "dataList": {
                                    "微信号": "wxid_p6c2womqqupg12"
                                }
                            },
                            {
                                "id": "10",
                                "name": "刘明琛",
                                "symbolSize": 33,
                                label: {
                                    normal: {
                                        position: 'right',
                                        formatter: '{b}{star|}',
                                        rich:{
                                            star:{
                                                height:12,
                                                backgroundColor: {
                                                    image: '/images/icon_star_1.png'
                                                }
                                            }
                                        }
                                    }
                                },
                                "attributes": {
                                    "modularity_class": 1
                                },
                                "dataList": {
                                    "微信号": "wxid_ljli6a68326w12"
                                }
                            },
                            {
                                "id": "11",
                                "name": "乔丹",
                                "symbolSize": 21,
                                "attributes": {
                                    "modularity_class": 1
                                },
                                "dataList": {
                                    "微信号": "qiao715665508[丸子]"
                                }
                            },
                            {
                                "id": "12",
                                "name": "睿海测试",
                                "symbolSize": 12,
                                "attributes": {
                                    "modularity_class": 1
                                },
                                "dataList": {
                                    "微信号": "wxid_hqmkhs5oenzw22[熊处]"
                                }
                            },
                            {
                                "id": "13",
                                "name": "-.-",
                                "symbolSize": 4,
                                "attributes": {
                                    "modularity_class": 1
                                },
                                "dataList": {
                                    "微信号": "wxid_6x2jfk4pl5f051"
                                }
                            },
                            {
                                "id": "14",
                                "name": "张维",
                                "symbolSize": 4,
                                "attributes": {
                                    "modularity_class": 1
                                },
                                "dataList": {
                                    "微信": "wxid_69ps222y1aq622[Zwei]"
                                }
                            },
                            {
                                "id": "15",
                                "name": "夏洛特",
                                "symbolSize": 13,
                                "attributes": {
                                    "modularity_class": 1
                                },
                                "dataList": {
                                    "微信": "maodou_1010"
                                }
                            },
                            {
                                "id": "16",
                                "name": "测试电信",
                                "symbolSize": 18,
                                "attributes": {
                                    "modularity_class": 1
                                },
                                "dataList": {
                                    "微信": "wxid_85wtmmqqsk0d22"
                                }
                            },
                            {
                                "id": "17",
                                "name": "刘姐",
                                "symbolSize": 1,
                                "attributes": {
                                    "modularity_class": 1
                                },
                                "dataList": {
                                    "微信": "LY59177[燕]"
                                }
                            },
                            {
                                "id": "18",
                                "name": "测试移动",
                                "symbolSize": 13,
                                "attributes": {
                                    "modularity_class": 1
                                },
                                "dataList": {
                                    "微信": "testyidong[测试移动]"
                                }
                            },
                            {
                                "id": "19",
                                "name": "董老六",
                                "symbolSize": 3,
                                "attributes": {
                                    "modularity_class": 1
                                },
                                "dataList": {
                                    "微信": "A2941727290[收获季节]"
                                }
                            },
                            {
                                "id": "20",
                                "name": "张彬",
                                "symbolSize": 2,
                                "attributes": {
                                    "modularity_class": 1
                                },
                                "dataList": {
                                    "微信": "zhangbin192832"
                                }
                            },
                            {
                                "id": "21",
                                "name": "小熊sunna",
                                "symbolSize": 40,
                                "attributes": {
                                    "modularity_class": 3
                                },
                                "dataList": {
                                    "新浪微博": 1258752167,
                                    "本篇报表频次":"47次",
                                    "其他报表频次":"120次",
                                    "信息库频次":"1200次"
                                }
                            },
                            {
                                "id": "22",
                                "name": "新浪新闻",
                                "symbolSize": 47,
                                "attributes": {
                                    "modularity_class": 3
                                },
                                "dataList": {
                                    "新浪微博": 2028810631
                                }
                            },
                            {
                                "id": "23",
                                "name": "李静",
                                "symbolSize": 1,
                                "attributes": {
                                    "modularity_class": 3
                                },
                                "dataList": {
                                    "新浪微博": 1610536214
                                }
                            },
                            {
                                "id": "24",
                                "name": "小恩爱",
                                "symbolSize": 3,
                                "attributes": {
                                    "modularity_class": 3
                                },
                                "dataList": {
                                    "新浪微博": 2516593910
                                }
                            },
                            {
                                "id": "25",
                                "name": "Mi子酱",
                                "symbolSize": 7,
                                "attributes": {
                                    "modularity_class": 3
                                },
                                "dataList": {
                                    "新浪微": 2117706524
                                }
                            },
                            {
                                "id": "276",
                                "name": "熊",
                                "symbolSize": 2,
                                "attributes": {
                                    "modularity_class": 3
                                },
                                "dataList": {
                                    "腾讯微博": 33607585
                                }
                            },
                            {
                                "id": "27",
                                "name": "yaojing-ai",
                                "symbolSize": 1,
                                "attributes": {
                                    "modularity_class": 3
                                },
                                "dataList": {
                                    "腾讯微博": "sunna198678"
                                }
                            },
                            {
                                "id": "28",
                                "name": "Dora佳琦",
                                "symbolSize": 1,
                                "attributes": {
                                    "modularity_class": 3
                                },
                                "dataList": {
                                    "腾讯微博": "yaojing-ai"
                                }
                            },
                            {
                                "id": "29",
                                "name": "雷黄桃",
                                "symbolSize": 1,
                                "attributes": {
                                    "modularity_class": 0
                                },
                                "dataList": {
                                    "腾讯微博": "yaojing-ai"
                                }
                            },
                            {
                                "id": "30",
                                "name": "菊姐姑娘",
                                "symbolSize": 5,
                                "attributes": {
                                    "modularity_class": 0
                                },
                                "dataList": {
                                    "腾讯微博": "yaojing-ai"
                                }
                            },
                            {
                                "id": "31",
                                "name": "花店",
                                "symbolSize": 5,
                                "attributes": {
                                    "modularity_class": 0
                                },
                                "dataList": {
                                    "腾讯微博": "yaojing-ai"
                                }
                            },
                            {
                                "id": "32",
                                "name": "黄硕",
                                "symbolSize": 20,
                                "attributes": {
                                    "modularity_class": 0
                                },
                                "dataList": {
                                    "腾讯微博": "yaojing-ai"
                                }
                            },
                            {
                                "id": "33",
                                "name": "妈家",
                                "symbolSize": 30,
                                "attributes": {
                                    "modularity_class": 0
                                },
                                "dataList": {
                                    "腾讯微博": "yaojing-ai"
                                }
                            },
                            {
                                "id": "34",
                                "name": "992743350(西楼读石)",
                                "symbolSize": 10,
                                "attributes": {
                                    "modularity_class": 2
                                },
                                "dataList": {
                                    "腾讯微博": "yaojing-ai"
                                }
                            },
                            {
                                "id": "35",
                                "name": "1550360831(情水伊人)",
                                "symbolSize": 2,
                                "attributes": {
                                    "modularity_class": 2
                                },
                                "dataList": {
                                    "腾讯微博": "yaojing-ai"
                                }
                            },
                            {
                                "id": "36",
                                "name": "55162198(.Grief)",
                                "symbolSize": 20,
                                "attributes": {
                                    "modularity_class": 2
                                },
                                "dataList": {
                                    "腾讯微博": "yaojing-ai"
                                }
                            },
                            {
                                "id": "37",
                                "name": "1153794432(~快乐就好*.*)",
                                "symbolSize": 5,
                                "attributes": {
                                    "modularity_class": 2
                                },
                                "dataList": {
                                    "腾讯微博": "yaojing-ai"
                                }
                            },
                            {
                                "id": "38",
                                "name": "304885814(等鱼的鱼)",
                                "symbolSize": 15,
                                "attributes": {
                                    "modularity_class": 2
                                },
                                "dataList": {
                                    "腾讯微博": "yaojing-ai"
                                }
                            },
                            {
                                "id": "39",
                                "name": "364417555('¤¤崽崽_)",
                                "symbolSize": 5,
                                "attributes": {
                                    "modularity_class": 2
                                },
                                "dataList": {
                                    "腾讯微博": "yaojing-ai"
                                }
                            },
                            {
                                "id": "40",
                                "name": "爱人",
                                "symbolSize": 50,
                                "attributes": {
                                    "modularity_class": 1
                                },
                                "dataList": {
                                    "腾讯微博": "yaojing-ai"
                                }
                            },
                            {
                                "id": "41",
                                "name": "业务员小雪",
                                "symbolSize": 10,
                                "attributes": {
                                    "modularity_class": 1
                                },
                                "dataList": {
                                    "腾讯微博": "yaojing-ai"
                                }
                            },
                            {
                                "id": "42",
                                "name": "老妈",
                                "symbolSize": 30,
                                "attributes": {
                                    "modularity_class": 1
                                },
                                "dataList": {
                                    "腾讯微博": "yaojing-ai"
                                }
                            },
                            {
                                "id": "43",
                                "name": "艳军",
                                "symbolSize": 5,
                                "attributes": {
                                    "modularity_class": 1
                                },
                                "dataList": {
                                    "腾讯微博": "yaojing-ai"
                                }
                            }
                        ],
                        "links": [
                            {
                                "id": "0",
                                "name": null,
                                "source": "0",
                                "target": "1",
                                "lineStyle": {
                                    "normal": {}
                                }
                            },
                            {
                                "id": "1",
                                "name": null,
                                "source": "0",
                                "target": "2",
                                "lineStyle": {
                                    "normal": {}
                                }
                            },
                            {
                                "id": "2",
                                "name": null,
                                "source": "0",
                                "target": "3",
                                "lineStyle": {
                                    "normal": {}
                                }
                            },
                            {
                                "id": "3",
                                "name": null,
                                "source": "0",
                                "target": "4",
                                "lineStyle": {
                                    "normal": {}
                                }
                            },
                            {
                                "id": "4",
                                "name": null,
                                "source": "0",
                                "target": "5",
                                "lineStyle": {
                                    "normal": {}
                                }
                            },
                            {
                                "id": "5",
                                "name": null,
                                "source": "0",
                                "target": "6",
                                "lineStyle": {
                                    "normal": {}
                                }
                            },
                            {
                                "id": "6",
                                "name": null,
                                "source": "0",
                                "target": "7",
                                "lineStyle": {
                                    "normal": {}
                                }
                            },
                            {
                                "id": "7",
                                "name": null,
                                "source": "0",
                                "target": "8",
                                "lineStyle": {
                                    "normal": {}
                                }
                            },
                            {
                                "id": "8",
                                "name": null,
                                "source": "0",
                                "target": "11",
                                "lineStyle": {
                                    "normal": {}
                                }
                            },
                            {
                                "id": "9",
                                "name": null,
                                "source": "0",
                                "target": "12",
                                "lineStyle": {
                                    "normal": {}
                                }
                            },
                            {
                                "id": "10",
                                "name": null,
                                "source": "0",
                                "target": "13",
                                "lineStyle": {
                                    "normal": {}
                                }
                            },
                            {
                                "id": "11",
                                "name": null,
                                "source": "0",
                                "target": "14",
                                "lineStyle": {
                                    "normal": {}
                                }
                            },
                            {
                                "id": "12",
                                "name": null,
                                "source": "0",
                                "target": "15",
                                "lineStyle": {
                                    "normal": {}
                                }
                            },
                            {
                                "id": "13",
                                "name": null,
                                "source": "0",
                                "target": "16",
                                "lineStyle": {
                                    "normal": {}
                                }
                            },
                            {
                                "id": "14",
                                "name": null,
                                "source": "0",
                                "target": "17",
                                "lineStyle": {
                                    "normal": {}
                                }
                            },
                            {
                                "id": "15",
                                "name": null,
                                "source": "0",
                                "target": "18",
                                "lineStyle": {
                                    "normal": {}
                                }
                            },
                            {
                                "id": "16",
                                "name": null,
                                "source": "0",
                                "target": "19",
                                "lineStyle": {
                                    "normal": {}
                                }
                            },
                            {
                                "id": "17",
                                "name": null,
                                "source": "0",
                                "target": "20",
                                "lineStyle": {
                                    "normal": {}
                                }
                            },
                            {
                                "id": "18",
                                "name": null,
                                "source": "0",
                                "target": "21",
                                "lineStyle": {
                                    "normal": {}
                                }
                            },
                            {
                                "id": "19",
                                "name": null,
                                "source": "0",
                                "target": "22",
                                "lineStyle": {
                                    "normal": {}
                                }
                            },
                            {
                                "id": "20",
                                "name": null,
                                "source": "0",
                                "target": "23",
                                "lineStyle": {
                                    "normal": {}
                                }
                            },
                            {
                                "id": "21",
                                "name": null,
                                "source": "0",
                                "target": "24",
                                "lineStyle": {
                                    "normal": {}
                                }
                            },
                            {
                                "id": "22",
                                "name": null,
                                "source": "0",
                                "target": "25",
                                "lineStyle": {
                                    "normal": {}
                                }
                            },
                            {
                                "id": "23",
                                "name": null,
                                "source": "0",
                                "target": "26",
                                "lineStyle": {
                                    "normal": {}
                                }
                            },
                            {
                                "id": "24",
                                "name": null,
                                "source": "0",
                                "target": "27",
                                "lineStyle": {
                                    "normal": {}
                                }
                            },
                            {
                                "id": "25",
                                "name": null,
                                "source": "0",
                                "target": "28",
                                "lineStyle": {
                                    "normal": {}
                                }
                            },
                            {
                                "id": "26",
                                "name": null,
                                "source": "0",
                                "target": "9",
                                "lineStyle": {
                                    "normal": {}
                                }
                            },
                            {
                                "id": "27",
                                "name": null,
                                "source": "0",
                                "target": "10",
                                "lineStyle": {
                                    "normal": {}
                                }
                            },
                            {
                                "id": "28",
                                "name": null,
                                "source": "0",
                                "target": "29",
                                "lineStyle": {
                                    "normal": {}
                                }
                            },
                            {
                                "id": "29",
                                "name": null,
                                "source": "0",
                                "target": "30",
                                "lineStyle": {
                                    "normal": {}
                                }
                            },
                            {
                                "id": "30",
                                "name": null,
                                "source": "0",
                                "target": "31",
                                "lineStyle": {
                                    "normal": {}
                                }
                            },
                            {
                                "id": "31",
                                "name": null,
                                "source": "0",
                                "target": "32",
                                "lineStyle": {
                                    "normal": {}
                                }
                            },
                            {
                                "id": "32",
                                "name": null,
                                "source": "0",
                                "target": "33",
                                "lineStyle": {
                                    "normal": {}
                                }
                            },
                            {
                                "id": "33",
                                "name": null,
                                "source": "0",
                                "target": "34",
                                "lineStyle": {
                                    "normal": {}
                                }
                            },
                            {
                                "id": "34",
                                "name": null,
                                "source": "0",
                                "target": "35",
                                "lineStyle": {
                                    "normal": {}
                                }
                            },
                            {
                                "id": "35",
                                "name": null,
                                "source": "0",
                                "target": "36",
                                "lineStyle": {
                                    "normal": {}
                                }
                            },
                            {
                                "id": "36",
                                "name": null,
                                "source": "0",
                                "target": "37",
                                "lineStyle": {
                                    "normal": {}
                                }
                            },
                            {
                                "id": "37",
                                "name": null,
                                "source": "0",
                                "target": "38",
                                "lineStyle": {
                                    "normal": {}
                                }
                            },
                            {
                                "id": "38",
                                "name": null,
                                "source": "0",
                                "target": "39",
                                "lineStyle": {
                                    "normal": {}
                                }
                            },
                            {
                                "id": "39",
                                "name": null,
                                "source": "0",
                                "target": "40",
                                "lineStyle": {
                                    "normal": {}
                                }
                            },
                            {
                                "id": "40",
                                "name": null,
                                "source": "0",
                                "target": "41",
                                "lineStyle": {
                                    "normal": {}
                                }
                            },
                            {
                                "id": "41",
                                "name": null,
                                "source": "0",
                                "target": "42",
                                "lineStyle": {
                                    "normal": {}
                                }
                            },
                            {
                                "id": "42",
                                "name": null,
                                "source": "0",
                                "target": "43",
                                "lineStyle": {
                                    "normal": {}
                                }
                            }
                        ]
                    };

                    myChart.hideLoading();
                    var categories = [];
                    categories[0] = {name:'手机'};
                    categories[2] = {name:'微信'};
                    categories[1] = {name:'QQ'};
                    categories[3] = {name:'微博'};
                    // for (var i = 0; i < 9; i++) {
                    //     categories[i] = {
                    //         name: '类目' + i
                    //     };
                    // }
                    function checkOhter(arr){
                        for(var i=0;i<arr.length;i++){
                            if(arr[i].other == 0){
                                return true;
                            }
                        }
                        return false;
                    }
                    graph.nodes.forEach(function (node) {
                        node.itemStyle = null;
                        node.id=node.id.toString();
                        node.symbolSize=node.symbolSize?node.symbolSize:20;
                        node.value = node.symbolSize;
                        node.symbolSize /= 4;
                        if(node.symbolSize>50){
                            node.symbolSize=50;
                        }
                        // node.label = {
                        //     normal: {
                        //         show: node.symbolSize > 4  //大于10的显示
                        //     }
                        // };
                        if(!node.label){
                            node.label={
                                normal:{
                                    show:true
                                }
                            }
                        }else{
                            node.label.normal.show=true;
                        }
                        if(node.id != 0){
                            if(isObj(node.dataList)){
                                if(node.dataList.其他报表频次!=0 && !!node.dataList.其他报表频次){
                                    node.label={
                                        normal: {
                                            show:true,
                                            formatter: '{b}{star|}',
                                            rich:{
                                                star:{
                                                    height:12,
                                                    backgroundColor: {
                                                        image: '/images/icon_star_1.png'
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }else{
                                    node.label={
                                        normal:{
                                            show:true
                                        }
                                    }
                                }
                            }
                            else if(isArray(node.dataList)){
                                var list=node.dataList;
                                if(!!checkOhter(list)){
                                    node.label={
                                        normal: {
                                            show:true,
                                            formatter: '{b}{star|}',
                                            rich:{
                                                star:{
                                                    height:12,
                                                    backgroundColor: {
                                                        image: '/images/icon_star_1.png'
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }else{
                                    node.label={
                                        normal:{
                                            show:true
                                        }
                                    }
                                }

                            }
                        }
                        node.category = node.attributes.modularity_class;
                    });
                    var text;
                    if(!!reportList){
                        var reportListARR=JSON.parse(reportList);
                        text=[
                            '{title|报表关系图:}'
                        ];
                        for(var i=0;i<reportListARR.length;i++){
                            text.push(reportListARR[i].statementName);
                        }
                        text=text.join('\n');
                    }else{
                        text=[
                            '{star|}{titleS|表示该帐号或手机号在其他报表出现过}',
                            '{title|《'+name+'》报表关系图}'
                        ].join('\n');
                    }
                    option = {
                        title: {
                            // text: '表示该帐号或手机号在其他报表出现过\n《'+name+'》报表关系图',
                            text:text,
                            // subtext: '',
                            top: 'bottom',
                            right: ($window.innerWidth-1000)/2,
                            textStyle:{
                                rich:{
                                    star:{
                                        height:12,
                                        backgroundColor: {
                                            image: '/images/icon_star_1.png'
                                        },
                                        marginRight:10
                                    },
                                    titleS:{
                                        padding:[0,6],
                                        fontSize:12,
                                        lineHeight:40
                                    },
                                    title:{
                                        fontSize: 18,
                                        lineHeight:24,
                                        fontFamily: 'Microsoft YaHei',
                                        borderColor: '#000',
                                        fontWeight:'bold'
                                    }
                                }
                            }
                        },
                        legend: [{    //图例组件
                            // selectedMode: 'single',
                            data: categories.map(function (a) {
                                return a.name;
                            }),
                            top:0,
                            left:($window.innerWidth-1000)/2,
                            itemGap:26,
                            textStyle:{
                                padding:[0,12]
                            },
                            backgroundColor:'#f5f5f5'
                        }],
                        tooltip:{
                            // hideDelay:1000,
                            // enterable:true,
                            // position:'right'
                            confine:true
                        },
                        animationDurationUpdate: 1500,
                        animationEasingUpdate: 'quinticInOut',
                        series : [
                            {
                                type: 'graph',
                                layout: 'circular',
                                circular: {
                                    rotateLabel: true   //字体是否随圆旋转
                                },
                                data: graph.nodes,
                                links: graph.links,
                                categories: categories,   //节点分类的类目
                                // focusNodeAdjacency:true,  //高亮显示

                                roam: true,   //添加缩放和移动
                                label: {
                                    normal: {
                                        position: 'right',
                                        formatter: '{b}'
                                    }
                                },
                                lineStyle: {
                                    normal: {
                                        color: 'target',
                                        curveness: 0.3
                                    }
                                },
                                tooltip:{
                                    padding:[14,20],
                                    textStyle:{
                                        lineHeight:28
                                    },
                                    formatter:function(param){
                                        var oPop=angular.element('.fixed-pop');
                                        if(oPop.length>0){

                                        }else{
                                            if(param.dataType=='node'){
                                                // console.log(param);
                                                var contents='';
                                                var name=param.name;
                                                var secondName='';
                                                var dataList=param.data.dataList;
                                                var category=param.data.category;
                                                if(isObj(dataList)){
                                                    var id=param.data.id;
                                                    var marker=param.marker;
                                                    for(i in dataList){
                                                        if(id==0){
                                                            var colorEl='';
                                                            if(i=="手机号" || i=="本机号码" || i=="手机"){
                                                                colorEl="#c23531";
                                                            }
                                                            else if(i=="QQ"){
                                                                colorEl="#2f4554";
                                                            }
                                                            else if(i=="微信"){
                                                                colorEl="#61a0a8";
                                                            }
                                                            else if(i=="新浪微博"){
                                                                colorEl="#d48265";
                                                            }
                                                            else if(i=="腾讯微博"){
                                                                colorEl="#d48265";
                                                            }
                                                            var marker0 = '<span style="display:inline-block;margin-right:10px;border-radius:10px;width:9px;height:9px;background-color:'+colorEl+';"></span>';
                                                            marker=marker0;
                                                        }
                                                        if(typeof(dataList[i])=='object'){
                                                            var contains='';
                                                            if(isObj(dataList[i])){
                                                                for(var n in dataList[i]){
                                                                    if(!!n || !!dataList[i][n]){
                                                                        contains+='<p class="tin20" style="display:block;">'+n+':'+dataList[i][n]+'</p>';
                                                                    }
                                                                }
                                                            }
                                                            else if(isArray(dataList[i])){
                                                                if(!!dataList[i]){
                                                                    for(var j=0;j<dataList[i].length;j++){
                                                                        if(!!dataList[i][j]){
                                                                            contains+='<p class="tin20" style="display:block;">'+dataList[i][j]+'</p>';
                                                                        }
                                                                    }
                                                                }

                                                            }

                                                            contents+='<h3>'+marker+i+':</h3>'+contains+'';
                                                        }else{
                                                            var word='';
                                                            for(n in dataList){
                                                                if(n!='本篇报表频次' && n!='其他报表频次' && n!='信息库频次'){
                                                                    word=dataList[n];
                                                                }
                                                            }
                                                            var url='';
                                                            var target='';
                                                            if(i=='本篇报表频次'){
                                                                url='#/wordFrequency?form='+$scope.data.currentFormName+'&type=all&wd='+encodeURIComponent(word);
                                                                target='_blank';
                                                            }
                                                            else if(i=='其他报表频次'){
                                                                url='/mixresult?wd='+word+'&word=&unison=0&exclude='+$scope.data.currentFormName+'&listCount=20&currentPage=1';
                                                                target='_blank';
                                                            }
                                                            else if(i=='信息库频次'){
                                                                url='/mixresult?wd='+word+'&word=&unison=0&listCount=20&currentPage=1';
                                                                target='_blank';
                                                            }else{
                                                                url='';
                                                            }
                                                            contents+='<p><a href="'+url+'" target="'+target+'">'+marker+i+':'+dataList[i]+'</a></p>';
                                                        }
                                                    }
                                                }
                                                else if(isArray(dataList)){
                                                    var ul1content='';
                                                    var ul2content='';
                                                    secondName='('+param.name+')';
                                                    switch(category){
                                                        case 0:
                                                            name='手机联系人';
                                                            break;
                                                        case 2:
                                                            name='微信联系人';
                                                            break;
                                                        case 1:
                                                            name='QQ联系人';
                                                            break;
                                                        case 3:
                                                            name='微博联系人';
                                                            break;
                                                    }

                                                    for(var i=0;i<Math.ceil(dataList.length/2);i++){
                                                        var star='';
                                                        var star2='';
                                                        var item2='';

                                                        if(dataList[2*i].other == 0){
                                                            star='<a href="/mixresult?wd='+dataList[2*i].accountNum+'&word=&unison=0&exclude='+$scope.data.currentFormName+'&listCount=20&currentPage=1" class="star" target="_blank"></a>';
                                                        }
                                                        if(!!dataList[2*i+1]){
                                                            if(dataList[2*i+1].other == 0){
                                                                star2='<a href="/mixresult?wd='+dataList[2*i+1].accountNum+'&word=&unison=0&exclude='+$scope.data.currentFormName+'&listCount=20&currentPage=1" class="star" target="_blank"></a>';
                                                            }
                                                            var data2="'"+dataList[2*i+1].accountNum+"'";
                                                            item2='<li><a href="#/wordFrequency?form='+$scope.data.currentFormName+'&type=all&wd='+encodeURIComponent(dataList[2*i+1].accountNum)+'" class="name" title="'+dataList[2*i+1].accountNum+'" target="_blank">'+dataList[2*i+1].accountNum+'('+dataList[2*i+1].selfNum+'次)</a>'+star2+'</li>'
                                                        }
                                                        var data1="'"+dataList[2*i].accountNum+"'";
                                                        ul1content+='<li><a href="#/wordFrequency?form='+$scope.data.currentFormName+'&type=all&wd='+encodeURIComponent(dataList[2*i].accountNum)+'" class="name" title="'+dataList[2*i].accountNum+'" target="_blank">'+dataList[2*i].accountNum+'('+dataList[2*i].selfNum+'次)</a>'+star+'</li>';
                                                        ul2content+=item2;
                                                    }
                                                    contents='<ul class="fl mr40">'+ul1content+'</ul><ul class="fl mr8">'+ul2content+'</ul>';
                                                }
                                                var ellStyle='';
                                                if(dataList.length>30){
                                                    ellStyle='block';
                                                }else{
                                                    ellStyle='none';
                                                }
                                                return [
                                                    '<h2 class="clearfix"><p class="fl">'+name+secondName+'</p><span class="fr" onclick="hidebtn()"></span></h2><hr />',
                                                    '<div class="pop-list clearfix">'+contents+'</div><p class="ellipsis" style="display:'+ellStyle+'">......</p><p class="fixed-pop-hints">(* 点击圆点可固定浮窗，搜索字段)</p>'
                                                ].join('');
                                            }else{
                                                var nodes=graph.nodes;
                                                var sourceId=param.data.source;
                                                var targetId=param.data.target;
                                                return nodes[sourceId].name+'>'+nodes[targetId].name;
                                            }
                                        }

                                    }
                                }
                            }
                        ]
                    };
                    $scope.switch.isRelationShow=true;
                    // console.log(option);
                    myChart.setOption(option);

                    myChart.on('click',function(param){
                        param.event.event.stopPropagation();
                        var oPop=angular.element('.fixed-pop');
                        if(param.dataType === 'node' && oPop.length === 0){
                            var ele=$('.relation-container>div:eq(1)');
                            var Ea=ele.find('a');
                            var oH2=ele.find('h2').eq(0);
                            var oH2Width=oH2.width();
                            var oPWidth=oH2.find('p').eq(0).width();
                            var oSpan=oH2.find('span').eq(0);
                            var oSpanWidth=oSpan.width();
                            var widthGap=oH2Width-oPWidth-oSpanWidth;
                            if(widthGap<72){
                                oSpan.css({'margin-left':72})
                            }
                            for(var i=0;i<Ea.length;i++){
                                console.log(!Ea.eq(i).attr('href'));
                                if(!Ea.eq(i).attr('href')){
                                    Ea.eq(i).addClass('nohover');
                                }
                            }
                            $('.relation-container').append('<div class="fixed-pop"></div>');
                            $('.fixed-pop').css({
                                top:ele.position().top>-154?ele.position().top:0,
                                left:ele.position().left>0?ele.position().left:0
                            });
                            $('.fixed-pop').html($compile(ele.html())($scope));
                        }else{
                            angular.element('.fixed-pop').remove();
                        }
                    })
                }

            }else{
                $rootScope.layer(false,'您还没有登录，请点击“确定”返回登录页');
            }
        }).
        error(function(err){
            $scope.switch.isloadRelation=false;
        });
    }
});
app.controller('wordFrequency-ctrl',function($scope,$http,$location,$state,$rootScope,$compile){
    $scope.switch.isShowHistory=false;
    $scope.switch.isShowFastSearch=true;//快速搜索显示
    $scope.path.currentFormWordPath='#';
    function GetKey(){
        if(!!location.href.split('?')[1]){
            this.urlHrefStr=location.href;
            this.urlStr=this.urlHrefStr.substring(Number(this.urlHrefStr.indexOf('?'))+1,this.urlHrefStr.length);
            this.urlArr=this.urlStr.split('&');
            this.urlJson={};
            for(var i=0;i<this.urlArr.length;i++){
                var arr=this.urlArr[i].split('=');
                this.urlJson[arr[0]]=decodeURIComponent(arr[1]);
            }
            return this.urlJson;
        }else{
            return;
        }
    }
    var key=new GetKey();
    $scope.data.currentFormName=key.form;
    $scope.data.currentFormState=key.type;
    $scope.data.currentUuid=key.uuid;
    if($scope.data.currentFormState=='dataMining'){
        $scope.switch.isWordFrequency=false;
    }else{
        $scope.switch.isWordFrequency=true;
    }
    //报表列表和词频统计页面切换
    $scope.toWordPage=function(){
        $scope.switch.isShowHistory=false;
        $scope.switch.isShowFastSearch=true;//快速搜索显示
    };
    $scope.getFullTextWordFrequencyStatistics=function(){
        var req='fullTextWordFrequencyStatistics';
        getFullWord(req);
    };
    $scope.getWordFrequencyStatistics=function(){
        var req='wordFrequencyStatistics';
        getFullWord(req);
    };
    if($scope.data.currentFormState=='chat'){
        $scope.getWordFrequencyStatistics();
    }
    else if($scope.data.currentFormState=='all'){
        $scope.getFullTextWordFrequencyStatistics();
    }
    function getFullWord(req){
        $scope.switch.isLoading=true;  //加载中
        $scope.switch.isRuihai=true;  //是睿海
        $http({
            url:'http://'+newIp+'/jwcloud/html/'+req,
            method:'POST',
            data:$.param({dogId:$scope.query.dogId,"uuid":$scope.data.currentUuid}),
            withCredentials:true
        }).success(function(data){
            if(data.status == 0){
                var list=data.hits;
                $scope.switch.isLoading=false;
                $scope.data.wordFrequencyList=list;
                if(list.length>0){
                    $scope.switch.isRuihai=true;
                    $scope.addScrolling($scope.data.wordFrequencyList);
                }else{
                    $scope.switch.isRuihai=false;
                }
            }else{
                if(!!data.message){
                    $scope.switch.isLoading=false;
                    $scope.switch.isRuihai=false;
                }else{
                    $rootScope.layer(false,'您还没有登录，请点击“确定”返回登录页');
                }

            }
        })
    }
    $scope.addScrolling=function(data){
        var dataList=data;
        var a=1;
        angular.element('.word-frequency-list').off('scroll');
        $scope.data.viewWordList=[];
        if(dataList.length>0){
            $scope.data.firstWordCount=dataList[0].count;       //第一个词汇的数量  以这个为基础 显示比例条
        }else{
            $scope.data.firstWordCount=0;
        }
        $scope.create50=function(num){
            for(var i=50*(num-1);i<50*num;i++){
                var data=dataList[i];
                if(!!data){
                    $scope.data.viewWordList.push(data);
                }
            }
        };
        $scope.create50(a);
        angular.element('.word-frequency-list').on('scroll',function(){
            console.log($scope.switch.lock1);
            if(!$scope.switch.lock1)return;

            var time = Math.ceil(dataList.length/50);
            var wholeHeight ='';
            if($scope.data.currentFormState=='dataMining'){
                wholeHeight = angular.element('.word-frequency-list .datamining-list').height();
            }else{
                wholeHeight = angular.element('.word-frequency-list ol').height();
            }
             angular.element('.word-frequency-list ol').height();
            var scrollTop = angular.element('.word-frequency-list').scrollTop();
            var divHeight = angular.element('.word-frequency-list').height();
            if(scrollTop + divHeight >= wholeHeight-1){
                $scope.switch.lock1=false;
                a++;
                if(a>time){

                }else{
                    $scope.create50(a);
                    $scope.$apply();
                }
            }


        });
    };
    //返回报表列表
    $scope.backFormList=function(){
        $scope.data.wordFrequencyList=[];
        $scope.data.viewWordList=[];
        $scope.switch.showPress='default';
        // $location.path('word');
        $state.go('word',{page:$.cookie().indexPage});

    };
    $scope.searchWord=function(e){   //搜索词汇
        var keycode = window.event?e.keyCode:e.which;
        if(keycode==13){
            if($scope.data.currentSearchWord.length===0){
                $scope.addScrolling($scope.data.wordFrequencyList);
                $scope.path.currentFormWordPath='';
                $scope.switch.isShowIframe=false;
                $scope.switch.isShowFastSearchChangeBtn=false;
                $scope.switch.showPress='default';
            }else{
                $scope.data.currentPageIndex=0;
                $scope.switch.showPress='default';
                var arr=[];
                for(var i=0;i<$scope.data.wordFrequencyList.length;i++){
                    if($scope.data.wordFrequencyList[i].word.indexOf($scope.data.currentSearchWord)!=-1){
                        arr.push($scope.data.wordFrequencyList[i]);
                    }
                }
                $scope.data.searchWordFrequencyList=arr;
                $scope.addScrolling($scope.data.searchWordFrequencyList);
                $rootScope.setRecord(3);
            }
        }
    };
    $scope.initView=function(){
        $scope.path.currentFormWordPath='';
        $scope.switch.isShowIframe=false;
        $scope.switch.isShowFastSearchChangeBtn=false;
        $scope.switch.showPress='default';
    }
    $scope.searchFormWord=function(e){    //快速搜索报表内容
        var keycode = window.event?e.keyCode:e.which;
        if(keycode==13){
            $scope.searchFormWordFn();
        }
    };
    $scope.searchFormWordFn=function(){
        $scope.switch.showPress='default';
        $scope.data.NowItemNum=1;
        if($scope.data.currentSearchFormWord.length != 0){
            $scope.showFile($scope.data.currentSearchFormWord,1);
            $scope.switch.isShowFastSearchChangeBtn=true;
            console.log(2);
            $rootScope.setRecord(2);
        }else{
            $scope.initView();
        }
    };

    angular.element(window).bind('load', function() {
        // var defaultSearchWord=$.cookie('getFullSearchWord');
        // var currentForm=$.cookie('currentForm');
        // if(!!defaultSearchWord){
        //     $scope.data.currentSearchFormWord=defaultSearchWord;
        //     $.cookie('getFullSearchWord',null,{ expires: 0, path: '/' });
        //     if(!!currentForm){
        //         $scope.data.currentFormName=currentForm;
        //         $.cookie('currentForm',null,{ expires: 0, path: '/' });
        //     }
        //     $scope.toWordPage();
        //     $scope.getFullTextWordFrequencyStatistics();
        //     $scope.searchFormWordFn();
        // }
        // console.log($scope.switch.showPress);
        if(!!key.wd){
            console.log(key.wd);
            $scope.data.currentSearchFormWord = key.wd;
            $scope.getFullTextWordFrequencyStatistics();
            $scope.searchFormWordFn();
        }
        if($scope.switch.showPress != 'default'){
            var ele=angular.element('.word-frequency-list li.active p').eq(0);
            console.log(ele.text());
            if(ele.length>0){
                console.log(111);
                $scope.showFile(ele.text());
            }
        }
        if(!!key.wdS){

        }
    });
    var oFrame;
    // function setHash(hash,del,add){
    //     var arg=hash.split('?');
    //     var keyArr=arg[1].split('&');
    //     for(var i=0;i<keyArr.length;i++){
    //         var arr = keyArr[i].split('=');
    //         if(arr[0]==del){
    //             keyArr.splice(i,1);
    //         }
    //     }
    //     arg[1]=keyArr.join('&');
    //     return arg.join('?')+add;
    // }
    $scope.showFile=function(n,type){    //点击左侧显示页面
        $scope.path.currentFormWordPath='';
        $scope.data.NowItemNum=1;     //frame里高亮元素中第几位
        $scope.data.currentPageIndex=0; //当前报表单词路径列表的索引

        var searchType='';
        if(!!type || $scope.data.currentFormState == 'all' || $scope.data.currentFormState == "dataMining"){
            searchType='all';
            $scope.data.currentFormState='all';
        }else{
            searchType='chatRecord';
            $scope.data.currentFormState='chat';
        }
        $http({
            url:'http://'+newIp+'/jwcloud/html/searchSpecifiedStatement',
            method:'POST',
            data:$.param({dogId:$scope.query.dogId,"uuid":$scope.data.currentUuid,"text":n,"searchType":searchType}),
            withCredentials:true
        }).success(function(data){
            // var data={
            //     paths:[
            //         '瑞海报表/瑞海报表_2017-12-01-15-33_727/HUAWEI-20170830/HUAWEI-20170830/HUAWEI-20170830_Report/Main10.html',
            //         '瑞海报表/瑞海报表_2017-12-01-15-33_727/HUAWEI-20170830/HUAWEI-20170830/HUAWEI-20170830_Report/Main11.html',
            //         '瑞海报表/瑞海报表_2017-12-01-15-33_727/HUAWEI-20170830/HUAWEI-20170830/HUAWEI-20170830_Report/Main12.html',
            //         '瑞海报表/瑞海报表_2017-12-01-15-33_727/HUAWEI-20170830/HUAWEI-20170830/HUAWEI-20170830_Report/Main13.html',
            //         '瑞海报表/瑞海报表_2017-12-01-15-33_727/HUAWEI-20170830/HUAWEI-20170830/HUAWEI-20170830_Report/Main14.html'
            //     ]
            // };
            // if(data.status==0){
                var pathlist=data.paths;
                $scope.switch.isShowIframe=true; //显示iframe 隐藏说明
                if(pathlist.length>0){
                    $scope.data.noSearchData=false;
                    $scope.data.currentWord=n;
                    $scope.path.currentFormWordPathList=pathlist;
                    $scope.frameAddSrc('',type);
                }else{
                    $scope.data.noSearchData=true;
                }
            // }else{
            //     $rootScope.layer(false,'您还没有登录，请点击“确定”返回登录页');
            // }

        });
    };
    $scope.frameAddSrc=function(n,type){
        $('#word-frame-container').remove();
        $scope.path.currentFormWordPath=$scope.path.currentFormWordPathList[$scope.data.currentPageIndex];
        var iFrame=$compile('<iframe src="'+$scope.path.currentFormWordPath+'" frameborder="0" id="word-frame-container"></iframe>')($scope);
        angular.element('.appendFrame').append(iFrame);
        oFrame=angular.element('#word-frame-container');
        oFrame.on('load',function(){
            $scope.addHighLight(n,type);
            oFrame.off('load');
        });
    };
    $scope.delAttr=function(){
        angular.element('#word-frame-container').contents().find('body a').removeAttr('href onclick');
        angular.element('#word-frame-container').contents().find('html script').remove();
    };
    $scope.addHighLight=function(n,type){
        $scope.delAttr();
        oFrame=angular.element('#word-frame-container');
        var currentWord=$scope.data.currentWord.replace(/\./g,'\\.').replace(/\+/g,'\\+').replace(/\$/g,'\\$').replace(/\^/g,'\\^').replace(/\[/g,'\\[').replace(/\]/g,'\\]').replace(/\(/g,'\\(').replace(/\)/g,'\\)').replace(/\{/g,'\\{').replace(/\}/g,'\\}').replace(/\|/g,'\\|').replace(/\*/g,'\\*').replace(/\?/g,'\\?');
        var reg=new RegExp('('+currentWord+')','img');
        if(!!type || $scope.data.currentFormState == 'all'){
            //全文的
            var contents=angular.element('#word-frame-container').contents().find('body').html();
            function keyLight(){var sText = contents,
                num = -1,
                rHtml = new RegExp("\<.*?\>","ig"), //匹配html元素
                aHtml = sText.match(rHtml); //存放html元素的数组
                sText = sText.replace(rHtml, '{~}');  //替换html标签

                sText = sText.replace(reg,'<span class="highLight" style="background:#ffea00;color:#000;">$1</span>'); //替换key
                sText = sText.replace(/{~}/g,function(){  //恢复html标签
                    num++;
                    return aHtml[num];
                });
                angular.element('#word-frame-container').contents().find('body').html(sText);
            }
            keyLight();
        }else{
            //微信聊天记录
            reg = new RegExp ('('+encodeHTML(currentWord)+')','img');
            var isFriend=oFrame.contents().find('caption a:contains(好友)').parent().parent();
            var noUse='';
            var flag1 = oFrame.contents().find('caption a:contains(删除)').parent().parent();
            var flag2 = oFrame.contents().find('caption a:contains(漂流瓶)').parent().parent();
            var flag3 = oFrame.contents().find('caption a:contains(其他)').parent().parent();
            if(flag2.length>0){
                noUse = flag2;
            }
            else if(flag3.length>0){
                noUse = flag3;
            }
            else if(flag1.length>0){
                noUse = flag1;
            }
            var thWeChatList=oFrame.contents().find('caption a:contains("群聊天")');

            var isFriendIndex;
            if(isFriend.length ==0){
                isFriendIndex=-1;
            }else{
                isFriendIndex=Number(oFrame.contents().find('table').index(isFriend));
            }
            var noUseIndex = Number(oFrame.contents().find('table').index(noUse));
            if(!!noUse){

            }else{
                noUseIndex=-1;
            }
            if(isFriendIndex==-1 && noUseIndex==-1){

            }
            else if(noUseIndex-(isFriendIndex+1)<0){
                noUseIndex=noUseIndex-(isFriendIndex+1);
                isFriendIndex=0;
            }
            var wechatList = oFrame.contents().find('table:gt('+isFriendIndex+'):lt('+noUseIndex+')');
            for(var i=0;i<thWeChatList.length;i++){
                wechatList.push(thWeChatList.eq(i).parent().parent());
            }

            // console.log(isFriend);
            // console.log(noUse);
            // console.log(thWeChatList);
            // console.log(isFriendIndex);
            // console.log(noUseIndex);
            // console.log(wechatList);

            if(wechatList.length>0){
                angular.forEach(wechatList,function(value,key){
                    if(angular.element(value).length>0){
                        var tdList=angular.element(value).find('tbody td:first-child:contains("内容")');
                        angular.forEach(tdList,function(value2,key2){
                            if(angular.element(value2).text().length === 2){

                                var ele=angular.element('+td',value2);

                                var txt=ele.html();
                                var test=reg.test(txt);
                                if(test){
                                    txt=txt.replace(reg,'<span class="highLight" style="background:#ffea00;color:#000;">$1</span>');
                                    ele.html(txt);
                                }
                            }
                        })
                    }
                });
            }
            //微信公众号
            function encodeHTML(source) {
                return String(source)
                    .replace(/&/g, '&amp;')
                    .replace(/</g, '&lt;')
                    .replace(/>/g, '&gt;')
                    .replace(/"/g, '&quot;')
                    .replace(/'/g, '&#39;');
            }

            //qq聊天记录
            var thQqList = oFrame.contents().find('th:contains("消息")');
            angular.forEach(thQqList,function(value,key){
                if(angular.element(value).text().length === 2){
                    var trList = angular.element(value).parent().parent().parent().find('tr');
                    angular.forEach(trList,function(value2,key2){
                        if(!!value2){
                            if(angular.element('td',value2).length>0){
                                var td=angular.element("td[rowspan='8']",value2);
                                if(td.length>0){
                                    var ele = td.eq(1);
                                    if(ele.length>0){
                                        var txt=ele.html();
                                        if(reg.test(txt)){
                                            console.log('qq聊天');
                                            txt=txt.replace(reg,'<span class="highLight" style="background:#ffea00;color:#000;">$1</span>');
                                            ele.html(txt);
                                        }
                                    }
                                }
                            }
                        }
                    })
                }
            });
            //新浪微博
            var isMsg = oFrame.contents().find('table caption a:contains("私信")');
            if(isMsg.length>0){
                var tdList=isMsg.parent().parent().find('td:contains("内容")');
                angular.forEach(tdList,function(value2,key2){
                    if(angular.element(value2).text().length === 2){
                        var ele=angular.element('+td',value2);
                        var txt=ele.html();
                        if(reg.test(txt)){
                            console.log('新浪微博')
                            txt=txt.replace(reg,'<span class="highLight" style="background:#ffea00;color:#000;">$1</span>');
                            ele.html(txt);
                        }
                    }
                })
            }
            //腾讯微博
            var isTencent=oFrame.contents().find('table caption a:contains("腾讯微博")');
            if(isTencent.length>0){
                var isMsg = oFrame.contents().find('table caption a:contains("私信")');
                if(isMsg.length>0){
                    var tdList=isMsg.parent().parent().find('td:contains("消息")');
                    angular.forEach(tdList,function(value2,key2){
                        if(angular.element(value2).text().length === 2){
                            var ele=angular.element('+td',value2);
                            var txt=ele.html();
                            if(reg.test(txt)){
                                console.log('腾讯微博');
                                txt=txt.replace(reg,'<span class="highLight" style="background:#ffea00;color:#000;">$1</span>');
                                ele.html(txt);
                            }
                        }
                    })
                }
            }
            //短消息
            var isMessage=oFrame.contents().find('table caption a:contains("短信息")');
            if(isMessage.length>0){
                var thisMsg = oFrame.contents().find('th:contains("姓名")');
                if(thisMsg.length>0){
                    angular.forEach(thisMsg,function(v,i){
                        if(!!v){
                            var tdEle = angular.element(v).parent().parent().parent().find('td:contains("内容")');
                            angular.element(tdEle,function(v2,i2){
                                if(!!v2){
                                    var ele=angular.element('+td',v2);
                                    var txt=ele.html();
                                    if(reg.test(txt)){
                                        console.log('短消息');
                                        txt=txt.replace(reg,'<span class="highLight" style="background:#ffea00;color:#000;">$1</span>');
                                        ele.html(txt);
                                    }
                                }
                            })
                        }
                    })
                }
            }
        }

        var highLight = oFrame.contents().find('.highLight');
        if(n=='reverce'){
            $scope.data.NowItemNum=highLight.length;
        }else{
            $scope.data.NowItemNum=1;
        }
        $scope.skipWordPosition();
    };
    $scope.skipWordPosition=function(n){     //跳到词汇位置
        var highLight = angular.element('#word-frame-container').contents().find('.highLight');
        if(highLight.length>0){
            highLight.css({background:'#ffea00',color:'#000'});
            highLight.eq($scope.data.NowItemNum-1).css({background:'#ea4844',color:'#fff'});
            oFrame.contents().find('html,body').scrollTop(highLight.eq($scope.data.NowItemNum-1).offset().top-10);
        }
        limitBtn=true;
    };
    var limitBtn=true;
    $scope.skipPrevWord=function(type){    //跳到上一个位置
        if(!limitBtn)return;
        limitBtn=false;
        oFrame=oFrame?oFrame:angular.element('#word-frame-container');
        var highLight = oFrame.contents().find('.highLight');
        $scope.data.NowItemNum--;

        if($scope.data.NowItemNum<1 ){
            if($scope.data.currentPageIndex<=0){
                alert('已经是第一条了');
                limitBtn=true;
            }else{
                $scope.data.currentPageIndex--;
                $scope.frameAddSrc('reverce',type);
            }
        }else{
            $scope.skipWordPosition();
        }
    };
    $scope.skipNextWord=function(type){   //跳到下一个位置
        if(!limitBtn)return;

        limitBtn=false;

        oFrame=oFrame?oFrame:angular.element('#word-frame-container');
        var highLight = oFrame.contents().find('.highLight');
        $scope.data.NowItemNum++;
        console.log($scope.data.NowItemNum);
        if($scope.data.NowItemNum>highLight.length){
            if($scope.data.currentPageIndex<$scope.path.currentFormWordPathList.length-1){
                $scope.data.currentPageIndex++;
                $scope.frameAddSrc('',type);
            }else{
                $scope.data.NowItemNum=highLight.length;
                alert('已经是最后一条了');
                limitBtn=true;
            }
        }else{
            $scope.skipWordPosition();
        }

    };

    for(var i in $scope.data.floatDataList){
        (function(i){
            $http({
                url:'http://'+newIp+'/jwcloud/html/dataMining',
                method:"POST",
                data:$.param({
                    // getDogId:$scope.query.dogId,
                    // action:'getWordFrequencyRecord',
                    dogId:$scope.query.dogId,
                    type:$scope.data.floatDataList[i].class,
                    uuid:$scope.data.currentUuid
                }),
                withCredentials:true
            })
                .success(function(data){
                    console.log($scope.data.floatDataList[i].class);

                    console.log(data);
                })
                .error(function(err){

                })
        })(i);
    }

    $scope.searchbar=function(n,page){
        $scope.initView();
        $scope.data.viewWordList=[];
        $scope.data.dataMiningNum=0;
        $scope.switch.isLoading=true;
        $scope.switch.isRuihai=true;
        $scope.path.itemImage=$scope.data.floatDataList[n].onImg;
        $scope.data.dataMiningName=$scope.data.floatDataList[n].name;

        $http({
            url:'http://'+newIp+'/jwcloud/html/dataMining',
            method:"POST",
            data:$.param({
                // getDogId:$scope.query.dogId,
                // action:'getWordFrequencyRecord',
                dogId:$scope.query.dogId,
                type:$scope.data.floatDataList[$scope.data.selectbar].class,
                uuid:$scope.data.currentUuid
            }),
            withCredentials:true
        }).success(function(data){
            data1={
                status:0/-1,
                hits:[
                    {
                        word:'230121199211080612',
                        count:50
                    },
                    {
                        word:'230121199211080613',
                        count:30
                    },
                    {
                        word:'230121199211080614',
                        count:20
                    },
                    {
                        word:'230121199211080615',
                        count:10
                    },
                    {
                        word:'230121199211080616',
                        count:9
                    },
                    {
                        word:'230121199211080617',
                        count:8
                    },
                    {
                        word:'230121199211080618',
                        count:7
                    },
                    {
                        word:'230121199211080619',
                        count:6
                    },
                    {
                        word:'230121199211080610',
                        count:5
                    },
                    {
                        word:'230121199211080621',
                        count:4
                    }
                ],
                totalCount:10
            };
            if(data.status==0){
                $scope.switch.isLoading=false;
                if(data.hits.length>0){
                    $scope.data.viewWordList = data.hits;
                    $scope.data.dataMiningNum = data.totalCount;
                    $scope.addScrolling($scope.data.viewWordList);
                }else{
                    $scope.switch.isRuihai=false;
                }
            }else{
                $rootScope.layer(false,'您还没有登录，请点击“确定”返回登录页');
            }
        });
    };
    if($scope.data.currentFormState=='dataMining'){
        $scope.searchbar($scope.data.selectbar);
    }
});
app.controller('viewFile-ctrl',function($scope,$compile,$location,$http,$state,$rootScope){
    var cookieData=$.cookie();
    var url=cookieData.VFURL;
    $('#view-frame').remove();
    var iFrame=$compile('<iframe src="'+url+'" frameborder="0" id="view-frame"></iframe>')($scope);
    angular.element('.view-add').append(iFrame);
    angular.element('#view-frame').on('load',function(){
        var oFrame=angular.element('#view-frame').contents();
        oFrame.find('body a').removeAttr('href onclick');
        oFrame.find('html script').remove();
        // angular.element('.view-add').off('load');
        var oImg=oFrame.find('img');
        oFrame.find('html').css({'width':'100%','height':'100%'});
        oFrame.find('body').css({'width':'100%','height':'100%'});
        if(oImg.length==1){
            oImg.eq(0).css({'max-width':'100%','max-height':'100%'});
        }
    });
    console.log($location.search());
    console.log($.cookie());
    $scope.back=function(){
        $state.go('word',{path:$location.search().path,basePath:$location.search().basepath,'page':cookieData.indexPage});
    };

    //加载历史记录
        $http({
            url:'http://'+newIp+'/jwcloud/user/getRecord',
            method:"POST",
            data:$.param({
                dogId:$scope.query.dogId,
                getDogId:$scope.query.dogId,
                action:'getWordFrequencyRecord'
            }),
            withCredentials:true
        }).success(function(data){
            if(data.status==0){
                $scope.data.historyList=data.recordList;
            }else{
                $rootScope.layer(false,'您还没有登录，请点击“确定”返回登录页');
            }
        });
});

app.controller('reportMap-ctrl',function($scope,$http,$state,$rootScope,$timeout,$location){
    $scope.hideShade=function(){
        $scope.switch.isShowReport=false;
    };
    $scope.getSearchReportDataList=function(e){
        if(e){
            var keycode = window.event?e.keyCode:e.which;
            if(keycode==13){
                $scope.data.searchFormResult=[];
                seachReport(1);
            }
        }else{
            $scope.data.searchFormResult=[];
            seachReport(1);
        }
        function seachReport(i){
            $http({
                url:'http://'+newIp+'/jwcloud/html/getStatements',
                method:'POST',
                data:$.param({dogId:$scope.query.dogId,"keyword":$scope.data.reportSearchWord,"page":i,"class":'all'}),
                withCredentials:true
            }).success(function(data){
                if(data.status==0){
                    var dataList=data.hits;
                    $scope.data.searchFormResultTotalPage=data.totalPage;
                    for(var i=0;i<dataList.length;i++){
                        $scope.data.searchFormResult.push(dataList[i]);
                    }
                    console.log($scope.data.searchFormResult);
                    $scope.switch.lock2=true;
                    $timeout(function(){
                        $rootScope.setReportBoxTop();
                    },0);
                }else{
                    $rootScope.layer(false,'您还没有登录，请点击“确定”返回登录页');
                }

            });
        }
    };
    $scope.switch.lock2=true;
    angular.element('.report-search-list').on('scroll',function(){
        if(!$scope.switch.lock2)return;

        var scrollTop = $('.report-search-list').scrollTop();
        console.log(scrollTop);
        var oH = $('.report-search-list').height();
        var oUh = $('.report-search-list ul').height();
        if($scope.data.searchFormResultPage<$scope.data.searchFormResultTotalPage){
            if(scrollTop+oH>oUh-20){
                $scope.switch.lock2=false;
                $scope.data.searchFormResultPage++;
                seachReport();
            }
        }


    });
    $scope.insertSelectList=function(i){
        var json=$scope.data.searchFormResult[i];

        if(!findSameObj($scope.data.SelectList,json)){
            if(findSameSource($scope.data.SelectList,json)){
                for(var i=0;i<$scope.data.SelectList.length;i++){

                    if($scope.data.SelectList[i].path.split('/').slice(-1) == json.path.split('/').slice(-1)){
                        json.class=$scope.data.SelectList[i].class;
                        break;
                    }
                }
            }else{
                json.class=$scope.data.classList[0];
                $scope.data.classList.shift();
            }
            $scope.data.SelectList.unshift(json);
        }
        if($scope.data.SelectList.length>10){
            $scope.delSelect(10);
        }
    };
    $scope.delSelect=function(i){
        var className=$scope.data.SelectList[i].class;
        $scope.data.SelectList.splice(i,1);
        if(!findSameClass($scope.data.SelectList,className)){
            $scope.data.classList.push(className);

        }
    };
    function findSameObj(arr,obj){  //找相同数据源相同报表
        for(var i=0;i<arr.length;i++){
            if(arr[i].uuid == obj.uuid){
                return true;
            }
        }
        return false;
    }
    function findSameSource(arr,obj){   //找相同数据源
        for(var i=0;i<arr.length;i++){
            var pathArr=arr[i].path.split('/');
            var sourceName = pathArr.slice(-1);
            var objSource=obj.path.split('/').slice(-1);
            if(sourceName==objSource){
                return true;
            }
        }
        return false;
    }
    function findSameClass(arr,n){   //找相同className
        for(var i=0;i<arr.length;i++){
            if(arr[i].class==n){
                return true;
            }
        }
        return false;
    }
    $scope.getRelation=function(n){
        $scope.data.currentFormName=n;
        $.cookie('currentFormName',$scope.data.currentFormName);
        localStorage.removeItem('reportList');
    };
    $scope.createMap=function(){
        if($scope.data.SelectList.length==1){
            $scope.getRelation($scope.data.SelectList[0].statementName);
            $location.url('/relationGraph?uuid='+$scope.data.SelectList[0].uuid);
        }else{
            localStorage.setItem('reportList',JSON.stringify($scope.data.SelectList));
            $state.go('relationGraph');
        }
    }
});
app.config(function($stateProvider,$urlRouterProvider){
    $urlRouterProvider.when('','word?page=1');
    $urlRouterProvider.otherwise("/word?page=1");
    $stateProvider.state('word',{
        url:"/word",
        templateUrl:"./wordFrequencyStat/word",
        reload:true,
        params:{'path':null,'page':null,'basePath':null}
    }).state('relationGraph',{
        url:"/relationGraph",
        templateUrl:"./wordFrequencyStat/relationGraph",
        params:{'reportList':null}
    }).state('wordFrequency',{
        url:"/wordFrequency",
        templateUrl:"./wordFrequencyStat/wordFrequency"
    }).state('viewFile',{
        url:"/viewFile",
        templateUrl:"./wordFrequencyStat/viewFile"
    }).state('dataMining',{
        url:"/dataMining",
        templateUrl:"./wordFrequencyStat/dataMining"
    })
});
app.config(function($httpProvider){
    $httpProvider.defaults.headers.post = { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' };
});
// app.config(['$locationProvider', function($locationProvider) {
//     $locationProvider.html5Mode(true);
// }]);
// app.filter("fileSize",[function(e){
//     //格式化文件大小
//     return function(size, e){
//         if( size < 1024 ){
//             return size + "B";
//         }else if( size < 1048576){
//             return (size / 1024).toFixed(3) + "KB";
//         }else if( size < 1073741824){
//             return (size / 1048576).toFixed(3) + "MB";
//         }else{
//             return (size / 1073741824).toFixed(3) + "GB";
//         }
//     }
// }]);
app.filter({
    "fileSize":[function(e){
        //格式化文件大小
        return function(size, e){
            if( size < 1024 ){
                return size + "B";
            }else if( size < 1048576){
                return (size / 1024).toFixed(3) + "KB";
            }else if( size < 1073741824){
                return (size / 1048576).toFixed(3) + "MB";
            }else{
                return (size / 1073741824).toFixed(3) + "GB";
            }
        }
    }],
    "percentCount":function(){
        return function(e){
            if(e<=0.0134385){
                return '1.34385%';
            }else{
                return e*100+'%';
            }

        }
    },
    "setRecordState":function(){
        return function(n){
            if(n==1){
                return "搜索报表";
            }
            else if(n==2){
                return "全文搜索词汇";
            }
            else if(n==3){
                return "搜索统计词汇";
            }
        }
    },
    "getSource":function(){
        return function(n){
            console.log(n);
            var arr=n.split('_');
            return arr[0];
        }
    }

});
// app.filter("percentCount",function(){
//    return function(e){
//        if(e<=0.0134385){
//            return '1.34385%';
//        }else{
//            return e*100+'%';
//        }
//
//    }
// });
// app.filter("setRecordState",function(){
//     return function(n){
//         if(n==1){
//             return "搜索报表";
//         }
//         else if(n==2){
//             return "全文搜索词汇";
//         }
//         else if(n==3){
//             return "搜索统计词汇";
//         }
//     }
// });
app.directive({
    'repeatFinished':function() {
        return {
            link: function (scope, element, attr) {
                console.log(scope.$last);
                if (scope.$last == true) {
                    scope.switch.lock1 = true;
                }
            }
        }
    },
    'watchHeight':function(){
        return{
            link: function (scope, element, attr) {
                scope.$watch(element.height(),function(){
                    // console.log(element.height());
                });
            }
        }
    }
});
