var express = require('express');
var router = express.Router();
var http = require('http');
var fs = require('fs');
var path = require('path');
var scandir = require('scandir').create();
var xlsx=require('node-xlsx');
// var XLSX=require('xlsx');
var multiparty= require('connect-multiparty');
var multipartMiddleware=multiparty({uploadDir:'./uploader'});

var readLine = require('fs-readline');

var iconv = require('iconv-lite');
var spawn = require('child_process').spawn;
var appDir = path.dirname(require.main.filename);
var cfginfoObj = require('./cfginfo.js');

var testE = require('./testE');

var state=false;    //true是升级中 false是正常

var specialLength={};
var objLength={};
var allCount={};
var sendFilecount={};

var administrator=['admin_03','admin_05','admin_06'];
/* GET home page. */
var newIp='21.26.96.110:8080';
var ip='21.26.96.108:9200';
var lastIp='21.26.96.110:80';
// var newIp='21.26.96.110:8080';
// var ip='21.26.96.108:9200';
// var lastIp='21.26.96.108:80';

router.get('/', function(req, res) {
  res.render('search',{newIp:newIp,ip:ip,lastIp:lastIp});
});
router.get('/login',function(req,res){
  res.render('login',{newIp:newIp,ip:ip,lastIp:lastIp});
});
router.get('/searchForm',function(req,res){
    var Cookies={};
    req.headers.cookie && req.headers.cookie.split(';').forEach(function(Cookie){
        var parts = Cookie.split('=');
        Cookies[ parts[0].trim() ] = ( parts[1] || '' ).trim();
    });
    var username=Cookies.login_user;
    if(!!username) {
        var rendUrl='';
        function checkUser(){
            for(var i=0;i<administrator.length;i++){
                if(administrator[i]==username){
                    return true;
                }
            }
            return false;
        }

        if(!state || !!checkUser()){
            rendUrl='searchForm'
        }else{
            rendUrl='default/fixing';
        }
        res.render(rendUrl, {newIp: newIp, ip: ip, lastIp: lastIp});
    }
});
router.get('/wanganLogin',function(req,res){
    var Cookies={};
    req.headers.cookie && req.headers.cookie.split(';').forEach(function(Cookie){
        var parts = Cookie.split('=');
        Cookies[ parts[0].trim() ] = ( parts[1] || '' ).trim();
    });

    var username=Cookies.login_user;
    if(!!username) {
        var rendUrl = '';

        function checkUser() {
            for (var i = 0; i < administrator.length; i++) {
                if (administrator[i] == username) {
                    return true;
                }
            }
            return false;
        }

        if (!state || !!checkUser()) {
            rendUrl = 'wangan/login'
        } else {
            rendUrl = 'default/fixing';
        }
        res.render(rendUrl, {newIp: newIp, ip: ip, lastIp: lastIp});
    }
});
router.get('/sourcelist',function(req,res){
    res.render('sourcelist',{newIp:newIp,ip:ip,lastIp:lastIp});
});
router.get('/search',function(req,res){
    var Cookies={};
    req.headers.cookie && req.headers.cookie.split(';').forEach(function(Cookie){
        var parts = Cookie.split('=');
        Cookies[ parts[0].trim() ] = ( parts[1] || '' ).trim();
    });

    var username=Cookies.login_user;
    if(!!username) {
        var rendUrl = '';

        function checkUser() {
            for (var i = 0; i < administrator.length; i++) {
                if (administrator[i] == username) {
                    return true;
                }
            }
            return false;
        }

        if (!state || !!checkUser()) {
            rendUrl = 'search'
        } else {
            rendUrl = 'default/fixing';
        }
        res.render(rendUrl,{newIp: newIp, ip: ip, lastIp: lastIp});
    }else{
        res.render('search',{newIp: newIp, ip: ip, lastIp: lastIp});
    }
});
router.get('/result',function(req,res){
  res.render('result',{newIp:newIp,ip:ip,lastIp:lastIp});
});
router.get('/wordFrequencyStat',function(req,res){
    res.render('wordFrequency/index',{newIp:newIp,ip:ip,lastIp:lastIp});
});
router.get('/wordFrequencyStat/relationGraph',function(req,res){
    res.render('wordFrequency/relationGraph',{newIp:newIp,ip:ip,lastIp:lastIp});
});
router.get('/wordFrequencyStat/wordFrequency',function(req,res){
   res.render('wordFrequency/wordFrequency',{newIp:newIp,ip:ip,lastIp:lastIp});
});
router.get('/wordFrequencyStat/dataMining',function(req,res){
    res.render('wordFrequency/dataMining',{newIp:newIp,ip:ip,lastIp:lastIp});
});
router.get('/wordFrequencyStat/viewFile',function(req,res){
    res.render('wordFrequency/viewFile',{newIp:newIp,ip:ip,lastIp:lastIp});
});
router.get('/wordFrequencyStat/pageitem',function(req,res){
    res.render('wordFrequency/pageitem',{newIp:newIp,ip:ip,lastIp:lastIp});
});
router.get('/wordFrequencyStat/word',function(req,res){
    res.render('wordFrequency/word',{newIp:newIp,ip:ip,lastIp:lastIp});
});
router.get('/wordFrequencyStat/reportMap',function(req,res){
    res.render('wordFrequency/component/reportMap',{newIp:newIp,ip:ip,lastIp:lastIp});
});
router.get('/right',function(req,res){
    res.render('right',{newIp:newIp,ip:ip,lastIp:lastIp});
});

router.post('/test',function(req,res){
    // res.render('test',{newIp:newIp,ip:ip,lastIp:lastIp});
    setTimeout(function(){res.send('返回结果');},2000);

});
function isFindSame(arr,n){
    for(var i=0;i<arr.length;i++){
        if(arr[i]==n){
            return true;
        }
    }
    return false;
}
router.get('/admin',function(req,res){
    var Cookies={};
    var isHighAdmin;
    req.headers.cookie && req.headers.cookie.split(';').forEach(function(Cookie){
        var parts = Cookie.split('=');
        Cookies[ parts[0].trim() ] = ( parts[1] || '' ).trim();
    });

    var username=Cookies.login_user;
    if(!!username){
        if(isFindSame(administrator,username)){
            isHighAdmin = true;
        }else{
            isHighAdmin = false;
        }
        var rendUrl='';
        function checkUser(){
            for(var i=0;i<administrator.length;i++){
                if(administrator[i]==username){
                    return true;
                }
            }
            return false;
        }

        if(!state || !!checkUser()){
            rendUrl='admin/admin'
        }else{
            rendUrl='default/fixing';
        }
        sendFilecount[username]={};
        allCount[username]={};
        specialLength[username]=[];
        objLength[username]=0;
        delFile(username);
        res.render(rendUrl,{newIp:newIp,ip:ip,lastIp:lastIp,isHighAdmin:isHighAdmin});
    }
});
router.get('/accountManage',function(req,res){
    res.render('admin/accountManage',{newIp:newIp,ip:ip,lastIp:lastIp});
});
router.get('/newCreate',function(req,res){
    var Cookies={};
    var isHighAdmin;
    req.headers.cookie && req.headers.cookie.split(';').forEach(function(Cookie){
        var parts = Cookie.split('=');
        Cookies[ parts[0].trim() ] = ( parts[1] || '' ).trim();
    });

    var username=Cookies.login_user;
    if(!!username) {
        if(isFindSame(administrator,username)){
            isHighAdmin = true;
        }else{
            isHighAdmin = false;
        }
        res.render('admin/newCreate', {newIp: newIp, ip: ip, lastIp: lastIp,isHighAdmin:isHighAdmin});
    }
});
router.get('/applyList',function(req,res){
    res.render('admin/applyList',{newIp:newIp,ip:ip,lastIp:lastIp});
});
router.get('/historyList',function(req,res){
    res.render('admin/historyList',{newIp:newIp,ip:ip,lastIp:lastIp});
});
router.get('/detail',function(req,res){
    res.render('detail',{newIp:newIp,ip:ip,lastIp:lastIp});
});
router.get('/uploadForms',function(req,res){
    res.render('admin/uploadForms',{newIp:newIp,ip:ip,lastIp:lastIp}) ;
});
router.get('/whiteList',function(req,res){
    res.render('admin/whiteList',{newIp:newIp,ip:ip,lastIp:lastIp});
});
router.get('/testExplorer',function(req,res){
    res.render('testExplorer',{newIp:newIp,ip:ip,lastIp:lastIp});
});

router.post('/getExplorer',function(req,res){
    var Cookies={};
    req.headers.cookie && req.headers.cookie.split(';').forEach(function(Cookie){
        var parts = Cookie.split('=');
        Cookies[ parts[0].trim() ] = ( parts[1] || '' ).trim();
    });
    var username=Cookies.login_user;
    if(!!username){
        var Epath=req.body.path;
        var action = req.body.action;
        var index = req.body.index;
        var callbackData='';
        switch(action){
            case "getsize":
                callbackData = CalcFolderSize(Epath);
                break;
            default:
                callbackData = GetFolder(Epath);
                break;
        }

        //请求路径
        function GetCurrentPath(){
            var currentPath = Epath;
            if(currentPath == ''){
                currentPath = '/';
            }
            currentPath = currentPath.replace('..','').replace('//','/');
            if(currentPath.substring(0,1) != '/'){
                currentPath = '/'+currentPath;
            }
            return currentPath;
        }
        //获取当前路径的父路径
        function GetParentPath(e){
            var find = e.lastIndexOf('/');
            if(find == '-1'){
                return '/';
            }else{
                return e.substring(0,find);
            }
        }

        //返回某个目录的文件大小
        function CalcFolderSize(p) {
            var dataJson;
            dataJson={
                "index":index?index:0,
                "parent":GetParentPath(p),
                "path":p,
                "size":parseInt(GetFolderSize(p))+1
            };
            return dataJson;
        }
        //（递归）获取某个目录下所有文件大小
        function GetFolderSize(q){
            var openFolder = OpenFolder(q);
            var size=0;
            openFolder.file.forEach(function(v,i){
                size+=v.size;
            });
            openFolder.folder.forEach(function(v,i){
                size+=GetFolderSize(q+'/'+v.name);
            });
            return size;
        }
        //(API返回)返回某个目录的所有文件和文件夹
        function GetFolder(path){
            return OpenFolder(path);
        }
        //打开目录
        function OpenFolder(q){
            var json = {
                "path":q,
                "folder":[],
                "file":[]
            };
            var files = fs.readdirSync(q);
            files.forEach(function(value,index){
                if(value !='.' && value !='..'){
                    var realname = q+'/'+value;
                    if(fs.lstatSync(realname).isDirectory()){
                        var folderJson = {};
                        folderJson.name=value;
                        json.folder.push(folderJson);
                    }else{
                        var fileJson={};
                        fileJson.name=value;
                        fileJson.size=fs.statSync(realname).size;
                        json.file.push(fileJson);
                    }
                }
            });
            return json;
        }
        res.send(callbackData);
    }else{
        res.send({
            status:-1,
            msg:'对不起，请您登陆'
        })
    }
});
router.post('/uploader/delFiles',function(req,res){
    var Cookies={};
    req.headers.cookie && req.headers.cookie.split(';').forEach(function(Cookie){
        var parts = Cookie.split('=');
        Cookies[ parts[0].trim() ] = ( parts[1] || '' ).trim();
    });
    var username=Cookies.login_user;
    if(!!username){
        objLength[username]=0;
        specialLength[username]=[];
        delFile(Cookies.login_user);
        res.send({status:0});
    }
});
router.get('/messageCenter',function(req,res){
    res.render(state?'default/fixing':'messageCenter',{newIp:newIp,ip:ip,lastIp:lastIp});
});
router.get('/mixresult',function(req,res){
    var Cookies={};
    req.headers.cookie && req.headers.cookie.split(';').forEach(function(Cookie){
        var parts = Cookie.split('=');
        Cookies[ parts[0].trim() ] = ( parts[1] || '' ).trim();
    });

    var username=Cookies.login_user;
    if(!!username) {
        var rendUrl = '';

        function checkUser() {
            for (var i = 0; i < administrator.length; i++) {
                if (administrator[i] == username) {
                    return true;
                }
            }
            return false;
        }

        if (!state || !!checkUser()) {
            rendUrl = 'mixresult'
        } else {
            rendUrl = 'default/fixing';
        }
        res.render(rendUrl, {newIp: newIp, ip: ip, lastIp: lastIp});
    }
});
router.post('/ConfigInfo',function(req,res){
    var appPath = path.normalize(appDir+'/../');
    var cfginfo = new cfginfoObj(appPath);

    var qrInfo = req.query.data;
    if(qrInfo.length >0)
    {
        var query = "AuthCode";
        // console.log(query.toUpperCase());
        // console.log(qrInfo.toUpperCase());

        if(query.toUpperCase() == qrInfo.toUpperCase())
        {
            // console.log('Got Authentication Code Request.');
            var authCode = cfginfo.getAuthCode();

            res.send(authCode);
        }
        var query = "VendorId";
        if(query.toUpperCase() == qrInfo.toUpperCase())
        {
            // console.log('Got Authentication Vendor ID Request.')
            var vendorID = cfginfo.getVendorID();
            // console.log('Vendor ID:'+vendorID);
            res.send(vendorID);
        }
    }
});
// 删除文件函数（只删除选中文件夹里内容）
function delFile(username){
    var rootFile = 'data/files/'+username;
    var emptyDir = function(fileUrl){
        var files = fs.readdirSync(fileUrl);
        files.forEach(function(file){
            var stats=fs.statSync(fileUrl+'/'+file);
            if(stats.isDirectory()){
                emptyDir(fileUrl+'/'+file);
            }else{
                fs.unlinkSync(fileUrl+'/'+file);
                console.log('删除文件'+fileUrl+file+'成功');
            }
        });
    };
    var rmEmptyDir = function (fileUrl){
        var files=fs.readdirSync(fileUrl);

        if(files.length>0){
            var tempFile = 0;
            files.forEach(function(fileName){
                tempFile++;
                rmEmptyDir(fileUrl+'/'+fileName);
            });
            if(tempFile == files.length){
                if(fileUrl != 'data/files/'+username){
                    fs.rmdirSync(fileUrl);
                    console.log('删除文件夹'+fileUrl+'成功');
                }else{
                    return;
                }
            }
        }else{
            if(fileUrl != 'data/files/'+username ){
                fs.rmdirSync(fileUrl);
                console.log('删除文件夹'+fileUrl+'成功');
            }else{
                return;
            }
        }
    };
    var checkFile=fs.existsSync(rootFile);
    if(!!checkFile){
        emptyDir(rootFile);
        rmEmptyDir(rootFile);
    }
}
//删除它自己
function delFileSelf(folder){
    var rootFile = folder;
    var emptyDir = function(fileUrl){
        var files = fs.readdirSync(fileUrl);
        files.forEach(function(file){
            var stats=fs.statSync(fileUrl+'/'+file);
            if(stats.isDirectory()){
                emptyDir(fileUrl+'/'+file);
            }else{
                fs.unlinkSync(fileUrl+'/'+file);
                console.log('删除文件'+fileUrl+file+'成功');
            }
        });
    };
    var rmEmptyDir = function (fileUrl){
        var files=fs.readdirSync(fileUrl);
        if(files.length>0){
            var tempFile = 0;
            files.forEach(function(fileName){
                tempFile++;
                rmEmptyDir(fileUrl+'/'+fileName);
            });
            if(tempFile == files.length){
                fs.rmdirSync(fileUrl);
                console.log('删除文件夹'+fileUrl+'成功');
            }
        }else{
            fs.rmdirSync(fileUrl);
            console.log('删除文件夹'+fileUrl+'成功');
        }
    };
    var checkFile=fs.existsSync(rootFile);
    if(!!checkFile){
        emptyDir(rootFile);
        rmEmptyDir(rootFile);
    }
}

router.post('/uploaderfile',multipartMiddleware,function(req,res){
    var Cookies={};
    req.headers.cookie && req.headers.cookie.split(';').forEach(function(Cookie){
        var parts = Cookie.split('=');
        Cookies[ parts[0].trim() ] = ( parts[1] || '' ).trim();
    });
    var username=Cookies.login_user;
    if(!!username) {
        var sourceField = req.body.source;
        console.log(sourceField);
        var path = req.files.file.path;
        console.log(path);
        var fileName = req.files.file.name;
        var filesType = req.body.filesType;
        var whiteType = req.body.whiteType;
        var lastName = req.body.lastName;
        var format = fileName.substring(fileName.lastIndexOf('.') + 1, fileName.length).toLowerCase();
        console.log(sourceField,fileName);
        console.log('上传');
        if (filesType != 'whiteList') {

        fs.exists('data/files/' + username, function (exi) {     //判断是否有“用户名”文件夹
            if (!!exi) {
                console.log('有用户名');
                fs.exists('data/files/' + username + '/forms', function (exis) {  //有用户名  判断是否有forms文件夹
                    console.log('判断是否有forms=' + exis);
                    if (!exis) {
                        console.log('没有forms判断files文件');
                        if (filesType == 'forms') {
                            console.log('没有forms创建');
                            fs.mkdir('data/files/' + username + '/forms/', function (err) {  //没有forms创建forms
                                if (err) {
                                    res.send({
                                        status: -1,
                                        message: err
                                    })
                                } else {
                                    console.log('创建完forms 创建下属数据源');
                                    fs.mkdir('data/files/' + username + '/forms/' + sourceField, function (err) {
                                        if (err) {
                                            console.error(err);
                                        } else {
                                            var source = fs.createReadStream(path);  // 读取上传文件默认存储地方的文件
                                            var dest = fs.createWriteStream('data/files/' + username + '/forms/' + sourceField + '/' + fileName, {encoding: 'utf8'});  //写入创建的文件夹
                                            source.pipe(dest);   // 先执行读取 读取出来的数据用作 写入
                                            source.on('end',
                                                function () {
                                                    fs.exists(path, function (exists) {
                                                        if (!!exists) {
                                                            fs.unlinkSync(path);
                                                            res.json({status: 0, success: true});
                                                        }
                                                    });
                                                }
                                                );
                                        }
                                    });
                                }
                            });
                        } else {
                            console.log('判断是否存在files文件夹');
                            fs.exists('data/files/' + username + '/files', function (e) {
                                console.log(e);
                                if (!e) {
                                    console.log('没有files创造files');
                                    fs.mkdirSync('data/files/' + username + '/files', function (err) {
                                        if (err) {
                                            res.send({
                                                status: -1,
                                                message: err
                                            })
                                        }
                                    });
                                    console.log('创建数据源文件夹');
                                    fs.mkdir("data/files/" + username + '/files/' + sourceField, function (err) {
                                        if (err) {
                                            console.error(err);
                                        } else {
                                            var source = fs.createReadStream(path);  // 读取上传文件默认存储地方的文件
                                            var dest = fs.createWriteStream('data/files/' + username + '/files/' + sourceField + '/' + fileName, {encoding: 'utf8'});  //写入创建的文件夹
                                            source.pipe(dest);   // 先执行读取 读取出来的数据用作 写入
                                            source.on('end',
                                                function () {
                                                    fs.exists(path, function (exists) {
                                                        if (!!exists) {
                                                            fs.unlinkSync(path);
                                                            console.log('文件:' + path + '删除成功！');
                                                            res.json({status: 0, success: true});
                                                        }
                                                    });

                                                });
                                        }
                                    });
                                } else {
                                    fs.exists('data/files/' + username + '/files/' + sourceField, function (exists) {  //判断上传的数据源对应是否有文件夹
                                        if (!!exists) {   // 如果有直接插入文件
                                            var source = fs.createReadStream(path);
                                            var dest = fs.createWriteStream('data/files/' + username + '/files/' + sourceField + '/' + fileName, {encoding: 'utf-8'});
                                            source.pipe(dest);
                                            source.on('end',
                                                function () {
                                                    fs.unlinkSync(path);
                                                    res.json({status: 0, success: true});
                                                });
                                        } else {   // 如果没有 先创建文件夹
                                            delFile(username + '/files');
                                            fs.mkdir('data/files/' + username + '/files/' + sourceField, function (err) {
                                                if (err) {
                                                    console.error(err);
                                                } else {
                                                    var source = fs.createReadStream(path);  // 读取上传文件默认存储地方的文件
                                                    var dest = fs.createWriteStream('data/files/' + username + '/files/' + sourceField + '/' + fileName, {encoding: 'utf8'});  //写入创建的文件夹
                                                    source.pipe(dest);   // 先执行读取 读取出来的数据用作 写入
                                                    source.on('end',
                                                        function () {
                                                            fs.unlinkSync(path);
                                                            res.json({status: 0, success: true});
                                                        });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    } else {  //有forms
                        if (filesType == 'forms') {
                            fs.exists('data/files/' + username + '/forms/' + sourceField, function (exists) {  //判断上传的数据源对应是否有文件夹
                                console.log('有forms判断数据源文件是否存在' + exists);
                                if (!!exists) {   // 如果有直接插入文件
                                    console.log('有form有数据源,直接移动');
                                    var source = fs.createReadStream(path);
                                    console.log(path);
                                    var dest = fs.createWriteStream('data/files/' + username + '/forms/' + sourceField + '/' + fileName, {encoding: 'utf-8'});
                                    source.pipe(dest);
                                    source.on('end',
                                        function () {
                                            fs.exists(path, function (exists) {
                                                if (!!exists) {
                                                    fs.unlinkSync(path);
                                                    res.json({status: 0, success: true});
                                                }
                                            });
                                        });
                                } else {   // 如果没有指定数据源文件夹  先创建文件夹
                                    delFile(username + '/forms');
                                    fs.mkdir("data/files/" + username + '/forms/' + sourceField, function (err) {
                                        if (err) {
                                            console.error(err);
                                        } else {
                                            var source = fs.createReadStream(path);  // 读取上传文件默认存储地方的文件
                                            var dest = fs.createWriteStream('data/files/' + username + '/forms/' + sourceField + '/' + fileName, {encoding: 'utf8'});  //写入创建的文件夹
                                            source.pipe(dest);   // 先执行读取 读取出来的数据用作 写入
                                            source.on('end',
                                                function () {
                                                    fs.exists(path, function (exists) {
                                                        if (!!exists) {
                                                            fs.unlinkSync(path);
                                                            res.json({status: 0, success: true});
                                                        }
                                                    });
                                                });
                                        }
                                    });
                                }
                            });
                        } else {
                            console.log('有username判断是否有files');
                            fs.exists('data/files/' + username + '/files', function (e) {
                                console.log(e);
                                if (!e) {
                                    console.log('如果没有创建files');
                                    fs.mkdir('data/files/' + username + '/files', function (err) {
                                        if (err) {
                                            console.error(err);
                                        }
                                        console.log('创建完files创建数据源文件夹');
                                        fs.mkdir('data/files/' + username + '/files/' + sourceField, function (err) {
                                            if (err) {
                                                console.error(err);
                                            } else {

                                                var source = fs.createReadStream(path);  // 读取上传文件默认存储地方的文件
                                                var dest = fs.createWriteStream('data/files/' + username + '/files/' + sourceField + '/' + fileName, {encoding: 'utf8'});  //写入创建的文件夹
                                                source.pipe(dest);   // 先执行读取 读取出来的数据用作 写入
                                                source.on('end',
                                                    function () {
                                                        fs.unlinkSync(path);
                                                        res.json({status: 0, success: true});
                                                    });
                                            }
                                        });
                                    })
                                } else {
                                    console.log('如果有判断数据源文件夹');
                                    fs.exists('data/files/' + username + '/files/' + sourceField, function (exists) {  //判断上传的数据源对应是否有文件夹
                                        if (!!exists) {   // 如果有直接插入文件
                                            var source = fs.createReadStream(path);
                                            var dest = fs.createWriteStream('data/files/' + username + '/files/' + sourceField + '/' + fileName, {encoding: 'utf-8'});
                                            source.pipe(dest);
                                            source.on('end',
                                                function () {
                                                    fs.unlinkSync(path);
                                                    res.json({status: 0, success: true});
                                                });
                                        } else {   // 如果没有 先创建文件夹
                                            delFile(username + '/files');
                                            fs.mkdir('data/files/' + username + '/files/' + sourceField, function (err) {
                                                if (err) {
                                                    console.error(err);
                                                } else {
                                                    var source = fs.createReadStream(path);  // 读取上传文件默认存储地方的文件
                                                    var dest = fs.createWriteStream('data/files/' + username + '/files/' + sourceField + '/' + fileName, {encoding: 'utf8'});  //写入创建的文件夹
                                                    source.pipe(dest);   // 先执行读取 读取出来的数据用作 写入
                                                    source.on('end',
                                                        function () {
                                                            fs.unlinkSync(path);
                                                            res.json({status: 0, success: true});
                                                        });
                                                }
                                            });
                                        }
                                    });
                                }
                            });

                        }
                    }
                })
            } else {
                fs.mkdir('data/files/' + username, function (err) {
                    if (err) {
                        res.send({
                            status: -1,
                            message: err
                        })
                    } else {
                        if (filesType == 'forms') {
                            fs.mkdir('data/files/' + username + '/forms', function (err) {
                                if (err) {
                                    res.send({
                                        status: -1,
                                        message: err
                                    })
                                } else {
                                    fs.mkdir('data/files/' + username + '/forms/' + sourceField, function (err) {
                                        if (err) {
                                            res.send({
                                                status: -1,
                                                message: err
                                            })
                                        } else {
                                            if (format == 'html' || format == 'zip') {
                                                var source = fs.createReadStream(path);  // 读取上传文件默认存储地方的文件
                                                var dest = fs.createWriteStream('data/files/' + username + '/forms/' + sourceField + '/' + fileName, {encoding: 'utf-8'});  //写入创建的文件夹
                                                source.pipe(dest);   // 先执行读取 读取出来的数据用作 写入
                                                source.on('end',
                                                    function () {
                                                        fs.unlinkSync(path);
                                                        res.json({status: 0, success: true});
                                                    });
                                            }

                                        }
                                    })
                                }
                            });
                        } else {
                            fs.mkdir('data/files/' + username + '/files', function (err) {
                                if (err) {
                                    res.send({
                                        status: -1,
                                        message: err
                                    })
                                } else {
                                    fs.mkdir('data/files/' + username + '/files/' + sourceField, function (err) {
                                        if (err) {
                                            res.send({
                                                status: -1,
                                                message: err
                                            })
                                        } else {
                                            if (format != 'html' && format != 'zip') {
                                                var source = fs.createReadStream(path);  // 读取上传文件默认存储地方的文件
                                                var dest = fs.createWriteStream('data/files/' + username + '/files/' + sourceField + '/' + fileName, {encoding: 'utf-8'});  //写入创建的文件夹
                                                source.pipe(dest);   // 先执行读取 读取出来的数据用作 写入
                                                source.on('end',
                                                    function () {
                                                        fs.unlinkSync(path);
                                                        res.json({status: 0, success: true});
                                                    });
                                            }
                                        }
                                    })
                                }
                            });
                        }
                    }
                })
            }
        })
    }else {
            fs.exists('data/backup/whiteList',function(exists){
                if(!exists){
                    fs.mkdir('data/backup/whiteList',function(err){
                        if(err){
                            console.log({
                                error:err,
                                line:528
                            });
                        }else{
                            fs.mkdir('data/backup/whiteList/'+whiteType,function(err){
                                if(err){
                                    console.log(err);
                                }else{
                                    fs.mkdir('data/backup/whiteList/'+whiteType+'/'+whiteType+lastName,function(err){
                                        if(err){
                                            console.log(err);
                                        }else{
                                            moveFile();
                                        }
                                    })
                                }
                            });
                        }
                    })
                }else{
                    fs.exists('data/backup/whiteList/'+whiteType,function(exists){
                        if(!exists){
                            fs.mkdir('data/backup/whiteList/'+whiteType,function(err){
                                if(err){
                                    console.log({
                                        error:err,
                                        line:553
                                    });
                                }else{
                                    fs.mkdir('data/backup/whiteList/'+whiteType+'/'+whiteType+lastName,function(err){
                                        if(err){
                                            console.log({
                                                error:err,
                                                line:560
                                            });
                                        }else{
                                            moveFile();
                                        }
                                    })
                                }
                            })
                        }else{
                            fs.exists('data/backup/whiteList/'+whiteType+'/'+whiteType+lastName,function(exi){
                                if(!exi){
                                    fs.mkdir('data/backup/whiteList/'+whiteType+'/'+whiteType+lastName,function(err){
                                        if(err){
                                            console.log({
                                                error:err,
                                                line:575
                                            });
                                        }else{
                                            moveFile();
                                        }
                                    })
                                }else{
                                    moveFile();
                                }
                            })
                        }
                    });
                }
            });

            function moveFile(){
                var source = fs.createReadStream(path);  // 读取上传文件默认存储地方的文件
                var dest = fs.createWriteStream('data/backup/whiteList/'+whiteType+'/'+whiteType+lastName+'/'+fileName, {encoding: 'utf8'});  //写入创建的文件夹
                source.pipe(dest);   // 先执行读取 读取出来的数据用作 写入
                source.on('end',function(){
                    // exe(["-jar", "lib/jwcloud.jar","addWhiteList",'data/backup/whiteList/'+whiteType+'/'+whiteType+lastName ]);
                    res.send({
                        status:0
                    })
                })
            }

        }
    }
});
router.post('/whiteList/sendWhiteList',function(req,res){
    var whiteType = req.body.whiteType;
    var lastName = req.body.lastName;
    var Cookies={};
    req.headers.cookie && req.headers.cookie.split(';').forEach(function(Cookie){
        var parts = Cookie.split('=');
        Cookies[ parts[0].trim() ] = ( parts[1] || '' ).trim();
    });
    var username=Cookies.login_user;
    if(!!username) {
        exe(["-jar", "lib/jwcloud.jar","addWhiteList",'data/backup/whiteList/'+whiteType+'/'+whiteType+lastName ]);
    }
    function exe(command) {
        var cmd = spawn('java',command);
        cmd.stdout.setEncoding('ASCII');
        cmd.stdout.on('data', function (data) {
            data = data.replace(/\s/g, '');
            if(data=='false'){
                console.log('白名单上传失败');
            }else{
                console.log('-------------------------------');
                console.log('exec', command);
                console.log('stdout:' + data);

                console.log('stdout');
                console.log('data='+data);
            }
        });
        cmd.stderr.on('data', function (data) {
            console.log('-------------------------------');
            console.log('stderr:' + data);
            console.log('-------------------------------');
            console.log('stderr');
            console.log('data'+data);
            return;
        });
        cmd.on('exit', function (code) {
            console.log('exit');
            if (code === 0) {
                console.log('白名单上传成功');
                res.send({
                    status:0,
                    msg:'白名单上传成功'
                })
            }
            console.log('exited with code:' + code);
            console.log('-------------------------------');
        });
    }

});
router.post('/uploadImg',multipartMiddleware,function(req,res){
    var Cookies={};
    req.headers.cookie && req.headers.cookie.split(';').forEach(function(Cookie){
        var parts = Cookie.split('=');
        Cookies[ parts[0].trim() ] = ( parts[1] || '' ).trim();
    });
    var username=Cookies.login_user;
    if(!!username) {
        var path = req.files.file.path;
        console.log(path);
        var fileName = req.files.file.name;
        var format = fileName.substring(fileName.lastIndexOf('.') + 1, fileName.length).toLowerCase();
        fs.exists('data/files/'+username+'/QR_code', function (exi) {
            console.log(exi);
            if(!!exi){
                delFile(username + '/QR_code/');
                var source = fs.createReadStream(path);  // 读取上传文件默认存储地方的文件
                var dest = fs.createWriteStream('data/files/' + username + '/QR_code/' + fileName, {encoding: 'utf8'});  //写入创建的文件夹
                source.pipe(dest);   // 先执行读取 读取出来的数据用作 写入
                source.on('end',
                    function () {
                        fs.exists(path, function (exists) {
                            if (!!exists) {
                                fs.unlinkSync(path);
                                res.json({
                                    status: 0,
                                    success: true
                                });
                            }
                        });
                    }
                );
            }else{
                fs.mkdir('data/files/'+username+'/QR_code',function(err){
                    if(err){
                        res.send({
                            status:-1,
                            line:826
                        })
                    }
                    var source = fs.createReadStream(path);  // 读取上传文件默认存储地方的文件
                    var dest = fs.createWriteStream('data/files/' + username + '/QR_code/' + fileName, {encoding: 'utf8'});  //写入创建的文件夹
                    source.pipe(dest);   // 先执行读取 读取出来的数据用作 写入
                    source.on('end',
                        function () {
                            fs.exists(path, function (exists) {
                                if (!!exists) {
                                    fs.unlinkSync(path);
                                    res.json({
                                        status: 0,
                                        success: true
                                    });
                                }
                            });
                        }
                    );
                })
            }
        });
    }
});


router.post('/uploader/compareFile',function(req,res){
    var sourceField = req.body.source;
    var filesType = req.body.filesType;
    var Cookies={};
    req.headers.cookie && req.headers.cookie.split(';').forEach(function(Cookie){
        var parts = Cookie.split('=');
        Cookies[ parts[0].trim() ] = ( parts[1] || '' ).trim();
    });
    var username=Cookies.login_user;
    if(!!username) {
        console.log('进入compare');
        var delForms=[];
        var delFiles=[];
        if(filesType == 'forms'){

            console.log('今入forms');
            delForms=JSON.parse(req.body.delFile).filesArr;
            var delArr = delForms;
            var rootFile = 'data/files/' + username + '/forms/' + sourceField;

            var files = fs.readdirSync(rootFile);// 同步读取 该文件夹下的所有文件（同步读取是指该条执行执行完才可以往下执行！）
            var format = files[0].substring(files[0].lastIndexOf('.') + 1, files[0].length).toLowerCase();   //获取上传文件的格式
            if (!compareForm()) {

                for (var i = 0; i < delArr.length; i++) {
                    fs.unlinkSync('data/files/' +username+'/forms/'+ sourceField + '/' + delArr[i]);
                }
                res.send({
                    status: -1,
                    message: '上传的文件格式不一致'
                })
            }else{
                console.log('判断文件');
                if(format=='html' || format=='zip'){
                    res.send({
                        status:0,
                        filesCount:files.length
                    })
                }else{
                    for (var i = 0; i < delArr.length; i++) {
                        fs.unlinkSync('data/files/' +username+'/forms/'+ sourceField + '/' + delArr[i]);
                    }
                    res.send({
                        status:-1,
                        message: '您添加的文件格式不正确，请添加html、zip格式的文件'
                    })
                }
            }
        }else{
            delFiles=JSON.parse(req.body.delFile).filesArr;
            var rootFile = 'data/files/' + username + '/files/' + sourceField;
            console.log('rootFile了');
            var files = fs.readdirSync(rootFile);// 同步读取 该文件夹下的所有文件（同步读取是指该条执行执行完才可以往下执行！）

            if (!compareForm()) {
                // var delArr = delFiles.split(',');
                var delArr = delFiles;
                for (var i = 0; i < delArr.length; i++) {
                    fs.unlinkSync('data/files/' +username+'/files/'+ sourceField + '/' + delArr[i]);
                }
                res.send({
                    status: -1,
                    message: '上传的文件格式不一致'
                })
            } else {
                var format = files[0].substring(files[0].lastIndexOf('.') + 1, files[0].length);   //获取上传文件的格式
                console.log(format);
                if (format === 'csv') {     //当文件格式是csv时

                    console.log('进入format-csv1111');
                    var firstRows = [];
                    for (var i = 0; i < files.length; i++) {
                        var outArr = [];
                        (function(i){
                        var rl = readLine('data/files/'+ username+ '/files/'+ sourceField + '/' + files[i],{retainBuffer: true});
                        rl.on('line', function (line, idx) {
                            outArr.push(iconv.decode(line, 'GBK'));
                        }).on('abort', function () {
                        }).on('close', function () {
                            if(outArr.length>0){
                                var firstRow = outArr[0].replace(/\s/g, ''); //  将读取得 字符串 分解出第一行
                                var secondRow = outArr[1].replace(/\s/g, '');
                                if (!checkEmptyFile(firstRow,secondRow)){
                                    firstRows.push(firstRow);
                                    if (firstRows.length === files.length) {
                                        if(compareFormatSame(firstRows)){
                                            // for (var i = 0; i < files.length; i++) {
                                            var str = '';
                                            var key=false;
                                            for (var n = 0; n < outArr.length; n++) {
                                                var strB=outArr[n].replace(/,*$/g,'');
                                                // 将csv中单元格有逗号的 处理掉
                                                var arr = strB.replace(/\s/g, '').split(',');  //把该字符串分为数组
                                                var arr2 = [];
                                                for(var j=0;j<arr.length;j++){
                                                    var reg = new RegExp('^\"');
                                                    var reg2 = new RegExp('\"$');
                                                    var reg3 = new RegExp('\\S+"$');

                                                    if(reg.test(arr[j]) && !reg3.test(arr[j])) {
                                                        key=true;
                                                        str += arr[j] + ',';
                                                    }else{
                                                        if(!!key){
                                                            if(j>=arr.length){

                                                            }else{
                                                                if(!!arr[j]){
                                                                    str+=arr[j]+',';
                                                                    if(reg2.test(arr[j])){
                                                                        str=str.replace(/\"/g,'');
                                                                        str=str.substring(0,str.length-1);
                                                                        arr2.push(str);
                                                                        key = false;
                                                                        str='';
                                                                    }
                                                                }else{
                                                                    str+='';
                                                                }
                                                            }
                                                        }else{
                                                            arr2.push(arr[j]);
                                                        }
                                                    }
                                                    if(j==arr.length-1){
                                                        if (arr2.length> objLength[username]) {
                                                            objLength[username] = arr2.length;
                                                            specialLength[username].push(arr2);
                                                        }
                                                    }
                                                }
                                            }
                                            if(i==files.length-1){
                                                outArr=[];
                                                res.send({
                                                    status: 0,
                                                    message: '文件格式正确'
                                                });
                                            }
                                            // }
                                        }
                                    }
                                }else{
                                    res.send({
                                        status:-1,
                                        message:'不要上传空文件,请重新上传该批文件'
                                    })
                                }
                            }else{
                                res.send({
                                    status:-1,
                                    message:'读取文件失败，请重新上传'
                                })
                            }

                        }).on('error', function(err) {
                            console.log('error: ', err.message)
                        });
                        })(i);
                    }
                }
                else if (format === 'xls' || format === 'xlsx') {
                    var firstRows = [];
                    for(var i = 0; i < files.length; i++){
                        var obj = xlsx.parse('data/files/' +username+'/files/'+ sourceField + '/' + files[i]);
                        if(!obj){
                            res.send({
                                status:-1,
                                message:'读取文件错误434line'
                            })
                        }
                        var excelObj = obj[0].data;  //sheet1
                        objLength[username]=excelObj[0].length;
                        if (excelObj.length === 0) {
                            var delArr = delFiles;
                            for (var i = 0; i < delArr.length; i++) {
                                fs.unlinkSync('data/files/'+username+ '/files/'+ sourceField + '/' + delArr[i]);
                            }
                            res.send({
                                status: -1,
                                message: '不要上传空文件,请重新上传该批文件'
                            })
                        } else {
                            firstRows.push(excelObj[0].join(',').replace(/\s/g, ''));
                            if (firstRows.length === files.length) {
                                if(compareFormatSame(firstRows)){
                                    for(var k=0;k<files.length;k++){
                                        var obj = xlsx.parse('data/files/' +username+'/files/'+ sourceField + '/' + files[k]);
                                        var excelObj = obj[0].data;  //sheet1
                                        for (var n = 0; n < excelObj.length; n++) {
                                            if(excelObj[n].length > objLength[username]){
                                                objLength[username] = excelObj[n].length;
                                                specialLength[username].push(excelObj[n]);
                                            }
                                        }
                                        if(k == files.length-1){
                                            res.send({
                                                status: 0,
                                                message: '文件格式正确'
                                            })
                                        }
                                    }
                                }else{
                                    res.send({
                                        status: -1,
                                        message: '文件格式正确'
                                    })
                                }
                            }
                        }
                    }
                }
                else {
                    var delArr = delFiles;
                    for (var i = 0; i < delArr.length; i++) {
                        fs.unlinkSync('data/files/' +username+'/files/'+ sourceField + '/' + delArr[i]);
                    }
                    res.send({
                        status: -1,
                        message: '您添加的文件格式不正确，请添加csv、xls、xlsx格式的文件'
                    });
                }
            }
        }
//  比较 （文件格式是否一致）
        function compareForm() {
            if (files.length === 1) {
                return true;
            } else {
                for (var i = 1; i < files.length; i++) {
                    var name1 = files[0].substring(files[0].lastIndexOf('.') + 1, files[0].length);
                    var name2 = files[i].substring(files[i].lastIndexOf('.') + 1, files[i].length);
                    if (name1 != name2) {
                        return false;
                    }
                }
                return true;
            }
        }
        function checkEmptyFile(firstRow,secondRow) {
            if (firstRow.length == 0 && secondRow.length == 0) {
                var delArr = delFiles;
                for (var i = 0; i < delArr.length; i++) {
                    fs.unlinkSync('data/files/' + username + '/files/' + sourceField + '/' + delArr[i]);
                }
                return true;
            }else{
                return false;
            }
        }
        function compareFormat() {    //判断各文件第一行字段是否一致
            for (var j = 1; j < firstRows.length; j++) {
                if (firstRows[0] !== firstRows[j]) {
                    return false;
                }
            }
            return true;
        }

        //查找字段行中是否有一样的字段
        function compareFormatSame(firstRows) {
            if (compareFormat()) {      //判断各文件第一行字段是否一致
                var reg = new RegExp('^\"');
                var reg2 = new RegExp('\"$');
                var reg3 = new RegExp('\\S+"$');
                var arr = firstRows[0].replace(/\s/g, '').split(',');
                var arr2 = [];
                var str='';
                var key = false;
                for(var j=0;j<arr.length;j++){

                    if(reg.test(arr[j]) && !reg3.test(arr[j])) {
                        key=true;
                        str += arr[j] + ',';
                    }else{
                        if(!!key){
                            if(j>=arr.length){

                            }else{
                                if(!!arr[j]){
                                    str+=arr[j]+',';
                                    if(reg2.test(arr[j])){
                                        str=str.replace(/"/g,'');
                                        str=str.substring(0,str.length-1);
                                        arr2.push(str);
                                        key=false;
                                        str='';
                                    }
                                }else{
                                    str+='';
                                }
                            }
                        }else{
                            arr2.push(arr[j].replace(/"/g,''));
                        }
                    }
                    if(j==arr.length-1){
                        if (arr2.length> objLength[username]) {
                            objLength[username] = arr2.length;
                            specialLength[username].push(arr2);
                        }

                    }

                }
                if (findSame(arr2)) {
                    var delArr = delFiles;
                    for (var i = 0; i < delArr.length; i++) {
                        fs.unlinkSync('data/files/'+ username +'/files/' + sourceField + '/' + delArr[i]);
                    }
                    res.send({
                        status: -1,
                        message: '文件中有重复字段，请修改后重新上传'
                    });
                    return false;
                } else {
                  return true;
                }
            } else {            //如果第一行有一致的
                var delArr = delFiles;
                for (var i = 0; i < delArr.length; i++) {
                    fs.unlinkSync('data/files/' +username+'/files/'+ sourceField + '/' + delArr[i]);
                }
                res.send({
                    status: -1,
                    message: '您添加的文件字段不一致请重新添加'
                });
                return false;
            }
        }

        //查重
        function findSame(arr) {  //判断第一行字段中个字段之间是否有重复
            for (var i = 0; i < arr.length; i++) {
                if (!arr[i]) {  //如果有空的字段跳过

                } else {
                    for (var j = i + 1; j < arr.length; j++) {
                        if (arr[i] === arr[j]) {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
    }
});
router.post('/uploader/getFile',function(req,res){
    var sourceField = req.body.source;
    var Cookies={};
    req.headers.cookie && req.headers.cookie.split(';').forEach(function(Cookie){
        var parts = Cookie.split('=');
        Cookies[ parts[0].trim() ] = ( parts[1] || '' ).trim();
    });
    var username=Cookies.login_user;
    if(!!username) {
        var rootFile = 'data/files/' + username+'/files/' + sourceField;
        var files = fs.readdirSync(rootFile);
        var format = files[0].substring(files[0].lastIndexOf('.') + 1, files[0].length);   //获取上传文件的格式
        console.log('getfile里的='+format);
        if (format === 'csv') {
            console.log('进入csvgetfile');
            var rows = [];
            var rl = readLine('data/files/'+ username+ '/files/'+ sourceField + '/' + files[0],{retainBuffer: true});
            rl.on('line', function (line, idx) {
                rows.push(iconv.decode(line, 'GBK'));
                if(idx==4){
                    this.abort();
                }
            }).on('abort', function () {
                console.log('读取已终止');
            }).on('close', function () {
                if (rows.length > 4) {
                    rows = rows.slice(0, 4);
                }
                console.log(rows);
                //当文件格式是csv时
                var reg = new RegExp('^\"');
                var reg2 = new RegExp('\"$');
                var reg3 = new RegExp('\\S+"$');
                orderNows();
                var str='';
                var key=false;
                // 将csv中单元格有逗号的 处理掉
                function orderNows() {
                    for(var i=0;i<rows.length;i++){
                        var arr = rows[i].split(',');
                        var arr2 = [];
                        for(var j=0;j<arr.length;j++){
                            if(reg.test(arr[j]) && !reg3.test(arr[j])) {
                                key=true;
                                str += arr[j] + ',';
                            }else{
                                if(!!key){
                                    if(j>=arr.length) {

                                    }
                                    else{
                                        if(!!arr[j]){
                                            str+=arr[j]+',';
                                            if(reg2.test(arr[j])){
                                                str=str.replace(/\"/g,'');
                                                str=str.substring(0,str.length-1);
                                                arr2.push(str);
                                                key = false;
                                                str = '';
                                            }
                                        }else{
                                            str+='';
                                        }
                                    }
                                }else{
                                    arr2.push(arr[j].replace(/\"/g,''));
                                }

                            }
                            if(j==arr.length-1){
                                rows[i] = arr2;
                            }
                        }
                        if (i == rows.length-1) {
                            console.log(specialLength);
                            specialLength[username].shift();
                            res.send({
                                status: 0,
                                data: rows,
                                specialData: specialLength[username]
                            });
                            rows = [];
                            return;
                        }
                    }
                }
            }).on('error', function(err) {
                console.log('error: ', err.message)
            });
        }
        else if (format === 'xls' || format === 'xlsx') {
            console.log('处理xls');
            var obj = xlsx.parse('data/files/'+username+'/files/' + sourceField + '/' + files[0]);
            var excelObj = obj[0].data;
            if (excelObj.length > 4) {
                excelObj = excelObj.slice(0, 4);
                for (var i = 0; i < 4; i++) {
                    excelObj[i].length = objLength[username];
                }
            }
            console.log('1464='+JSON.stringify(specialLength));
            specialLength[username].shift();
            res.send({
                status: 0,
                data: excelObj,
                specialData: specialLength[username]
            });
        }
        else {
            res.send({
                status: -1,
                message: '上传出错，请刷新页面重新上传'
            })
        }
    }
});
router.post('/uploader/changeFile',function (req,res) {
    var sourceField = req.body.source;
    var Cookies={};
    req.headers.cookie && req.headers.cookie.split(';').forEach(function(Cookie){
        var parts = Cookie.split('=');
        Cookies[ parts[0].trim() ] = ( parts[1] || '' ).trim();
    });
    var username=Cookies.login_user;
    if(!!username) {
        var folder=fs.readdirSync('data/files/'+username+'/files');
        fs.rename('data/files/'+username+'/files/'+ folder[0] ,'data/files/' +username+'/files/'+sourceField, function (err) {
            if(err){
                res.rend({status:-1,message:'请联系技术人员\n734'});
            }
            objLength[username]=0;
            specialLength[username]=[];
            var rootFile = 'data/files/'+username+'/files/' + sourceField;
            var files = fs.readdirSync(rootFile);
            var format = files[0].substring(files[0].lastIndexOf('.') + 1, files[0].length);
            if (format === 'csv') {
                //上传给es
                for (var i = 0; i < files.length; i++) {
                    (function (i) {
                        var filename=files[i];
                        var rl = readLine('data/files/' +username+'/files/'+ sourceField + '/' + filename,{retainBuffer: true});
                        var dest=fs.createWriteStream('data/files/' +username+'/files/'+ sourceField + '/' + filename+'_1', {encoding: 'utf8'});
                        rl.on('line', function (line, idx) {
                            var line2 = iconv.decode(line, 'gbk')+'\n';
                            dest.write(line2);
                        }).on('abort', function () {
                            console.log('读取已终止');
                        }).on('close', function () {
                            dest.end();
                            fs.unlinkSync('data/files/' +username+'/files/'+ sourceField + '/' + filename);
                            fs.rename('data/files/' +username+'/files/'+ sourceField + '/' + filename+'_1','data/files/' +username+'/files/'+ sourceField + '/' + filename,function(err){
                                console.log(err);
                            });
                            if (i === files.length - 1) {
                                res.send({
                                    status: 0
                                });
                            }
                        });
                    })(i);
                }
            }
            else if (format === 'xls' || format === 'xlsx') {
                for (var i = 0; i < files.length; i++) {
                    var obj = xlsx.parse('data/files/'+username+'/files/' + sourceField + '/' + files[i]);
                    var excelObj = obj[0].data;
                    var data = [];
                    for (var n in excelObj) {
                        var arr = [];
                        var value = excelObj[n];
                        for(var j=0;j<value.length;j++){
                            if(!(value[j]+'')){
                                arr.push('');
                            }else{
                                arr.push(value[j]);
                            }
                        }
                        data.push(arr);
                    }
                    var buffer = xlsx.build([
                        {
                            name: 'sheet1',
                            data: data
                        }
                    ]);
                    fs.unlinkSync('data/files/' + username+'/files/'+sourceField + '/' + files[i]);
                    if (format === 'xls') {
                        fs.writeFileSync('data/files/' +username+'/files/'+ sourceField + '/' + files[i] + '.xlsx', buffer, {'flag': 'w'});
                    } else {
                        fs.writeFileSync('data/files/' +username+'/files/'+ sourceField + '/' + files[i], buffer, {'flag': 'w'});
                    }

                    if (i === files.length - 1) {
                        res.send({
                            status: 0
                        })
                    }
                }
            }
            else {
                res.send({
                    status: -1,
                    message: '编码出错，请刷新页面重新上传'
                })
            }

        });

    }
});

router.post('/uploader/sendFiles',function (req,res){
    var sourceField=req.body.source;
    var batchNum=req.body.batchNum;
    var sendTitle=req.body.sendTitle;
    var sourceDir=req.body.sourceDir;
    var sendType=req.body.sendType;
    var firstClass=req.body.firstClass;
    var Cookies={};
    req.headers.cookie && req.headers.cookie.split(';').forEach(function(Cookie){
        var parts = Cookie.split('=');
        Cookies[ parts[0].trim() ] = (parts[1] || '').trim();
    });
    var username=Cookies.login_user;
    if(!!username){
        if(sendType=='form'){
            var filePath = "data/files/" +username+"/forms/"+ sourceField;
            console.log('准备更名');
            fs.rename(filePath ,'data/files/' +username+'/forms/'+username+'_batch'+batchNum, function (err) {
                if (err) {
                    res.send({
                        status:-1,
                        message:err+'\n1062line'
                    });
                }
                console.log('更名成功开始读取文件');
                var batchPath='data/files/' +username+'/forms/'+username+'_batch'+batchNum;
                var files = fs.readdirSync(batchPath);          // 同步读取 该文件夹下的所有文件（同步读取是指该条执行执行完才可以往下执行！）
                var format = files[0].substring(files[0].lastIndexOf('.') + 1, files[0].length);

                // var date = new Date();
                // var y = date.getFullYear();
                // var m = date.getMonth() + 1;
                // var d = date.getDate();
                // var h = date.getHours();
                // var mi = date.getMinutes();
                // var time = y + '-' + toDou(m) + '-' + toDou(d) + '-' + toDou(h) + '-' + toDou(mi);
                // var randomNum=+parseInt(Math.random()*1000);
                fs.exists('data/backup/'+sourceField,function(exi){
                    if(exi){
                        fs.mkdir('data/backup/'+sourceField+'/'+sourceField+'_'+sourceDir,function(err){
                            if(err){
                                res.send({
                                    status:-1,
                                    message:err+'\n913lin'
                                })
                            }else{
                                fs.writeFile('data/backup/' + sourceField+'/'+sourceField+'_'+sourceDir+'_bin',sendTitle,{flag:'w',encoding:'utf-8',mode:'0666'},function(err) {
                                    if (err) {
                                        res.send({
                                            status:-1,
                                            message:err+'\n920line'
                                        })
                                    } else {
                                        console.log("forms配置文件写入成功");
                                    }
                                });
                                var i = 0;
                                move();
                                function move() {
                                    if (i === files.length) {
                                        fs.rmdirSync(batchPath);
                                        return res.send({
                                            status: 0,
                                            message: '备份成功'
                                        });

                                    } else {
                                        var source = fs.createReadStream(batchPath + '/' + files[i]);  // 读取上传文件该批次存储地方的文件
                                        var dest = fs.createWriteStream('data/backup/' +sourceField+'/' +sourceField+'_'+sourceDir+'/' + files[i]);  //写入创建的文件夹
                                        source.pipe(dest);   // 先执行读取 读取出来的数据用作 写入
                                        source.on('end',
                                            function () {
                                                fs.unlinkSync(batchPath + '/' + files[i]);
                                                i++;
                                                move();
                                            });
                                    }
                                }
                            }
                        });
                    }else{
                        fs.mkdir('data/backup/' + sourceField, function (err) {
                            if (err) {
                                res.send({
                                    status:-1,
                                    message:err+'\n957line'
                                });
                            } else {
                                // for(var i=0;i<files.length;i++){
                                fs.mkdir('data/backup/'+sourceField+'/'+sourceField+'_'+sourceDir,function(err){
                                    if(err){
                                        res.send({
                                            status:-1,
                                            message:err+'\n965line'
                                        })
                                    }else{
                                        fs.writeFile('data/backup/' + sourceField+'/'+sourceField+'_'+sourceDir+'_bin',sendTitle,{flag:'w',encoding:'utf-8',mode:'0666'},function(err) {
                                            if (err) {
                                                res.send({
                                                    status:-1,
                                                    message:err+'\n972line'
                                                })
                                            } else {
                                                console.log("forms配置文件写入成功");
                                            }
                                        });
                                        var i = 0;
                                        move();
                                        function move() {
                                            if (i === files.length) {
                                                fs.rmdirSync(batchPath);
                                                res.send({
                                                    status: 0,
                                                    message: '备份成功'
                                                });

                                            } else {
                                                var source = fs.createReadStream(batchPath + '/' + files[i]);  // 读取上传文件该批次存储地方的文件
                                                var dest = fs.createWriteStream('data/backup/' +sourceField+'/' +sourceField+'_'+sourceDir+'/' + files[i]);  //写入创建的文件夹
                                                source.pipe(dest);   // 先执行读取 读取出来的数据用作 写入
                                                source.on('end',
                                                    function () {
                                                        fs.unlinkSync(batchPath + '/' + files[i]);
                                                        i++;
                                                        move();
                                                    });
                                            }
                                        }
                                    }
                                });

                                // }
                            }
                        });
                    }
                });
            });
        }else{
            var filePath = "data/files/" +username+"/files/"+ sourceField;
            var targetPath='';
            if(firstClass != '地图'){
                var files = fs.readdirSync('data/files/'+username+'/QR_code/')[0];
                targetPath = sourceField + '_' + sourceDir+'/'+files;
                fs.mkdir('data/QR_code/'+sourceField + '_' + sourceDir+'/',function(err){
                    if(err){
                        res.send({
                            status: -1,
                            message: err + '\n1549line'
                        })
                    }else{
                        fs.rename('data/files/'+username+'/QR_code/'+files,'data/QR_code/'+sourceField + '_' + sourceDir+'/'+files,function(err){
                            if(err){
                                res.send({
                                    status: -1,
                                    message: err + '\n1780line'
                                })
                            }
                        });
                    }
                });
            }
            fs.rename(filePath,'data/files/' +username+'/files/'+username+'_batch'+batchNum, function (err) {
                if (err) {
                    res.send({
                        status:-1,
                        message:err+'\n1314line'
                    });
                }
                var batchPath='data/files/' +username+'/files/'+username+'_batch'+batchNum;
                var files = fs.readdirSync(batchPath);          // 同步读取 该文件夹下的所有文件（同步读取是指该条执行执行完才可以往下执行！）
                // var date = new Date();
                // var y = date.getFullYear();
                // var m = date.getMonth() + 1;
                // var d = date.getDate();
                // var h = date.getHours();
                // var mi = date.getMinutes();
                // var time = y + '-' + toDou(m) + '-' + toDou(d) + '-' + toDou(h) + '-' + toDou(mi);
                // var randomNum=+parseInt(Math.random()*1000);
                fs.exists('data/backup/'+sourceField,function(exi){
                    if(exi){
                        fs.mkdir('data/backup/'+sourceField+'/'+sourceField+'_'+sourceDir,function(err){
                            if(err){
                                res.send({
                                    status:-1,
                                    message:err+'\n913lin'
                                })
                            }else{
                                fs.writeFile('data/backup/' + sourceField+'/'+sourceField+'_'+sourceDir+'_bin',sendTitle,{flag:'w',encoding:'utf-8',mode:'0666'},function(err) {
                                    if (err) {
                                        res.send({
                                            status:-1,
                                            message:err+'\n1281line'
                                        })
                                    } else {
                                        console.log("files配置文件写入成功");
                                    }
                                });
                                var i = 0;
                                move();
                                function move() {
                                    if (i === files.length) {
                                        var backupFilePath = 'data/backup/'+sourceField+'/' +sourceField+'_'+sourceDir;
                                        fs.rmdirSync(batchPath);
                                        if(firstClass == '地图'){
                                            exe(["-jar", "lib/jwcloud.jar","uploadMap",backupFilePath,'data/backup/' + sourceField+'/'+sourceField+'_'+sourceDir+'_bin',targetPath]);
                                        }else{
                                            exe(["-jar", "lib/jwcloud.jar","upload",backupFilePath,'data/backup/' + sourceField+'/'+sourceField+'_'+sourceDir+'_bin',targetPath]);
                                        }
                                    } else {
                                        var source = fs.createReadStream(batchPath + '/' + files[i]);  // 读取上传文件该批次存储地方的文件
                                        var dest = fs.createWriteStream('data/backup/' +sourceField+'/' +sourceField+'_'+sourceDir+'/' + files[i]);  //写入创建的文件夹
                                        source.pipe(dest);   // 先执行读取 读取出来的数据用作 写入
                                        source.on('end',
                                            function () {
                                                fs.unlinkSync(batchPath + '/' + files[i]);
                                                i++;
                                                move();
                                            });
                                    }
                                }
                            }
                        });
                    }else{
                        fs.mkdir('data/backup/' + sourceField, function (err) {
                            if (err) {
                                res.send({
                                    status:-1,
                                    message:err+'\n957line'
                                });
                            } else {
                                // for(var i=0;i<files.length;i++){
                                fs.mkdir('data/backup/'+sourceField+'/'+sourceField+'_'+sourceDir,function(err){
                                    if(err){
                                        res.send({
                                            status:-1,
                                            message:err+'\n965line'
                                        })
                                    }else{
                                        fs.writeFile('data/backup/' + sourceField+'/'+sourceField+'_'+sourceDir+'_bin',sendTitle,{flag:'w',encoding:'utf-8',mode:'0666'},function(err) {
                                            if (err) {
                                                res.send({
                                                    status:-1,
                                                    message:err+'\n1372line'
                                                })
                                            } else {
                                                console.log("files配置文件写入成功");
                                            }
                                        });
                                        var i = 0;
                                        move();
                                        function move() {
                                            if (i === files.length) {
                                                var backupFilePath = 'data/backup/'+sourceField+'/' +sourceField+'_'+sourceDir;
                                                fs.rmdirSync(batchPath);
                                                if(firstClass == '地图'){
                                                    exe(["-jar", "lib/jwcloud.jar","uploadMap",backupFilePath,'data/backup/' + sourceField+'/'+sourceField+'_'+sourceDir+'_bin',targetPath]);
                                                }else{
                                                    exe(["-jar", "lib/jwcloud.jar","upload",backupFilePath,'data/backup/' + sourceField+'/'+sourceField+'_'+sourceDir+'_bin',targetPath]);
                                                }
                                            } else {
                                                var source = fs.createReadStream(batchPath + '/' + files[i]);  // 读取上传文件该批次存储地方的文件
                                                var dest = fs.createWriteStream('data/backup/' +sourceField+'/' +sourceField+'_'+sourceDir+'/' + files[i]);  //写入创建的文件夹
                                                source.pipe(dest);   // 先执行读取 读取出来的数据用作 写入
                                                source.on('end',
                                                    function () {
                                                    console.log('移动完毕');
                                                        fs.unlinkSync(batchPath + '/' + files[i]);
                                                        i++;
                                                        move();
                                                    });
                                            }
                                        }
                                    }
                                });
                                // }
                            }
                        });
                    }
                });
            });
        }

            function exe(command) {
            var cmd = spawn('java',command);
            cmd.stdout.setEncoding('ASCII');
            cmd.stdout.on('data', function (data) {
                data = data.replace(/\s/g, '');
                if(data=='false'){
                    return res.send({
                        status: -1,
                        message: '上传的数据格式不符，请核对后重新上传',
                        count: sendFilecount
                    });
                }else{
                    console.log('-------------------------------');
                    console.log('exec', command);
                    console.log('stdout:' + data);

                    console.log('stdout');
                    console.log('data='+data);
                    if(!data){

                    }
                    else if (data.indexOf('success') != -1 ) {
                        var wordArr=data.match(/(success)/g);
                        sendFilecount[username]['batch'+batchNum]=sendFilecount[username]['batch'+batchNum]+wordArr.length;
                    }
                }
            });
            cmd.stderr.on('data', function (data) {
                console.log('-------------------------------');
                console.log('stderr:' + data);
                console.log('-------------------------------');
                console.log('stderr');
                console.log('data'+data);
                return;
            });
            cmd.on('exit', function (code) {
                console.log('exit');
                if (code === 0) {
                    res.send({
                        status: 0,
                        message: '建立索引成功',
                        count: sendFilecount
                    });
                } else {
                    res.send({
                        status: -1,
                        message: '建立索引失败，请再次点击“确定”按钮，重新建立索引'
                    })
                }
                console.log('exited with code:' + code);
                console.log('-------------------------------');
            });
        }
            function toDou(n) {
                return n < 10 ? '0' + n : '' + n
            }

    }
});

router.post('/unzipFile',function(req,res){
    console.log('unzipfile链接');
    var sourceField=req.body.source;
    var batchNum=req.body.batchNum;
    var sourceDir=req.body.sourceDir;
    var Cookies={};
    req.headers.cookie && req.headers.cookie.split(';').forEach(function(Cookie){
        var parts = Cookie.split('=');
        Cookies[ parts[0].trim() ] = ( parts[1] || '').trim();
    });
    var username=Cookies.login_user;
    if(!!username) {
        console.log('更名成功开始读取文件');
        var backupFilePath = 'data/backup/' + sourceField + '/' + sourceField + '_' + sourceDir;
        var files = fs.readdirSync(backupFilePath);          // 同步读取 该文件夹下的所有文件（同步读取是指该条执行执行完才可以往下执行！）
        var format = files[0].substring(files[0].lastIndexOf('.') + 1, files[0].length);

        if (format == 'html') {
            // exe(["-jar", "lib/jwcloud-upload-html.jar", backupFilePath, backupFilePath + '_bin']);
            res.send({
                status: 0,
                message: 'html文件'
            })
        } else if (format == 'zip') {
            console.log('进入解压开始解压');
            var j = 0;
            function unzip(command){
                console.log(command);
                var cmd = spawn('unzip',command);
                cmd.stdout.setEncoding('utf-8');
                cmd.stdout.on('data', function (data) {
                    data = data.replace(/\s/g, '');
                    if(data=='false'){
                        return res.send({
                            status: -1,
                            message: '上传的数据格式不符，请核对后重新上传',
                            count: sendFilecount
                        });
                    }else{
                        // console.log('stdout');
                        // console.log('data='+data);
                    }
                });
                cmd.stderr.on('data', function (data) {
                    console.log('-------------------------------');
                    console.log('stderr:' + data);
                    console.log('-------------------------------');
                    console.log('stderr');
                    console.log('data'+data);
                    return;
                });
                cmd.on('exit', function (code) {
                    console.log('exit');
                    if (code === 0) {
                        fs.exists(backupFilePath + "/" + files[j],function(e){
                            if(!!e){
                                fs.unlinkSync(backupFilePath + "/" + files[j]);
                                j++;
                                unzipFile();
                            }
                        });

                    } else {

                    }
                    console.log('exited with code:' + code);
                    console.log('-------------------------------');
                });
            }
            unzipFile();
            function unzipFile(){
                console.log('进入函数');
                console.log(j,files.length);
                if (j >= files.length) {
                    console.log('发送返回数据');
                    return res.send({
                        status: 0,
                        message: '解压完成'
                    })
                } else {
                    unzip(["-oO","CP936",backupFilePath + "/" + files[j],"-d",backupFilePath + "/"]);

                }
            }
        }
    }
});

router.post('/toEs',function(req,res){
    var sourceField=req.body.source;
    var batchNum=req.body.batchNum;
    var sourceDir=req.body.sourceDir;
    var Cookies={};
    req.headers.cookie && req.headers.cookie.split(';').forEach(function(Cookie){
        var parts = Cookie.split('=');
        Cookies[ parts[0].trim() ] = ( parts[1] || '').trim();
    });
    var username=Cookies.login_user;
    if(!!username) {
        var backupFilePath = 'data/backup/' + sourceField + '/' + sourceField + '_' + sourceDir;
        var files = fs.readdirSync(backupFilePath);          // 同步读取 该文件夹下的所有文件（同步读取是指该条执行执行完才可以往下执行！）
        var format = files[0].substring(files[0].lastIndexOf('.') + 1, files[0].length);

        exe(["-jar", "lib/jwcloud.jar", "uploadHtml",backupFilePath, backupFilePath + '_bin']);

        function exe(command) {
            var cmd = spawn('java', command);
            cmd.stdout.setEncoding('ASCII');
            cmd.stdout.on('data', function (data) {
                data = data.replace(/\s/g, '');
                if (data == 'false') {
                    return res.send({
                        status: -1,
                        message: '上传的数据格式不符，请核对后重新上传',
                        count: sendFilecount
                    });
                } else {
                    console.log('-------------------------------');
                    console.log('exec', command);
                    console.log('stdout:' + data);

                    console.log('stdout');
                    console.log('data=' + data);
                    console.log('-------------------------------');
                    if (!data) {
                        sendFilecount[username]['batch' + batchNum]++;
                    }
                    else if (data.indexOf('success') != -1) {
                        var wordArr = data.match(/(success)/g);
                        sendFilecount[username]['batch' + batchNum] = sendFilecount[username]['batch' + batchNum] + wordArr.length;
                    }
                }

            });
            cmd.stderr.on('data', function (data) {
                console.log('-------------------------------');
                console.log('stderr:' + data);
                console.log('-------------------------------');
                console.log('stderr');
                console.log('data' + data);
                return;
            });
            cmd.on('exit', function (code) {
                console.log('进入exit');
                if (code === 0) {
                    console.log('返回状态');
                    console.log('sendfilescount='+JSON.stringify(sendFilecount));
                    return res.send({
                        status: 0,
                        message: '建立索引成功',
                        count: sendFilecount
                    });
                } else {
                    res.send({
                        status: -1,
                        message: '建立索引失败，请再次点击“确定”按钮，重新建立索引'
                    })
                }
                console.log('exited with code:' + code);
                console.log('-------------------------------');
            });
        }
    }
});
router.post('/uploader/getSendAllCount',function(req,res){
    var sourceField = req.body.source;
    var batchNum = req.body.batchNum;
    var getType = req.body.getType;
    var Cookies={};
    console.log('进入---getSendAllCount');
    req.headers.cookie && req.headers.cookie.split(';').forEach(function(Cookie){
        var parts = Cookie.split('=');
        Cookies[ parts[0].trim() ] = ( parts[1] || '' ).trim();
    });
    var username=Cookies.login_user;
    if(!!username) {
            var filePath='';
            console.log(username,'batch'+batchNum);
            console.log(JSON.stringify(sendFilecount));
            sendFilecount[username]['batch'+batchNum]=0;
            if(getType == 'forms'){
                console.log('forms');
                filePath = "data/files/"+username+"/forms/" + sourceField;
            }else{
                filePath = "data/files/"+username+"/files/" + sourceField;
            }
            var files = fs.readdirSync(filePath);
            allCount[username]['batch'+batchNum]=files.length;
            console.log(allCount);
            res.send({
                status: 0,
                all: allCount[username]
            })
    }else{
        res.send({
            status: -1,
            message: '您没有登陆'
        })
    }
});
router.post('/uploader/getSendCount',function (req,res) {
    var Cookies={};
    req.headers.cookie && req.headers.cookie.split(';').forEach(function(Cookie){
        var parts = Cookie.split('=');
        Cookies[ parts[0].trim() ] = ( parts[1] || '' ).trim();
    });
    var username=Cookies.login_user;
    if(!!username){
        res.send({
            status:0,
            count:sendFilecount[username]
        })
    }
});

/* vinnie start */
// 数据源管理
router.get('/sourceManage',function(req,res){
    res.render('admin/sourceManage',{newIp:newIp,ip:ip,lastIp:lastIp});
});
//数据源管理
router.post('/sourceEdit',function(req,res){
    var sourceName = req.body.sourceName;
    var action = req.body.action;
    var className=req.body.className;
    var oldField = req.body.oldField;
    var newField = req.body.newField;
    var binData = req.body.binData;
    console.log( "===============" );
    console.log( sourceName );
    console.log( className);
    console.log(oldField);
    console.log(newField);
    console.log(binData);
    console.log( "===============" );

    if ( action == 'sourceEdit' ) { // 修改字段
        var path = 'data/backup/' + sourceName + "/";
        var updateBinInfo = className + '\n' + sourceName + '\n' + binData;
        var binUpdatePath = path + "binUpdate";
        fs.writeFile(binUpdatePath, updateBinInfo, {flag: 'w', encoding: 'utf-8', mode: '0666'}, function (err) {
            function exe(command) {
                var cmd = spawn('java', command);
                cmd.stdout.setEncoding('ASCII');
                cmd.stdout.on('data', function (data) {
                    console.log('-------------------------------');
                    console.log('exec', command);
                    console.log('stdout:' + data);
                    data = data.replace(/\s/g, '');
                    console.log('stdout');
                });
                cmd.stderr.on('data', function (data) {
                    console.log('-------------------------------');
                    console.log('stderr:' + data);
                    console.log('-------------------------------');
                    console.log('stderr');
                });
                cmd.on('exit', function (code) {
                    console.log(code);
                    if (code == 0) {
                    } else {
                        // res.send({
                        //     status: -1,
                        //     message: '建立索引失败，请再次点击“确定”按钮，重新建立索引'
                        // })
                    }
                    console.log('exited with code:' + code);
                    console.log('-------------------------------');
                });
            }
            if (err) {
                console.log(err);
                console.log("配置文件写入失败" + binUpdatePath);
            } else {
                if (className == '地图地图') {
                    exe(["-jar", "lib/jwcloud.jar", 'updateMap', binUpdatePath]);
                } else {
                    exe(["-jar", "lib/jwcloud.jar", 'update', binUpdatePath]);
                }
                res.send({
                    status: 0,
                    message: '建立索引成功'
                });
            }
        });
    }
});

router.post('/delSource',function(req,res){
    var sourceName = req.body.sourceName;
    var className = req.body.className;
    var Cookies = {};
    req.headers.cookie && req.headers.cookie.split(';').forEach(function(Cookie){
        var parts = Cookie.split('=');
        Cookies[ parts[0].trim() ] = ( parts[1] || '' ).trim();
    });
    var username=Cookies.login_user;
    if(!!username){
        var rootFile ='data/backup/'+sourceName;
        var QrCodefiles= fs.readdirSync('data/QR_code');
        var delQrCodeArr=[];
        for(var i=0;i<QrCodefiles.length;i++){
            var name=QrCodefiles[i].split('_')[0];
            if(name == sourceName){
                delQrCodeArr.push(QrCodefiles[i]);
            }
        }

        // var emptyDir = function(fileUrl){
        //     var files = fs.readdirSync(fileUrl);
        //     files.forEach(function(file){
        //         var stats=fs.statSync(fileUrl+'/'+file);
        //         if(stats.isDirectory()){
        //             emptyDir(fileUrl+'/'+file);
        //         }else{
        //             fs.unlinkSync(fileUrl+'/'+file);
        //             console.log('删除文件'+fileUrl+file+'成功');
        //         }
        //     });
        // };
        // var rmEmptyDir = function (fileUrl){
        //     var files=fs.readdirSync(fileUrl);
        //     if(files.length>0){
        //         var tempFile = 0;
        //         files.forEach(function(fileName){
        //             tempFile++;
        //             rmEmptyDir(fileUrl+'/'+fileName);
        //         });
        //         if(tempFile == files.length){
        //             fs.rmdirSync(fileUrl);
        //             console.log('删除文件夹'+fileUrl+'成功');
        //         }
        //     }else{
        //         fs.rmdirSync(fileUrl);
        //         console.log('删除文件夹'+fileUrl+'成功');
        //     }
        // };
        // emptyDir(rootFile);
        // rmEmptyDir(rootFile);
        var deleteFolderRecursive = function(url) {
            var files = [];
            //判断给定的路径是否存在
            if( fs.existsSync(url) ) {
                //返回文件和子目录的数组
                files = fs.readdirSync(url);
                files.forEach(function(file,index){
                    // var curPath = url + "/" + file;
                    var curPath = path.join(url,file);
                    //fs.statSync同步读取文件夹文件，如果是文件夹，在重复触发函数
                    if(fs.statSync(curPath).isDirectory()) { // recurse
                        deleteFolderRecursive(curPath);
                        // 是文件delete file
                    } else {
                        fs.unlinkSync(curPath);
                        console.log('删除文件'+curPath+file+'成功')
                    }
                });
                //清除文件夹
                fs.rmdirSync(url);
            }else{
                console.log("给定的路径不存在，请给出正确的路径");
            }
        };
        if(delQrCodeArr.length>0){
            for(var i=0;i<delQrCodeArr.length;i++){
                deleteFolderRecursive('data/QR_code/'+delQrCodeArr[i]);
            }
        }

        deleteFolderRecursive(rootFile);
        exe(["-jar", "lib/jwcloud.jar",'delete',className,sourceName]);
        return res.send({
            status:0
        });
    }
    function exe(command) {
        var cmd = spawn('java',command);
        cmd.stdout.on('data', function (data) {
        });
        cmd.stderr.on('data', function (data) {
        });
        cmd.on('exit', function (code) {
            if(code==0){
                console.log('删除成功');
            }
        });
    }
});
/* vinnie end */

router.post('/leadInto',function(req,res){
    fs.readFile('data/forms/iPhone 5C_358539052356958_2017_04_28_16_12_19/iPhone 5C_358539052356958_2017_04_28_16_12_19_Report/Main27.html', function (err, data) {   // 读取该文件内容
        if (err) {
            res.send({
                status:-1,
                message:'读取文件失败307line'
            });
        }else {
            var buffer = data;
            var str = iconv.decode(buffer, 'UTF-8');//  使用 iconv.decode解析buffer格式的数据
            res.send({
                status:0,
                data:str
            });
        }
    })
});


/*
* 文件管理器插件接口
* */









module.exports = router;
