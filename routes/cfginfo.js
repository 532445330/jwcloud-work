var path = require('path');
var parserString = require('xml2js').parseString;
var fs = require('fs');
function cfginfo(filepath){
  this.rootpath = filepath;
  this.s_authfile = path.join(this.rootpath , '/data/serverdata/auth_code.xml');
  this.s_fctrfile = path.join(this.rootpath , "/data/serverdata/auth_factor.xml");
}

cfginfo.prototype.getVendorID = function(){
  if (typeof this.s_vendorID !== 'undefined' && this.s_vendorID !== null)
    return this.s_vendorID;
  if (!fs.existsSync(this.s_authfile)) {
    // console.log('Not find auth file on:'+this.s_authfile);
    throw new Error('Authentication file not exist');
  }
  var data = fs.readFileSync(this.s_authfile,'utf8');
  var authdata = null;
  parserString(data,function(err,data){
    authdata = data.dogauth;
  });
  // console.log("to json ->", authdata.vendor);
  var vendorTag = authdata.vendor;
  for (var i = 0, len = vendorTag.length; i < len; i++) {
    // console.log('Tag '+i+' is');
    console.dir(vendorTag[0]);
    // console.dir(vendorTag[0].$);
    this.vendorTag = vendorTag[0].$.id;
    // console.dir(this.vendorTag);
    return this.vendorTag;
  }
};
cfginfo.prototype.getAuthCode = function(){
  if (typeof this.s_authcode !== 'undefined' && this.s_authcode !== null){
    // console.log('authcode not empty.it is:'+this.s_authcode);
    return this.s_authcode;
  }
    if (!fs.existsSync(this.s_authfile)) {
        // console.log('Not find auth file on:'+this.s_authfile);
      throw new Error('Authentication file not exist');
    }
    var data = fs.readFileSync(this.s_authfile,'utf8');
    // console.log('auth file data:'+data);
    var authdata = null;
    parserString(data,function(err,data){
      authdata = data.dogauth;
    });
    // console.log('authdata:'+authdata);
    // console.dir(authdata);
    var authcode = authdata.authcode;
    for (var i = 0, len = authcode.length; i < len; i++) {
      this.s_authcode = authcode[i];
      return this.s_authcode;
    }
};
//need to fix.
cfginfo.prototype.getAuthFactor = function(){
  if (typeof this.s_authFactor !== 'undefined' && this.s_authFactor !== null)
    return this.s_authFactor;
    if (!fs.existsSync(this.s_fctrfile)) {
      throw new Error('Authentication factor file not exist');
    }
    var data = fs.readFileSync(this.s_authfile,'utf8');
    var authdata = null;
    parserString(data,function(err,data){
      authdata = data.dogauth;
    });
    for (var i = 0, len = authdata.length; i < len; i++) {
      var acodeTag = authdata[i].factor;
      this.s_authcode = acodeTag[0];
      if('' == this.s_authFactor)
      {
        this.s_authFactor = "00000000";
      }
      return this.s_authcode;
    }
};

module.exports = cfginfo;
