var ip=ipNum;
var lastIp=lastIpNum;
var newIp=newIpNum;
var loginName=decodeURIComponent($.cookie('login_user'));
var token=$.cookie('login_token');
var admin=$.cookie('admin');
var dogId=$.cookie('dogId');
var sourceTeamList;
var sourceUserList;
var submitTeamList;
var submitUserList;
var sourceState=0;
var batch=0;
var secondClass;
var appointTime='';         //指定时间字段
var newSourceB=0;
var limitNewSource=false;  //添加数据源限制;
var timer;
var timer2;
var addTitle=1;
$.ajax({
    url:'http://'+lastIp+'/index.php',
    type:'POST',
    async: false,
    data:JSON.stringify({
        interface:'getType',
        act:'getAll',
        Token:token,
        username:loginName
    }),
    success:function(dataStr){
        var data =eval('('+dataStr+')');
        if(data.status==0){
            secondClass=data.Item;
            var second=secondClass[4].son;
            for(var i=0;i<second.length;i++){
                var oP='<p data="typeId'+second[i].typeId+'">'+second[i].typeName+'</p>';
                $('.m-search-second-class-option .slipe-box').append(oP);
            }
        }
    },
    error:function(err){
        console.log(err);
    }
});

//二级、数据源联动
$('.search-second-class-option').on('click','.slipe-box p',function(){
    $('.uploader-source .search-select-box').hide();
    $('#source-value').text('选择上传的目标数据源');
    $('.search-select-value i').css('backgroundImage', 'url("../images/icon_search_select_dropdown.png")');
    $('.uploader-source .search-select-title').removeClass('border-b0');
    $('.search-select-option .slipe-box').html('');
    $.ajax({
        url: 'http://' + lastIp + '/index.php',
        type: 'POST',
        data:JSON.stringify({
            interface:'getSource',
            act:'getSourceType',
            typeId:$(this).attr('data').replace(/typeId/g,''),
            Token:token,
            username:loginName
        }),
        success: function (dataStr) {
            var dataObj = eval('(' + dataStr + ')');
            if(dataObj.status==0){
                var data = dataObj.Item;
                if(data.length!=0){
                    $('.search-source-cover').hide();
                    //data=['楼盘备案','物流快递','购房登记','出行记录','通讯记录','人员信息'];
                    appendAllSource();
                    function appendAllSource() {
                        for (var i in data) {
                            $('#search-select-allsource .slipe-box').append('<p data="'+data[i].sourceId+'" title="'+data[i].sourceName+'">' + data[i].sourceName + '</p>');
                        }
                    }
                }else{
                    $('.search-source-cover').show();
                }
            }else{
                $('.search-source-cover').show();
            }
        },
        error: function (err) {
            console.log(err);
        }
    });
});

$('.search-second-class-option').on('click','.slipe-box p',function(){
    var val = $(this).text();
    var data=$(this).attr('data');
    $('.search-second-class-value span').text(val).attr({data:data});
});

//下拉框选择效果
$('.search-select-option .slipe-box').on('click', 'p', function () {
    var val = $(this).text();
    $('.search-select-value span').text(val).attr({title: val});
    sourceState = -1;
});

//新建数据源弹窗弹出
$('.m-uploader-create-source-btn').on('click',function(){
    if(limitNewSource)return;
    $('#new-source-txt',parent.document).val('');
    $('.new-creatsource-box',parent.document).show();
    $('.layer-bg',parent.document).show();
});
//新建数据源弹窗关闭
$(' .box-off,.new-creat-concel',parent.document).on('click', function () {
    $('#new-source-txt',parent.document).val('');
    $('.layer-layout',parent.document).hide();
    $('.layer-bg',parent.document).hide();
});
//重新创建按钮
$('.recreat-confirm',parent.document).on('click', function () {
    $('.hints-layer',parent.document).hide();
    $('.new-creatsource-box',parent.document).show();
});
//新建数据源按钮
$('.new-creat-confirm',parent.document).on('click', function () {
    if(limitNewSource)return;
    if($('.manage-nav li.on',parent.document).text()=='上传取证报表') {
        var newSource = $('#new-source-txt', parent.document).val();
        var reg = new RegExp('^[0-9a-z\u4E00-\u9FA5-_\(\)]+$');
        if (newSource.length == 0) {
            parent.window.setLayer(false, '请输入新建数据源名称', true);
        }
        else if (newSource.length >= 80) {
            parent.window.setLayer(false, '数据源名称长度超过上限', true);
        }
        else if (!reg.test(newSource)) {
            alert('数据源名称格式不正确');
        }
        else {
            $('#new-source-txt').val('');
            $('.new-creatsource-box', parent.document).hide();
            $.ajax({
                url: 'http://' + newIp + '/jwcloud/source/findSource',
                type: 'POST',
                sync: 'false',
                data:{
                    dogId:dogId
                },
                xhrFields:{
                    withCredentials:true
                },
                success: function (data, status) {
                    if(data.status == 0){
                        var dataArr = data.dataSource;
                        for (var i = 0; i < dataArr.length; i++) {
                            if (newSource == dataArr[i]) {
                                parent.window.setLayer(false, '您创建的数据源已经存在，请重新创建或者取消', true);
                                return;
                            }
                        }
                        limitNewSource = true;
                        $('#m-source-value').text(newSource).attr('title', newSource);
                        // $('#search-select-allsource .slipe-box').prepend('<p title="'+newSource+'">' + newSource + '</p>');

                        parent.window.setLayer(true, '数据源创建完成', false);
                        $('.search-source-cover').hide();
                        setTimeout(function () {
                            $('.hints-layer', parent.document).fadeOut();
                            $('.layer-bg', parent.document).fadeOut();
                        }, 1000);
                    }else{

                    }


                    // $.ajax({         //或得用户组
                    //     url:'http://'+newIp+'/jwcloud-interface/Jwcloud?interface=allUserAndAllUserGroup&loginName='+loginName+'&token='+token,
                    //     type:'POST',
                    //     success:function(dataStr){
                    //         var data=eval('('+dataStr+')');
                    //         if(data.status==0){
                    //             sourceTeamList=data.teamList;
                    //             sourceUserList=data.userList;
                    //             sourceState=0;
                    //             eachTeamList();
                    //             eachUserList();
                    //         }else{
                    //             layer(false,'您还没有登录，请点击“确定”返回登录页');
                    //         }
                    //     }
                    // });
                },
                error: function (err) {
                    console.log(err);
                }
            });
        }
    }
});
//下拉框展示隐藏效果
$('.search-select').on('click', function (e) {
    e.stopPropagation();
    var box = $(this).find('.search-select-box');
    if (box.css('display') == 'none') {
        $('.search-select .search-select-box').hide();
        $('.search-select-title').removeClass('border-b0');
        $(this).addClass('border-b0');
        box.show();
        //清除搜索选中样式
//                var optionList=$(this).find('.search-select-option .slipe-box p');
//                for(var j=0;j<optionList.length;j++){
//                    optionList.eq(j).removeClass('select-blue-p');
//                }
        $('.search-select p i').css('backgroundImage', 'url("../images/icon_search_select_dropdown.png")');
        $(this).find('.search-select-value').find('i').css('backgroundImage', 'url("../images/icon_search_select_putaway.png")');
        $(this).find('.search-class-value').find('i').css('backgroundImage', 'url("../images/icon_search_select_putaway.png")');
        $(this).find('.search-second-class-value').find('i').css('backgroundImage', 'url("../images/icon_search_select_putaway.png")');

    } else {
        $(this).removeClass('border-b0');
        $('.search-select p i').css('backgroundImage', 'url("../images/icon_search_select_dropdown.png")');
        box.hide();
        $(this).find('.search-select-value').find('i').css('backgroundImage', 'url("../images/icon_search_select_dropdown.png")');
        $(this).find('.search-class-value').find('i').css('backgroundImage', 'url("../images/icon_search_select_dropdown.png")');
        $(this).find('.search-second-class-value').find('i').css('backgroundImage', 'url("../images/icon_search_select_dropdown.png")');
    }
});

//搜索数据源列表
$('.search-source-input img').on('click', function () {
    var optionList = $(this).parent().parent().find('.search-select-option .slipe-box p');
    for (var j = 0; j < optionList.length; j++) {
        optionList.eq(j).removeClass('select-blue-p');
    }
    if (!!$(this).prev().val()) {
        var key = $(this).prev().val();
        for (var i = 0; i < optionList.length; i++) {

            if (optionList.eq(i).text().indexOf(key) !== -1) {
                optionList.eq(i).addClass('select-blue-p');
                var oParent = optionList.eq(i).parent().parent();
                oParent.scrollTop(36 * i - 50);
                return;
            }
        }
    }
});
$(document).click(function () {
    $('.search-select-box').hide();
    $('.search-select').removeClass('border-b0');
    $('.uploader-class-box').find('i').css('backgroundImage', 'url("../images/icon_search_select_dropdown.png")');
    $('.uploader-source').find('i').css('backgroundImage', 'url("../images/icon_search_select_dropdown.png")');
    $('.search-grade-list').hide();
});

//上传插件
//上传插件代码
var uploader = WebUploader.create({
    // swf文件路径
    swf: '../javascripts/Uploader.swf',
    // 文件接收服务端。
    server: './uploaderfile',
    method: 'POST',
    // 选择文件的按钮。可选。
    // 内部根据当前运行是创建，可能是input元素，也可能是flash.
    pick: '#picker',
    auto: true,
    formData: {
        source: $('#m-source-value').text().replace(/[,.\s]/g,''),
        filesType:'forms'
    },
    // chunked:true,
    // threads: 3,
    // chunkSize: 2048000,
    // 不压缩image, 默认如果是jpeg，文件上传前会压缩一把再上传！
    resize: false
    // accept: {
    //     title: 'xls and csv',
    //     extensions: 'xlsx,xls,csv',
    //     mimeType: 'application/excel,text/comma-separated-values'
    // }
});
// 文件上传进度
uploader.on('uploadProgress', function (file, percentage) {
    parent.window.setCreatLinkHints('no', '添加文件'+parseInt(percentage*100)+'%');
});
//文件成功失败处理
uploader.on('uploadSuccess', function (file) {
    $('#' + file.id).find('p.state').text('添加成功，等待上传');
});
uploader.on('uploadError', function (file) {
    $('#' + file.id).find('p.state').text('添加出错，请重新添加');

});
uploader.on('uploadFinished', function () {
    //     上传完判断文件格式是否一致
    var filesArr = [];
    var files = uploader.getFiles();
    for (var j = 0; j < files.length; j++) {
        filesArr.push(files[j].name);
    }

    var fileJson ={};
    fileJson.filesArr =filesArr;
    $.post('./uploader/compareFile', {source: $('#m-source-value').text().replace(/[,.\s]/g,''), delFile: JSON.stringify(fileJson),filesType:'forms'}, function (data) {
        if (data.status === 0) {
            //如果一致  进入概览 ，弹出上传成功
           $('.uploader-btn').hide();
           $('.uploader-forms-body').show();
           $('.uploader-files-count span').text(data.filesCount);
           $('.layer-bg',parent.document).hide();
           $('.creat-link',parent.document).hide();
           $('#picker').css({
               top:$('.uploader-view-continue').position().top,
               left:$('.uploader-view-continue').position().left
           })
        } else {
            parent.window.setLayer(false, data.message, false);
            // 插件重置
            //不一致加以提示
        }
    });
    uploader.reset();

});
//上传按钮 限制
$('#ctlBtn').on('click', function () {
    uploader.options.formData.source = $('#m-source-value').text().replace(/[,.\s]/g,'');
    uploader.upload();
    $('#source').css({'color': '#ccc'}).attr('disabled', 'true');
});
$('#picker').click(function () {
    uploader.options.formData.source = $('#m-source-value').text().replace(/[,.\s]/g,'');
});
var oPicker = document.getElementById('picker');
oPicker.addEventListener('click', function (e) {
    if($('#m-source-second-class-value').text() == '选择上传的二级类目' ){
        e.preventDefault();
        parent.window.setLayer(false, '请选择上传信息的二级类目', false);
    }
    else if ($('#m-source-value').text() == '选择上传的目标数据源') {
        e.preventDefault();
        parent.window.setLayer(false, '请选择数据源', false);
    }
}, true);
//确认提交按钮
var limitClick=true;
$('.uploader-view-confirm').on('click',function(){
    if(!limitClick)return;
    limitClick=false;
    var reg = new RegExp('^[0-9a-z\u4E00-\u9FA5-_\(\)]+$');
    $('.uploader-view-confirm').css({cursor:'pointer'});
    if(reg.test($('#m-source-value').text())){
        var sourceName=$('#m-source-value').text().replace(/[,.\s]g/,'');
        var firstClass='取证';
        var firstId=5;
        var secondClass=$('#m-source-second-class-value').text();
        var secondId=$('#m-source-second-class-value').attr('data').replace(/typeId/g,'');
        var caseName=$('#case-name').val();
        var caseNum= $('#case-num').val();
        var suspectPhone=$('#suspect-phone').val();
        var suspectImei=$('#suspect-imei').val();
        var suspectImsi=$('#suspect-imsi').val();
        var checkedSourceId=$('.uplader-forms-source input:checked').attr('id');
        var checkedSource=$('.uplader-forms-source label[for='+checkedSourceId+']').text();
        if(checkedSource=='睿海'){
            checkedSource=0;
        }else{
            checkedSource=1;
        }
        if(!!secondClass){
            if(sourceName != '选择上传的目标数据源'){
                parent.window.batch++;
                limitNewSource=false;
                initUploaderView();

                $.ajax({
                    url:'./uploader/getSendAllCount',
                    data:{
                        source: sourceName,
                        batchNum:parent.window.batch,
                        getType:'forms'
                    },
                    type:'POST',
                    success:function (data) {
                        if (data.status == 0) {
                            parent.window.sendAllCount['batch'+parent.window.batch]=data.all['batch'+parent.window.batch];
                            //插入进度显示
                            $('.uploader-press-container',parent.document).show();
                            var oLi='<li class="clearfix fl batch'+parent.window.batch+'"><h2 class="fl">进程'+parent.window.batch+'</h2>'+
                                '<div class="fl press-bar-container"><div class="press-box">'+
                                '<span class="press-bar" data="'+parent.window.sendAllCount['batch'+parent.window.batch]+'"></span></div><p class="clearfix">'+
                                '<span class="fl press-source-name">'+ sourceName +'</span>'+
                                '<span class="fr press-rate">上传完成</span></p></div>'+
                                '<a href="javascript:void(0);" class="fr press-del" onclick="delPressBox(this)" >关闭</a> </li><span class="press-line fl"></span>';
                            window.onbeforeunload=function(){
                                return '';
                            };
                            $('.press-container ul',parent.document).append(oLi);
                            parent.window.checkPressNum();
                            if(parent.window.batch == 1){
                                parent.window.runSetInterval();
                            }
                            function toDou(n){
                                return n<10?'0'+n:''+n;
                            }
                            var date = new Date();
                            var y = date.getFullYear();
                            var m = date.getMonth() + 1;
                            var d = date.getDate();
                            var h = date.getHours();
                            var mi = date.getMinutes();
                            var time = y + '-' + toDou(m) + '-' + toDou(d) + '-' + toDou(h) + '-' + toDou(mi);
                            var randomNum=+parseInt(Math.random()*1000);
                            var fieldName=time+'_'+randomNum;       //储存文件夹名称
                            var batchNum=parent.window.batch;
                            $.ajax({
                                url: './uploader/sendFiles',
                                type: 'POST',
                                data: {
                                    source: sourceName,
                                    batchNum:batchNum,
                                    sendTitle:firstClass+secondClass+'-'+sourceName+'\n'+caseName+'\n'+caseNum+'\n'+suspectPhone+'\n'+suspectImei+'\n'+suspectImsi+'\n'+checkedSource,
                                    sourceDir:fieldName,
                                    sendType:'form'
                                },
                                success: function (data) {
                                    $('.batch'+batch+' .press-bar').css({
                                        width:100+'%'
                                    });
                                    // parent.window.checkPressNum();

                                    if (data.status === 0) {
                                        $.ajax({
                                            url: './unzipFile',
                                            type: 'POST',
                                            data: {
                                                source: sourceName,
                                                batchNum: batchNum,
                                                sourceDir: fieldName
                                            },
                                            success: function (data) {
                                                if (data.status === 0){
                                                    $.ajax({
                                                        url:'./toEs',
                                                        type:'POST',
                                                        data:{
                                                            source: sourceName,
                                                            batchNum: batchNum,
                                                            sourceDir: fieldName
                                                        },
                                                        success:function(data){
                                                            if(data.status===0){
                                                                $('.batch'+batch+' .press-bar',parent.document).css({
                                                                    width:100+'%'
                                                                });
                                                                parent.window.checkPressNum();
                                                                $.ajax({    //演示完需要解除注释
                                                                    url:'http://'+lastIp+'/index.php',
                                                                    type:'POST',
                                                                    data:JSON.stringify({
                                                                        interface:'getSource',
                                                                        act:'setSourceField',
                                                                        Token:token,
                                                                        username:loginName,
                                                                        firstItem:5,         //取证id是5
                                                                        secondItem:secondId,
                                                                        sourceName:sourceName,
                                                                        sourceTitle:[{"fieldName":"1","level":'1'}],
                                                                        right:{sourceName: sourceName,userRights: 'all'},
                                                                        sourceDir:sourceName+'_'+fieldName,
                                                                        addTitle:addTitle,
                                                                        time:appointTime
                                                                    }),
                                                                    success:function(dataStr){
                                                                        var data = eval('('+dataStr+')');
                                                                        if(data.status == 0){
                                                                            limitNewSource=false;
                                                                            parent.window.checkPressNum();
                                                                        }else{

                                                                        }
                                                                    },
                                                                    error:function(err){
                                                                        console.log(err);
                                                                    }
                                                                })
                                                            }else{
                                                                clearInterval(parent.window.timer2);
                                                                parent.window.setCreatLinkHints(false);
                                                            }


                                                        },
                                                        error:function(err){
                                                            console.log(err);
                                                        }
                                                    });

                                                }else{
                                                    clearInterval(parent.window.timer2);
                                                    parent.window.setCreatLinkHints(false);
                                                }
                                            },
                                            error: function (err) {
                                                console.log(err);
                                            }
                                        });

                                    } else {
                                        clearInterval(parent.window.timer2);
                                        parent.window.setCreatLinkHints(false);
                                    }
                                },
                                error: function (err) {
                                    console.log(err);
                                }
                            });

                        } else {

                        }
                    },
                    error:function(err){
                        console.log(err);
                    }
                });
            }else{
                limitClick=true;
                parent.window.setLayer(false, '请选择数据源', false);
            }
        }else{
            limitClick=true;
            parent.window.setLayer(false, '请选择上传类别', false)
        }
    }
});

function initUploaderView(){
    $('#m-source-value').text('选择上传的目标数据源');
    $('.uploader-files-count span').text('0');
    $('.uploader-forms-body').hide();
    $('.uploader-btn').show();
    limitClick=true;
    $('#case-name').val('');
    $('#case-num').val('');
    $('#suspect-phone').val('');
    $('#suspect-imei').val('');
    $('#suspect-imsi').val('');
    $('#picker').css({
        top:188,
        left:30
    });

}
