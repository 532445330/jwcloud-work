var ip=ipNum;
var newIp=newIpNum;
var admin=$.cookie('admin');
var loginName=decodeURIComponent($.cookie('login_user'));
var token=$.cookie('login_token');
function getKey(){
    if(!!location.href.split('?')[1]){
        var urlArr=location.href.split('?')[1].split('&');
        var urlJson={};
        for(var i=0;i<urlArr.length;i++){
            var arr=urlArr[i].split('=');
            urlJson[arr[0]]=decodeURIComponent(arr[1]);
        }
        return urlJson;
    }else{
        return;
    }
}
var key = getKey();
$(document).keyup(function(e){
    if(e.keyCode == 13){
        $('#res-search-btn').trigger('click');
    }
});
$('#res-search-btn').on('click',function(){
    if(!!$('#res-search-text').val()) {
        var skipUrl='/mixresult?wd='+ encodeURIComponent($('#res-search-text').val())+'&word='+(key.word?key.word:'')+'&unison='+(key.unison?key.unison:'')+'&mate'+(key.mate?key.mate:'')+'&listCount=20&currentPage=1';
        $.ajax({
            url: 'http://' + newIp + '/jwcloud-interface/Jwcloud?interface=userOperate&loginName=' + loginName + '&token=' + token,
            type: 'POST',
            data: {
                username: loginName,
                operate: "搜索关键词  '" + $('#res-search-text').val().trim().replace(/"/g,'\\"') + "'",
                operateState: 0,
                keyword:$('#res-search-text').val().trim().replace(/"/g,'\\"'),
                url:skipUrl
            },
            success: function (dataStr) {
                var data = eval('(' + dataStr + ')');
                if (data.status == 0) {
                    window.location.href=skipUrl;
                }
            },
            error: function (err) {
                console.log(err);
            }
        })
    }
});

$.ajax({
    url:'http://'+newIp+'/jwcloud-interface/Jwcloud?interface=getUserSearchRecord&loginName='+loginName+'&token='+token,
    type:'POST',
    data:{
        action:"getSearchRecord"
    },
    success:function(dataStr){
         var data=eval('('+dataStr+')');
         if(data.status==0){
             var historyList=data.historyList;
             for(var i=0;i<historyList.length;i++){
                 var oLi='<li><a href="'+historyList[i].url+'">搜索关键词"'+historyList[i].keyword+'"</a></li>';
                 $('.res-history-list ul').append(oLi);
             }
         }else{
             layer(false,'')
         }

    },
    error:function(err){
        console.log(err);
    }
});


$('.res-back').click(function(){
    window.location.href = '/search';
});

//用户登录信息
$('#yes_user').hover(function(){
    $('.login-info-box').show();
},function(){
    $('.login-info-box').hide();
});
$.support.cors = true;

$('#res-search-text').val(getKey().wd);
$(function(){

    if(!!location.href.split('?')[1]) {
        function checkChinese(){
            return /[\u4E00-\u9FA5]/img.test(key.wd);
        }
        getDataList();
        function getDataList(){
            var mate;
            if(checkChinese()){
                if(!key.mate){
                    mate='match_phrase';
                }else{
                    mate='match'
                }
            }else{
                if(!key.mate){
                    mate='term';
                }else{
                   mate='wildcard';
                }
            }
            var url;// 请求数据源列表
            var word;//传过来的字段
            if(!!key.word){
                if(admin==0){
                    url='http://'+newIp+'/jwcloud-interface/GetSpecifiedAliases?name='+key.word;
                }else{
                    url='http://'+newIp+'/jwcloud-interface/Jwcloud?interface=getSpecifiedAliases&loginName='+loginName+'&token='+token+'&name='+key.word+'&username='+loginName;
                }
                word=key.word;
            }else{
                if(admin==0){
                    url='http://'+newIp+'/jwcloud-interface/Jwcloud?interface=getAllDataSource&loginName='+loginName+'&token='+token;
                }else{
                    url='http://'+newIp+'/jwcloud-interface/Jwcloud?interface=getAllAliases&loginName='+loginName+'&token='+token+'&username'+loginName;
                }
                word='_all';
            }
            $.ajax({
                url: url,
                type: 'GET',
                success: function (dataStr) {
                    if(admin==0){
                        var data=eval(dataStr);
                        setList(data);
                    }else{
                        var data=eval('('+dataStr+')');
                        if(data.status==0){
                            var list=data.sourceList;
                            setList(list);
                        }else{
                            layer(false,'您还没有登录，请点击“确定”返回登录页');
                        }
                    }

                },
                error:function(err){
                    console.log(err);
                }
            });
            function setList(list){
                var count=0; // 数据源数量
                var orderNum=0;
                for(var n in list){
                    count+=1;
                }
                $('.so-search-process span:eq(0)').text(count); //显示数据源数量

                if(count===0){
                    $('.empty-data-hints').show();
                    $('.so-search-process span:eq(2)').text(0);
                    $('.so-search-process span:eq(3)').text(0);
                }else {
                    $('.so-search-process span:eq(2)').text(1);
                    $('.so-search-process span:eq(3)').text(count-1);
                }

                for(var dataList in list){
                    orderNum+=1;
                    if(admin==0){
                        var oBox='<li class="source-list-content clearfix"><a href="javascript:void(0);" id="skip-'+orderNum+'">'+
                            '<p class="source-list-order">'+(orderNum)+'</p>'+
                            '<p class="source-list-name">'+list[dataList]+'</p>'+
                            '<p class="source-list-process">等待加载</p>'+
                            '</a></li>';
                            $('#source-list-ul').append(oBox);
                    }else{
                        var rightsBox='';
                        if(list[dataList].rights == 0){
                            rightsBox=''
                        }
                        else if(list[dataList].rights == -1){
                            rightsBox='<div class="source-list-rights fl"><input type="button" value="申请权限" class="source-apply-rights" ></div>';
                        }
                        else if(list[dataList].rights == 1){
                            rightsBox='<div class="source-list-rights fl"><input type="button" value="审核中" class="source-checking"></div>';
                        }

                        var oBox='<li class="source-list-content clearfix"><a href="javascript:void(0);" id="skip-'+orderNum+'">'+
                            '<p class="source-list-order">'+(orderNum)+'</p>'+
                            '<p class="source-list-name">'+list[dataList].sourceName+'</p>'+
                            '<p class="source-list-process">等待加载</p>'+rightsBox+
                            '</a></li>';
                        $('#source-list-ul').append(oBox);
                    }

                    if(orderNum===count){
                        var aLi=$('#source-list-ul li');
                        var num=0;
                        var finishCount=0;
                        var waitingCount=count-1;

                        getSearchCount(); //得到每条搜索结果数
                        function getSearchCount(){
                            var data={};
                            var a={};
                            var b={};
                            var source=aLi.eq(num).find('.source-list-name').text();
                            var getDataNumUrl;// 请求每个数据源  匹配数 链接
                            if(!!key.word){
                                getDataNumUrl='http://'+ip+'/'+source+'/_count';
                            }else{
                                getDataNumUrl='http://'+ip+'/'+source+'/_count';
                            }

                            aLi.eq(num).find('.source-list-process').text('加载中...');
                            aLi.eq(num).addClass('source-list-loading');
                            if(checkChinese()){
                                b[word]=key.wd;
                            }else{
                                if(!key.mate){
                                    b[word]=key.wd;
                                }else{
                                    b[word]='*'+key.wd+'*';
                                }
                            }

                            a[mate]=b;
                            data.query=a;
                            // 每个请求 返回匹配数
                            $.ajax({
                                url:getDataNumUrl,
                                type:'POST',
                                data:JSON.stringify(data),
                                success:function (data) {
                                    aLi.eq(num).removeClass('source-list-loading');
                                    aLi.eq(num).addClass('source-list-complete');
                                    if(data.count === 0){
                                        aLi.eq(num).css({background:'#f8f8f8',borderColor:'#c8c8c8'}).find('a').css({color:'#a0a0a0'}).find('.source-list-process').html('加载完成，共<span>'+data.count+'</span>条结果');
                                        $('#skip-'+(num+1)+'').removeAttr('href');
                                    }else{
                                        if(admin==0){
                                            aLi.eq(num).find('.source-list-process').html('加载完成，共<span>'+data.count+'</span>条结果，点击查看>');
                                            var source=aLi.eq(num).find('.source-list-name').text();
                                            $('#skip-'+(num+1)+'').attr({
                                                href:'/result?wd='+key.wd+'&source='+source+'&word='+(key.word?key.word:"")+'&mate='+(key.mate?key.mate:"")+'&listCount=20&currentPage=1'
                                            })
                                        }else{
                                            if(list[num].rights == 0 ){
                                                aLi.eq(num).find('.source-list-process').html('加载完成，共<span>'+data.count+'</span>条结果，点击查看>');
                                                var source=aLi.eq(num).find('.source-list-name').text();
                                                $('#skip-'+(num+1)+'').attr({
                                                    href:'/result?wd='+key.wd+'&source='+source+'&word='+(key.word?key.word:"")+'&mate='+(key.mate?key.mate:"")+'&listCount=20&currentPage=1'
                                                })
                                            }else if(list[num].rights == -1){
                                                aLi.eq(num).css({background:'#f8f8f8',borderColor:'#c8c8c8'}).find('a').css({color:'#a0a0a0'}).find('.source-list-process').html('加载完成，共<span>'+data.count+'</span>条结果（无权限）>');
                                                aLi.eq(num).find('.source-list-rights .source-apply-rights').show();
                                                $('#skip-'+(num+1)+'').removeAttr('href');
                                                var source=aLi.eq(num).find('.source-list-name').text();
                                            }else if(list[num].rights == 1){
                                                aLi.eq(num).css({background:'#f8f8f8',borderColor:'#c8c8c8'}).find('a').css({color:'#a0a0a0'}).find('.source-list-process').html('加载完成，共<span>'+data.count+'</span>条结果（无权限）>');
                                                aLi.eq(num).find('.source-list-rights .source-checking').show();
                                                $('#skip-'+(num+1)+'').removeAttr('href');
                                                var source=aLi.eq(num).find('.source-list-name').text();
                                            }
                                        }

                                    }

                                    num+=1;// 每请求完一个  就请求下一个
                                    if(num==count){
                                        $('.so-search-process span:eq(2)').text(0);
                                        $('.so-search-process span:eq(3)').text(0);
                                        $('.so-search-process span:eq(1)').text(count);
                                        return;

                                    }else{
                                        getSearchCount();
                                        finishCount+=1;
                                        $('.so-search-process span:eq(1)').text(finishCount);
                                        $('.so-search-process span:eq(2)').text(0);
                                        waitingCount=count-1-finishCount;
                                        $('.so-search-process span:eq(3)').text(waitingCount);
                                    }
                                }
                            })
                        }
                    }
                }
            }

        }

    }
    $(document).on('click','.source-apply-rights',function(){
        var name=$(this).parent().parent().find('.source-list-name').text();
        if($('.popBox').length==0){
            var oBox='<div class="popBox">'+
                '<h2>申请 "'+name+'"查询权限</h2>'+
                '<textarea class="source-apply-content" maxlength="100" onchange="this.value=this.value.substring(0,100)" cols="30" rows="10"></textarea>'+
                '<p><input type="button" value="确定" class="confirm-btn" /><input type="button" value="取消" class="pop-off" /></p>'+
                '<span class="pop-point"></span></div>';
            $(this).parent().append(oBox);

        }else{
            $('.popBox').remove();
        }
        $('.pop-off').on('click',function(){
            $('.popBox').remove();
        });
        $('.confirm-btn').on('click',function(){
            var applyMsg=$('.source-apply-content').val();
            var _this=this;
            $.ajax({
                url:'http://'+newIp+'/jwcloud-interface/Jwcloud?interface=applyForRights&loginName='+loginName+'&token='+token,
                type:'POST',
                data:{
                    username:loginName,
                    applyReason:applyMsg,
                    sourceName:name
                },
                success:function(dataStr){
                    var data = eval('('+dataStr+')');
                    if(data.status == 0){
                        $(_this).parent().parent().parent().find('.source-apply-rights').val('审核中').attr('class','source-checking');
                        $('.popBox').remove();
                    }else{
                        $('.popBox').remove();
                        layer(false,data.message,false);
                    }
                },
                error:function(err){
                    console.log(err);
                }
            })
        })
    });

});

