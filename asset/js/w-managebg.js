var ip=ipNum;
var lastIp=lastIpNum;
var newIp=newIpNum;
var loginName=decodeURIComponent($.cookie('login_user'));
var token=$.cookie('login_token');
var admin=$.cookie('admin');
var sourceTeamList;
var sourceUserList;
var submitTeamList;
var submitUserList;
var sourceState=0;
var batch=0;
var secondClass;
var sourceTitle=[];
var appointTime='';         //指定时间字段
var editData='';
// var timer;
var timer2;

$(document).click(function () {
    $('.search-select-box').hide();
    $('.search-select').removeClass('border-b0');
    $('.w-uploader-class-box').find('i').css('backgroundImage', 'url("../images/icon_search_select_dropdown.png")');
    $('.w-uploader-source').find('i').css('backgroundImage', 'url("../images/icon_search_select_dropdown.png")');
    $('.search-grade-list').hide();
});

// 加载数据源
if(admin==0) {
    //获取二级分类
    $.ajax({
        url: 'http://' + lastIp + '/index.php',
        type: 'POST',
        async: false,
        data: JSON.stringify({
            interface: 'getType',
            act: 'getAll',
            Token: token,
            username: loginName
        }),
        success: function (dataStr) {
            var data = eval('(' + dataStr + ')');
            if (data.status == 0) {
                secondClass = data.Item
            }
        },
        error: function (err) {
            console.log(err);
        }
    });
    $('.uploader-container').on('click','.uploader-data-view table .add-mark-box dl:not(".level") input[type=radio]',function(){
        var radioCheck= $(this).val();
        console.log($(this).attr('checked'));
        if("1"==radioCheck){
            $(this).attr("checked",false);
            $(this).val("0");
        }else{
            $(this).parent().parent().find('input[type=radio]').attr('checked',false).val("0");
            var name=$(this).attr('name');
            $('.load-view-table tr input[name='+name+']').attr('checked',false).val("0");
            $(this).attr('checked','checked').val("1");
        }
    });
    //分类部分下拉框选择
    for (var i = 0; i < secondClass.length; i++) {
        var oP = '<p data="typeId' + secondClass[i].typeId + '">' + secondClass[i].typeName.toUpperCase() + '</p>';
        $('.w-search-class-option .w-slipe-box').append(oP);
    }
    //一级二级联动
    $('.w-search-class-option').on('click', '.w-slipe-box p', function () {
        $('.w-search-second-class-option .w-slipe-box').html('');
        $('#w-search-select-allsource .w-slipe-box').html('');
        $('.w-search-source-cover').show();
        $('.w-search-second-class-cover').show();
        $('.w-search-second-class-value span').text('选择二级类目');
        $('#w-source-value').text('选择目标数据源');
        var val = $(this).text();
        var data = $(this).attr('data');
        $('.w-search-class-value span').text(val).attr({data: data});
        var typeId = Number($(this).attr('data').replace('typeId', ''));
        var second = secondClass[typeId - 1].son;
        if (!!second) {
            $('.w-search-second-class-cover').hide();
            for (var i = 0; i < second.length; i++) {
                var oP = '<p data="typeId' + second[i].typeId + '">' + second[i].typeName.toUpperCase() + '</p>';
                $('.w-search-second-class-option .w-slipe-box').append(oP);
            }
        } else {
            $('.w-search-second-class-cover').show();
            $('.w-search-second-class-value span').text('选择二级类目');
            $('.w-search-second-class-option .w-slipe-box').html('');
            $('#w-search-select-allsource .w-slipe-box').html('');
        }

    });
    //二级数据源联动
    $('.w-search-second-class-option').on('click', '.w-slipe-box p', function () {
        $('.w-uploader-source .w-search-select-box').hide();
        $('#w-source-value').text('选择目标数据源');
        var val = $(this).text();
        var data = $(this).attr('data');
        $('.w-search-select-value i').css('backgroundImage', 'url("../images/icon_search_select_dropdown.png")');
        $('.w-search-second-class-value span').text(val).attr({data: data});
        $('.w-uploader-source .w-search-select-title').removeClass('border-b0');
        $('#w-search-select-allsource .w-slipe-box').html('');
        // 获取资源库
        $.ajax({
            url: 'http://' + lastIp + '/index.php',
            type: 'POST',
            data: JSON.stringify({
                interface: 'getSource',
                act: 'getSourceType',
                typeId: $(this).attr('data').replace(/typeId/g, ''),
                Token: token,
                username: loginName
            }),
            success: function (dataStr) {
                var dataObj = eval('(' + dataStr + ')');
                if (dataObj.status == 0) {
                    var data = dataObj.Item;
                    if (data.length != 0) {
                        $('.w-search-source-cover').hide();
                        //data=['楼盘备案','物流快递','购房登记','出行记录','通讯记录','人员信息'];
                        appendAllSource();
                        function appendAllSource() {
                            for (var i in data) {
                                $('#w-search-select-allsource .w-slipe-box').append('<p data="' + data[i].sourceId + '" title="' + data[i].sourceName + '">' + data[i].sourceName + '</p>');
                            }
                        }
                    } else {
                        $('.w-search-source-cover').show();
                    }
                } else {
                    $('.w-search-source-cover').show();
                }

            },
            error: function (err) {
                console.log(err);
            }
        });
    });
    var _SourceField = '';  // 数据源 原字段
    $('#w-search-select-allsource').on('click', '.w-slipe-box p', function () {
        var val = $(this).text();
        $('.w-search-select-value span').text(val).attr({title: val});
        $('.w-search-select-value span').attr({'data': $(this).attr('data')});

        if($('#w-source-class-value').text()=='取证'){
            $('.w-uploader-data-view').show();
            $('.w-tab-change-edit').hide();
            $('.case-hide').hide();
            $('.layer-bg').hide();
            $('.creat-link').hide();
            $('.w-load-view-table').html('');
            $('.w-forms-hints').remove();
        }else {
            //获取删除数据远权限
            $.ajax({              // 加载字段
                url: 'http://' + lastIp + '/index.php',
                type: 'POST',
                data: JSON.stringify({
                    interface: 'getField',
                    act: 'getFieldBySource',
                    sourceId: $(this).attr('data'),
                    Token: token,
                    username: loginName
                }),
                success: function (dataStr) {
                    var dataObj = eval('(' + dataStr + ')');
                    var data = dataObj.Item;
                    editData= dataObj.Item.field;
                    var field = data.field;
                    if (dataObj.status != 1) {
                        // 弹出错误提示
                    }
                    _SourceField = field;

                    // $('.uploader-btn').hide();
                    $('.uploader-data-view').show();
                    $('.w-tab-change-edit').show();
                    $('.case-hide').show();
                    $('.layer-bg').hide();
                    $('.creat-link').hide();
                    $('.w-forms-hints').remove();
                    $('.w-load-view-table').html('');
                    var oDiv = '';
                    for (var i = 0; i < uniqueList.length; i++) {
                        var oSpan = '<span>' + uniqueList[i].uniquefieldname + '</span>';
                        oDiv += oSpan;
                    }
                    var oTr = '<tr></tr>';
                    $('.w-load-view-table').append(oTr);
                    for (var i = 0; i < field.length; i++) {
                        var unique = '';
                       var star1='';
                       var star2='';
                       var star3='';
                        switch (field[i].level)
                        {
                            case '1':
                                star1='checked="checked"';
                                break;
                            case '2':
                                star2='checked="checked"';
                                break;
                            case '3':
                                star3='checked="checked"';
                                unique = '<div class="search-grade search-only-field fl"><span class="search-grade-title">'+field[i].name+'</span><i></i> <div class="search-grade-list">'+oDiv+ '<span>其他</span></div></div>';
                                break;
                        }
                        var positionBox='';
                        var stationBox='';
                        var selectTime='';
                        if($('#w-source-second-class-value').attr('data')=='typeId21'){
                            var longitudeChecked ='';
                            var latitudeChecked ='';
                            var LAC='';
                            var CID='';
                            var SID='';
                            var BID='';
                            var NID='';
                            console.log(field[i].isLongitude,field[i].isLatitude);
                            if(field[i].isLongitude==0){
                                longitudeChecked='checked="checked"';
                            }
                            if(field[i].isLatitude==0){
                                latitudeChecked='checked="checked"';
                            }
                            if(field[i].isLAC==0){
                                LAC = 'checked="checked"';
                            }
                            if(field[i].isCID==0){
                                CID = 'checked="checked"';
                            }
                            if(field[i].isSID==0){
                                SID = 'checked="checked"';
                            }
                            if(field[i].isBID==0){
                                BID = 'checked="checked"';
                            }
                            if(field[i].isNID==0){
                                NID = 'checked="checked"';
                            }
                            positionBox='<dl class="position-box"><dt>经纬</dt><dd><input type="radio" id="longitude'+i+'" name="position-longitude" '+longitudeChecked+' /><label for="longitude'+i+'">经度</label></dd><dd><input type="radio" id="latitude'+i+'" name="position-latitude" '+latitudeChecked+'/><label for="latitude'+i+'">纬度</label></dd></dl><span></span>';
                            stationBox='<dl class="station-box"><dt>基站</dt><dd><input type="radio" id="LAC'+i+'" name="LAC" '+LAC+' /><label for="LAC'+i+'">LAC</label></dd><dd><input type="radio" id="CID'+i+'" name="CID" '+CID+'/><label for="CID'+i+'">CID</label></dd>' +
                                '<dd><input type="radio" id="SID'+i+'" name="SID" '+SID+'/><label for="SID'+i+'">SID</label></dd><dd><input type="radio" id="BID'+i+'" name="BID" '+BID+'/><label for="BID'+i+'">BID</label></dd><dd><input type="radio" id="NID'+i+'" name="NID" '+NID+'/><label for="NID'+i+'">NID</label></dd></dl><span></span>';
                        }
                        if(field[i].isTime==1){
                            selectTime='checked="checked"';
                        }
                        // var oTh = '<th class="clearfix"><div class="table-title fl">' + field[i].fieldName + '</div><div class="fl">' +
                        //     '<div class="search-grade w-search-grade"><span class="search-grade-title search-grade-value">' + field[i].level + '星</span><i></i> <div class="search-grade-list">' +
                        //     '<span onclick="hideOnlyField(this)">1星</span><span onclick="hideOnlyField(this)">2星</span><span onclick="showOnlyField(this)">3星</span></div></div></div>' + unique + '</th>';

                        var oTh='<th class="clearfix"><div class="table-title fl">'+field[i].fieldName+'</div><div class="fl add-mark-container"><div class="clearfix add-mark-btn" onclick="showMarkBox(this)"><p class="fl"><span></span><span></span><span></span></p>'+
                            '<p class="fl">添加标识</p></div>'+
                            '<div class="add-mark-box clearfix"><dl><dt>星级</dt><dd><input type="radio" id="start'+i+'_1" name="star'+i+'"  onclick="hideOnlyField(this)" level="1" '+star1+'/><label for="start'+i+'_1" onclick="hideOnlyField(this)">一星</label></dd><dd><input type="radio" id="start'+i+'_2" name="star'+i+'" onclick="hideOnlyField(this)" level="2" '+star2+'/><label for="start'+i+'_2" onclick="hideOnlyField(this)">二星</label></dd><dd><input type="radio" id="start'+i+'_3" name="star'+i+'" onclick="showOnlyField(this)" level="3" '+star3+' /><label for="start'+i+'_3" onclick="showOnlyField(this)">三星</label></dd></dl>'+positionBox+stationBox+
                            '<dl><dt>时间</dt><dd><input type="radio" id="screen-time'+i+'" name="screen-time" '+selectTime+'/><label for="screen-time'+i+'" >筛选时间</label></dd></dl><b></b></div></div>'+unique+'</th>';

                        $('.w-load-view-table tr:last-child').append(oTh);
                    }
                    setMarkBoxWidth();
                    addTitle = 2;
                    $('.bg-cover').css({
                        width: $('.w-uploader-data-table table').width(),
                        height: $('.w-uploader-data-table table').height()
                    });
                    //data=['楼盘备案','物流快递','购房登记','出行记录','通讯记录','人员信息'];

                },
                error: function (err) {
                    console.log(err);
                }
            });
        }

        $.ajax({
            url:'http://'+newIp+'/jwcloud/user/getUser',
            type:'POST',
            data:{
                dogId:dogId,
                getDogId:dogId
            },
            xhrFields:{
                withCredentials:true
            },
            success:function(data){
                if(data.status == 0){
                    if(data.delSourcePermission == 0){
                        $('.w-uploader-view-del-btn').hide();
                        $('.uploader-data-view-title em.case-hide').eq(1).remove();
                        if($('.uploader-view-change-btn').css('display')=='none'){
                            $('.uploader-data-view-title').append('<span class="w-forms-hints">您没有删除数据源权限</span>');
                        }
                    }else{
                        $('.w-forms-hints').remove();
                        $('.w-uploader-view-del-btn').show();
                    }
                }else{

                }
            },
            error:function(err){
                console.log(err);
            }
        });


        $.ajax({ // 加载文件夹
            url: 'http://' + lastIp + '/index.php',
            type: 'POST',
            data: JSON.stringify({
                interface: 'getSource',
                act: 'getSourceDir',
                sourceId: $(this).attr('data'),
                Token: token,
                username: loginName
            }),
            success: function (dataStr) {
                var dataObj = eval('(' + dataStr + ')');
                var data = dataObj.Item;
                $(".w-load-view-table-delete").html('');
                var oTr = '<tr><th><label for="checkAllbox"><input type="checkbox" name="selectall" value="checkbox1" id="checkAllbox" onclick="checkBoxChose(this)"> 全选</label></th><th>数据源文件</th></tr>'
                //$('.w-load-view-table-delete').append(oTr);
                var oTd = '';
                for (var i = 0; i < data.length; i++) {
                    oTd = oTd + '<tr><td><input type="checkbox" name="checkbox" value="' + data[i].sourceDir + '"></td><td>' + data[i].sourceDir + '</td><tr>';
                }
                $('.w-load-view-table-delete').append(oTr + oTd);
            },
            error: function (err) {
                console.log(err)
            }
        });
    });
    // checkbox 全选/反选
    function checkBoxChose(me) {
        if ($(me).is(':checked')) {
            $('input[name="checkbox"]').each(function () {
                //此处如果用attr，会出现第三次失效的情况
                $(this).prop("checked", true);
            });
        } else {
            $('input[name="checkbox"]').each(function () {
                $(this).removeAttr("checked", false);
            });
            //$(this).removeAttr("checked");
        }

    }
    //显示添加标识框
    function showMarkBox(n){
        var $box=$(n).parent().find('.add-mark-box');
        if($box.css('display')=='none'){
            $box.show();
        }else{
            $box.hide();
        }
    }
    //设置表示狂宽度
    function setMarkBoxWidth(){
        setTimeout(function(){
            var width=0;
            var ele=$('.add-mark-box').eq(0).find('dl');
            for(var i=0;i<ele.length;i++){
                width+=ele.eq(i).outerWidth(true);
            }
            $('.add-mark-box').css({
                width:width+10
            })
        },0);
    }


//获取唯一字段列表
    var uniqueList;
    $.ajax({
        url: 'http://' + lastIp + '/index.php',
        type: 'POST',
        async: false,
        data: JSON.stringify({
            interface: 'getField',
            act: 'getUnique',
            Token: token,
            username: loginName
        }),
        success: function (dataStr) {
            var data = eval('(' + dataStr + ')');
            uniqueList = data.Item;
        },
        error: function (err) {
            console.log(err);
        }
    });


    function showOnlyField(n) {
        $(n).parent().parent().parent().parent().find('.search-only-field').remove();
        var oDiv = '';
        for (var i = 0; i < uniqueList.length; i++) {
            var oSpan = '<span>' + uniqueList[i].uniquefieldname + '</span>';
            oDiv += oSpan;
        }
        oDiv = '<div class="search-grade search-only-field fl"><span class="search-grade-title"></span><i></i> <div class="search-grade-list">' + oDiv + '<span>其他</span></div></div>';
        $(n).parent().parent().parent().parent().append(oDiv);
    }

    function hideOnlyField(n) {
        $(n).parent().parent().parent().parent().parent().find('.search-only-field').remove();
    }

    //选择星级部分
    $('.uploader-data-view').on('click', '.search-grade', function (e) {
        e.stopPropagation();
        $('.search-grade-list').hide();
        var list = $(this).find('.search-grade-list');
        if (list.css('display') == 'none') {
            list.show();
        } else {
            list.hide();
        }
    });
    $('.uploader-data-view').on('click', '.search-grade-list span', function (e) {
        e.stopPropagation();
        $(this).parent().parent().find('.search-grade-title').text($(this).text());
        $('.search-grade-list').hide();
    });
    var limitAdd = true;
    //修改字段按钮点击

    $('.uploader-view-change-btn').on('click', function () {
        $('.w-tab-change-delete').hide();
        $('.w-tab-change-edit').show();
        $('.w-uploader-view-btn').show();
        $('.bg-cover').hide();
        if (limitAdd) {
            $('.table-title').attr({'contenteditable': true});
            $('.w-load-view-table th').addClass('editTh');
            addTitle = 2;
        }
    });
    // 删除数据源
    $('.w-uploader-view-del-btn').on('click', function () {
        var sourceId = $('.w-search-select-value span').attr('data');
        var sourceName = $('.w-search-select-value span').text().replace(/[,.\s]/g, '');
        var firstClass = $('.w-search-class-value span').text();
        var secondClass = $('.w-search-second-class-value span').text();
        $('.delSource .w-del-source ', parent.document).off('click');
        console.log(secondClass);
        if (firstClass == '选择类别') {
            firstClass = '';
        }
        if (secondClass == '选择二级类目') {
            secondClass = '';
        }
        if (!!firstClass && !!secondClass) {
            $('.delSource', parent.document).show();
            $('.layer-bg', parent.document).show();
            $('.delSource .hints-txt', parent.document).text('确认删除数据源"' + sourceName + '"吗？');
            $('.delSource .del-hints-no,.delSource .box-off', parent.document).on('click', function () {
                $('.delSource', parent.document).hide();
                $('.layer-bg', parent.document).hide();
                // $('.load-view-table tr').html('');
                // $('.w-uploader-data-view').hide();
            });
            var limitDelSource = false;
            $('.delSource .w-del-source ', parent.document).on('click', function () {
                if (limitDelSource)return;
                limitDelSource = true;
                $(this).css({cursor:'wait'});
                $.ajax({    //php删除
                    url: 'http://' + lastIp + '/index.php',
                    type: 'POST',
                    data: JSON.stringify({
                        interface: 'getSource',
                        act: 'delSource',
                        sourceId: sourceId,
                        Token: token,
                        username: loginName
                    }),
                    success: function (dataStr) {
                        $('.delSource', parent.document).hide();
                        $('.layer-bg', parent.document).hide();
                        var data = eval('(' + dataStr + ')');
                        if (data.status != -1) {
                            $.ajax({
                                url: 'delSource', //后台删除
                                type: 'POST',
                                data: {
                                    sourceName : sourceName,
                                    className : firstClass+secondClass
                                },
                                success: function (dataStr) {
                                    $('.delSource .w-del-source ', parent.document).css({cursor:'default'});

                                    $('.w-load-view-table tr').html('');
                                    $('.w-uploader-data-view').hide();
                                    $('.w-uploader-view-btn').hide();
                                    limitDelSource = false;
                                    $('#w-source-value').text('选择目标数据源');
                                    var sourceItem = $('#w-search-select-allsource .w-slipe-box p');
                                    for (var i = 0; i < sourceItem.length; i++) {
                                        console.log(sourceItem.eq(i).text());
                                        if (sourceItem.eq(i).text() == sourceName) {
                                            sourceItem[i].remove();
                                        }
                                    }
                                },
                                error: function (err) {
                                    console.log(err);
                                }
                            })
                        }
                    },
                    error: function (err) {
                        console.log(err)

                    }
                });

            });

        } else {

        }

    });

    //添加字段 按钮点击
    $('.uploader-view-add-btn').on('click', function () {
        if (limitAdd) {
            addTitle = 1;
            var addLength = $('.w-load-view-table tr:eq(0) th').length;
            $('.w-load-view-table .search-grade,.w-load-view-table .search-grade-radio').remove();
            $('.table-title').removeAttr('contenteditable').removeAttr('class');
            var oTr = '<tr></tr>';
            $('.w-load-view-table').prepend(oTr);
            for (var i = 0; i < addLength; i++) {
                var oTh = '<th class="clearfix editTh"><div class="table-title fl" contenteditable="true"></div><div class="fl">' +
                    '<div class="search-grade w-search-grade"><span class="search-grade-title">1星</span><i></i> <div class="search-grade-list">' +
                    '<span onclick="hideOnlyField(this)">1星</span><span onclick="hideOnlyField(this)">2星</span><span onclick="showOnlyField(this)">3星</span></div></div></div></th>';
                $('.w-load-view-table tr:first-child').append(oTh);
            }
            limitAdd = false;
        }
    });
    function getArrIndex(arr,str){
        for(var i=0;i<arr.length;i++){
            if(arr[i]==str){
                return i;
                break;
            }
        }
    }
    var limitClick = true;
    // 确定修改按钮
    $('.w-uploader-view-confirm').on('click', function () {
        var sourceName = $('.w-search-select-value span').text();
        var sourceId = $('.w-search-select-value span').attr('data');
        var firstClass = $('.w-search-class-value span').text();
        var secondClass = $('.w-search-second-class-value span').text();
        var other='';
        var uniqueWord = '';
        var checkPostionSame=true;
        var checkPostionNoLongitude=true;
        var checkPostionNoLatitude=true;
        var screenTime='';
        var lac='';
        var cid='';
        var bid='';
        var sid='';
        var nid='';
        var lng='';
        var lat='';
        if (firstClass == '选择类别') {
            firstClass = '';
        }
        if (secondClass == '选择二级类目') {
            secondClass = '';
        }
        var tableTitleArr = [];
        var titleLength = $('.w-load-view-table tr:eq(0) th .table-title').length;
        for (var n = 0; n < titleLength; n++) {
            tableTitleArr.push($('.w-load-view-table tr:eq(0) th .table-title').eq(n).text().replace(/\s/g, ''));
        }
        if($('#w-source-second-class-value').attr('data')=='typeId21'){

            if($('input:radio[name="position-longitude"]:checked').length===0){
                checkPostionNoLongitude=false;
            }
            if($('input:radio[name="position-latitude"]:checked').length===0){
                checkPostionNoLatitude=false;
            }
            lac=$('input:radio[name=LAC]:checked').length>0?$('input:radio[name=LAC]:checked').parent().parent().parent().parent().parent().find('.table-title').text():'';
            cid=$('input:radio[name=CID]:checked').length>0?$('input:radio[name=CID]:checked').parent().parent().parent().parent().parent().find('.table-title').text():'';
            bid=$('input:radio[name=BID]:checked').length>0?$('input:radio[name=BID]:checked').parent().parent().parent().parent().parent().find('.table-title').text():'';
            sid=$('input:radio[name=SID]:checked').length>0?$('input:radio[name=SID]:checked').parent().parent().parent().parent().parent().find('.table-title').text():'';
            nid=$('input:radio[name=NID]:checked').length>0?$('input:radio[name=NID]:checked').parent().parent().parent().parent().parent().find('.table-title').text():'';
            lng=$('input:radio[name=position-longitude]:checked').length>0?$('input:radio[name=position-longitude]:checked').parent().parent().parent().parent().parent().find('.table-title').text():'';
            lat=$('input:radio[name=position-latitude]:checked').length>0?$('input:radio[name=position-latitude]:checked').parent().parent().parent().parent().parent().find('.table-title').text():'';
            lac=lac?'LAC#separator#'+lac+'\n':'';
            cid=cid?'CID#separator#'+cid+'\n':'';
            bid=bid?'BID#separator#'+bid+'\n':'';
            sid=sid?'SID#separator#'+sid+'\n':'';
            nid=nid?'NID#separator#'+nid+'\n':'';
            lng=lng?'经度#separator#'+lng+'\n':'';
            lat=lat?'纬度#separator#'+lat+'\n':'';
            other=lac+cid+bid+sid+nid+lng+lat;
            //声明完成等待使用
        }
        sourceTitle = [];
        var uniqueJson={};
        for (var i = 0; i < $('.table-title').length; i++) {
            var json = {};
            json.fieldName = $('.table-title').eq(i).text().replace(/[,.\s]/g, '');
            json.level = $('.w-load-view-table input:radio[name="star'+i+'"]:checked').attr('level');
            var onlyField = $('.table-title').eq(i).parent().find('.search-only-field .search-grade-title').text().replace(/[,.\s]/g,'');
            if (!!onlyField) {
                if (onlyField == '其他') {
                    onlyField = $('.table-title').eq(i).text().replace(/[,.\s]/g, '');
                }
                if(!uniqueJson[onlyField]){
                    uniqueJson[onlyField]=new Array();
                }
                if($('#w-source-second-class-value').attr('data')=='typeId21'){
                    uniqueJson[onlyField].push(tableTitleArr[i]);
                }else{
                    uniqueJson[onlyField].push(i);
                }
            }else{
                onlyField='';
            }

            if($('#w-source-second-class-value').attr('data')=='typeId21'){  //当是地图时判断同意字段  禁止同时选择经度纬度
                var longitude=$('.w-uploader-data-table #longitude'+i);
                var latitude=$('.w-uploader-data-table #latitude'+i);
                var isLAC=$('.uploader-data-table #LAC'+i);
                var isCID=$('.uploader-data-table #CID'+i);
                var isBID=$('.uploader-data-table #BID'+i);
                var isSID=$('.uploader-data-table #SID'+i);
                var isNID=$('.uploader-data-table #NID'+i);
                json.isLongitude =longitude.attr('checked')?0:-1;
                json.isLatitude =latitude.attr('checked')?0:-1;
                json.isLAC =isLAC.attr('checked')?0:-1;
                json.isCID =isCID.attr('checked')?0:-1;
                json.isBID =isBID.attr('checked')?0:-1;
                json.isSID =isSID.attr('checked')?0:-1;
                json.isNID =isNID.attr('checked')?0:-1;
                if(json.isLongitude==0 && json.isLatitude==0){
                    checkPostionSame=false;
                }
            }else{
                json.isLongitude=-1;
                json.isLatitude =-1;
            }
            if($('#screen-time'+i).attr('checked') == 'checked'){
                if($('#w-source-second-class-value').attr('data')=='typeId21'){
                    screenTime='筛选时间#separator#'+tableTitleArr[i];
                }else{
                    screenTime='筛选时间#separator#'+i;
                }
                json.isTime=1;  //选中
            }else{
                json.isTime=0;  //未选中
            }
            json.name = onlyField;
            sourceTitle.push(json);
            if ($('#time' + i + ':checked').length > 0) {
                appointTime = $('#time' + i + ':checked').parent().parent().parent().find('.table-title').text();   //指定时间
            }
        }
        for(var i in uniqueJson){
            uniqueWord+=i+'#separator#'+uniqueJson[i].join(',')+'\n';
        }
        var oldFieldStr='';
        var newFieldStr='';
        for(var k=0;k<editData.length;k++){
            oldFieldStr+= editData[k].fieldName+',';
            newFieldStr+= sourceTitle[k].fieldName+',';
        }



        if (!checkFalsePos(tableTitleArr)) {   //检查是否有空位
            if (!checkFindSame(tableTitleArr)) {
                if (!!firstClass && !!secondClass) {
                    if(!!checkPostionSame){
                        if(!!checkPostionNoLongitude){
                            if(!!checkPostionNoLatitude){
                                if(sourceName != '选择上传的目标数据源') {
                                    if (!limitClick)return;
                                    limitClick = false;

                                    oldFieldStr=oldFieldStr.substring(0,oldFieldStr.length-1);
                                    newFieldStr=newFieldStr.substring(0,newFieldStr.length-1);

                                    $('.w-uploader-view-confirm').css('cursor', 'wait');
                                    $.ajax({        // 更新字段
                                        url: 'http://' + lastIp + '/index.php',
                                        type: 'POST',
                                        data: JSON.stringify({
                                            interface: 'getField',
                                            act: 'updateField',
                                            Token: token,
                                            username: loginName,
                                            sourceId: $('.w-search-select-value span').attr('data'),
                                            field: sourceTitle
                                        }),
                                        success: function (dataStr) {
                                            var data = eval('(' + dataStr + ')');
                                            if (data.status == 0) {
                                                // var fieldChangeFlag = false; // 标记是否改过字段
                                                // console.log('_sourcefield='+_SourceField);
                                                // for (var i = 0; i < _SourceField.length; i++) {
                                                //     if (_SourceField[i].fieldName !== sourceTitle[i].fieldName) {
                                                //         fieldChangeFlag = true;
                                                //     }
                                                // }
                                                // if (!fieldChangeFlag) { // 如果没有修改过字段，不修改bin文件
                                                //     $('.w-load-view-table tr').html('');
                                                //     $('.w-uploader-data-view').hide();
                                                //     $('.w-uploader-view-confirm').css('cursor', 'default');
                                                //     return false;
                                                // }
                                                // 调用写bin文件，批量上传文件
                                                $.ajax({
                                                    url: "sourceEdit",
                                                    type: "POST",
                                                    data: {
                                                        sourceName: sourceName,
                                                        className: firstClass +'\n'+ secondClass,
                                                        action: 'sourceEdit',
                                                        oldField:oldFieldStr,
                                                        newField:newFieldStr,
                                                        binData:uniqueWord+other+screenTime
                                                    },
                                                    success: function (data) {
                                                        $('.w-load-view-table tr').html('');
                                                        $('.w-uploader-data-view').hide();
                                                        $('.w-uploader-view-confirm').css('cursor', 'default');
                                                        limitClick = true;
                                                        $('.bg-cover').show();
                                                    },
                                                    error: function (err) {
                                                        console.error(err)
                                                    }
                                                });
                                                //window.location.reload();
                                            } else {
                                                alert('修改失败');
                                                //window.location.reload();
                                            }
                                        },
                                        error: function (err) {
                                            console.log(err);
                                        }
                                    });

                                }else{
                                    limitClick=true;
                                    setLayer(false, '请选择数据源', false);
                                }
                            }else{
                                limitClick=true;
                                setLayer(false, '您没有选则纬度', false);
                            }
                        }else{
                            limitClick=true;
                            setLayer(false, '您没有选择经度', false);
                        }
                    }else{
                        limitClick=true;
                        setLayer(false, '同一字段不允许同时选择经度和纬度', false);
                    }


                } else {
                    limitClick = true;
                    setLayer(false, '请选择类别', false);
                }
            } else {
                limitClick = true;
                setLayer(false, '修改有重复字段', false);
            }
            //判断是否有修改  没有修改不传参数

        } else {
            limitClick = true;
            setLayer(false, '编写的字段有空位', false);
        }
    });
    function viewHide() {
        $('.w-uploader-data-view').hide();
        $('.w-uploader-data-table .w-load-view-table').html('');
        $('.w-uploader-view-btn').hide();
    }


    // 点击确定删除按钮
    $(".w-uploader-view-confirm-delete").on('click', function () {
        var sourceDir = new Array();
        $("input[name=checkbox]:checkbox:checked").each(function () {
            sourceDir.push($(this).val());
        });
        console.log(sourceDir);
    });

    //取消按钮 点击
    $('.w-uploader-view-cancel').on('click', function () {
        window.location.reload();
    });

    //检查修改的字段是否有空位
    function checkFalsePos(arr) {
        for (var i = 0; i < arr.length; i++) {
            if (!arr[i]) {
                return true;  //如果有空位返回true
            }
        }
        return false;
    }

    //检查修改的字段是否有空位
    function delEmpty(arr) {
        for (var i = 0; i < arr.length; i++) {
            if (!arr[i]) {
                arr.splice(i, 1);
                return true;  //如果有空位返回true
            }
        }
        return false;
    }

    // 弹窗
    function setLayer(boolean, value, n) {
        //boolean：正确提示还是错误提示；value：提示内容；n：是否有按钮（true、false）
        $('.hints-layer img', parent.document).removeAttr();
        $('.hints-layer .hints-txt', parent.document).text('');
        if (boolean) {
            $('.hints-layer img', parent.document).attr({
                src: "../images/icon_upload_complete.png"
            })
        } else {
            $('.hints-layer img', parent.document).attr({
                src: "../images/icon_upload_error.png"
            })
        }
        if (n) {
            $('.hints-layer .hints-layer-btn', parent.document).show();
        } else {
            $('.hints-layer .hints-layer-btn', parent.document).hide();
        }
        $('.hints-layer .hints-txt', parent.document).text(value);
        $('.hints-layer', parent.document).show();
        $('.layer-bg', parent.document).show();
    }

//查重
    function checkFindSame(arr) {
        for (var i = 0; i < arr.length; i++) {
            for (var j = i + 1; j < arr.length; j++) {
                if (arr[i] == arr[j]) {
                    return true;
                }
            }
        }
        return false;
    }

    //查重
    function findIsDiffer(a, n) {
        for (var i = 0; i < a.length; i++) {
            if (a[i] == n) {
                return false;
            }
        }
        return true;
    }

//下拉框展示隐藏效果
    $('.w-search-select').on('click', function (e) {
        e.stopPropagation();
        var box = $(this).find('.w-search-select-box');
        if($('#w-source-class-value').text()!='取证'){   //当大类为取证时不应藏
            viewHide();
        }
        if (box.css('display') == 'none') {
            $('.w-search-select .w-search-select-box').hide();
            $(this).addClass('border-b0');
            box.show();

            //清除搜索选中样式
//                var optionList=$(this).find('.search-select-option .slipe-box p');
//                for(var j=0;j<optionList.length;j++){
//                    optionList.eq(j).removeClass('select-blue-p');
//                }
            $(this).find('.w-search-select-value').find('i').css('backgroundImage', 'url("../images/icon_search_select_putaway.png")');
            $(this).find('.w-search-class-value').find('i').css('backgroundImage', 'url("../images/icon_search_select_putaway.png")');
            $(this).find('.w-search-second-class-value').find('i').css('backgroundImage', 'url("../images/icon_search_select_putaway.png")');

        } else {
            $(this).removeClass('border-b0');
            box.hide();
            $(this).find('.w-search-select-value').find('i').css('backgroundImage', 'url("../images/icon_search_select_dropdown.png")');
            $(this).find('.w-search-class-value').find('i').css('backgroundImage', 'url("../images/icon_search_select_dropdown.png")');
            $(this).find('.w-search-second-class-value').find('i').css('backgroundImage', 'url("../images/icon_search_select_dropdown.png")');
        }
    });
    $(document).click(function () {
        $('.w-search-select-box').hide();
        $('.w-search-select').removeClass('border-b0');
        $('.w-search-select-value').find('i').css('backgroundImage', 'url("../images/icon_search_select_dropdown.png")')
        $('.w-search-grade-list').hide();
    });
//搜索数据源列表
    $('.w-search-source-input img').on('click', function () {
        var optionList = $(this).parent().parent().find('.w-search-select-option .w-slipe-box p');
        for (var j = 0; j < optionList.length; j++) {
            optionList.eq(j).removeClass('select-blue-p');
        }
        if (!!$(this).prev().val()) {
            var key = $(this).prev().val();
            for (var i = 0; i < optionList.length; i++) {

                if (optionList.eq(i).text().indexOf(key) !== -1) {
                    optionList.eq(i).addClass('select-blue-p');
                    var oParent = optionList.eq(i).parent().parent();
                    oParent.scrollTop(36 * i - 50);
                    return;
                }
            }
        }
    });
//数据源搜索输入框 阻止冒泡
    $('.w-search-source-input').click(function (e) {
        return false;
        e.stopPropagation();
    });


//下拉框选择效果
    /*
     $('.w-search-select-option .w-slipe-box').on('click', 'p', function () {
     var val = $(this).text();
     $('.w-search-select-value span').text(val);
     sourceState=-1;
     $.ajax({
     url: 'http://'+newIp+'/jwcloud-interface/Jwcloud?interface=getSourceRights&loginName='+loginName+'&token='+token,
     data: {
     sourceName:val
     },
     success:function(dataStr){
     var data=eval('('+dataStr+')');
     if(data.status == 0) {
     if(data.userRights == 'all'){
     $('#all-user').trigger('click');
     }
     else if(data.status == 'team'){
     $('#special-team').trigger('click');
     }
     else if(data.status == 'user'){
     $('#special-user').trigger('click');
     }
     sourceTeamList=data.teamList;
     sourceUserList=data.userList;
     //eachTeamList();
     //eachUserList();
     }
     else{
     layer(false,'您还没有登录，请点击“确定”返回登录页')
     }
     },
     error:function(err){
     console.log(err);
     }
     })
     });*/


}
