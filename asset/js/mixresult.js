var ip=ipNum;
var lastIp=lastIpNum;
var newIp=newIpNum;
var loginName=decodeURIComponent($.cookie('login_user'));
var token=$.cookie('login_token');
var admin=$.cookie('admin');
var secondSourceList;
var pages=0;   //主页总页码
var typePages=0;  //数据源匹配列表部分总页码
var dataListArr=[];     //数据源匹配列表点击“点击查看”用该该数据
var wordArr;
var uniqueWord='';
var whiteList = [];
var currentSourceName='';  //当前数据源名字
var sourceDataList={};  //点击“点击查看”用到该数据组
window.localStorage.clear();

function getKey(){
    if(!!location.href.split('?')[1]){
        var urlHrefStr=location.href;
        var urlStr=urlHrefStr.substring(Number(urlHrefStr.indexOf('?'))+1,urlHrefStr.length);
        var urlArr=urlStr.split('&');
        var urlJson={};
        for(var i=0;i<urlArr.length;i++){
            var arr=urlArr[i].split('=');
            urlJson[arr[0]]=decodeURIComponent(arr[1]);
        }
        return urlJson;
    }else{
        return;
    }
}

var key = getKey();
var keyWd=key?key.wd:'';
var regT=/\\t/g;
keyWd=keyWd.replace(regT,'\t');
if(!!key){
    $('#res-search-text').val(keyWd);
}
var perpageCount = Number(key?key.listCount:0);//每页条数

var currentPage=Number(key?key.currentPage:0);
var currentTypePage=1;
function checkChinese(){
    return /[\u4E00-\u9FA5]/img.test(keyWd);
}
var exclude;
if(!!key.exclude){
    exclude=key.exclude.trim();
}else{
    exclude='';
}
$(document).keyup(function(e){
    if(e.keyCode == 13){
        $('#res-search-btn').trigger('click');
    }
});
$.ajax({
    url:'http://'+newIp+'/jwcloud/user/getRecord',  //用户搜索记录
    type:'POST',
    data:{
        dogId:dogId,
        getDogId:dogId,
        action:"getSearchRecord"
    },
    xhrFields:{
        withCredentials:true
    },
    success:function(data){
        if(data.status==0){
            var historyList=data.recordList;
            for(var i=0;i<historyList.length;i++){
                var searchWord = historyList[i].keyword.replace(/"/g,'\\"')+historyList[i].url;
                var oLi='<li><a href="/mixresult?wd='+searchWord+'">搜索关键词"'+historyList[i].keyword+'"</a></li>';
                $('.res-history-list ul').append(oLi);
            }
        }else{
            layer(false,'您还没有登录，请点击“确定”返回登录页')
        }
    },
    error:function(err){
        console.log(err);
    }
});
$.ajax({
    url: 'http://' + lastIp + '/index.php',    //获取指定字段列表
    type: 'POST',
    async:false,
    data: JSON.stringify({
        interface: 'getField',
        act: 'getUnique',
        Token: token,
        username: loginName,
        level: 3
    }),
    success: function (dataStr) {
        var data = JSON.parse(dataStr);
        if (data.status == 0) {
            var onlyWordList = data.Item;
            var oContent='';
            for (var i = 0; i < onlyWordList.length; i++) {
                oContent += '<li><input type="radio" id="only' + i + '" name="only"><label for="only' + i + '">' + onlyWordList[i].uniquefieldname + '</label></li>';

            }
            $('.search-only-btn ul').append(oContent);

        } else {
            ;
        }
    },
    error: function (err) {
        console.log(err);
    }
});
if(!!key.word){
    var radioS=$('.search-only-btn ul li input[type=radio]');
    for(var i=0;i<radioS.length;i++){
        var word = radioS.eq(i).parent().find('label').eq(0).text();
        if(word==key.word){
            radioS.removeAttr('checked');
            radioS.eq(i).attr({'checked':'checked'});
            $('.search-only-btn p span').text(word);
        }
    }
    $.ajax({
        url: 'http://' + lastIp + '/index.php',
        type: 'POST',
        async: false,
        data: JSON.stringify({
            interface: 'getField',
            act: 'getFieldByUnique',
            uniqueName: key.word,
            Token: token,
            username: loginName
        }),
        success: function (dataStr) {
            var data = eval('(' + dataStr + ')');
            if (data.status == 0) {
                $.cookie('uniqueWord',$('.search-only input:radio:checked').next().text());
                if(data.Item.length===0){
                    wordArr = key.word;
                }else{
                    var arr=[];
                    for(var i=0;i<data.Item.length;i++){
                        arr.push(data.Item[i].fieldName);
                    }
                    wordArr=arr;
                }
            } else {
                wordArr = key.word;
            }
        },
        error: function (err) {
            console.log(err);
        }
    });
}
if(!!key.unison){
    if(key.unison==1){
        $('.res-search-container .unisonSearchBox #unisonSearch').attr({'checked':'checked'});
        $('.res-search-container .unisonSearchBox').show();
    }else{
        if($('.search-only-btn p span').text()=='姓名'){
            $('.res-search-container .unisonSearchBox').show();
        }
        $('.res-search-container .unisonSearchBox #unisonSearch').removeAttr('checked');
    }

}
function evil(fn) {
    var Fn = Function;
    return new Fn('return ' + fn)();
}
//请求组合json
function getRequestJson(){  //n如果是 ‘toLower’ 就转成小写
    var bool;
    bool={
        "bool":{
            "must_not":getWhiteJson()
        }
    };
    return bool;
}
function getWhiteJson(n){
    var aWhiteList=[];
    for(var i=0;i<whiteList.length;i++){
        var json ={},json1={};
        if(n=='toLower'){
            json._all=whiteList[i].toLowerCase();
        }else{
            json._all=whiteList[i];
        }
        json1.wildcard = json;
        aWhiteList.push(json1);
    }
    return aWhiteList;
}
//指定字段下拉框
$('.search-only-btn p').on('click',function(e){
    e.stopPropagation();
    var dropDown = $(this).parent().find('div').eq(0);
    var oIcon=$(this).find('i');
    if(dropDown.css('display')=='none'){
        dropDown.show();
        oIcon.addClass('down');
    }else{
        dropDown.hide();
        oIcon.removeClass('down');
    }
});
$('.search-only-btn div').on('click',function(e){
    e.stopPropagation();
});

$('.search-only-btn ul').on('click','input[type=radio]',function(){
    var text=$(this).parent().find('label').text();
    $('.search-only-btn p span').text(text);
    if(text=='姓名'){
        $('.res-search-container .unisonSearchBox').show();
    }else{
        $('.res-search-container .unisonSearchBox').hide();
        $('.res-search-container .unisonSearchBox #unisonSearch').removeAttr('checked');
    }
})



$('#res-search-btn').on('click',function(){
    if(!!$('#res-search-text').val()) {
        var searchKeyword=$('#res-search-text').val().trim();
        var word='';
        var unison='';
        if(searchKeyword.length>45){
            searchKeyword=searchKeyword.substring(0,45);
        }else{
            searchKeyword=searchKeyword.substring(0,searchKeyword.length);
        }
        var text=$('.search-only-btn ul input:radio:checked').next('label').text();
        if(text=='全文搜索'){
            unison = 0;
            word='';
        }else{
            word=text;
        }
        if($('#unisonSearch:checkbox:checked').length>0){
            unison = 1;
        }else{
            unison = 0;
        }
        var skipUrl='/mixresult?wd='+ searchKeyword+'&word='+word+'&unison='+unison+'&listCount=20&currentPage=1';

        $.ajax({
            url: 'http://' + newIp + '/jwcloud/user/addRecord',
            type: 'POST',
            data: {
                dogId:dogId,
                operateInfo: "搜索关键词  '" + searchKeyword.trim().replace('\\','\\\\').replace(/"/g,'\\"') + "'",
                operateState: 0,
                keyword:searchKeyword.trim().replace('\\','\\\\').replace(/"/g,'\\"').replace(/:/g, '\\:'),
                url:'&word='+(key.word?key.word.trim().replace(/"/g,'\\"'):'')+'&unison='+(key.unison?key.unison:'')+'&mate='+(key.mate?key.mate:'')+'&listCount=20&currentPage=1'
            },
            xhrFields:{
                withCredentials:true
            },
            success: function (data) {
                if (data.status == 0) {
                    window.location.href=skipUrl;
                }else{
                    layer(false,'您还没有登录，请点击“确定”返回登录页')
                }
            },
            error: function (err) {
                console.log(err);
            }
        })
    }
});
$('.mixr-nav').on('mouseenter','.have-data',function(){
    $(this).addClass('move-active');
});
$('.mixr-nav').on('mouseleave','.have-data',function(){
    $(this).removeClass('move-active');
});

$('.mixr-nav').on('click','.have-data',function(e){
    if($(this).attr('data')=='typeId5'){

    }else{
        if($(this).attr('class').indexOf('visit-active') < 0){
            $('.mixr-nav-list').removeClass('visit-active active');
            $(this).addClass('visit-active active');
        }else{
            $(this).removeClass('visit-active');
        }
    }
    e.stopPropagation();
});

$(document).on('click',function(){
    $('.mixr-nav-list').removeClass('visit-active');
    $('.search-only-btn div').hide();
    $('.search-only-btn p i').removeClass('down');
    $('.mixr-source-list-ul li em').removeClass('account-num-down');
    $('.account-num').next().hide();
    $('.other-status-container').hide();
});

var sourceStr;
var secondtypeId;
var firstTypeId;
$('.mixr-nav-list').on('click','.ishave',function(e) {
    e.stopPropagation();
    var firstClass=$(this).parent().parent().parent().find('.mixr-nav-title p:first').text();
    var secondClass=$(this).text().replace(/\(\d+\)$/g,'').toLowerCase();
    currentPage = 1;
    if($(this).attr('data')=='typeId21'){   //地图
        var id=$(this).attr('data').replace(/^typeId/, '');
        $.cookie('mapPath',JSON.stringify(key));
        getSearchResult(id,firstClass+secondClass);
    }else{
        firstTypeId=$(this).parent().parent().parent().attr('data');
        sourceStr=firstClass+secondClass;
        secondtypeId = $(this).attr('data').replace(/^typeId/, '');
        $('.mixr-less-list li').removeClass('on');
        $(this).addClass('on');
        $('.mixr-white-num b').text($(this).text().replace(/\(\d+\)$/g,''));

        $('.mixr-nav-list').removeClass('visit-active');
        clearindex();
        backindex();

        getSearchResult();
    }
});

//地图按钮特殊处理
$('.mixr-address').on('click',function(){
    $(this).find('.mixr-less-list .ishave').eq(0).trigger('click');
});

$.ajax({
    url:'http://'+lastIp+'/index.php',
    type:'POST',
    async: false,
    data:JSON.stringify({
        interface:'getType',
        act:'getAll',
        Token:token,
        username:loginName
    }),
    success:function(dataStr){
        var data =eval('('+dataStr+')');
        if(data.status==0){
            $('.load-box').show();
            var list=data.Item;
            var json={};
            for(var a=0;a<list.length;a++){
                var secondJson={};
                if(!!list[a].son){
                    for(var b=0;b<list[a].son.length;b++){
                        var arr=[];
                        for(var c=0;c<list[a].son[b].source.length;c++){
                            arr.push(list[a].son[b].source[c].sourceName.replace(/[,.\s]/g,''));
                        }
                        secondJson['typeId'+list[a].son[b].typeId]=arr;
                    }
                }
                json['typeId'+(list[a].typeId-1)]=secondJson;
            }
            secondSourceList=json;               //数据源下字段 等级
            for(var i=0;i<list.length;i++){
                if(!!list[i].son){
                    for(var j=0;j<list[i].son.length;j++){
                        var oLi= '<li data="typeId'+list[i].son[j].typeId+'">'+list[i].son[j].typeName+'(<span>0</span>)</li>';
                        $('.mixr-nav-list[data=typeId'+i+'] .mixr-less-list ul').append(oLi);
                    }
                }
                if(i == list.length-1){
                    getMateCount();
                }
            }
        }
    },
    error:function(err){
        console.log(err);
    }
});
function  getFromDate(){
    var viewDate={};
    var date=new Date();
    var y=date.getFullYear();
    var m=date.getMonth();
    var d=date.getDate();

    if(m+1-Number(key.date)>0){
        viewDate.from=y+'-'+toDou(m+1-Number(key.date))+'-'+toDou(d);
    }else{
        viewDate.from=y-1+'-'+toDou(m+13-Number(key.date))+'-'+toDou(d);
    }
    viewDate.to=y+'-'+toDou(m+1)+'-'+d;
    return viewDate;
}
function toDou(n){
    return n<10?'0'+n:''+n;
}


//test
// function getMateCount() {
//     $.each(secondSourceList, function (i, v) {
//         console.log('最外面循环');
//         var index = i;
//         var firstClass = $('.mixr-nav div[data=' + i + '] .mixr-nav-title p:first').text();
//
//         var allCount = 0;
//         $.each(v, function (k, n) {
//             console.log('循环');
//             $.ajax({
//                 url: 'test',
//                 type: 'POST',
//                 async: false,
//                 data: {
//                     key: k,
//                     value: v
//                 },
//                 success: function (dataStr) {
//                     console.log('里面循环');
//                     console.log(k);
//                     $('li[data=' + k + ']').parent().parent().parent().addClass('active');
                    // var data = {totalHits:2};
                    // console.log(k);
                    // $('li[data=' + k + '] span').text(data.totalHits);
                    // console.log(data.totalHits);
                    // allCount+=Number(data.totalHits);
                    // $('li[data=' + k + ']').addClass('ishave');
                    //
                    // $('.mixr-nav-list[data='+index+'] ').find('.mixr-nav-title span').text(allCount);
                    // if($('.ishave').length == 1){
                    //
                    //     $('.ishave:eq(0)').parent().parent().parent().addClass('active');
                    //     $('.mixr-less-list li').removeClass('on');
                    //     $('.ishave:eq(0)').addClass('on');
                    //     $('.mixr-white-num b').text($('.ishave:eq(0)').text().replace(/\(\d+\)$/g,''));
                    //
                    //     $('.mixr-nav-list').removeClass('visit-active');
                    //     $('.mixr-list').show();
                    //     $('.mixr-detail').hide();
                    //     $('.res-pages').show();
                    //     var firstClass=$('.ishave:eq(0)').parent().parent().parent().find('.mixr-nav-title p:first').text();
                    //     var secondClass=$('.ishave:eq(0)').text().replace(/\(\d+\)$/g,'').toLowerCase();
                    //     sourceStr=firstClass+secondClass;
                    //     secondtypeId = $('.ishave:eq(0)').attr('data').replace(/^typeId/, '');
                    //     currentPage = 1;
                    //     getSearchResult();
                    // }
//                 }
//             })
//         });
//
//     });
// }
//endtest

var clickNum=1;
function getMateCount(){   //获取匹配个数
    var data;
    var reqUrl;
    var reqNum=0;
    var reqNumCount=0;
    $.each(secondSourceList,function(i,v){
        $.each(v,function(j,k){
            if(k.length>0){
                reqNum++;
            }
        })
    });
    $.each(secondSourceList,function(i,v){
        var index=i;
        var firstClass=$('.mixr-nav div[data='+i+'] .mixr-nav-title p:first').text();
        var allCount=0;
            $.each(v,function(k,n) {
                var secondClass = $('.mixr-less-list ul li[data=' + k + ']').text().replace(/\(\d+\)$/g, '');
                if (n.length == 0) {

                } else {
                    ajaxFn(secondClass,k,v);
                }

            });
                function ajaxFn(secondClass,k,v){
                    if(!!key.word){
                        if (!!key.date) {
                            var date = getFromDate();
                            data ={
                                dogId:dogId,
                                "alias":firstClass+secondClass.toLowerCase(),
                                "text":keyWd,
                                "field":key.word,
                                "startDate":date.from,
                                "endDate":date.to
                            }
                        } else {
                            data = {
                                dogId:dogId,
                                "alias":firstClass+secondClass.toLowerCase(),
                                "text":keyWd,
                                "field":key.word,
                                "startDate":'',
                                "endDate":''
                            }
                        }
                    }else{
                        if (!!key.date) {
                            var date = getFromDate();
                            data ={
                                dogId:dogId,
                                "alias":firstClass+secondClass.toLowerCase(),
                                "text":keyWd,
                                "startDate":date.from,
                                "endDate":date.to,
                                "exclude":exclude
                            }
                        } else {
                            data = {
                                dogId:dogId,
                                "alias":firstClass+secondClass.toLowerCase(),
                                "text":keyWd,
                                "exclude":exclude,
                                "startDate":'',
                                "endDate":''
                            }
                        }
                    }

                    if (k == 'typeId18' || k == 'typeId19') {
                        if (!!key.word) {
                            if (keyWd.indexOf('?') != -1 || keyWd.indexOf('*') != -1) {     //报表指定字段搜索模糊匹配
                                reqUrl = 'html/fuzzyQuerySpecifiedFieldSearcCount';
                            } else {     //报表指定字段搜索精确匹配
                                reqUrl = 'html/preciseQuerySpecifiedFieldSearchCount';
                            }
                        } else {
                            if (keyWd.indexOf('?') != -1 || keyWd.indexOf('*') != -1) {  //报表全文搜索模糊匹配
                                reqUrl = 'html/fuzzyQueryFullTextSearchCount';
                            } else {   //报表全文搜索精确匹配
                                reqUrl = 'html/preciseQueryFullTextSearchCount';
                            }
                        }
                    }
                    else {
                        if(k == 'typeId21'){
                            if(!!key.word){
                                if(keyWd.indexOf('?') != -1 || keyWd.indexOf('*') !=-1) {  //指定字段模糊匹配
                                    reqUrl = 'map/fuzzyQuerySpecifiedFieldSearchCount';
                                }else{  //指定字段精确匹配
                                    if(key.unison != 0){
                                        reqUrl = 'map/homophonesQuerySpecifiedFieldSearchCount';
                                    }else{
                                        reqUrl = 'map/preciseQuerySpecifiedFieldSearchCount';
                                    }

                                }
                            }else{
                                if(keyWd.indexOf('?') != -1 || keyWd.indexOf('*') !=-1) {  //全文搜索模糊匹配
                                    reqUrl = 'map/fuzzyQueryFullTextSearchCount';
                                }else{  //全文搜索精确匹配
                                    reqUrl = 'map/preciseQueryFullTextSearchCount';
                                }
                            }
                        }else{
                            if(!!key.word){
                                if(keyWd.indexOf('?') != -1 || keyWd.indexOf('*') !=-1) {  //指定字段模糊匹配
                                    reqUrl = 'file/fuzzyQuerySpecifiedFieldSearchCount';
                                }else{  //指定字段精确匹配
                                    if(key.unison != 0){
                                        reqUrl = 'file/homophonesQuerySpecifiedFieldSearchCount';
                                    }else{
                                        reqUrl = 'file/preciseQuerySpecifiedFieldSearchCount';
                                    }

                                }
                            }else{
                                if(keyWd.indexOf('?') != -1 || keyWd.indexOf('*') !=-1) {  //全文搜索模糊匹配
                                    reqUrl = 'file/fuzzyQueryFullTextSearchCount';
                                }else{  //全文搜索精确匹配
                                    reqUrl = 'file/preciseQueryFullTextSearchCount';
                                }
                            }
                        }

                    }
                    $.ajax({
                        url: 'http://' + newIp + '/jwcloud/'+reqUrl,
                        type: 'POST',
                        data: data,
                        xhrFields:{
                            withCredentials:true
                        }
                    })
                    .done(function (data) {
                        if(data.status == 0){
                            reqNumCount++;
                            if(data.totalHits != 0){
                                $('li[data=' + k + '] span').text(data.totalHits);
                                allCount+=Number(data.totalHits);
                                $('li[data=' + k + ']').addClass('ishave');
                                $('.mixr-nav-list[data='+index+']').find('.mixr-nav-title span').text(allCount);

                                if($('.ishave').length == clickNum){
                                    if($('.ishave:eq(0)').attr('data')!='typeId21'){
                                        $('.ishave:eq(0)').trigger('click');
                                        $('.ishave:eq(0)').parent().parent().parent().addClass('active');
                                    }else{
                                        clickNum=2;
                                        $('.ishave:eq(1)').trigger('click');
                                    }
                                }
                                $('.mixr-nav-list[data='+index+']').removeClass('mixr-nohave').addClass('have-data');
                                $('.mixr-source-list h2').remove();
                            }
                            if(reqNum==reqNumCount){
                                if($('.ishave').length == 0){
                                    $('.mixr-source-list h2').remove();
                                    $('.mixr-source-list').append('<h2 style="font-size:20px;">没有搜索结果</h2>');
                                    showindex();
                                }
                            }
                        }else{
                            layer(false,'您还没有登录，请点击“确定”返回登录页');
                        }
                    })
                    .fail(function (err) {
                        console.log(err);
                    });

                }

    });

}

//时间筛选
if(key.date == 1){
    $('.res-option a:eq(0)').addClass('on');
}else if(key.date == 3){
    $('.res-option a:eq(1)').addClass('on');
}else if(key.date == 12){
    $('.res-option a:eq(2)').addClass('on');
}
$('.res-option a:eq(0)').on('click',function(){
    if(!!key.date){
        var href = window.location.href;
        href=href.replace(/&date=\d+$/g,'');
        window.location.href=href+'&date=1';
    }else{
        window.location.href=window.location+'&date=1';
    }
});
$('.res-option a:eq(1)').on('click',function(){
    if(!!key.date){
        var href = window.location.href;
        href=href.replace(/&date=\d+$/g,'');
        window.location.href=href+'&date=3';
    }else{
        window.location.href=window.location+'&date=3';
    }
});
$('.res-option a:eq(2)').on('click',function(){
    if(!!key.date){
        var href = window.location.href;
        href=href.replace(/&date=\d+$/g,'');
        window.location.href=href+'&date=12';
    }else{
        window.location.href=window.location+'&date=12';
    }
});


function getSearchResult(id,source){
    var data;
    var reqUrl;
    if(!!key.word){
        if (!!key.date) {
            var date = getFromDate();
            data ={
                dogId:dogId,
                "alias":source?source:sourceStr,
                "text":keyWd,
                "page":currentPage,
                "field":key.word,
                "startDate":date.from,
                "endDate":date.to,
                "type":''
            }
        } else {
            data = {
                dogId:dogId,
                "alias":source?source:sourceStr,
                "text":keyWd,
                "page":currentPage,
                "field":key.word,
                "startDate":'',
                "endDate":'',
                "type":''
            }
        }
    }else{
        if (!!key.date) {
            var date = getFromDate();
            data ={
                dogId:dogId,
                "alias":source?source:sourceStr,
                "text":keyWd,
                "page":currentPage,
                "startDate":date.from,
                "endDate":date.to,
                "exclude":exclude,
                "type":''
            }
        } else {
            data = {
                dogId:dogId,
                "alias":source?source:sourceStr,
                "text":keyWd,
                "page":currentPage,
                "exclude":exclude,
                "startDate":'',
                "endDate":'',
                "type":''
            }
        }
    }
    if(!!id){
        if (!!key.word) {
            if(keyWd.indexOf('?') != -1 || keyWd.indexOf('*') !=-1) {  //指定字段搜索模糊匹配
                reqUrl = 'map/fuzzyQuerySpecifiedFieldSearch';
            }else{          //指定字段搜索精确匹配
                if(key.unison != 0){
                    reqUrl = 'map/homophonesQuerySpecifiedFieldSearch';
                }else{
                    reqUrl = 'map/preciseQuerySpecifiedFieldSearch';
                }

            }
        } else {
            if(keyWd.indexOf('?') !=-1 || keyWd.indexOf('*') != -1) { //全文搜索模糊匹配
                reqUrl = 'map/fuzzyQueryFullTextSearch';
            }else{  //全文搜索精确匹配
                reqUrl = 'map/preciseQueryFullTextSearch';
            }
        }
    }else {
        if (secondtypeId == 18 || secondtypeId == 19) {
            if (!!key.word) {
                if (keyWd.indexOf('?') != -1 || keyWd.indexOf('*') != -1) { //报表指定字段搜索模糊匹配
                    reqUrl = 'html/fuzzyQuerySpecifiedFieldSearch';
                } else {  //报表指定字段搜索精确匹配
                    reqUrl = 'html/preciseQuerySpecifiedFieldSearch';
                }
            } else {
                if (key.wd.indexOf('?') != -1 || key.wd.indexOf('*') != -1) {  //报表全文搜索模糊搜索
                    reqUrl = 'html/fuzzyQueryFullTextSearch';
                } else {  //报表全文搜索精确匹配
                    reqUrl = 'html/preciseQueryFullTextSearch';
                }
            }
        } else {
            if (!!key.word) {
                if (keyWd.indexOf('?') != -1 || keyWd.indexOf('*') != -1) {  //指定字段搜索模糊匹配
                    reqUrl = 'file/fuzzyQuerySpecifiedFieldSearch';
                } else {          //指定字段搜索精确匹配
                    if (key.unison != 0) {
                        reqUrl = 'file/homophonesQuerySpecifiedFieldSearch';
                    } else {
                        reqUrl = 'file/preciseQuerySpecifiedFieldSearch';
                    }

                }
            } else {
                if (keyWd.indexOf('?') != -1 || keyWd.indexOf('*') != -1) { //全文搜索模糊匹配
                    reqUrl = 'file/fuzzyQueryFullTextSearch';
                } else {  //全文搜索精确匹配
                    reqUrl = 'file/preciseQueryFullTextSearch';
                }
            }

        }
    }
    if(!!id){
        window.localStorage.setItem('mapData',JSON.stringify({
            url:'http://' + newIp + '/jwcloud/'+reqUrl,
            type:'POST',
            async: false,
            data:data,
            xhrFields:{
                withCredentials:true
            }
        }));
        window.open('/map/');

    }else{
        $.ajax({
            url:'http://' + newIp + '/jwcloud/'+reqUrl,
            type:'POST',
            data:data,
            xhrFields:{
                withCredentials:true
            },
            success:function(data){
                var dataList=data.data;
                pages = data.totalPages;
                $('.mixr-source-list').html('');
                var dataArr=[];
                for(var i=0;i<dataList.length;i++){
                    var dataJson={};

                    var hits = dataList[i].hits;
                    var total = dataList[i].total;
                    var hitsArr=[];
                    for(var j=0;j<hits.length;j++){
                        var formName='';
                        var uuid='';
                        var hitsJson={};

                        var sourceArr=[];

                        $.each(hits[j]._source,function(i,v){
                            var json={};
                            if(i=='上传时间' || i=='展示时间' || i=='flag' || i=='statementName' || i=='htmlNum' || i=='uploadDate'){
                                return;
                            }else{
                                json.field=i;
                                json.value=v;
                            }
                            sourceArr.push(json);
                        });
                        hitsJson.highLight=hits[j].highlight;
                        hitsJson.fieldList=sourceArr;
                        hitsJson.sourceName=hits[j]._type;
                        for(var n=0;n<sourceArr.length;n++){
                            if(sourceArr[n].field=='报表名称'){
                                formName=sourceArr[n].value;
                            }
                            if(sourceArr[n].field=='uuid'){
                                uuid=sourceArr[n].value;
                            }
                        }
                        hitsJson.formName=formName;
                        hitsJson.uuid=uuid;
                        hitsArr.push(hitsJson);
                    }
                    dataJson.data=hitsArr;
                    dataJson.type=dataList[i].type;
                    dataJson.total=total;

                    dataArr.push(dataJson);
                }
                setLevel(dataArr,'list');
            },
            error:function(err){
                console.log(err);
            }
        });
    }


}
function getSearchTypeResult(sourceName){   //获取带有数据源名称的结果列表
    var data;
    var reqUrl;
    if(!!key.word){
        if (!!key.date) {
            var date = getFromDate();
            data ={
                dogId:dogId,
                "alias":sourceStr,
                "text":keyWd,
                "page":currentTypePage,
                "type":sourceName,
                "field":key.word,
                "startDate":date.from,
                "endDate":date.to
            }
        } else {
            data = {
                dogId:dogId,
                "alias":sourceStr,
                "text":keyWd,
                "page":currentTypePage,
                "type":sourceName,
                "field":key.word,
                "startDate":'',
                "endDate":''
            }
        }
    }else{
        if (!!key.date) {
            var date = getFromDate();
            data ={
                dogId:dogId,
                "alias":sourceStr,
                "text":keyWd,
                "page":currentTypePage,
                "type":sourceName,
                "startDate":date.from,
                "endDate":date.to,
                "exclude":exclude
            }
        } else {
            data = {
                dogId:dogId,
                "alias":sourceStr,
                "text":keyWd,
                "page":currentTypePage,
                "type":sourceName,
                "startDate":'',
                "endDate":'',
                "exclude":exclude
            }
        }
    }

    if(secondtypeId==18 || secondtypeId==19){
        if(!!key.word){
            if(keyWd.indexOf('?') != -1 || keyWd.indexOf('*') !=-1) { //报表指定字段搜索模糊匹配
                reqUrl = 'html/fuzzyQuerySpecifiedFieldSearch';
            }else{  //报表指定字段搜索精确匹配
                reqUrl = 'html/preciseQuerySpecifiedFieldSearch';
            }
        }else{
            if(key.wd.indexOf('?') != -1 || key.wd.indexOf('*') != -1){  //报表全文搜索模糊搜索
                reqUrl = 'html/fuzzyQueryFullTextSearch';
            }else{  //报表全文搜索精确匹配
                reqUrl = 'html/preciseQueryFullTextSearch';
            }
        }
    }else {
        if (!!key.word) {
            if(keyWd.indexOf('?') != -1 || keyWd.indexOf('*') !=-1) {  //指定字段搜索模糊匹配
                reqUrl = 'file/fuzzyQuerySpecifiedFieldSearch';
            }else{          //指定字段搜索精确匹配
                if(key.unison != 0){
                    reqUrl = 'file/homophonesQuerySpecifiedFieldSearch';  //同音
                }else{
                    reqUrl = 'file/preciseQuerySpecifiedFieldSearch ';
                }
            }
        } else {
            if(keyWd.indexOf('?') !=-1 || keyWd.indexOf('*') != -1) { //全文搜索模糊匹配
                reqUrl = 'file/fuzzyQueryFullTextSearch';
            }else{  //全文搜索精确匹配
                reqUrl = 'file/preciseQueryFullTextSearch';
            }
        }
    }
    if(secondtypeId==21){
        window.localStorage.setItem('mapData',JSON.stringify({
            url:'http://' + newIp + '/jwcloud/'+reqUrl,
            type:'POST',
            async: false,
            data:data,
            xhrFields:{
                withCredentials:true
            }
        }));
        window.open('/map/');
    }else{
        $.ajax({
            url:'http://' + newIp + '/jwcloud/'+reqUrl,
            type:'POST',
            data:data,
            xhrFields:{
                withCredentials:true
            },
            success:function(data){
                var dataList=data.hits;
                if(data.privacy != 0){
                    $('.mixr-white-num').show();
                    $('.mixr-white-num').html('由于隐私,数据源"<b>'+sourceName+'</b>"隐藏了 <span>'+data.privacy+'</span> 条信息');
                }
                // else{
                //     $('.mixr-white-num').html('');
                //     $('.mixr-white-num').hide();
                // }
                var dataArr=[];
                for(var i=0;i<dataList.length;i++){
                    var dataJson={};
                    var sourceArr=[];
                    var sourceList=dataList[i]._source;
                    var highlight=dataList[i].highlight?dataList[i].highlight:{};
                    $.each(sourceList,function(i,v){
                        var json={};
                        if(i=='上传时间' || i=='展示时间' || i=='flag' || i=='statementName' || i=='uploadDate' || i=='htmlNum'){
                            return;
                        }else{
                            json.field=i;
                            json.value=v;
                            sourceArr.push(json);
                        }
                    });
                    dataJson.sourceName=dataList[i]._type.replace(/-\d+$/,'');
                    dataJson.fieldList=sourceArr;
                    dataJson.highLight=highlight;
                    dataArr.push(dataJson);
                }
                typePages=data.totalPages;
                // console.log(dataArr);

                setLevel(dataArr,'typelist');
            },
            error:function(err){
                console.log(err);
            }
        });
    }
}
var objToString = Object.prototype.toString;
function isArray(value) {
    return objToString.call(value) === '[object Array]';
}
function setLevel(dataArr,type){   //请求字段登记进行排序
    $.ajax({
        url:'http://'+lastIp+'/index.php',
        type:'POST',
        async: false,
        data:JSON.stringify({
            interface:'getSource',
            act:'getSourceTypeField',
            typeId:secondtypeId,
            Token:token,
            username:loginName
        }),
        success:function(dataStr){
            var data=eval('('+dataStr+')');
            if(data.status==0){
                var sourceJson={};
                var sourceList=data.Item;
                for(var i=0;i<sourceList.length;i++){
                    var fieldList=sourceList[i].field.field;
                    var sourceNameS=sourceList[i].sourceName.replace(/[,.\s]/g,'');
                    sourceJson[sourceNameS]={};
                    for(var j=0;j<fieldList.length;j++){
                        // sourceJson[sourceNameS][fieldList[j].fieldName]={};
                        // sourceJson[sourceNameS][fieldList[j].fieldName].level=fieldList[j].level;
                        sourceJson[sourceNameS][fieldList[j].fieldName]=fieldList[j].level;
                        // sourceJson[sourceNameS][fieldList[j].fieldName].name=fieldList[j].name;
                    }
                }
                if(type=='list'){
                    for(var h=0;h<dataArr.length;h++){
                        eachDataArr(dataArr[h].data);
                    }
                }
                else if(type == 'typelist'){
                    eachDataArr(dataArr);
                }
                function eachDataArr(dataArr){
                    for(var k=0;k<dataArr.length;k++){
                        var fieldList = dataArr[k].fieldList;
                        var highLight = dataArr[k].highLight;
                        var sourceNameK=dataArr[k].sourceName.replace(/[,.\s]/g,'');
                        for(var n=0;n<fieldList.length;n++){
                            if(!!sourceJson[sourceNameK]){
                                if(sourceJson[sourceNameK][fieldList[n].field]==2){  //把两星的字段放前面
                                    var fieldJson=fieldList.splice(n,1);
                                    fieldList=fieldJson.concat(dataArr[k].fieldList);
                                    dataArr[k].fieldList=fieldList;
                                }
                                if(n==fieldList.length-1){
                                    for(var b=0;b<fieldList.length;b++){
                                        if(sourceJson[sourceNameK][fieldList[b].field]==3){  //把三星的字段放前面
                                            var fieldJson=fieldList.splice(b,1);
                                            fieldList=fieldJson.concat(dataArr[k].fieldList);
                                            dataArr[k].fieldList=fieldList;
                                        }
                                        if(b==fieldList.length-1){  //把指定字段或匹配到的放前面
                                            if(!!isSelf(fieldList)){  //判断报表来源，false是没有报表来源，true分为睿海和其他
                                                var a = keyWd.replace(/\\/,'\\\\').replace(/\./g,'\\.').replace(/\+/g,'\\+').replace(/\$/g,'\\$').replace(/\^/g,'\\^').replace(/\[/g,'\\[').replace(/\]/g,'\\]').replace(/\(/g,'\\(').replace(/\)/g,'\\)').replace(/\{/g,'\\{').replace(/\}/g,'\\}').replace(/\|/g,'\\|').replace(/\*/g,'\\S*').replace(/\?/g,'\\S{1}');
                                                var reg = new RegExp(a,'img');
                                                for(var g=0;g<fieldList.length;g++){
                                                    if(typeof fieldList[g].value =='string' || fieldList[g].value==null){

                                                        if(!!reg.test(fieldList[g].value)){
                                                            // console.log(dataArr[k].fieldList);
                                                            var fieldJson=fieldList.splice(g,1);
                                                            fieldList=fieldJson.concat(dataArr[k].fieldList);
                                                            dataArr[k].fieldList=fieldList;
                                                            // console.log(dataArr[k].fieldList);
                                                        }
                                                    }else if(typeof fieldList[g].value =='object' && fieldList[g].value !=null){
                                                        var valueArr=fieldList[g].value;
                                                        for(var e=0;e<valueArr.length;e++){
                                                            if(!!reg.test(valueArr[e])){
                                                                var fieldJson=fieldList.splice(g,1);
                                                                fieldList=fieldJson.concat(dataArr[k].fieldList);
                                                                dataArr[k].fieldList=fieldList;
                                                                break;
                                                            }
                                                        }
                                                    }
                                                }
                                            }else{
                                                if(!!key.word){      //如果有指定字段
                                                    if(key.unison==0){
                                                        $.each(wordArr,function(i,v){
                                                            for(var b=0;b<fieldList.length;b++){
                                                                if(wordArr[i]==fieldList[b].field && !!checkWd(fieldList[b].value)){      //把匹配到的字段放前
                                                                    var fieldJson=fieldList.splice(b,1);
                                                                    fieldList=fieldJson.concat(dataArr[k].fieldList);
                                                                    dataArr[k].fieldList=fieldList;
                                                                }
                                                            }
                                                        });
                                                    }else{        // 指定字段 同音查询
                                                        $.each(wordArr,function(i,v){
                                                            for(var b=0;b<fieldList.length;b++){
                                                                if(wordArr[i]==fieldList[b].field){      //把匹配到的字段放前
                                                                    var fieldJson=fieldList.splice(b,1);
                                                                    fieldJson[0].isUnique=1;
                                                                    fieldList=fieldJson.concat(dataArr[k].fieldList);
                                                                    dataArr[k].fieldList=fieldList;
                                                                }
                                                                if(b==fieldList.length-1){
                                                                    for(c=0;c<fieldList.length;c++){

                                                                        if(!!fieldList[c].isUnique){
                                                                            $.each(highLight,function(index,value){
                                                                                for(var f=0;f<value.length;f++){
                                                                                    if(fieldList[c].value.indexOf(value[f])!=-1){
                                                                                        var fieldJson=fieldList.splice(c,1);
                                                                                        fieldList=fieldJson.concat(dataArr[k].fieldList);
                                                                                        dataArr[k].fieldList=fieldList;
                                                                                    }
                                                                                }
                                                                            })

                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        });
                                                        $.each(highLight,function(i,v){
                                                            for(var b=0;b<fieldList.length;b++){
                                                                if(fieldList[b].field == i ){
                                                                    var fieldJson=fieldList.splice(b,1);
                                                                    fieldList=fieldJson.concat(dataArr[k].fieldList);
                                                                    dataArr[k].fieldList=fieldList;
                                                                }
                                                            }
                                                        })
                                                    }
                                                }else{                 //没字段  查询
                                                    for(var c=0;c<fieldList.length;c++){
                                                        if(!!checkWd(fieldList[c].value)){   //把匹配到的字段放前
                                                            var fieldJson=fieldList.splice(c,1);
                                                            fieldList=fieldJson.concat(dataArr[k].fieldList);
                                                            dataArr[k].fieldList=fieldList;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if(type=='list'){
                    for(var i=0;i<dataArr.length;i++){
                        sourceDataList[dataArr[i].type]=dataArr[i].data
                    }
                    setIndexPage(dataArr);//首页列表
                }else{
                    dataListArr=dataArr;
                    setPage(dataArr);  //数据源结果列表
                }
            }else{

            }
        },
        error:function(err){
            console.log(err);
        }
    })
}


function findArrSame(a,n){
    if(typeof(a)=='string'){
        if(a==n){
            return true;
        }else{
            return false;
        }
    }else{
        for(var i=0;i<a.length;i++){
            if(a[i]==n) {
                return true;
            }
        }
        return false;
    }
}
function isSelf(n){
    for(var i=0;i<n.length;i++){
        if(n[i].field=='报表来源'){
            if(n[i].value == '睿海'){
                return '睿海';
            }else if(n[i].value == '其他厂商'){
                return '其他厂商';
            }
        }
    }
    return false;
}

var re;

function checkWd(n){
    if(n){
        if(keyWd.indexOf('?')!=-1 || keyWd.indexOf('*')!=-1){
            var wdArr = keyWd.split('?');

            var regStr='';
            var regStr2='';
            for(var i=0;i<wdArr.length;i++){
                regStr+=wdArr[i]+'\\S{1}';
            }
            regStr=regStr.substring(0,regStr.length-5);
            if(regStr.indexOf('*')!=-1){
                var wdArr2 = regStr.split('*');
                for(var i=0;i<wdArr2.length;i++){
                    regStr2 += wdArr2[i]+'\\S*';
                }
                regStr2=regStr2.substring(0,regStr2.length-3);
            }else{
                regStr2=regStr;
            }
            re=new RegExp(regStr2,'img');
        }
        // else if(key.wd.indexOf('*')!=-1){
        //
        //     var regStr='';
        //     for(var i=0;i<wdArr.length;i++){
        //         regStr += wdArr[i]+'\\S*';
        //     }
        //     regStr=regStr.substring(0,regStr.length-3);
        //     re=new RegExp(regStr,'img');
        // }
        else{
            var key;
            key=keyWd.replace(/\*/g,'\\*').replace(/\./g,'\\.').replace(/\?/g,'\\?').replace(/\+/g,'\\+').replace(/\$/g,'\\$').replace(/\^/g,'\\^').replace(/\[/g,'\\[').replace(/\]/g,'\\]').replace(/\(/g,'\\(').replace(/\)/g,'\\)').replace(/\{/g,'\\{').replace(/\}/g,'\\}').replace(/\|/g,'\\|');
            re=new RegExp(key,'img');
        }
        return n.match(re) ;
    }else{
        return false;
    }
}
function backindex(){
    $('.mixr-list ul').html('');
    $('.mixr-list').hide();
    $('.mixr-detail-ul').html('');
    $('.mixr-detail-ol').html('');
    $('.mixr-detail').hide();
    $('.res-pages').hide();
    $('.mixr-source-list').show();
    $('.res-source-pages').show(); //加载完显示
    $('.mixr-white-num').html('');
    $('.mixr-white-num').hide();
}
function clearindex(){
    $('.load-box').show();
    $('.mixr-source-list').html('');
    $('.res-source-pages').html('');
}
function showindex(){
    $('.load-box').hide();
    $('.mixr-source-list').show();
    $('.res-source-pages').show();
}
function toSecondPage(){
    $('.mixr-list').show();
    $('.mixr-detail-ul').html('');
    $('.mixr-detail-ol').html('');
    $('.mixr-detail').hide();
    $('.res-pages').show();
    $('.mixr-source-list').hide();
    $('.res-source-pages').hide();
    $('.mixr-white-num').show();
}
function clearSecondPage(){
    $('.load-box').show();
    $('.mixr-list ul').html('');
    $('.res-pages').html('');
    $('.mixr-list h2').hide();
}
function showSecondPage(){
    $('.load-box').hide();
    $('.res-pages').show();
    $('.mixr-list').show();
    $('.mixr-list h2').show();
}
function toDetailPage(){
    $('.mixr-list').hide();
    $('.mixr-detail').show();
    $('.res-pages').hide();
    $('.mixr-source-list').hide();
    $('.res-source-pages').hide();
    $('.mixr-white-num').hide();
}
var cloneObj = function(obj){
    var str, newobj = obj.constructor === Array ? [] : {};
    if(typeof obj !== 'object'){
        return;
    } else if(window.JSON){
        str = JSON.stringify(obj);//系列化对象
        newobj = JSON.parse(str); //还原
    } else {
        for(var i in obj){
            newobj[i] = typeof obj[i] === 'object' ?
                cloneObj(obj[i]) : obj[i];
        }
    }
    return newobj;
};
function setIndexPage(datat){
    var list = cloneObj(datat);
    backindex();
    // $('.mixr-source-list').html('').show();
    for(var c=0;c<list.length;c++){
        var sourceName=list[c].type;
        var oDiv='';
        var viewAll='';
        if(list[c].total>3){
            viewAll='<a href="javascript:;" data="'+sourceName+'" class="mixr-link" onclick="viewSourceAll(this)">查看全部<span></span></a>';
        }
        oDiv += '<p class="clearfix mixr-source-list-title"><span class="fl">'+sourceName+'</span>'+viewAll+'</p>';
        var data = list[c].data;
        var datastr='';
        for(var j=0;j<data.length;j++){
            var listArr=[];
            listArr = data[j].fieldList;
            var highJson=data[j].highLight;
            var sourceName=data[j].sourceName;
            var formName=data[j].formName;
            var uuid;
            var ulLi='';
            var aLink='javascript:void(0);';
            var isBlank='';
            var aData='';
            var aRhForm='';
            var otherViewNum=0;
            var viewOtherNum=0;
            var addS=true;
            for(var t=0;t<listArr.length;t++){
                if(listArr[t].field =='报表来源'){
                    otherViewNum++;
                }else if(listArr[t].field =='展示路径'){
                    otherViewNum++;
                }else if(listArr[t].field == 'flag'){
                    otherViewNum++;
                }else if(listArr[t].field == 'uuid'){
                    uuid = listArr[t].value;
                    otherViewNum++;
                }
                if(listArr[t].field == '二维码'){
                    listArr.splice(t,1);
                }
            }
            if((listArr.length-otherViewNum)<6){
                addS = false;
            }else{
                addS=true;
            }
            for(var k=0;k<6+viewOtherNum;k++) {
                if(!!listArr[k] || !$.isEmptyObject(highJson)){
                    var value = listArr[k]?listArr[k].value:'';
                    var keyField = listArr[k]?listArr[k].field:'';
                    var logoRh = '';
                    if (!!isSelf(listArr)) {
                        var a=keyWd.replace(/\\/,'\\\\').replace(/\./g,'\\.').replace(/\+/g,'\\+').replace(/\$/g,'\\$').replace(/\^/g,'\\^').replace(/\[/g,'\\[').replace(/\]/g,'\\]').replace(/\(/g,'\\(').replace(/\)/g,'\\)').replace(/\{/g,'\\{').replace(/\}/g,'\\}').replace(/\|/g,'\\|').replace(/\*/g,'\\S*').replace(/\?/g,'\\S{1}');
                        var re=new RegExp('('+a+')','img');
                        logoRh = '<span class="ruihai-logo"></span>';
                        if ($.isEmptyObject(highJson)) {
                            if (!!listArr[k]) {
                                if (listArr[k].field == '展示路径') {
                                    viewOtherNum++;
                                } else if (listArr[k].field == '报表来源') {
                                    viewOtherNum++;
                                }else if(listArr[k].field == 'flag'){
                                    viewOtherNum++;
                                }else if(listArr[k].field == 'uuid'){
                                    viewOtherNum++;
                                }
                                if (addS && ((k+ viewOtherNum) == (Number(5) + viewOtherNum+viewOtherNum))) {
                                    ulLi += '<li class="mixr-list-last">其他虚拟身份: <em class="other-status-btn">( ' + (listArr.length - 5 - otherViewNum) + ' )</em></li>';
                                } else {
                                    if (typeof listArr[k].value == 'string') {
                                        if (re.test(listArr[k].value)) {
                                            value = listArr[k].value.replace(reg, '<span class="res-high-light" title="' + listArr[k].value + '">' + listArr[k].value + '</span>');
                                        }
                                    } else if (typeof listArr[k].value == 'object') {
                                        var valueArr = listArr[k].value;
                                        var valueArrLength = 0;
                                        var oList = '';
                                        var oDownSelect = '';
                                        for (var i = 0; i < valueArr.length; i++) {
                                            if (!!valueArr[i] && valueArr[i] != '无') {
                                                valueArrLength++;
                                                if (re.test(valueArr[i])) {
                                                    var value=valueArr[i].replace(re,'<span class="res-high-light">$1</span>');
                                                    oList += '<em title="' + valueArr[i] + '" ><a href="/mixresult?wd=' + valueArr[i] + '&word=' + (key.word ? key.word : '') + '&unison=' + key.unison + '&listCount=20&currentPage=1" target="_blank">' + value + '</a></em>';
                                                } else {
                                                    oList += '<em title="' + valueArr[i] + '"> <a href="/mixresult?wd=' + valueArr[i] + '&word=' + (key.word ? key.word : '') + '&unison=' + key.unison + '&listCount=20&currentPage=1" target="_blank">' + valueArr[i] + '</a></em>';
                                                }
                                            }
                                        }
                                        oDownSelect = '<div class="down-select">' + oList + '</div>';
                                        valueArrLength = '<em class="account-num">( ' + valueArrLength + ' )</em>';
                                        if (valueArrLength == '<em class="account-num">( 1 )</em>'){
                                            valueArrLength = oList;
                                            oDownSelect = '';
                                        }
                                        value = valueArrLength + oDownSelect;
                                    }

                                    if (listArr[k].field != '报表来源' && listArr[k].field != '展示路径' && listArr[k].field != 'flag' && listArr[k].field != 'uuid' && listArr[k].field != '二维码'){
                                        ulLi += '<li>' + listArr[k].field + ':' + value + '</li>';
                                    }

                                }
                            }
                        } else {
                            if (!!highJson.内容[k]) {
                                for (var i = 0; i < listArr.length; i++) {
                                    if (listArr[i].field == '展示路径') {
                                        aData = 'data="' + listArr[i].value + '"';
                                        aLink = 'searchForm?wd=' + keyWd + '&word=' + (key.word ? key.word : '') + '&unison=' + key.unison;
                                    }
                                }
                                isBlank = 'target="_blank"';
                                if (k == 3) {
                                    ulLi += '<li style="max-width: 224px;">' + (highJson.内容[k]) + '</li>';
                                } else {
                                    ulLi += '<li style="width: 270px;margin-right:8px;">' + (highJson.内容[k]) + '</li>';
                                }
                            }
                        }
                    }
                    // else if (isSelf(listArr) == '其他厂商') {
                    //     if (!!highJson.内容[k]) {
                    //         for (var i = 0; i < listArr.length; i++) {
                    //             if (listArr[i].field == '展示路径') {
                    //                 aData = 'data="' + listArr[i].value + '"';
                    //                 aLink = 'searchForm?wd=' + keyWd + '&word=' + (key.word ? key.word : '') + '&unison=' + key.unison;
                    //             }
                    //         }
                    //         isBlank = 'target="_blank"';
                    //         if (k == 3) {
                    //             ulLi += '<li style="max-width: 224px;">' + highJson.内容[k]+ '</li>';
                    //         } else {
                    //             ulLi += '<li style="width: 270px;margin-right:8px;">' + highJson.内容[k] + '</li>';
                    //         }
                    //     }
                    // }
                    else {
                        if (!!listArr[k]) {
                            if (k == 5) {
                                ulLi += '<li class="mixr-list-last">' + listArr[k].field + ':' + listArr[k].value + '</li>';
                            } else {
                                if (!!key.word) {
                                    if (findArrSame(wordArr, keyField)) {
                                        if (key.unison == 0) {   //如果不是同音正常流程
                                            var regArr = checkWd(value);
                                            if (!!regArr) {
                                                for (var i = 0; i < regArr.length; i++) {
                                                    var reg = new RegExp(regArr[i], 'img');
                                                    value = value.replace(reg, '<span class="res-high-light">' + regArr[i] + '</span>');
                                                }
                                            } else {

                                            }
                                        } else {    //如果是同音
                                            $.each(highJson, function (e, v) {
                                                for (var i = 0; i < v.length; i++) {
                                                    if (e == keyField || value.indexOf(v[i]) != -1) {
                                                        var a = v[i].replace(/\*/g,'\\*').replace(/\./g,'\\.').replace(/\?/g,'\\?').replace(/\+/g,'\\+').replace(/\$/g,'\\$').replace(/\^/g,'\\^').replace(/\[/g,'\\[').replace(/\]/g,'\\]').replace(/\(/g,'\\(').replace(/\)/g,'\\)').replace(/\{/g,'\\{').replace(/\}/g,'\\}').replace(/\|/g,'\\|');
                                                        var reg = new RegExp(a, 'img');
                                                        value = value.replace(reg, '<span class="res-high-light">' + v[i] + '</span>');
                                                    }
                                                }

                                            })
                                        }
                                    }
                                } else {
                                    if (!!checkWd(value)) {
                                        var regArr = checkWd(value);
                                        for (var i = 0; i < regArr.length; i++) {
                                            var regStr=regArr[i].replace(/\*/g,'\\*').replace(/\./g,'\\.').replace(/\?/g,'\\?').replace(/\+/g,'\\+').replace(/\$/g,'\\$').replace(/\^/g,'\\^').replace(/\[/g,'\\[').replace(/\]/g,'\\]').replace(/\(/g,'\\(').replace(/\)/g,'\\)').replace(/\{/g,'\\{').replace(/\}/g,'\\}').replace(/\|/g,'\\|');
                                            var reg = new RegExp(regStr, 'img');
                                            value = value.replace(reg, '<span class="res-high-light">' + regArr[i] + '</span>');
                                        }
                                    }
                                }
                                ulLi += '<li>' + listArr[k].field + ':' + value + '</li>';
                            }
                        }
                    }
                }
            }
            var otherStatusList='';
            if(isSelf(listArr)=='睿海'){
                var a=keyWd.replace(/\\/,'\\\\').replace(/\./g,'\\.').replace(/\+/g,'\\+').replace(/\$/g,'\\$').replace(/\^/g,'\\^').replace(/\[/g,'\\[').replace(/\]/g,'\\]').replace(/\(/g,'\\(').replace(/\)/g,'\\)').replace(/\{/g,'\\{').replace(/\}/g,'\\}').replace(/\|/g,'\\|').replace(/\*/g,'\\S*').replace(/\?/g,'\\S{1}');
                var re=new RegExp('('+a+')','img');
                if($.isEmptyObject(highJson)){
                    var otherList='';
                    for(var e=(Number(5)+viewOtherNum);e<listArr.length;e++){
                        if (typeof listArr[e].value == 'string') {
                            if(listArr[e].field !='报表来源' && listArr[e].field !='reportFlag' && listArr[e].field !='展示路径'){
                                var value=listArr[e].value.replace(re,'<span class="res-high-light">$1</span>');
                                otherList+='<div class="clearfix"><div class="fl"><b>'+listArr[e].field+'</b> : </div><div class="fl"><p title="'+listArr[e].value+'">'+value+'</p></div></div>'+(e==listArr.length-1?'':'<hr />');
                            }
                        } else if (typeof listArr[e].value == 'object') {
                            var valueArr = listArr[e].value;
                            var valueList='';
                            for(var t=0;t<valueArr.length;t++){
                                if(!!valueArr[t] && valueArr[t] != '无'){
                                    var value=valueArr[t].replace(re,'<span class="res-high-light">$1</span>');

                                    valueList+='<p title="'+valueArr[t]+'">'+value+'</p>';
                                }
                            }
                            otherList+='<div class="clearfix"><div class="fl"><b>'+listArr[e].field+'</b> : </div><div class="fl">'+valueList+'</div></div>'+(e==listArr.length-1?'':'<hr />');
                        }
                    }
                    otherStatusList='<li class="other-status-container ">'+otherList+'</li>';
                }
            }
            var type="'list'";
            var addClass1='';
            if(firstTypeId=='typeId4'){  //报表
                if($.isEmptyObject(highJson)){
                    if(listArr.length<=2){
                        addClass1='mixr-content-forms-single';
                    }
                }else{
                    if(highJson.内容.length>0){
                        if(highJson.内容.length<=2){
                            addClass1='mixr-content-forms-single';
                        }
                    }

                }
            }else{   // csv xlsx
                if($.isEmptyObject(highJson)){
                    if(listArr.length<=3){
                        addClass1='mixr-content-files-single';
                    }
                }else{
                    if(!!highJson.内容){
                        if(highJson.内容.length<=2){
                            addClass1='mixr-content-files-single';
                        }
                    }
                }
            }
            datastr+='<li class="clearfix"><p class="fl">'+(20*(currentTypePage-1)+j+1)+'、</p><ul class="fl mixr-content-list '+addClass1+'">'+ulLi+
                '<li class="mixr-list-link"><a href="'+aLink+'" class="mixr-link" '+isBlank+aRhForm+' id="list'+j+'" '+aData+' sourcename="'+sourceName+'" uuid="'+uuid+'" onclick="createMixrLink(this,'+type+')">点击查看 <span></span></a></li>'+otherStatusList+'</ul>'+logoRh+'</li>';
        }
        oDiv+='<ul class="mixr-source-list-ul">'+datastr+'</ul>';
        showindex();
        $('.mixr-source-list').append(oDiv);
    }
    createPage('list');
}
//转义符
// function encodeHTML(source) {
//     return String(source)
//         .replace(/&/g, '&amp;')
//         .replace(/</g, '&lt;')
//         .replace(/>/g, '&gt;')
//         .replace(/"/g, '&quot;')
//         .replace(/'/g, '&#39;');
// }
function viewSourceAll(obj){
    var _this = $(obj);
    var sourceName=_this.attr('data');
    $('.mixr-list h2 span').text(sourceName);
    currentSourceName=sourceName;
    currentTypePage=1;
    clearSecondPage();
    toSecondPage();
    getSearchTypeResult(sourceName);
}
function setPage(data) {
    var list = cloneObj(data);
    $('.mixr-list-ul').html('');
    for(var j=0;j<list.length;j++){
        var listArr=list[j].fieldList;
        var highJson=list[j].highLight;
        // console.log(listArr);
        var sourceName=list[j].sourceName;
        var uuid ;
        var ulLi='';
        var aLink='javascript:void(0);';
        var isBlank='';
        var aData='';
        var aRhForm='';
        var otherViewNum=0;
        var viewOtherNum=0;
        var addS=true;
        for(var t=0;t<listArr.length;t++){
            if(listArr[t].field =='报表来源'){
                otherViewNum++;
            }else if(listArr[t].field =='展示路径'){
                otherViewNum++;
            }else if(listArr[t].field == 'flag'){
                otherViewNum++;
            }else if(listArr[t].field == 'uuid'){
                uuid = listArr[t].value;
                otherViewNum++;
            }
            if(listArr[t].field == '二维码'){
                listArr.splice(t,1);
            }
        }
        if((listArr.length-otherViewNum)<6){
            addS = false;
        }else{
            addS=true;
        }
        for(var k=0;k<6+viewOtherNum;k++) {
            if(!!listArr[k]||!$.isEmptyObject(highJson)){
            var value =listArr[k]?listArr[k].value:'';
            var keyField = listArr[k]?listArr[k].field:'';
            var logoRh = '';
            if (!!isSelf(listArr)) {
                var a=keyWd.replace(/\./g,'\\.').replace(/\+/g,'\\+').replace(/\$/g,'\\$').replace(/\^/g,'\\^').replace(/\[/g,'\\[').replace(/\]/g,'\\]').replace(/\(/g,'\\(').replace(/\)/g,'\\)').replace(/\{/g,'\\{').replace(/\}/g,'\\}').replace(/\|/g,'\\|').replace(/\*/g,'\\S*').replace(/\?/g,'\\S{1}');
                var re=new RegExp(a,'img');
                logoRh = '<span class="ruihai-logo"></span>';
                if ($.isEmptyObject(highJson)) {
                    if (!!listArr[k]) {
                        // for(var i=0;i<listArr.length;i++){
                        //     if(listArr[i].field=='路径'){
                        //         aData='data="'+listArr[i].value.replace(/^\/home\/jwcloud\/data\/backup\//,'')+'"';
                        //         console.log(aData);
                        //         aLink='searchForm?wd='+key.wd+'&word=' + (key.word ? key.word : '') + '&unison=' + key.unison;
                        //
                        //     }
                        // }
                        // isBlank='target="_blank"';
                        if (listArr[k].field == '展示路径') {
                            viewOtherNum++;
                        } else if (listArr[k].field == '报表来源') {
                            viewOtherNum++;
                        }else if(listArr[k].field == 'flag'){
                            viewOtherNum++;
                        }else if(listArr[k].field == 'uuid'){
                            viewOtherNum++;
                        }
                        if (addS && ((k+ viewOtherNum) == (Number(5) + viewOtherNum+viewOtherNum))) {
                            ulLi += '<li class="mixr-list-last">其他虚拟身份: <em class="other-status-btn">( ' + (listArr.length - 5 - otherViewNum) + ' )</em></li>';
                        } else {

                            if (typeof listArr[k].value == 'string') {
                                if (re.test(listArr[k].value)) {
                                    value = listArr[k].value.replace(reg, '<span class="res-high-light" title="' + listArr[k].value + '">' + listArr[k].value + '</span>');
                                }
                            } else if (typeof listArr[k].value == 'object') {
                                var valueArr = listArr[k].value;
                                var valueArrLength = 0;
                                var oList = '';
                                var oDownSelect = '';
                                for (var i = 0; i < valueArr.length; i++) {
                                    if (!!valueArr[i] && valueArr[i] != '无') {
                                        valueArrLength++;
                                        if (re.test(valueArr[i])) {
                                            var value=valueArr[i].replace(re,'<span class="res-high-light">$1</span>');
                                            oList += '<em title="' + valueArr[i] + '" class="res-high-light"><a href="/mixresult?wd=' + valueArr[i] + '&word=' + (key.word ? key.word : '') + '&unison=' + key.unison + '&listCount=20&currentPage=1" target="_blank">' + value + '</a></em>';
                                        } else {
                                            oList += '<em title="' + valueArr[i] + '"> <a href="/mixresult?wd=' + valueArr[i] + '&word=' + (key.word ? key.word : '') + '&unison=' + key.unison + '&listCount=20&currentPage=1" target="_blank">' + valueArr[i] + '</a></em>';
                                        }
                                    }
                                }
                                oDownSelect = '<div class="down-select">' + oList + '</div>';
                                valueArrLength = '<em class="account-num">( ' + valueArrLength + ' )</em>';
                                if (valueArrLength == '<em class="account-num">( 1 )</em>') {
                                    valueArrLength = oList;
                                    oDownSelect = '';
                                }
                                value = valueArrLength + oDownSelect;
                            }
                            if (listArr[k].field != '报表来源' && listArr[k].field != '展示路径' && listArr[k].field != 'flag' && listArr[k].field != 'uuid') {
                                ulLi += '<li>' + listArr[k].field + ':' + value + '</li>';
                            }

                        }
                    }
                } else {
                    if (!!highJson.内容[k]) {
                        for (var i = 0; i < listArr.length; i++) {
                            if (listArr[i].field == '展示路径') {
                                aData = 'data="' + listArr[i].value + '"';
                                aLink = 'searchForm?wd=' + keyWd + '&word=' + (key.word ? key.word : '') + '&unison=' + key.unison;
                            }
                        }
                        isBlank = 'target="_blank"';
                        if (k == 3) {
                            ulLi += '<li style="max-width: 224px;">' + highJson.内容[k] + '</li>';
                        } else {
                            ulLi += '<li style="width: 270px;margin-right:8px;">' + highJson.内容[k] + '</li>';
                        }
                    }
                }
            }
            // else if (isSelf(listArr) == '其他厂商') {
            //     if (!!highJson.内容[k]) {
            //         for (var i = 0; i < listArr.length; i++) {
            //             if (listArr[i].field == '展示路径') {
            //                 aData = 'data="' + listArr[i].value + '"';
            //                 aLink = 'searchForm?wd=' + keyWd + '&word=' + (key.word ? key.word : '') + '&unison=' + key.unison;
            //             }
            //         }
            //         isBlank = 'target="_blank"';
            //         if (k == 3) {
            //             ulLi += '<li style="max-width: 224px;">' + highJson.内容[k] + '</li>';
            //         } else {
            //             ulLi += '<li style="width: 270px;margin-right:8px;">' + highJson.内容[k] + '</li>';
            //         }
            //     }
            // }
            else {
                if (!!listArr[k]) {
                    if (k == 5) {
                        ulLi += '<li class="mixr-list-last">' + listArr[k].field + ':' + listArr[k].value + '</li>';
                    } else {
                        if (!!key.word) {
                            if (findArrSame(wordArr, keyField)) {
                                if (key.unison == 0) {   //如果不是同音正常流程
                                    var regArr = checkWd(value);
                                    if (!!regArr) {
                                        for (var i = 0; i < regArr.length; i++) {
                                            var reg = new RegExp(regArr[i], 'img');
                                            value = value.replace(reg, '<span class="res-high-light">' + regArr[i] + '</span>');
                                        }
                                    } else {

                                    }
                                } else {    //如果是同音
                                    $.each(highJson, function (e, v) {
                                        for (var i = 0; i < v.length; i++) {

                                            if (e == keyField || value.indexOf(v[i]) != -1) {
                                                var a = v[i].replace(/\*/g,'\\*').replace(/\./g,'\\.').replace(/\?/g,'\\?').replace(/\+/g,'\\+').replace(/\$/g,'\\$').replace(/\^/g,'\\^').replace(/\[/g,'\\[').replace(/\]/g,'\\]').replace(/\(/g,'\\(').replace(/\)/g,'\\)').replace(/\{/g,'\\{').replace(/\}/g,'\\}').replace(/\|/g,'\\|');
                                                var reg = new RegExp(a, 'img');
                                                value = value.replace(reg, '<span class="res-high-light">' + v[i] + '</span>');
                                            }
                                        }

                                    })
                                }
                            }
                        } else {
                            if (!!checkWd(value)) {
                                var regArr = checkWd(value);
                                for (var i = 0; i < regArr.length; i++) {
                                    var regStr=regArr[i].replace(/\*/g,'\\*').replace(/\./g,'\\.').replace(/\?/g,'\\?').replace(/\+/g,'\\+').replace(/\$/g,'\\$').replace(/\^/g,'\\^').replace(/\[/g,'\\[').replace(/\]/g,'\\]').replace(/\(/g,'\\(').replace(/\)/g,'\\)').replace(/\{/g,'\\{').replace(/\}/g,'\\}').replace(/\|/g,'\\|');
                                    var reg = new RegExp(regStr, 'img');
                                    value = value.replace(reg, '<span class="res-high-light">' + regArr[i] + '</span>');
                                }
                            }
                        }
                        ulLi += '<li>' + listArr[k].field + ':' + value + '</li>';
                    }
                }
            }
        }
        }

        var otherStatusList='';
        if(isSelf(listArr)=='睿海'){

            if($.isEmptyObject(highJson)){
                var otherList='';
                for(var e=(Number(5)+viewOtherNum);e<listArr.length;e++){
                    if (typeof listArr[e].value == 'string') {
                        if(listArr[e].field !='报表来源' && listArr[e].field !='reportFlag' && listArr[e].field !='展示路径'){
                            otherList+='<div class="clearfix"><div class="fl"><p>'+listArr[e].field+'</p> : </div><div class="fl"><p title="'+listArr[e].value+'">'+listArr[e].value+'</p></div></div>'+(e==listArr.length-1?'':'<hr />');
                        }
                    } else if (typeof listArr[e].value == 'object') {
                        var valueArr = listArr[e].value;
                        var valueList='';
                        for(var t=0;t<valueArr.length;t++){
                            if(!!valueArr[t] && valueArr[t] != '无'){
                                valueList+='<p title="'+valueArr[t]+'">'+valueArr[t]+'</p>';
                            }
                        }
                        otherList+='<div class="clearfix"><div class="fl"><b>'+listArr[e].field+'</b> : </div><div class="fl">'+valueList+'</div></div>'+(e==listArr.length-1?'':'<hr />');
                    }
                }
                otherStatusList='<li class="other-status-container ">'+otherList+'</li>';
            }

        }
        var type="'typelist'";
        var addClass1='';
        if(firstTypeId=='typeId4'){  //报表
            if($.isEmptyObject(highJson)){
                if(listArr.length<=2){
                    addClass1='mixr-content-forms-single';
                }
            }else{
                if(highJson.内容.length>0){
                    if(highJson.内容.length<=2){
                        addClass1='mixr-content-forms-single';
                    }
                }

            }
        }else{   // csv xlsx
            if($.isEmptyObject(highJson)){
                if(listArr.length<=3){
                    addClass1='mixr-content-files-single';
                }
            }else{
                if(!!highJson.内容){
                    if(highJson.内容.length<=2){
                        addClass1='mixr-content-files-single';
                    }
                }
            }
        }
        var oLi='<li class="clearfix"><p class="fl">'+(20*(currentTypePage-1)+j+1)+'、</p><ul class="fl mixr-content-list '+addClass1+'">'+ulLi+
            '<li class="mixr-list-link"><a href="'+aLink+'" class="mixr-link" '+isBlank+aRhForm+' id="list'+j+'" '+aData+' sourceName="'+sourceName+'" uuid="'+uuid+'" onclick="createMixrLink(this,'+type+')">点击查看 <span></span></a></li>'+otherStatusList+'</ul>'+logoRh+'</li>';
        $('.mixr-list-ul').append(oLi);
    }

    showSecondPage();
    createPage('typelist');

    //添加数据库url
    $('#res-info-list li a').on('click',function(){
        var a=$(this).find('p').text().substring(0,50).trim().replace(/"/g,'\\"');
        var operate= "浏览信息  '"+a+"...'";
        $.ajax({
            url: 'http://' + newIp + '/jwcloud/user/addRecord',
            type: 'POST',
            data: {
                dogId:dogId,
                operateInfo: operate,
                operateState:-1,
                url:'',
                keyord:''
            },
            xhrFields:{
                withCredentials:true
            },
            success: function (data) {
                if (data.status == 0) {

                }else{
                    layer(false,'您还没有登录，请点击“确定”返回登录页');
                }
            },
            error: function (err) {
                console.log(err);
            }
        })
    });
}
function hightLightWord(value){
    var data=value.toString();
    var regArr = checkWd(data);
    if (!!regArr) {
        for (var i = 0; i < regArr.length; i++) {
            var regStr=regArr[i].replace(/\*/g,'\\*').replace(/\./g,'\\.').replace(/\?/g,'\\?').replace(/\+/g,'\\+').replace(/\$/g,'\\$').replace(/\^/g,'\\^').replace(/\[/g,'\\[').replace(/\]/g,'\\]').replace(/\(/g,'\\(').replace(/\)/g,'\\)').replace(/\{/g,'\\{').replace(/\}/g,'\\}').replace(/\|/g,'\\|');

            var reg = new RegExp(regStr, 'img');
            data = data.replace(reg, '<span class="res-high-light">' + regArr[i] + '</span>');
        }
    }
    return data;
}
var backType=true;//当概览列表页跳过去时是true，数据源列表页是false
function backCheck(){
    if(backType){
        backindex();
    }else{
        toSecondPage();
    }
}
function  createMixrLink(obj,type){
    var _this = $(obj);
    if(type=='list'){
        backType=true;
    }else{
        backType=false;
    }
    $.cookie('formLink', null, {expires: 0, path: '/'});
    $.cookie('linkArr', null, {expires: 0, path: '/'});

    if (!!_this.attr('data')) {
        $.cookie('formLink', _this.attr('data'));
        $.cookie('sourceName',_this.attr('sourcename'));
        $.cookie('uuid',_this.attr('uuid'));
    } else {
        if (secondtypeId == 21) {

        }
        else {
            var typeId = _this.attr('id').replace(/^list/, '');
            var sorceName=_this.attr('sourcename');
            var list;
            var viewList;
            if(type=='list'){
                list=sourceDataList;
                console.log(list);
                viewList=list[sorceName][typeId].fieldList;
            }else{
                list=dataListArr;
                console.log(list);
                viewList=list[typeId].fieldList;
            }

            toDetailPage();
            var a = '';
            for (var j = 0; j < viewList.length; j++) {
                a += viewList[j].field + ':' + viewList[j].value + ',';
            }
            if (a.length > 50) {
                a = a.substring(0, 50);
            } else {
                a = a.substring(0, a.length - 1);
            }
            a = a.trim().replace(/"/g, '\\"').replace(/:/g, '\\:');
            var operate = "浏览信息  '" + a + "...'";
            $.ajax({
                url: 'http://' + newIp + '/jwcloud/user/addRecord',
                type: 'POST',
                data: {
                    dogId:dogId,
                    operateInfo: operate,
                    operateState: -1,
                    url:'',
                    keyword:''
                },
                xhrFields:{
                    withCredentials:true
                },
                success: function (data) {
                    if (data.status == 0) {

                    }else{
                        layer(false,'您还没有登录，请点击“确定”返回登录页');
                    }
                },
                error: function (err) {
                    console.log(err);
                }
            });
            var smallList = [];
            var longList = [];
            var sourceField = '';
            var sourceFieldLength = 0;
            var qrCodeUrl='';
            for (var i = 0; i < viewList.length; i++) {
                if(viewList[i].field == '二维码'){
                    qrCodeUrl='qrcode/'+viewList[i].value;
                }
            }
            for (var i = 0; i < viewList.length; i++) {
                if (viewList[i].field == '数据来源' || viewList[i].field == '报表来源') {
                    sourceFieldLength = (viewList[i].value + viewList[i].field).length;
                    var oImg='';
                    if(!!qrCodeUrl){
                        oImg='<img src="/images/barcode-2d.png" alt="" qrurl="'+qrCodeUrl+'" onclick="viewerInfo(this)" />';
                    }
                    sourceField = '<li><p>' + viewList[i].field + '：</p><p style="max-width: 400px;">' + viewList[i].value + '</p>'+oImg+'</li>';
                } else {
                    if (viewList[i].field != '展示路径' && viewList[i].field != '二维码') {
                        if ((viewList[i].value + viewList[i].field).length >= 16) {
                            console.log(viewList[i]);
                            longList.push(viewList[i]);
                        } else {
                            console.log(viewList[i]);
                            smallList.push(viewList[i]);
                        }
                    }
                }
            }
            var regT1 = /\t/g;
            for (var i = 0; i < smallList.length; i++) {
                if (!!key.word) {
                    var oLi = '';
                    if (findArrSame(wordArr, smallList[i].field)) {
                        oLi = '<li><p>' + smallList[i].field + '：</p><p><a href="/mixresult?wd=' + smallList[i].value.replace(regT1, '\\t') + '&word=' + (key.word ? key.word : '') + '&unison=' + key.unison + '&listCount=20&currentPage=1" target="_blank">' + hightLightWord(smallList[i].value) + '</a></p></li>';
                    } else {
                        oLi = '<li><p>' + smallList[i].field + '：</p><p>' + smallList[i].value.toString().replace(regT1, '\\t') + '</p></li>';
                    }
                } else {
                    var oLi = '<li><p>' + smallList[i].field + '：</p><p><a href="/mixresult?wd=' + smallList[i].value.toString().replace(regT1, '\\t') + '&word=' + (key.word ? key.word : '') + '&unison=' + key.unison + '&listCount=20&currentPage=1" target="_blank">' + hightLightWord(smallList[i].value) + '</a></p></li>';
                }
                $('.mixr-detail-ul').append(oLi);
            }
            for (var j = 0; j < longList.length; j++) {
                if(longList[j].field=='uuid'){
                }else{
                    var oLi = '';
                    if (!!key.word) {
                        if (findArrSame(wordArr, longList[j].field)) {
                            oLi = '<li class="clearfix"><p>' + longList[j].field + '：</p><p><a href="/mixresult?wd=' + longList[j].value.replace(regT1, "\\t") + '&word=' + (key.word ? key.word : '') + '&unison=' + key.unison + '&listCount=20&currentPage=1" target="_blank">' + hightLightWord(longList[j].value) + '</a></p></li>';
                        } else {
                            oLi = '<li class="clearfix"><p>' + longList[j].field + '：</p><p>' + longList[j].value.toString().replace(regT1, "\\t") + '</p></li>';
                        }
                    } else {
                        var oLi = '<li class="clearfix"><p>' + longList[j].field + '：</p><p><a href="/mixresult?wd=' + longList[j].value.toString().replace(regT1, "\\t") + '&word=' + (key.word ? key.word : '') + '&unison=' + key.unison + '&listCount=20&currentPage=1" target="_blank">' + hightLightWord(longList[j].value) + '</a></p></li>';
                    }
                    $('.mixr-detail-ol').append(oLi);
                }
            }
            if (longList.length === 0) {
                if (sourceFieldLength > 18) {
                    $('.mixr-detail-ol').append(sourceField);
                } else {
                    $('.mixr-detail-ul').append(sourceField);
                }
            } else {
                $('.mixr-detail-ol').append(sourceField);
            }
        }
    }
}
function createPage(pagetype){

    var page;
    if(pagetype == 'list'){
        page=pages;
    }
    else if(pagetype == 'typelist'){
        page=typePages;
    }
    if (!!page) {
        var ele;
        var cP;
        var type="'"+pagetype+"'";
        if(pagetype == 'list'){
            ele=$('.res-source-pages');   //新增列表页
            cP=currentPage;
        }
        else if(pagetype == 'typelist'){
            ele=$('.res-pages');  //数据源列表页
            cP=currentTypePage;
        }
        ele.html('');
        if (cP >= 6) {
            if (page - cP < 4) {
                if (page < 10) {
                    for (var i = 1; i <= page; i++) {
                        if (i == cP) {
                            var oA = '<a href="javascript:;" class="on" onclick="skipPage(this,'+type+')">' + i + '</a>';
                        } else {
                            var oA = '<a href="javascript:;" onclick="skipPage(this,'+type+')">' + i + '</a>';
                        }
                        ele.append(oA);
                    }
                } else {
                    for (var i = page - 9; i <= page; i++) {
                        if (i == cP) {
                            var oA = '<a href="javascript:;" class="on"  onclick="skipPage(this,'+type+')">' + i + '</a>';
                        } else {
                            var oA = '<a href="javascript:;" onclick="skipPage(this,'+type+')">' + i + '</a>';
                        }
                        ele.append(oA);
                    }
                }
            } else {
                for (var i = (cP - 5); i <= (parseInt(cP) + 4); i++) {
                    if (i == cP) {
                        var oA = '<a href="javascript:;" class="on" onclick="skipPage(this,'+type+')">' + i + '</a>';
                    } else {
                        var oA = '<a href="javascript:;" onclick="skipPage(this,'+type+')">' + i + '</a>';
                    }
                    ele.append(oA);
                }
            }
        }
        else {
            for (var i = 1; i <= (page <= 10 ? page : 10); i++) {
                var oA;
                if (i ==cP ) {
                    oA = '<a href="javascript:;" class="on" onclick="skipPage(this,'+type+')">' + i + '</a>';
                } else {
                    oA = '<a href="javascript:;" onclick="skipPage(this,'+type+')">' + i + '</a>';
                }
                ele.append(oA);
            }
        }
        if(cP != 1){
            ele.prepend('<a href="javascript:;" class="res-prev-page"  onclick="prevPage(this,'+type+')">上一页</a>');
        }
        if(cP != page){
            ele.append('<a href="javascript:;" class="res-next-page" onclick="nextPage(this,'+type+')">下一页</a>');
        }
        // pageClick();
        $('.on').removeAttr('href');
    }else{
        ;
    }
}
function viewerInfo(n){
    var $img=$(n);
    if($img.css('width')=='16px'){
        var path = $img.attr('qrurl');
        $img.attr({'src':path});
    }else{
        $img.attr({'src':'/images/barcode-2d.png'});
    }
}
function skipPage(n,pagetype){
    if($(n).hasClass('on'))return;
    if(pagetype=='list'){
        currentPage=Number($(n).text());
        getSearchResult();
    }
    else if(pagetype == 'typelist'){
        currentTypePage=Number($(n).text());
        getSearchTypeResult(currentSourceName);
    }

}
function prevPage(n,pagetype){
    if(pagetype=='list'){
        currentPage--;
        getSearchResult();
    }else{
        currentTypePage--;
        getSearchTypeResult(currentSourceName);
    }

}
function nextPage(n,pagetype){
    if(pagetype=='list'){
        currentPage++;
        getSearchResult();
    }else{
        currentTypePage++;
        getSearchTypeResult(currentSourceName);
    }
}

//虚拟身份 多项的下拉框和选择效果
$('.mixr-list-ul').on('click','.mixr-content-list li .account-num',function(e){
    e.stopPropagation();
    var _this=this;
    if($(this).text().substring(1,$(this).text().length-1)==0){

    }else{
        switchMoreIdentity(_this);
    }

});
$('.mixr-list-ul').on('click','.mixr-content-list li .down-select',function(e){
    e.stopPropagation();
});
//其他虚拟身份下拉选择效果
$('.mixr-list-ul').on('click','.mixr-content-list li .other-status-btn',function(e){
    e.stopPropagation();
    var _this=this;
    switchOtherIdentity(_this);
});
$('.mixr-source-list').on('click','.mixr-source-list-ul .mixr-content-list li .other-status-btn',function(e){
    e.stopPropagation();
    var _this=this;
    switchOtherIdentity(_this);
});
$('.mixr-source-list').on('click','.mixr-source-list-ul .mixr-content-list li .account-num',function(e){
    e.stopPropagation();
    var _this=this;
    if($(this).text().substring(1,$(this).text().length-1)==0){

    }else {
        switchMoreIdentity(_this);
    }
});
function switchOtherIdentity(obj){
    var container=$(obj).parent().parent().find('.other-status-container');
    if(container.css('display')=='none'){
        $('.mixr-source-list-ul li em').removeClass('account-num-down');
        $('.account-num').next().hide();
        $('.other-status-container').hide();
        container.css({'display':'block'});
        $(obj).addClass('account-num-down');

    }else{
        container.css('display','none');
        $(obj).removeClass('account-num-down');
    }
}
function switchMoreIdentity(obj){
    if($(obj).next().css('display')=='none'){
        $('.mixr-source-list-ul li em').removeClass('account-num-down');
        $('.account-num').next().hide();
        $('.other-status-container').hide();
        $(obj).next().css({'display':'block',"width":$(obj).parent().width()-20});

        $(obj).addClass('account-num-down');
    }else{
        $(obj).next().css('display','none');
        $(obj).removeClass('account-num-down');
    }
}
