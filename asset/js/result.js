(function($) {
    var ip = ipNum;
    var newIp=newIpNum;
    var loginName=decodeURIComponent($.cookie('login_user'));
    var token=$.cookie('login_token');

    $('.res-back').click(function(){
        if (!key.source && !key.word && !key.mate) {
            window.location.href = '/sourcelist?wd=' + key.wd;
        } else {
            window.location.href = '/sourcelist?wd=' + key.wd+ '&source=' + key.source + '&word=' + key.word + '&mate=' + key.mate;
        }
        // window.history.back();
    });
    function getKey() {
        if (!!location.href.split('?')[1]) {
            var urlArr = location.href.split('?')[1].split('&');
            var urlJson = {};
            if(urlArr.length==0){
                urlJson=[];
            }else{
                for (var i = 0; i < urlArr.length; i++) {
                    var arr = urlArr[i].split('=');
                    urlJson[arr[0]] = decodeURIComponent(arr[1]);
                }
            }
            return urlJson;
        } else {
            return [];
        }
    }

    //用户登录信息
    $('#yes_user').hover(function(){
        $('.login-info-box').show();
    },function(){
        $('.login-info-box').hide();
    });

    var key = getKey();
    $('#res-pages').html('<h2>搜索中...</h2>');
    $(document).keyup(function(e){
        if(e.keyCode == 13){
            $('#res-search-btn').trigger('click');
        }
    });
    $('#res-search-btn').on('click', function () {
        if(!!$('#res-search-text').val()) {
            var skipUrl='/mixresult?wd='+ encodeURIComponent($('#res-search-text').val())+'&word='+(key.word?key.word:'')+'&unison='+(key.unison?key.unison:'')+'&mate'+(key.mate?key.mate:'')+'&listCount=20&currentPage=1';
            $.ajax({
                url: 'http://' + newIp + '/jwcloud-interface/Jwcloud?interface=userOperate&loginName=' + loginName + '&token=' + token,
                type: 'POST',
                data: {
                    username: loginName,
                    operate: "搜索关键词'" + $('#res-search-text').val().trim().replace(/"/g,'\\"') + "'",
                    operateState: 0,
                    keyword:$('#res-search-text').val().trim().replace(/"/g,'\\"'),
                    url:skipUrl
                },
                success: function (dataStr) {
                    var data = eval('(' + dataStr + ')');
                    if (data.status == 0) {
                        window.location.href=skipUrl;
                    }
                },
                error: function (err) {
                    console.log(err);
                }
            })
        }
    });
    $.ajax({
        url:'http://'+newIp+'/jwcloud-interface/Jwcloud?interface=getUserSearchRecord&loginName='+loginName+'&token='+token,
        type:'POST',
        data:{
            action:"getSearchRecord"
        },
        success:function(dataStr){
            var data=eval('('+dataStr+')');
            if(data.status==0){
                var historyList=data.historyList;
                for(var i=0;i<historyList.length;i++){
                    var oLi='<li><a href="'+historyList[i].url+'">搜索关键词"'+historyList[i].keyword+'"</a></li>';
                    $('.res-history-list ul').append(oLi);
                }
            }else{
                layer(false,'')
            }

        },
        error:function(err){
            console.log(err);
        }
    });


    if(!!key){
        $('#res-search-text').val(key.wd);
    }

    function checkChinese() {
        return /[\u4E00-\u9FA5]/img.test(key.wd);
    }

    var mate;  //模糊/精确  字段
    if (checkChinese()) {
        if (!key.mate) {
            mate = 'match_phrase';
        } else {
            mate = 'match'
        }
    } else {
        if (!key.mate) {
            mate = 'term';
        } else {
            mate = 'wildcard';
        }
    }
    if (!key.source) {
        $('.res-option span').text('全部数据源');
    } else {
        $('.res-option span').text(key.source);
    }
    // 打开页面先获取第一页数据列表
    var postUrl; // 请求地址 分为是否指定字段
    if (!!key.source) {
        if (!key.word) {
            postUrl = 'http://' + ip + '/' + key.source + '/_search';
        } else {
            postUrl = 'http://' + ip + '/' + key.source + '/_search';
        }
    } else {
        postUrl = 'http://' + ip + '/_all/_search';
    }
    var data;
    var perpageCount = key.listCount;//每页条数

    var currentPage=key.currentPage;
    console.log((currentPage-1)*perpageCount);
    if (!!key.source) {
        if (!key.mate) { //判断精确  模糊
            if (!key.word) {  // 搜索某一数据源匹配数据  精确匹配
                data = {  //精确匹配 无指定字段
                    "from": (currentPage-1)*perpageCount,     //(表示第一页  10  表示第二页依次后推(单页显示10为例)),
                    "size": perpageCount
                };
                var a = {};
                var b = {};
                var c = {};
                var d = {};
                c[mate] = {"_all": key.wd};
                b.filter = c;
                a.filtered = b;
                data.query = a;
            } else {//搜索某一数据源下某一字段的匹配数据 精确匹配
                var a = {};
                var b = {};
                var c = {};
                var d = {};
                data = { //精确匹配 有指定字段
                    "from": (currentPage-1)*perpageCount,     //(表示第一页  10  表示第二页依次后推(单页显示10为例)),
                    "size": perpageCount    //(单页显示10)
                };
                d[key.word] = key.wd;
                c[mate] = d;
                b.filter = c;
                a.filtered = b;
                data.query = a;
            }
        } else {
            if (!key.word) {
                if (checkChinese()) { //有中文 搜索某一数据源下 匹配数据  模糊匹配
                    data = {
                        "from": (currentPage-1)*perpageCount,     //(表示第一页  10  表示第二页依次后推(单页显示10为例)),
                        "size": perpageCount,    //(单页显示10)
                        "query": {
                            "filtered": {
                                "filter": {
                                    "match": {
                                        "_all": key.wd
                                    }
                                }
                            }
                        }
                    }
                } else {  //没有中文 搜索某一数据源下 匹配数据  模糊匹配
                    data = {
                        "from": (currentPage-1)*perpageCount,     //(表示第一页  10  表示第二页依次后推(单页显示10为例)),
                        "size": perpageCount,    //(单页显示10)
                        "query": {
                            "wildcard": {
                                "_all": '*'+key.wd+'*'
                            }
                        }
                    }
                }
            } else {
                if (checkChinese()) { // 有中文搜索某一数据源下某一字段的匹配数据 模糊匹配

                    data = {
                        "from": (currentPage-1)*perpageCount,     //(表示第一页  10  表示第二页依次后推(单页显示10为例)),
                        "size": perpageCount   //(单页显示10)
                    };
                    var a = {};
                    var b = {};
                    var c = {};
                    var d = {};
                    d[key.word]=key.wd;
                    c.match = d;
                    b.filter = c;
                    a.filtered = b;
                    data.query = a;

                } else {  // 没有中文搜索某一数据源下某一字段的匹配数据 模糊匹配
                    data = {
                        "from": (currentPage-1)*perpageCount,     //(表示第一页  10  表示第二页依次后推(单页显示10为例)),
                        "size": perpageCount    //(单页显示10)
                    };
                    var a = {};
                    var b = {};
                    b[key.word] = '*'+key.wd+'*';
                    a.wildcard = b;
                    data.query = a;
                }
            }

        }
    } else {
        if (!key.mate) { //  没有数据源  只根据指定字段匹配数据 精确匹配
            data = {
                "from": (currentPage-1)*perpageCount,
                "size": perpageCount
            };
            var a = {};
            var b = {};
            var c = {};
            var d = {};
            d[key.word] = key.wd;
            c[mate] = d;
            b.filter = c;
            a.filtered = b;
            data.query = a;
        } else {
            if (checkChinese()) {  // 有中文 没有数据源  只根据指定字段匹配数据 模糊匹配
                data = {
                    "from": (currentPage-1)*perpageCount,
                    "size": perpageCount
                };
                var a = {};
                var b = {};
                var c = {};
                var d = {};
                d[key.word] = key.wd;
                c.match = d;
                b.filter = c;
                a.filtered = b;
                data.query = a;
            } else { // 没有中文 没有数据源  只根据指定字段匹配数据 模糊匹配
                data = {
                    "from": (currentPage-1)*perpageCount,
                    "size": perpageCount
                };
                var a = {};
                var b = {};
                b[key.word] = '*'+key.wd+'*';
                a.wildcard = b;
                data.query = a;
            }
        }
    }

    $.ajax({
        url: postUrl,
        type: 'POST',
        data: JSON.stringify(data),
        success: function (data) {
            $('#res-pages').html('');
            $('.view-result-count p span:eq(0)').text(key.wd);
            $('.view-result-count p span:eq(1)').text(data.hits.total);
            console.log(data);
            setPage(data);
        },
        error: function (err) {
            console.log(err);
        }
    });


    // function setData2(){
    //     $.ajax({
    //         url:"../data/1?"+new Date().getTime(),
    //         type:'GET',
    //         success:function(data){
    //             setPage(data);
    //         },
    //         error:function(e){
    //             console.log('error:'+e);
    //         }
    //     });
    // }
    // function pageClick(){
    //     $('#res-pages a:not(.on)').on('click',function(e){
    //         // $.ajax({
    //         //     url:$(this).attr('href'),
    //         //     type:'GET',
    //         //     success:function(data){
    //         //         $('#res-pages').html('');
    //         //         setPage(data);
    //         //     }
    //         // })
    //     });
    // }
    var re;
    function checkWd(n){
        if(n){
            if(!!key.mate && checkChinese()){
                re=new RegExp('['+key.wd.split('').join('|')+']','img');
            }else{
                re=new RegExp(key.wd,'img');
            }
            return n.match(re) ;
        }else{
            return false;
        }

    }
    function setPage(data) {
        var pages = Math.ceil(data.hits.total / perpageCount);
        console.log(data.hits.total);
        var list = data.hits.hits;
        var reg;
        if(!!key.mate && checkChinese()){
            reg=new RegExp('['+key.wd.split('').join('|')+']','img');
        }else{
            reg=new RegExp(key.wd,'img');
        }

        var maxwidth=120;
        for(var j=0;j<list.length;j++){
            var source,time,addr;
            var listJson=list[j]._source;
            var id=list[j]._id;
            var wdStr='';
            var noWdStr='';
            $.each(listJson,function(k,v) {
                if (!!checkWd(k) || !!checkWd(v)) {
                    wdStr += k + '：' + v + '，';
                } else {
                    noWdStr += k + '：' + v + '，';
                }
            });

            var abstract=wdStr+noWdStr;
            if(abstract.length > maxwidth){
               abstract=abstract.substring(0,maxwidth);
               abstract+='...';
            }else{
                abstract=abstract.substring(abstract.length-1,abstract);
            }
            if(!!checkWd(abstract)){
                if(!key.mate){
                    abstract=abstract.replace(reg,'<span class="res-high-light">'+key.wd+'</span>');
                }else{
                    var kArr=checkWd(abstract);
                    for(var i=0;i<kArr.length;i++){
                        var reg=new RegExp(kArr[i],'img');
                        abstract=abstract.replace(reg,'<span class="res-high-light">'+kArr[i]+'</span>');
                    }
                }
            }else{
                return;
            }

            if(!!list[j]._index){
                var a=list[j]._index.replace(/-\d+$/,'');
                source='<span>数据源：'+a+'</span>';
            }else{
                source='';
            }

            if(!!listJson.时间){
                time='<span>时间：'+listJson.时间+'</span>';
            }else{
                time='';
            }
            if(!!listJson.地点){
                addr='<span>地点：'+list[j].address+'</span>';
            }
            else{
                addr='';
            }

            var oLi='<li class="clearfix">'+
                    '<div class="res-info-list-icon fl"></div>'+
                    '<a href="/detail?wd='+key.wd+'&source='+key.source+'&word='+(key.word?key.word:"")+'&mate='+(key.mate?key.mate:"")+'&listCount='+key.listCount+'&currentPage='+key.currentPage+'&id='+id+'" class="res-info-content" id="'+id+'" >'+
                '<p class="res-info-summary">'+abstract+'</p>'+
                '<p class="res-info-label">'+source+time+addr+
                '</p></a></li>';

            $('#res-info-list').append(oLi);
            (function(j){
                $('#'+id+'').click(function(){
                    $.cookie(list[j]._id,JSON.stringify(list[j]._source));
                })
            })(j);

        }

        if (!!pages) {
            if (currentPage >= 6) {
                if (pages - currentPage < 4) {
                    if (pages < 10) {
                        for (var i = 1; i <= pages; i++) {
                            if (i == currentPage) {
                                var oA = '<a href="/result?wd=' + $('#res-search-text').val() + '&source=' + key.source + '&word=' + key.word + '&mate=' + key.mate+'&listCount=20&currentPage='+i+'" class="on">' + i + '</a>';
                            } else {
                                var oA = '<a href="/result?wd=' + $('#res-search-text').val() + '&source=' + key.source + '&word=' + key.word + '&mate=' + key.mate+'&listCount=20&currentPage='+i+'">' + i + '</a>';
                            }
                            $('#res-pages').append(oA);
                        }
                    } else {
                        for (var i = pages - 9; i <= pages; i++) {
                            if (i == currentPage) {
                                var oA = '<a href="/result?wd=' + $('#res-search-text').val() + '&source=' + key.source + '&word=' + key.word + '&mate=' + key.mate+'&listCount=20&currentPage='+i+'" class="on">' + i + '</a>';
                            } else {
                                var oA = '<a href="/result?wd=' + $('#res-search-text').val() + '&source=' + key.source + '&word=' + key.word + '&mate=' + key.mate+'&listCount=20&currentPage='+i+'">' + i + '</a>';
                            }
                            $('#res-pages').append(oA);
                        }
                    }
                } else {
                    for (var i = (currentPage - 5); i <= (parseInt(currentPage) + 4); i++) {

                        if (i == currentPage) {
                            var oA = '<a href="/result?wd=' + $('#res-search-text').val() + '&source=' + key.source + '&word=' + key.word + '&mate=' + key.mate+'&listCount=20&currentPage='+i+'" class="on">' + i + '</a>';
                        } else {
                            var oA = '<a href="/result?wd=' + $('#res-search-text').val() + '&source=' + key.source + '&word=' + key.word + '&mate=' + key.mate+'&listCount=20&currentPage='+i+'">' + i + '</a>';
                        }
                        $('#res-pages').append(oA);
                    }
                }
            }
            else {
                for (var i = 1; i <= (pages <= 10 ? pages : 10); i++) {
                    var oA;
                    if (i ==currentPage ) {
                        oA = '<a href="/result?wd=' + $('#res-search-text').val() + '&source=' + key.source + '&word=' + key.word + '&mate=' + key.mate+'&listCount=20&currentPage='+i+'" class="on">' + i + '</a>';
                    } else {
                        oA = '<a href="/result?wd=' + $('#res-search-text').val() + '&source=' + key.source + '&word=' + key.word + '&mate=' + key.mate+'&listCount=20&currentPage='+i+'">' + i + '</a>';
                    }
                    $('#res-pages').append(oA);
                }
            }
                if(currentPage != 1){
                    $('#res-pages').prepend('<a href="/result?wd=' + $('#res-search-text').val() + '&source=' + key.source + '&word=' + key.word + '&mate=' + key.mate+'&listCount=20&currentPage='+(parseInt(currentPage)-1)+'" class="res-prev-page">上一页</a>');
                }
                if(currentPage != pages){
                    $('#res-pages').append('<a href="/result?wd=' + $('#res-search-text').val() + '&source=' + key.source + '&word=' + key.word + '&mate=' + key.mate+'&listCount=20&currentPage='+(parseInt(currentPage)+1)+'" class="res-next-page">下一页</a>');
                }
                // pageClick();
                $('.on').removeAttr('href');
            }else{
                $('#res-pages').html('<h2>没有搜到结果</h2>');
            }
            //添加数据库url
            $('#res-info-list li a').on('click',function(){
                var a=$(this).find('p').text().substring(0,50).trim().replace(/"/g,'\\"');
                var operate= "浏览信息  '"+a+"...'";
                $.ajax({
                    url: 'http://' + newIp + '/jwcloud-interface/Jwcloud?interface=userOperate&loginName=' + loginName + '&token=' + token,
                    type: 'POST',
                    data: {
                        username: loginName,
                        operate: operate,
                        operateState:-1
                    },
                    success: function (dataStr) {
                        var data = eval('(' + dataStr + ')');
                        if (data.status == 0) {

                        }
                    },
                    error: function (err) {
                        console.log(err);
                    }
                })
            });
        }


    // $('#res-search-btn').click(function(){
    //     setData2();
    // });

    // function setData(){
    //     $.ajax({
    //         url:'http://192.168.31.56:9200/index/fulltext/_search?_all/',
    //         type:'GET',
    //         success:function(data){
    //             var data=data.hits.hits;
    //             for(var i=0;i<data.length;i++){
    //                 var oLi='<li>'+
    //                     '<a href="#">'+data[i]._source.content+'</a>'+
    //                     '<p class="res-info-summary">'+data[i]._source.content+'</p>'+
    //                     '<p class="res-info-label">'+data[i]._id+
    //                     '</p></li>';
    //                 $('#res-info-list').append(oLi);
    //             }
    //         },
    //         error:function(e){
    //             console.log(e);
    //         }
    //     });
    // }

    //

    // if (!!getKey()) {
    //     // $('#res-search-text').val(getKey());
    //     $('#res-search-btn').trigger('click');
    // }
    //历史记录展示
    // getHistoryList();
    // function getHistoryList(){
    //     $.post('/add',{url:$(this).attr('href'),title:$(this).text()},function(data){
    //         $('.res-history-list ul').html('');
    //         for(var i=0;i<data.length;i++){
    //             $('.res-history-list ul').append('<li><a href="'+data[i].url+'">'+data[i].title+'</a></li>');
    //         }
    //     })
    // }

})(jQuery);