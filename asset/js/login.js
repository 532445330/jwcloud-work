var newIp=newIpNum;//8080端口的
var dogNotPresent = false;
var authCode = "";
var scope = "<dogscope/>";
var timer;
var checkReSend=true;
var dogId;
var admin = $.cookie('admin');
var hurl=$.cookie('hurl')?decodeURIComponent($.cookie('hurl')):'';
var cookieData=$.cookie();

for(var i=0;i<cookieData.length;i++){
    var cookieJson=cookieData[i];
    $.each(cookieJson,function(i,v){
        $.cookie(i, null, { expires: 0, path: '/' });
    })
}
//Callback function, if the dog has been removed the function will be called.
function removeDog()
{
    reportStatus(7);
}

//Callback function, if the dog still exists the function will be called.
function insertDog()
{
    window.location.href='login'
}

function checkDog()
{
    var stat = "";
    var scope = "<dogscope/>";

    //Get Auth Code
    if ("" == authCode) {
        authCode = getAuthCode();
    }
    //Get object
    objAuth = getAuthObject();
    if (navigator.userAgent.indexOf("Chrome") > 0) {
        objAuth.EnumDog(authCode);
    }
    else if(window.ActiveXObject || "ActiveXObject" in window){
        //Open Dog
        stat = objAuth.Open(scope, authCode);

        if(0 != stat)
        {
            dogNotPresent = true;
            reportStatus(stat);
        }
        else
        {
            if (dogNotPresent == true)
            {
                dogNotPresent = false;
                window.location.href = "login";
            }
        }
    }else{
        objAuth.Open(scope,authCode);
    }

    //Execute the check again after 2 seconds
    timer=setTimeout(checkDog, 2000);
}

//Load callback functions, insertDog() and removeDog()

function loadFunc()
{
    var objAuth;

    //Get object
    objAuth = getAuthObject();

    if (navigator.userAgent.indexOf("Window") > 0)
    {
        if (navigator.userAgent.indexOf("Chrome") > 0)  //Chrome
        {
            window.addEventListener("message", function (event) {
                if (event.source != window)
                    return;
                if (event.data.type == "SNTL_FROM_HOST") {
                    var ReturnText = event.data.text;

                    if ("EnumDog" == ReturnText.InvokeMethod) {

                        if (0 == ReturnText.Status) {
                            if(!checkReSend)return;
                                checkReSend=false;
                                var dog_info = ReturnText.EnumResultStr;
                                dog_info=dog_info.match(/(id=")(.*)(")/)[2];
                                dogId=dog_info;
                                console.log(dogId);
                                if(!!dogId){
                                    $.ajax({
                                        url:'http://'+newIp+'/jwcloud/user/getUsername',
                                        type:'POST',
                                        data:{
                                            dogId:dogId
                                        },
                                        xhrFields:{
                                            withCredentials:true
                                        },
                                        success:function(data){
                                            if(data.status==0){
                                                if(data.admin==0){
                                                    $('#admin-login').on('click',function(){adminLogin($(this))});
                                                    $('#admin-login').trigger('click');
                                                }else{
                                                    $('#admin-login').off('click');
                                                    $('#user-login').trigger('click');
                                                }

                                                $.cookie('admin',data.admin);
                                                errShow($('#error-hints'),false,'');
                                                $('#username').val(data.username);
                                            }else{
                                                $('#password').val('');
                                                errShow($('#error-hints'),true,'用户不存在');
                                            }
                                        },
                                        error:function(err){
                                            if(err.readyState==0){
                                                alert('接口调用出现问题，请检查');
                                            }
                                        }
                                    });
                                }

                        }
                        else {
                            checkReSend=true;
                            $('#username').val('');
                            reportStatus(parseInt(ReturnText.Status));
                            return;
                        }
                    }
                    else {
                        return;
                    }
                }
            }, false);

            setTimeout(checkDog, 1000);
        }
        else if (window.ActiveXObject || "ActiveXObject" in window)  //IE
        {
            objAuth.SetCheckDogCallBack("insertDog", "removeDog");
        }
        else   //firefox
        {
            setTimeout(checkDog, 1000);
        }
    }
    else if (navigator.userAgent.indexOf("Mac") > 0)
    {
        setTimeout(checkDog, 1000);
    }
    else if (navigator.userAgent.indexOf("Linux") > 0)
    {
        setTimeout(checkDog, 1000);
    }
    else
    {
        ;
    }
}
$(document).keyup(function(e){
    if(e.keyCode == 13){
        if($('#admin-submit-btn').length != 0){
            $('#admin-submit-btn').trigger('click');
        }else if($('#user-submit-btn').length != 0){
            $('#user-submit-btn').trigger('click');
        }
    }
});
function checkPwd(){
    var pwd=$('#password').val();
    if(pwd.length===0){
        errShow($('#error-hints'),true,'请输入密码');
        return false;
    }
    else if(pwd.length<6 || pwd.length>16)
    {
        errShow($('#error-hints'),true,'请输入6-16字符密码');
        return false;
    }
    else{
        return true;
    }
}
function checkUsername(){
    var username=$('#username').val();
    if(username.length===0){
        errShow($('#error-hints'),true,'没有检测到合法用户');
        return false;
    }else{
        return true;
    }
}
$('#password').on('blur',function(){
    checkPwd();
});
$('#username').on('blur',function(){
    checkUsername();
});

//登录部分
$('#user-login').on('click',function(){
    $(this).addClass('on');
    $('#admin-login').removeClass('on');
    $('.submit-btn').attr('id','user-submit-btn');
});
$('#admin-login').on('click',function(){
    adminLogin($(this));
});
function adminLogin(n){
    n.addClass('on');
    $('#user-login').removeClass('on');
    $('.submit-btn').attr('id','admin-submit-btn');
}
var checkSubmit=true;

$('.user-info').delegate('#user-submit-btn','click',function(){
   if(checkUsername() && checkPwd()){
       if(!checkSubmit)return;
       checkSubmit=false;
       $('#admin-submit-btn').css('cursor','wait');
       $.ajax({
           url:'http://'+newIp+'/jwcloud/user/login',
           type:'POST',
           data:{
               dogId:dogId,
               password:$('#password').val()
           },
           xhrFields:{
               withCredentials:true
           },
           success:function(data){
                if(data.status==0){
                    var skipUrl;
                    if(!!hurl){
                        skipUrl=hurl;
                        $.cookie('hurl',null,{ expires: 0, path: '/' });
                    }else{
                        skipUrl='/search';
                    }
                    $.cookie('dogId',dogId);
                    $.cookie('login_user',encodeURIComponent($('#username').val()));
                    $('#admin-submit-btn').val('登录成功');
                    errShow($('#error-hints'),false,'');
                    setTimeout(function(){
                        window.location.href=skipUrl;
                    },800);
                }
                else{
                    $('#password').val('');
                    $('#admin-submit-btn').css('cursor','default');
                    checkSubmit=true;
                    errShow($('#error-hints'),true,data.message);
                }
           },
           error:function(err){
               console.log(err);
           }
       })
   }
});
$('.user-info').delegate('#admin-submit-btn','click',function(){
    if(checkUsername() && checkPwd()){
        if(!checkSubmit)return;
        checkSubmit=false;
        $('#admin-submit-btn').css('cursor','wait');
        $.ajax({
            url:'http://'+newIp+'/jwcloud/user/login',
            type:'POST',
            data:{
                dogId:dogId,
                password:$('#password').val()
            },
            xhrFields:{
                withCredentials:true
            },
            success:function(data){
                if(data.status==0){
                    var skipUrl;
                    if(!!hurl){
                        skipUrl=hurl;
                        $.cookie('hurl',null,{ expires: 0, path: '/' });
                    }else{
                        skipUrl='/admin';
                    }
                    $.cookie('dogId',dogId);
                    $.cookie('login_user',encodeURIComponent($('#username').val()));
                    $('#admin-submit-btn').val('登录成功');
                    setTimeout(function(){
                        window.location.href=skipUrl;
                    },800);
                }else{
                    $('#password').val('');
                    $('#admin-submit-btn').css('cursor','default');
                    checkSubmit=true;
                    errShow($('#error-hints'),true,data.message);
                }
            },
            error:function(err){
                console.log(err);
            }
        })
    }
});



