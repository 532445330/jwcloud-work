
var uploader = WebUploader.create({

    // swf文件路径
    swf: '../javascripts/Uploader.swf',

    // 文件接收服务端。
    server: './uploaderfile',
    method: 'POST',
    // 选择文件的按钮。可选。
    // 内部根据当前运行是创建，可能是input元素，也可能是flash.
    pick: '#picker',
    formData: {
        source: $('#source').val()
    },
    threads:1,
    // 不压缩image, 默认如果是jpeg，文件上传前会压缩一把再上传！
    resize: false,
    accept: {
        title: 'xls and csv',
        extensions: 'xlsx,xls,csv',
        mimeType: 'application/excel,text/comma-separated-values'
    }
});

// 显示用户选择
uploader.on( 'fileQueued', function( file ) {
    $('#list').append( '<div id="' + file.id + '" class="item">' +
        '<h4 class="info">' + file.name + '</h4>' +
        '<p class="state">等待添加...</p>' +
        '</div>' );
});
uploader.on('filesQueued',function( file ){
    for (var i=0;i<file.length-1;i++){
        var name1=file[i].name.substring(file[i].name.lastIndexOf('.')+1,file[i].name.legnth);
        var name2=file[i+1].name.substring(file[i+1].name.lastIndexOf('.')+1,file[i+1].name.legnth);
        if(name1!== name2){
            alert('请添加相同类型的文件');
            $('#list').html('');
            for(var j=0;j<file.length;j++){
                uploader.removeFile( file[j],true);
            }

            return;
        }else{
            return;
        }

    }
});
// 文件上传进度
uploader.on( 'uploadProgress', function( file, percentage ) {
    var $li = $( '#'+file.id ),
        $percent = $li.find('.progress .progress-bar');

    // 避免重复创建
    if ( !$percent.length ) {
        $percent = $('<div class="progress progress-striped active">' +
            '<div class="progress-bar" role="progressbar" style="width: 0%">' +
            '</div>' +
            '</div>').appendTo( $li ).find('.progress-bar');
    }

    $li.find('p.state').text('添加中');

    $percent.css( 'width', percentage * 100 + '%' );
});
//文件成功失败处理
uploader.on( 'uploadSuccess', function( file ) {
    $( '#'+file.id ).find('p.state').text('添加成功，等待上传');

});

uploader.on( 'uploadError', function( file ) {
    $( '#'+file.id ).find('p.state').text('添加出错，请重新添加');

});

uploader.on( 'uploadComplete', function( file ) {

});
var tableTitle;
uploader.on('uploadFinished',function( ){
    //      //上传完判断文件格式是否一致

    $.post('./uploader/compareFile',{source:$('#source').val()},function (data) {
        if(data.status === 0){
            //如果一致  进入概览 ，弹出上传成功

            getFilesView();
        }else{
            alert(data.message);
            uploader.reset();
            console.log(uploader.getFiles().length);
            $('#list').html('');
            //不一致加以提示
        }
    });
});

$('#ctlBtn').on( 'click', function() {
    uploader.options.formData.source =$('#source').val();
    uploader.upload();
    $('#source').css({'color':'#ccc'}).attr('disabled','true');
});

// 获取概览
function getFilesView(){
    $.ajax({
        url:'./uploader/getFile',
        type:'POST',
        data:{
            source:$('#source').val()
        },
        success:function(data){
            if(data.status===0){
                var dataList=data.data;
                tableTitle=dataList[0].toString();
                for(var i=0;i<dataList.length;i++){
                    var oTr='<tr></tr>';
                    $('.load-view-table').append(oTr);
                    for(var j=0;j<dataList[1].length;j++){
                        if(i===0){
                            if(!dataList[i][j]){
                                dataList[i][j]='';
                            }
                            var oTh='<th><input type="text" class="table-title" value=" '+dataList[i][j]+'"></th>';
                            $('.load-view-table tr:last-child').append(oTh);
                        }else{
                            var oTd='<td>'+dataList[i][j]+'</td>';
                            $('.load-view-table tr:last-child').append(oTd);
                        }
                    }
                }
            }else{
                $('.load-view-table').text(data.message)
            }
        }
    })
}

$('#load-view-save').on('click',function(){
    var tableTitleArr=[];
    for(var n=0;n< ($('.table-title').length);n++){
        tableTitleArr.push($('.table-title').eq(n).val().replace(/\s/g,''));
    }
    console.log(tableTitleArr);
    //判断是否有修改  没有修改不传参数
    if(tableTitleArr.toString() === tableTitle.replace(/\s/g,'')){

        $.ajax({
            url:'./uploader/postTitle',
            type:'POST',
            data:{
              source:$('#source').val()
            },
            success:function(data){
                console.log(data);
            }
        })
    }else{
        tableTitleArr=tableTitleArr.toString();
        $.ajax({
            url:'./uploader/postTitle',
            type:'POST',
            data:{
                source:$('#source').val(),
                fields: tableTitleArr
            },
            success:function(data){
                console.log(data);
            }
        })
    }

});


