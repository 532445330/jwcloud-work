$.cookie('VFURL', null, { expires: 0, path: '/' });
$.cookie('currentFormName', null, { expires: 0, path: '/' });
$.cookie('indexPage', null, { expires: 0, path: '/' });
$.cookie('keyWord', null, { expires: 0, path: '/' });
    $(window).on('load resize', function () {
        $('#bg-white').css({
            height: $(window).height()
        })
    });
    $('#search-btn').click(function () {
        dropSearchResault();
    });
    $(document).keyup(function (e) {
        if (e.keyCode == 13) {
            $('#search-btn').trigger('click');
        }
    });
    $('#search-high-btn').on('click', function () {
        dropSearchResault();
    });
    //用户登录信息
    $('#yes_user').hover(function () {
        $('.login-info-box').show();
    }, function () {
        $('.login-info-box').hide();
    });

    function dropSearchResault() {

        if (!!$('#search-input-text').val()) {
            if ($('.search-high-box').css('display') === 'none') {
                // window.location.href = '/sourcelist?wd='+ encodeURIComponent($('#search-input-text').val());
                var word = '';
                var unison = 0;  //0不同音  1.同音

                word =  $('.search-only input:radio:checked').next().text();

                if ($('#unisonSearch').attr('checked') == 'checked') {
                    unison = 1;
                } else {
                    unison = 0;
                }
                var searchKeyword=$('#search-input-text').val().trim();
                if(searchKeyword.length>45){
                    searchKeyword=searchKeyword.substring(0,45);
                }else{
                    searchKeyword=searchKeyword.substring(0,searchKeyword.length);
                }
                $.ajax({
                    url: 'http://' + newIp + '/jwcloud/user/addRecord',
                    type: 'POST',
                    data: {
                        dogId:dogId,
                        operateInfo: "搜索关键词'"+searchKeyword.trim().replace(/"/g,'\\"')+"'",
                        operateState: 0,
                        keyword: searchKeyword.trim().replace(/"/g,'\\"'),
                        url: '&word=' + word + '&unison=' + unison + '&listCount=20&currentPage=1'
                    },
                    xhrFields:{
                        withCredentials:true
                    },

                    success: function (data) {
                        if (data.status == 0) {
                            window.location.href = '/mixresult?wd=' + encodeURIComponent($('#search-input-text').val().trim()) + '&word=' + word + '&unison=' + unison + '&listCount=20&currentPage=1';
                        }else{
                            layer(false,'您还没有登录，请点击“确定”返回登录页');
                        }
                    },
                    error: function (err) {
                        console.log(err);
                    }
                });

            } else {
                var wd, source, mate;
                var word = '';
                var unison = '';
                if ($('.change-source-btn').attr('alt') === 'off') {
                    source = '';
                } else {
                    if ($('#source-value').text() === '所有数据源（默认）') {
                        source = '';
                    } else {
                        source = encodeURIComponent($('#source-value').text());
                    }
                }
                if ($('.change-mate-btn').attr('alt') === 'off') {
                    mate = '';
                } else {
                    if ($('#mate-value').text() === '精确匹配（默认）') {
                        mate = '';
                    } else {
                        mate = encodeURIComponent($('#mate-value').text());
                    }
                }
                $.ajax({
                    url: 'http://' + lastIp + '/index.php',
                    type: 'POST',
                    async: false,
                    data: JSON.stringify({
                        interface: 'getField',
                        act: 'getFieldByUnique',
                        uniqueName: $('.search-only input:radio:checked').next().text()
                    }),
                    success: function (dataStr) {
                        var data = eval('(' + dataStr + ')');
                        if (data.status == 0) {
                            for (var i = 0; i < data.Item.length; i++) {
                                word += data.Item[i].fieldName + ',';
                            }
                            word = word.substring(0, word.length - 1);
                        } else {
                            word = '';
                        }
                    },
                    error: function (err) {
                        console.log(err);
                    }
                });
                if ($('#unisonSearch').attr('checked') == 'checked') {
                    unison = 1;
                } else {
                    unison = 0;
                }
                wd = encodeURIComponent($('#search-input-text').val().trim());

                $.ajax({
                    url: 'http://' + newIp + '/jwcloud-interface/Jwcloud?interface=userOperate&loginName=' + loginName + '&token=' + token,
                    type: 'POST',
                    data: {
                        username: loginName,
                        operate: "搜索关键词  '" + $('#search-input-text').val().trim().replace(/"/g,'\\"') + "'",
                        operateState: 0,
                        keyword: $('#search-input-text').val().trim().replace(/"/g,'\\"'),
                        url: '/result?wd=' + wd + '&word=' + word + '&mate=' + mate + '&unison=' + unison + '&listCount=20&currentPage=1'
                    },
                    success: function (dataStr) {
                        var data = eval('(' + dataStr + ')');
                        if (data.status == 0) {
                            window.location.href = '/result?wd=' + wd + '&word=' + word + '&mate=' + mate + '&unison=' + unison + '&listCount=20&currentPage=1';
                        }
                    },
                    error: function (err) {
                        console.log(err);
                    }
                });
            }
        } else {
            // layer(false, data.message, false);
        }
    }
// 高级搜索按钮
    var ip = ip;
    var lastIp = lastIpNum;
    var newIp = newIpNum;
    var admin = $.cookie('admin');
    var loginName = decodeURIComponent($.cookie('login_user'));
    var token = $.cookie('login_token');

    $.ajax({
        url: 'http://' + lastIp + '/index.php',
        type: 'POST',
        data: JSON.stringify({
            interface: 'getField',
            act: 'getUnique',
            Token: token,
            username: loginName,
            level: 3
        }),
        success: function (dataStr) {
            var data = eval('(' + dataStr + ')');
            if (data.status == 0) {
                var onlyWordList = data.Item;
                for (var i = 0; i < onlyWordList.length; i++) {
                    var oInput = '<input type="radio" id="only' + i + '" name="only" onclick="changeState(this)"><label for="only' + i + '">' + onlyWordList[i].uniquefieldname + '</label>';
                    $('.search-only').append(oInput);
                }
            } else {
                ;
            }
        },
        error: function (err) {
            console.log(err);
        }
    });
    function changeState(obj){
        var radioCheck= $(obj).val();
        if("1"==radioCheck){
            $(obj).attr("checked",false);
            $(obj).val("0");
        }else{
            $(obj).val("1");
        }
    }

    $('#search-high').on('click', function () {
        if ($('.search-high-box').css('display') == 'none') {
            $('.search-high-box').show();
            $('.search-high i').css('backgroundImage', 'url("../images/icon_search_putaway.png")');
            var url = 'http://' + newIp + '/jwcloud-interface/Jwcloud?interface=getAllDataSource';
            if (admin == -1) {
                url = 'http://' + newIp + '/jwcloud-interface/Jwcloud?interface=getAllAliases&loginName=' + loginName + '&token=' + token
            }
            $.ajax({
                url: url,
                success: function (data) {
                    var arr;
                    var dataN;
                    if (admin == 0) {
                        arr = data;
                        appendAllSource();
                    } else if (admin == -1) {
                        dataN = data;
                        appendAllSourceN();
                    }

                    function appendAllSource() {
                        for (var i = 0; i < arr.length; i++) {
                            $('#search-select-allsource .slipe-box').append('<p>' + arr[i] + '</p>');
                            if (i === arr.length - 1) {
                                addClick();
                            }
                        }
                    }

                    function appendAllSourceN() {
                        console.log(dataN);
                        if (dataN.status == 0) {
                            var sourceList = dataN.sourceList;
                            for (var i = 0; i < sourceList.length; i++) {
                                if (sourceList[i].rights == 0) {
                                    $('#search-select-allsource .slipe-box').append('<p>' + sourceList[i].sourceName + '</p>');
                                    if (i === sourceList.length - 1) {
                                        addClick();
                                    }
                                }
                            }
                        } else {
                            layer(false, '您还没有登录，请点击“确定”返回登录页');
                        }
                    }

                    function addClick() {
                        $('#search-select-allsource .slipe-box p').on('click', function () {
                            $('#search-select-allwords .slipe-box').html('').append('<p>所有字段（默认）</p>');
                            if ($(this).text() === '所有数据源（默认）') {
                                if (admin == 0) {
                                    getAllFields('GetAllFields');
                                } else {
                                    getAllFieldsN();
                                }

                            } else {
                                getAllFields('GetSpecifiedFields?name=' + $(this).text());
                            }
                        });
                    }

                    $('.change-source-btn').on('click', function () {
                        $('#search-select-allsource .slipe-box').html('').append('<p>所有数据源（默认）</p>');
                        $('#search-select-allwords .slipe-box').html('').append('<p>所有字段（默认）</p>');
                        $('#source-value').text('所有数据源（默认）');
                        $('#word-value').text('所有字段（默认）');
                        if (admin == 0) {
                            appendAllSource();
                            getAllFields('GetAllFields');
                        } else {
                            appendAllSourceN();
                            getAllFieldsN();
                        }
                        if ($('#word-value').text() === '所有字段（默认）') {
                            changeOff($('.change-word-btn'));
                        }
                        if ($('#mate-value').text() === '精确匹配（默认）') {
                            changeOff($('.change-mate-btn'));
                        }
                    });
                    $('.change-word-btn').on('click', function () {
                        if ($('#source-value').text() === '所有数据源（默认）') {
                            changeOff($('.change-source-btn'));
                        }
                        if ($('#mate-value').text() === '精确匹配（默认）') {
                            changeOff($('.change-mate-btn'));
                        }
                    });
                    $('.change-mate-btn').on('click', function () {
                        if ($('#source-value').text() === '所有数据源（默认）') {
                            changeOff($('.change-source-btn'));
                        }
                        if ($('#word-value').text() === '所有字段（默认）') {
                            changeOff($('.change-word-btn'));
                        }
                    });
                    function changeOff(n) {
                        n.attr({src: '../images/icon_search_off.png', alt: 'off'}).next().text('关闭');
                        n.parent().removeClass('blue');
                        n.parent().parent().find('.search-select-txt').show();
                        n.parent().parent().find('.search-select-b').hide();
                    }

                    //搜索数据源列表
                    $('.search-source-input img').on('click', function () {
                        var optionList = $(this).parent().parent().find('.search-select-option .slipe-box p');
                        for (var j = 0; j < optionList.length; j++) {
                            optionList.eq(j).removeClass('select-blue-p');
                        }
                        if (!!$(this).prev().val()) {
                            var key = $(this).prev().val();
                            for (var i = 0; i < optionList.length; i++) {

                                if (optionList.eq(i).text().indexOf(key) !== -1) {
                                    optionList.eq(i).addClass('select-blue-p');
                                    var oParent = optionList.eq(i).parent().parent();
                                    oParent.scrollTop(36 * i - 50);
                                    return;
                                }
                            }
                        }
                    });


                },
                error: function (err) {
                    console.log(err);
                }
            });
            if (admin == 0) {
                getAllFields('GetAllFields');
            } else {
                getAllFieldsN();
            }
            function getAllFields(n) {
                $.ajax({
                    url: 'http://' + newIp + '/jwcloud-interface/' + n,
                    success: function (data) {
                        var data = eval(data);
                        for (var i = 0; i < data.length; i++) {
                            $('#search-select-allwords .slipe-box').append('<p>' + data[i] + '</p>');
                        }
                    }
                })
            }

            function getAllFieldsN() {
                $.ajax({
                    url: 'http://' + newIp + '/jwcloud-interface/Jwcloud?interface=getSpecifiedFields&loginName=' + loginName + '&token=' + token,
                    type: 'POST',
                    data: {
                        username: loginName
                    },
                    success: function (dataStr) {
                        var data = eval('(' + dataStr + ')');
                        if (data.status == 0) {
                            var dataList = data.specialFieldsList;
                            for (var i = 0; i < dataList.length; i++) {
                                $('#search-select-allwords .slipe-box').append('<p>' + dataList[i] + '</p>');
                            }
                        } else {
                            layer(false, '您还没有登录，请点击“确定”返回登录页');
                        }
                    },
                    error: function (err) {
                        console.log(err);
                    }
                });
            }


        } else {
            $('.search-high-box').hide();
            $('.search-high i').css('backgroundImage', 'url("../images/icon_search_dropdown.png")')
        }

    });
//查重
    function findIsDiffer(a, n) {
        for (var i = 0; i < a.length; i++) {
            if (a[i] == n) {
                return false;
            }
        }
        return true;
    }

// 开关按钮效果
    $('.search-change-btn-img').on('click', function () {
        var oDiv = $(this).parent().parent();
        if ($(this).attr('alt') === 'off') {
            $(this).attr({src: '../images/icon_search_on.png', alt: 'on'});
            $(this).next().text('开启');
            $(this).parent().addClass('blue');
            oDiv.find('.search-select-txt').hide();
            oDiv.find('.search-select-b').show();
        } else {
            $(this).attr({src: '../images/icon_search_off.png', alt: 'off'});
            $(this).next().text('关闭');
            $(this).parent().removeClass('blue');
            oDiv.find('.search-select-txt').show();
            oDiv.find('.search-select-b').hide();
        }
    });
//下拉框展示隐藏效果
    $('.search-select-b').on('click', function (e) {
        e.stopPropagation();
        var box = $(this).find('.search-select-box');
        $(this).parent().parent().siblings('.search-high-item').find('.search-select-box').hide();
        if (box.css('display') === 'none') {
            box.show();
            $(this).addClass('border-b0');
            //清除搜索选中样式
//                var optionList=$(this).find('.search-select-option .slipe-box p');
//                for(var j=0;j<optionList.length;j++){
//                    optionList.eq(j).removeClass('select-blue-p');
//                }
            $(this).find('.search-select-value').find('i').css('backgroundImage', 'url("../images/icon_search_select_putaway.png")');
        } else {
            $(this).removeClass('border-b0');
            box.hide();
            $(this).find('.search-select-value').find('i').css('backgroundImage', 'url("../images/icon_search_select_dropdown.png")');
        }
        $(document).on('click', function () {
            $('.search-select-box').hide();
            $('.search-select-b').removeClass('border-b0');
            $('.search-select-value').find('i').css('backgroundImage', 'url("../images/icon_search_select_dropdown.png")');
        })
    });

    $('.search-only input,.search-only label').on('click', function (e) {
        return false;
        e.stopPropagation();
    });
    var flag=true;
    $('.search-only').on('click', 'input[type=radio]', function (e) {
        e.stopPropagation();
        if ($(this).next().text() == '姓名') {
            $('.unisonSearchBox').show();
        } else {
            $('.unisonSearch').attr('checked', 'false');
            $('.unisonSearchBox').hide();
        }
    });
    // $(document).on('click', function () {
    //     $('.search-only input[type=radio]').attr({checked: false});
    // });

//数据源搜索输入框 组织冒泡
    $('.search-source-input').click(function (e) {
        return false;
        e.stopPropagation();
    });
//下拉框选择效果
    $('.search-select-option .slipe-box').on('click', 'p', function () {
        var val = $(this).text();
        $(this).parent().parent().parent().parent().find('.search-select-value span').text(val);
    });

function clearMapData(){
    localStorage.removeItem('mapData');
}

