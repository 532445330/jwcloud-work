var newIp=newIpNum;//8080端口
var dogNotPresent = false;
var authCode = "";
var scope = "<dogscope/>";
var timer;
var checkReSend=true;
var dogId;
var stat = "";
var layerRightCount=0;
var layerErrorCount=0;
var sendData='?token='+$.cookie("login_token")+'&username='+decodeURIComponent($.cookie("login_user"));

embedTag();
if ("" == authCode) {
    authCode = getAuthCode();
}
//Get object
objAuth = getAuthObject();
//Callback function, if the dog has been removed the function will be called.
function removeDog()
{
    noDog();
}

//Callback function, if the dog still exists the function will be called.
function insertDog()
{
    stat = objAuth.Open(scope, authCode);
    if (stat != 0) {
        reportStatus(stat);
        $('#username').val('');
    }
    // Get username from the dog
    objAuth.GetDogID();
    dogID = objAuth.DogIdStr;

    if($('.layer-container').length>0){
        if(!layerRightCount){
            if($('.layer-msg').text()=='您还没有登录，请点击“确定”返回登录页'){

            }else{
                if($.cookie('dogId')==dogID){
                    var msg='检测到ukey，请继续使用';
                    layer(true,msg,true);
                    layerRightCount=1;
                }
            }

        }
    }
}

var username=decodeURIComponent($.cookie('login_user')?$.cookie('login_user'):'');
var dogId=$.cookie('dogId')?$.cookie('dogId'):'';
var userPage=$.cookie('_USER')?$.cookie('_USER'):'';
if(!dogId){
    var msg='您还没有登录，请点击“确定”返回登录页';
    $.cookie('login_user', null, { expires: 0, path: '/' });
    $.cookie('login_token', null, { expires: 0, path: '/' });
    $.cookie('news', null, { expires: 0, path: '/' });
    $.cookie('admin',null,{ expires: 0, path: '/' });
    $.cookie('formLink',null,{ expires: 0, path: '/' });
    $.cookie('linkArr',null,{ expires: 0, path: '/' });
    layer(false,msg);
}else {
    $('.login-username').text(username);
}

function checkDog()
{
    stat="";
    //Get Auth Code

    if (navigator.userAgent.indexOf("Chrome") > 0) {
        objAuth.EnumDog(authCode);
    }
    else {
        //Open Dog
        stat = objAuth.Open(scope, authCode);

        if(0 != stat)
        {
            dogNotPresent = true;
            reportStatus(stat);
        }
        else
        {
            if (dogNotPresent == true)
            {
                dogNotPresent = false;
                window.location.href = "login";
            }
        }
    }
    //Execute the check again after 2 seconds
    timer=setTimeout(checkDog, 2000);
}

loadFunc();
function loadFunc()
{
    var objAuth;

    //Get object
    objAuth = getAuthObject();

    if (navigator.userAgent.indexOf("Window") > 0)
    {
        if (navigator.userAgent.indexOf("Chrome") > 0)  //Chrome
        {
            window.addEventListener("message", function (event) {
                if (event.source != window)
                    return;
                if (event.data.type == "SNTL_FROM_HOST") {
                    var ReturnText = event.data.text;

                    if ("EnumDog" == ReturnText.InvokeMethod) {
                        if (0 == ReturnText.Status) {
                            var dog_info = ReturnText.EnumResultStr;
                            dog_info=dog_info.match(/(id=")(.*)(")/)[2];

                            if($('.layer-container').length>0){
                                console.log(dogId,dog_info);
                                if(dogId == dog_info){
                                    if(!layerRightCount){
                                        if(!layerRightCount){
                                            if($('.layer-msg').text()=='您还没有登录，请点击“确定”返回登录页'){

                                            }else{

                                                var msg='检测到ukey，请继续使用';
                                                layer(true,msg,true);
                                                layerRightCount=1;
                                            }
                                        }
                                    }
                                }
                                else{
                                    console.log(dogId ,dog_info);
                                    layer(false,'请插入正确ukey，或点击“确定”返回登录页');
                                    layerErrorCount=1;
                                }
                            }
                            return;
                        }
                        else {
                            checkReSend=true;

                            noDog();
                            return;
                        }
                    }
                    else {
                        return;
                    }
                }
            }, false);
            checkDog();
        }
        else if (window.ActiveXObject || "ActiveXObject" in window)  //IE
        {
            ieCheckDog();
        }
        else
        {
            checkDog();
        }
    }
    else if (navigator.userAgent.indexOf("Mac") > 0)
    {
        checkDog();
    }
    else if (navigator.userAgent.indexOf("Linux") > 0)
    {
        checkDog();
    }
    else
    {
        ;
    }
}
function ieCheckDog(){
    stat = objAuth.Open(scope, authCode);
    if (stat != 0) {
        noDog();
    }
    objAuth.SetCheckDogCallBack("insertDog", "removeDog");
    objAuth.Close();
}

//无超级狗执行函数
function noDog(){
    if(!layerErrorCount){
        layer(false,'未检测到ukey，请点击“确定”返回登录页');
        layerErrorCount=1;
    }
}
function layer(boolean,msg,auto){
    $('body').css({overflow:'hidden'});
    $('.append').html('');
    var oLayer='<div class="layer-bg-dim"></div>'+
        '<div class="layer-container">'+
        '<h2>提示</h2>'+
        '<div class="layer-body">'+
        '<span></span>'+
        '<p class="layer-msg">'+msg+'</p>'+
        '<a href="'+(!!userPage?"/wanganLogin":"/login")+'"><input type="button" value="确定" id="skip-login"/></a>'+
        '</div> </div>';
    $('.append').append(oLayer);
    $('.layer-bg-dim').css({'height':$(window).height()});
    $(window).on('resize',function(){
        $('.layer-bg-dim').css({'height':$(window).height()});
    });
    if(boolean){
        $('.layer-container input').remove();
        $('.layer-container').addClass('layer-right-container');
        layerErrorCount=0;
    }else{
        $('.layer-container').removeClass('layer-right-container');
        layerRightCount=0;
    }
    if(auto){
        setTimeout(function(){
            $('.layer-bg-dim').remove();
            $('.layer-container').remove();
            $('body').css({overflow:'auto'});
        },1000);
    }
    $('#skip-login').on('click',function(){
        $.cookie('login_user', null, { expires: 0, path: '/' });
        $.cookie('dogId', null, { expires: 0, path: '/' });
        $.cookie('editUser', null, { expires: 0, path: '/' });
        $.cookie('history', null, { expires: 0, path: '/' });
        $.cookie('admin',null,{ expires: 0, path: '/' });
        $.cookie('hurl',encodeURIComponent(window.location.href));
        $.cookie('formLink',null,{ expires: 0, path: '/' });
        $.cookie('linkArr',null,{ expires: 0, path: '/' });
    });
}
//退出
function exit(){
    $.cookie('login_user', null, { expires: 0, path: '/' });
    $.cookie('editUser', null, { expires: 0, path: '/' });
    $.cookie('history', null, { expires: 0, path: '/' });
    $.cookie('admin',null,{ expires: 0, path: '/' });
    $.cookie('hurl',null,{expires:0,path:'/'});
    $.cookie('formLink',null,{ expires: 0, path: '/' });
    $.cookie('linkArr',null,{ expires: 0, path: '/' });
    $.cookie('currentFormName',null,{ expires: 0, path: '/' });
    console.log(dogId);
    $.ajax({
        url:'http://'+newIp+'/jwcloud/user/logout',
        type:'POST',
        data:{
            dogId:dogId
        },
        xhrFields:{
            withCredentials:true
        },
        success:function(data){
            if(data.status==0){
                $.cookie('dogId',null,{ expires: 0, path: '/' });
                if(!!userPage){
                    window.location.href='/wanganLogin';
                }else{
                    window.location.href='/login';
                }
            }else{
                alert(data.message);
            }
        }
    })
}
$('.login-info-container').hover(function(){
    $('.login-info-box').show();
},function(){
    $('.login-info-box').hide();
});
$('#exit ,.out-user').on('click',function(){
    exit();
});
$('.login-info a').on('click',function(){
    exit();
});
checkUserNews();
//判断该用户是否有未读消息
function checkUserNews(){
    $.ajax({
        url:'http://'+newIp+'/jwcloud/user/newInfo',
        type:'POST',
        data:{
            dogId:dogId
        },
        xhrFields:{
            withCredentials:true
        },
        success:function(data){
            if($('#yes_user').length>0){
                if(data.news== 0){
                    $('.no-read').show();
                }else{
                    $('.no-read').hide();
                }
            }
        },
        error:function(err){
            console.log(err);
        }
    });
}




















