    var ip=ipNum;
    var lastIp=lastIpNum;
    var newIp=newIpNum;
    var loginName=decodeURIComponent($.cookie('login_user'));
    var token=$.cookie('login_token');
    var admin=$.cookie('admin');
    var dogId=$.cookie('dogId');
    var sourceTeamList;
    var sourceUserList;
    var submitTeamList;
    var submitUserList;
    var sourceState=0;
    var batch=0;
    var secondClass;
    var sourceTitle=[];
    var appointTime='';         //指定时间字段
    var newSourceB=0;
    var limitNewSource=false;  //添加数据源限制;
    var timer;
    var timer2;
    var addTitle=1;
    var filesArr = [];  //上传文件列表
    window.onbeforeunload = null;
    // if(loginName!='admin_03'){
    //     $('.white-list').remove();
    //     $('.white-list-container').remove();
    // }

    $('.login-info span i').text(loginName);
    $(window).on('resize load',function(){
        $('.server-b').css({
            height:$(window).height()-150
        });
        $('.admin-manage-section').css({
            height:($(window).height()-$('.header').height())+'px'
        });
        //弹窗虚化背景高度
        $('.layer-bg').css({height:$(window).height()});
    });
    //选项卡效果
    $('.manage-nav').on('click','li',function(){
        $(this).addClass('on').siblings('li').removeClass('on');
        $('.connent-box').eq($(this).index()).show().siblings('.connent-box').hide();
    });
    $('.manage-nav li.server-btn').on('click',function(){
        getHealthState();
        getHardPanState();
        getServerDataCount();
        timer=setInterval(function(){
            if($('.server-box').css('display')=='block'){
            getHealthState();
            getHardPanState();
            getServerDataCount();
            }
        },5000);
        $('.manage-nav li').not('.server-btn').on('click',function(){
            window.clearInterval(timer);
        });
    });
    bindClick('.white-list','#manage-white-list','/whiteList');
    bindClick('.account-btn','#account-manage-iframe','/accountManage');
    bindClick('.manage-source-btn','#w-account-manage-iframe','/sourceManage');
    bindClick('.up-forms-btn','#upload-forms-iframe','/uploadForms');
    function bindClick(o,b,src){
       $(o).on('click',function(){
           $(b).attr('src',src);
           $(o).off('click');
       })
    }
    function getHealthState(){   //得到服务器健康值
        $.ajax({
            url:'http://'+ip+'/_cluster/health',
            data:{
                dogId:dogId
            },
            success:function(data){
                if(data.status=='green'){
                    $('.server-status').text('正常');
                }
                else if(data.status=='yellow'){
                    $('.server-status').text('数据变动');
                }
                else if(data.status == 'red'){
                    $('.server-status').text('停运');
                }
            },
            error:function(err){
                console.log(err);
            }
        });
    }
    function getHardPanState(){    //得到硬盘状态
        $.ajax({
            url:'http://'+newIp+'/jwcloud/disk/get',
            type:'POST',
            data:{
                dogId:dogId
            },
            xhrFields:{
                withCredentials:true
            },
            success:function(data){
                if(data.status==0){
                    var allSize1=data.disk[0].server1.allSize;
                    var useSize1=data.disk[0].server1.useSize;
                    var allSize2=data.disk[1].server2.allSize;
                    var useSize2=data.disk[1].server2.useSize;
                    var allSize3=data.disk[2].server3.allSize;
                    var useSize3=data.disk[2].server3.useSize;
                    circleProgress('hard-pan1',parseInt((useSize1/allSize1)*100));
                    circleProgress('hard-pan2',parseInt((useSize2/allSize2)*100));
                    circleProgress('hard-pan3',parseInt((useSize3/allSize3)*100));
                }else{
                    layer(false,'您还没有登录，请点击“确定”返回登录页');
                }
            },
            error:function(err){
                console.log(err);
            }
        })
    }
    function getServerDataCount(){         //得到服务器数据数量
        $.ajax({
            url:'http://'+newIp+'/jwcloud/file/getTotal',
            type:'POST',
            data:{
                dogId:dogId
            },
            xhrFields:{
                withCredentials:true
            },
            success:function(data){
                if(data.status==0){
                    var count = data.total;
                    count=parseInt(count).toLocaleString();
                    $('.serverDataCount h2 span').text(count);
                }else{
                    layer(false,'您还没有登录，请点击“确定”返回登录页');
                }
            }
        });

    }
    function changeState(obj){   //单选按钮切换状态函数
        var radioCheck= $(obj).val();
        if("1"==radioCheck){
            $(obj).attr("checked",false);
            $(obj).val("0");
        }else{
            $(obj).val("1");
        }
    }
    $('.uploader-container').on('click','.uploader-data-view table .add-mark-box dl:not(".level") input[type=radio]',function(){
        var radioCheck= $(this).val();
        if("1"==radioCheck){
            $(this).attr("checked",false);
            $(this).val("0");
        }else{
            $(this).parent().parent().find('input[type=radio]').attr('checked',false).val("0");
            var name=$(this).attr('name');
            $('.load-view-table tr input[name='+name+']').attr('checked',false).val("0");
            $(this).attr('checked','checked').val("1");
        }
    });
    // 加载数据源
    if(admin==0) {
        //获取二级分类
        $.ajax({
            url:'http://'+lastIp+'/index.php',
            type:'POST',
            async: false,
            data:JSON.stringify({
                interface:'getType',
                act:'getAll',
                Token:token,
                username:loginName
            }),
            success:function(dataStr){
                var data =eval('('+dataStr+')');
                if(data.status==0){
                    secondClass=data.Item
                }
            },
            error:function(err){
                console.log(err);
            }
        });

        //分类部分下拉框选择
        for(var i=0;i<secondClass.length;i++){
            var oP='';
            if(secondClass[i].typeName!='取证'){
                oP='<p data="typeId'+secondClass[i].typeId+'">'+secondClass[i].typeName.toUpperCase()+'</p>';
                $('.search-class-option .slipe-box').append(oP);
            }

        }
        //一级类二级类联动
        $('.search-class-option').on('click','.slipe-box p',function(){
            limitNewSource=false;
            $('.search-second-class-option .slipe-box').html('');
            $('.search-source-cover').show();
            $('.search-second-class-cover').show();
            $('.search-second-class-value span').text('选择上传的二级类目');
            $('#source-value').text('选择上传的目标数据源');
            var val = $(this).text();
            var data=$(this).attr('data');
            $('.search-class-value span').text(val).attr({data:data});
            var typeId= Number($(this).attr('data').replace('typeId',''));
            var second=secondClass[typeId-1].son;
            if(!!second){
                $('.search-second-change').show();
                $('.search-second-class-cover').hide();
                for(var i=0;i<second.length;i++){
                    var oP='<p data="typeId'+second[i].typeId+'">'+second[i].typeName.toUpperCase()+'</p>';
                    $('.search-second-class-option .slipe-box').append(oP);
                }
            }else{
                $('.search-second-change').hide();
                $('.search-second-class-cover').show();
                $('.search-second-class-value span').text('选择上传的二级类目');
                $('.search-second-class-option .slipe-box').html('');
            }
        });
        //二级、数据源联动
        $('.search-second-class-option').on('click','.slipe-box p',function(){
            $('.uploader-source .search-select-box').hide();
            $('#source-value').text('选择上传的目标数据源');
            $('.search-select-value i').css('backgroundImage', 'url("../images/icon_search_select_dropdown.png")');
            $('.uploader-source .search-select-title').removeClass('border-b0');
            $('.search-select-option .slipe-box').html('');
            $.ajax({
                url: 'http://' + lastIp + '/index.php',
                type: 'POST',
                data:JSON.stringify({
                    interface:'getSource',
                    act:'getSourceType',
                    typeId:$(this).attr('data').replace(/typeId/g,''),
                    Token:token,
                    username:loginName
                }),
                success: function (dataStr) {
                    var dataObj = eval('(' + dataStr + ')');
                    limitNewSource=false;
                    if(dataObj.status==0){
                        var data = dataObj.Item;
                        if(data.length!=0){
                            $('.search-source-cover').hide();
                            //data=['楼盘备案','物流快递','购房登记','出行记录','通讯记录','人员信息'];
                            appendAllSource();
                            function appendAllSource() {
                                for (var i in data) {
                                    $('#search-select-allsource .slipe-box').append('<p data="'+data[i].sourceId+'" title="'+data[i].sourceName+'">' + data[i].sourceName + '</p>');
                                }
                            }
                        }else{
                            $('.search-source-cover').show();
                        }
                    }else{
                        $('.search-source-cover').show();
                    }
                },
                error: function (err) {
                    console.log(err);
                }
            });
        });
        //数据源获取相应字段
        $('#search-select-allsource').on('click','.slipe-box p',function(){
            $.ajax({
                url:'http://' + lastIp + '/index.php',
                type:'POST',
                data:JSON.stringify({
                    interface:'getField',
                    act:'getFieldBySource',    //获得数据源对应字段
                    sourceName:$(this).text(),
                    typeId:$('#source-second-class-value').attr('data').replace(/^typeId/,''),
                    Token:token,
                    username:loginName
                }),
                success:function(dataStr){
                    var data=eval('('+dataStr+')');
                    if(data.status==0){
                        newSourceB=-1;          //不是新数据源
                        if(!limitAdd){
                            limitAdd=true;
                            $('.load-view-table tr:eq(0)').remove();
                        }
                        $('.uploader-source-title tr').html('');
                        $('.uploader-source-title').show();
                        var fieldList=data.Item.field;
                        $('.uploader-data-view-title a').removeClass('uploader-view-change-btn').removeClass('uploader-view-add-btn');
                        $('.uploader-data-view-title a').off('click');
                        $('.uploader-data-view-title a').hover(function(){$('.noEtidHints').show()},function(){$('.noEtidHints').hide()});
                        $('.load-view-table .table-title').parent().removeClass('editTh').find('div:not(".table-title")').remove();
                        $('.load-view-table .table-title').removeClass('table-title');
                        var oDiv='';
                        getField();
                        for(var i=0;i<uniqueList.length;i++){
                            var oSpan='<span>'+uniqueList[i].uniquefieldname+'</span>';
                            oDiv+=oSpan;
                        }
                        for (var i = 0; i < fieldList.length; i++) {
                            var selectTime;
                            var unique = '';
                            var star1 = '';
                            var star2 = '';
                            var star3 = '';
                            var positionBox = '';
                            var stationBox = '';
                            if($('#source-second-class-value').attr('data')=='typeId21'){
                                var longitudeChecked = '';
                                var latitudeChecked = '';
                                var LAC='';
                                var CID='';
                                var SID='';
                                var BID='';
                                var NID='';
                                if(fieldList[i].isLongitude==0){
                                    longitudeChecked='checked="checked"';
                                }
                                if(fieldList[i].isLatitude==0){
                                    latitudeChecked='checked="checked"';
                                }
                                if(fieldList[i].isLAC==0){
                                    LAC = 'checked="checked"';
                                }
                                if(fieldList[i].isCID==0){
                                    CID = 'checked="checked"';
                                }
                                if(fieldList[i].isSID==0){
                                    SID = 'checked="checked"';
                                }
                                if(fieldList[i].isBID==0){
                                    BID = 'checked="checked"';
                                }
                                if(fieldList[i].isNID==0){
                                    NID = 'checked="checked"';
                                }
                                positionBox='<dl class="position-box"><dt>经纬</dt><dd><input type="radio" id="longitude'+i+'" name="position-longitude" '+longitudeChecked+' /><label for="longitude'+i+'">经度</label></dd><dd><input type="radio" id="latitude'+i+'" name="position-latitude" '+latitudeChecked+'/><label for="latitude'+i+'">纬度</label></dd></dl><span></span>';
                                stationBox='<dl class="station-box"><dt>基站</dt><dd><input type="radio" id="LAC'+i+'" name="LAC" '+LAC+' /><label for="LAC'+i+'">LAC</label></dd><dd><input type="radio" id="CID'+i+'" name="CID" '+CID+'/><label for="CID'+i+'">CID</label></dd>' +
                                    '<dd><input type="radio" id="BID'+i+'" name="BID" '+BID+'/><label for="BID'+i+'">BID</label></dd><dd><input type="radio" id="SID'+i+'" name="SID" '+SID+'/><label for="SID'+i+'">SID</label></dd><dd><input type="radio" id="NID'+i+'" name="NID" '+NID+'/><label for="NID'+i+'">NID</label></dd></dl><span></span>';
                            }
                            switch (fieldList[i].level)
                            {
                                case '1':
                                    star1='checked="checked"';
                                    break;
                                case '2':
                                    star2='checked="checked"';
                                    break;
                                case '3':
                                    star3='checked="checked"';
                                    unique='<div class="search-grade search-only-field fl"><span class="search-grade-title">'+fieldList[i].name+'</span><i></i> <div class="search-grade-list">'+oDiv+ '<span>其他</span><div class="cover"></div></div></div>';
                                    break;
                            }
                            if(fieldList[i].isTime==1){
                                selectTime='checked="checked"';
                            }
                            // var oTh = '<th class="clearfix"><div class="table-title fl">'+fieldList[i].fieldName+'</div><div class="fl">' +
                            //     '<div class="search-grade"><span class="search-grade-title search-grade-value">'+level+'</span><i></i> <div class="search-grade-list">'+
                            //     '<span onclick="hideOnlyField(this)">1星</span><span onclick="hideOnlyField(this)">2星</span><span onclick="showOnlyField(this)">3星</span></div></div><div class="search-grade-radio"><input type="radio" id="time'+i+'" name="select-time" '+selectTime+' value="time'+i+'"><label for="time'+i+'">筛选时间</label></div></div>'+unique+'</th>';

                            var oTh='<th class="clearfix"><div class="table-title fl">'+fieldList[i].fieldName+'</div><div class="fl add-mark-container"><div class="clearfix add-mark-btn" onclick="showMarkBox()"><p class="fl"><span></span><span></span><span></span></p>'+
                                '<p class="fl">添加标识</p></div>'+
                                '<div class="add-mark-box clearfix"><dl class="level"><dt>星级</dt><dd><input type="radio" id="start'+i+'_1" name="star" onclick="hideOnlyField(this)" level="1" '+star1+'><label for="start'+i+'_1" onclick="hideOnlyField(this)">一星</label></dd><dd><input type="radio" id="start'+i+'_2" name="star" onclick="hideOnlyField(this)" level="2" '+star2+'><label for="start'+i+'_2"  onclick="hideOnlyField(this)">二星</label></dd><dd><input type="radio" id="start'+i+'_3" name="star" onclick="showOnlyField(this)" level="3" '+star3+'><label for="start'+i+'_3" onclick="showOnlyField(this)">三星</label></dd></dl><span></span>'+positionBox+stationBox+
                                '<dl><dt>时间</dt><dd><input type="radio" id="screen-time'+i+'" name="screen-time" '+selectTime+'/><label for="screen-time'+i+'" >筛选时间</label></dd></dl><b></b><div class="cover"></div></div></div>'+unique+'</th>';

                            $('.uploader-source-table tr').append(oTh);

                        }
                        $('.search-grade').off('click');
                        setMarkBoxWidth();
                    }else{
                        isNewSource();
                    }
                },
                error:function(err){
                    console.log(err);
                }
            })
        });
        function isNewSource(){
            newSourceB=0;   //是新数据源
            if(!limitAdd){
                limitAdd=true;
                $('.load-view-table tr:eq(0)').remove();
            }
            $('.uploader-source-table tr').html('');
            $('.uploader-source-title').hide();
            $('.uploader-data-view-title a:eq(0)').addClass('uploader-view-change-btn');
            $('.uploader-data-view-title a:eq(1)').addClass('uploader-view-add-btn');
            $('.uploader-data-view-title a').unbind('mouseenter').unbind('mouseleave');
            $('.uploader-data-view').off('click','.search-grade');

            if(!limitAdd || limitNewSource){
                if($('.load-view-table tr th:eq(0) div').length>1){

                }else{

                    for(var i=0;i<$('.load-view-table th').length;i++){
                        $('.load-view-table th').eq(i).find('div:eq(0)').addClass('table-title');
                        var positionBox='';
                        var stationBox='';
                        if($('#source-second-class-value').attr('data')=='typeId21'){
                            positionBox='<dl class="position-box"><dt>经纬</dt><dd><input type="radio" id="longitude'+i+'" name="position-longitude" /><label for="longitude'+i+'">经度</label></dd><dd><input type="radio" id="latitude'+i+'" name="position-latitude"/><label for="latitude'+i+'">纬度</label></dd></dl><span></span>';
                            stationBox='<dl class="station-box"><dt>基站</dt><dd><input type="radio" id="LAC'+i+'" name="LAC"/><label for="LAC'+i+'">LAC</label></dd><dd><input type="radio" id="CID'+i+'" name="CID"/><label for="CID'+i+'">CID</label></dd>' +
                                '<dd><input type="radio" id="BID'+i+'" name="BID"/><label for="BID'+i+'">BID</label></dd><dd><dd><input type="radio" id="SID'+i+'" name="SID"/><label for="SID'+i+'">SID</label></dd><input type="radio" id="NID'+i+'" name="NID"/><label for="NID'+i+'">NID</label></dd></dl><span></span>';
                        }

                        // var oTh='<div class="fl"><div class="search-grade"><span class="search-grade-title search-grade-value">1星</span><i></i> <div class="search-grade-list">'+
                        //     '<span onclick="hideOnlyField(this)">1星</span><span onclick="hideOnlyField(this)">2星</span><span onclick="showOnlyField(this)">3星</span></div></div><div class="search-grade-radio"><input type="radio" id="time'+i+'" name="select-time" value="time'+i+'"><label for="time'+i+'">筛选时间</label></div></div></th>'

                        var oTh='<div class="fl add-mark-container"><div class="clearfix add-mark-btn" onclick="showMarkBox(this)"><p class="fl"><span></span><span></span><span></span></p>'+
                            '<p class="fl">添加标识</p></div>'+
                            '<div class="add-mark-box clearfix"><dl class="level"><dt>星级</dt><dd><input type="radio" id="start'+i+'_1" name="star'+i+'" checked="checked" onclick="hideOnlyField(this)" level="1"><label for="start'+i+'_1" onclick="hideOnlyField(this)">一星</label></dd><dd><input type="radio" id="start'+i+'_2" name="star'+i+'" onclick="hideOnlyField(this)" level="2"><label for="start'+i+'_2" onclick="hideOnlyField(this)">二星</label></dd><dd><input type="radio" id="start'+i+'_3" name="star'+i+'" onclick="showOnlyField(this)" level="3"><label for="start'+i+'_3" onclick="showOnlyField(this)">三星</label></dd></dl><span></span>'+positionBox+stationBox+
                            '<dl><dt>时间</dt><dd><input type="radio" id="screen-time'+i+'" name="screen-time"/><label for="screen-time'+i+'">筛选时间</label></dd></dl><b></b></div></div>';

                        $('.load-view-table th').eq(i).append(oTh);
                    }
                    setMarkBoxWidth();
                }

            }else{

            }
            bindGradeClick();
            //修改字段按钮点击
            $('.uploader-view-change-btn').on('click', function () {
                if(limitAdd){
                    $('.table-title').attr({'contenteditable': true});
                    $('.load-view-table th').addClass('editTh');
                }
            });
            //添加字段 按钮点击
            $('.uploader-view-add-btn').on('click', function () {
                if (limitAdd) {
                    var addLength = $('.load-view-table tr:eq(2) td').length;
                    $('.load-view-table .search-grade,.load-view-table .add-mark-container').remove();
                    $('.table-title').parent().removeClass('editTh');
                    $('.table-title').removeAttr('contenteditable').removeAttr('class');
                    var oTr = '<tr></tr>';
                    $('.load-view-table').prepend(oTr);

                    for (var i = 0; i < addLength; i++) {
                        var positionBox='';
                        var stationBox='';
                        if($('#source-second-class-value').attr('data')=='typeId21'){
                            positionBox='<dl class="position-box"><dt>经纬</dt><dd><input type="radio" id="longitude'+i+'" name="position-longitude" /><label for="longitude'+i+'">经度</label></dd><dd><input type="radio" id="latitude'+i+'" name="position-latitude"/><label for="latitude'+i+'">纬度</label></dd></dl><span></span>';
                            stationBox='<dl class="station-box"><dt>基站</dt><dd><input type="radio" id="LAC'+i+'" name="LAC" /><label for="LAC'+i+'">LAC</label></dd><dd><input type="radio" id="CID'+i+'" name="CID" /><label for="CID'+i+'">CID</label></dd>' +
                                '<dd><input type="radio" id="BID'+i+'" name="BID" /><label for="BID'+i+'">BID</label></dd><dd><input type="radio" id="SID'+i+'" name="SID"/><label for="SID'+i+'">SID</label></dd><dd><input type="radio" id="NID'+i+'" name="NID" /><label for="NID'+i+'">NID</label></dd></dl><span></span>';
                        }

                        // var oTh = '<th class="clearfix editTh"><div class="table-title fl" contenteditable="true"></div><div class="fl">' +
                        //     '<div class="search-grade"><span class="search-grade-title search-grade-value">1星</span><i></i> <div class="search-grade-list">'+
                        //     '<span onclick="hideOnlyField(this)">1星</span><span onclick="hideOnlyField(this)">2星</span><span onclick="showOnlyField(this)">3星</span></div></div><div class="search-grade-radio"><input type="radio" id="time'+i+'" name="select-time" value="time'+i+'"><label for="time'+i+'">筛选时间</label></div></div></th>';

                        var oTh='<th class="clearfix editTh"><div class="table-title fl" contenteditable="true"></div><div class="fl add-mark-container"><div class="clearfix add-mark-btn" onclick="showMarkBox(this)"><p class="fl"><span></span><span></span><span></span></p>'+
                            '<p class="fl">添加标识</p></div>'+
                            '<div class="add-mark-box clearfix"><dl class="level"><dt>星级</dt><dd><input type="radio" id="start'+i+'_1" name="star'+i+'" checked=checked"  onclick="hideOnlyField(this)" level="1"><label for="start'+i+'_1" onclick="hideOnlyField(this)">一星</label></dd><dd><input type="radio" id="start'+i+'_2" name="star'+i+'" onclick="hideOnlyField(this)" level="2"><label for="start'+i+'_2" onclick="hideOnlyField(this)">二星</label></dd><dd><input type="radio" id="start'+i+'_3" name="star'+i+'" onclick="showOnlyField(this)" level="3"><label for="start'+i+'_3" onclick="showOnlyField(this)">三星</label></dd></dl><span></span>'+positionBox+stationBox+
                            '<dl><dt>时间</dt><dd><input type="radio" id="screen-time'+i+'" name="screen-time"/><label for="screen-time'+i+'">筛选时间</label></dd></dl><b></b></div></div></th>';

                        $('.load-view-table tr:first-child').append(oTh);

                    }
                    setMarkBoxWidth();
                    limitAdd = false;
                }

            });
        }
        function showMarkBox(n){
            var $box=$(n).parent().find('.add-mark-box');
            if($box.css('display')=='none'){
                $box.show();
            }else{
                $box.hide();
            }
        }

        $('.search-second-class-option').on('click','.slipe-box p',function(){
            var val = $(this).text();
            var data=$(this).attr('data');
            $('.search-second-class-value span').text(val).attr({data:data});
        });
        //查重
        function findIsDiffer(a,n) {
            for (var i = 0; i < a.length; i++) {
                if (a[i] == n) {
                    return false;
                }
            }
            return true;
        }
//设置表示狂宽度
        function setMarkBoxWidth(){
            setTimeout(function(){
                var width=0;
                var ele=$('.add-mark-box').eq(0).find('dl');
                for(var i=0;i<ele.length;i++){
                    width+=ele.eq(i).outerWidth(true);
                }
                $('.add-mark-box').css({
                    width:width+10
                })
            },0);
        }


//下拉框展示隐藏效果
        $('.search-select').on('click', function (e) {
            e.stopPropagation();
            var box = $(this).find('.search-select-box');
            if (box.css('display') == 'none') {
                $('.search-select .search-select-box').hide();
                $('.search-select-title').removeClass('border-b0');
                $(this).addClass('border-b0');
                box.show();
                //清除搜索选中样式
//                var optionList=$(this).find('.search-select-option .slipe-box p');
//                for(var j=0;j<optionList.length;j++){
//                    optionList.eq(j).removeClass('select-blue-p');
//                }
                $('.search-select p i').css('backgroundImage', 'url("../images/icon_search_select_dropdown.png")');
                $(this).find('.search-select-value').find('i').css('backgroundImage', 'url("../images/icon_search_select_putaway.png")');
                $(this).find('.search-class-value').find('i').css('backgroundImage', 'url("../images/icon_search_select_putaway.png")');
                $(this).find('.search-second-class-value').find('i').css('backgroundImage', 'url("../images/icon_search_select_putaway.png")');

            } else {
                $(this).removeClass('border-b0');
                $('.search-select p i').css('backgroundImage', 'url("../images/icon_search_select_dropdown.png")');
                box.hide();
                $(this).find('.search-select-value').find('i').css('backgroundImage', 'url("../images/icon_search_select_dropdown.png")');
                $(this).find('.search-class-value').find('i').css('backgroundImage', 'url("../images/icon_search_select_dropdown.png")');
                $(this).find('.search-second-class-value').find('i').css('backgroundImage', 'url("../images/icon_search_select_dropdown.png")');
            }
        });
        $(document).click(function () {
            $('.search-select-box').hide();
            $('.search-select').removeClass('border-b0');
            $('.uploader-class-box').find('i').css('backgroundImage', 'url("../images/icon_search_select_dropdown.png")');
            $('.uploader-source').find('i').css('backgroundImage', 'url("../images/icon_search_select_dropdown.png")');
            $('.search-grade-list').hide();
        });
//搜索数据源列表
        $('.search-source-input img').on('click', function () {
            var optionList = $(this).parent().parent().find('.search-select-option .slipe-box p');
            for (var j = 0; j < optionList.length; j++) {
                optionList.eq(j).removeClass('select-blue-p');
            }
            if (!!$(this).prev().val()) {
                var key = $(this).prev().val();
                for (var i = 0; i < optionList.length; i++) {

                    if (optionList.eq(i).text().indexOf(key) !== -1) {
                        optionList.eq(i).addClass('select-blue-p');
                        var oParent = optionList.eq(i).parent().parent();
                        oParent.scrollTop(36 * i - 50);
                        return;
                    }
                }
            }
        });
//数据源搜索输入框 阻止冒泡
        $('.search-source-input').click(function (e) {
            return false;
            e.stopPropagation();
        });
//下拉框选择效果
        $('.search-select-option .slipe-box').on('click', 'p', function () {
            var val = $(this).text();
            $('.search-select-value span').text(val).attr({title:val});
            sourceState=-1;
            // $.ajax({   //获取权限列表
            //     url: 'http://'+newIp+'/jwcloud-interface/Jwcloud?interface=getSourceRights&loginName='+loginName+'&token='+token,
            //     data: {
            //         sourceName:val
            //     },
            //     success:function(dataStr){
            //         var data=eval('('+dataStr+')');
            //         if(data.status == 0) {
            //             if(data.userRights == 'all'){
            //                 $('#all-user').trigger('click');
            //             }
            //             else if(data.status == 'team'){
            //                 $('#special-team').trigger('click');
            //             }
            //             else if(data.status == 'user'){
            //                 $('#special-user').trigger('click');
            //             }
            //             sourceTeamList=data.teamList;
            //             sourceUserList=data.userList;
            //             eachTeamList();
            //             eachUserList();
            //         }
            //         else{
            //             layer(false,'您还没有登录，请点击“确定”返回登录页')
            //         }
            //     },
            //     error:function(err){
            //         console.log(err);
            //     }
            // })
        });
        function eachTeamList(){
            $('.special-team-ul').html('');
            if(sourceState==0){
                for(var i=0;i<sourceTeamList.length;i++){
                    var oLi = '<li><input name="' +sourceTeamList[i] + '" type="checkbox" id="' + sourceTeamList[i] + '"/><label for="' +sourceTeamList[i]+ '">' + sourceTeamList[i] + '</label></li>';
                    $('.special-team-ul').append(oLi);
                }
            }else{
                for (var i = 0; i < sourceTeamList.length; i++) {
                    var teamName = sourceTeamList[i].teamName;
                    var state = sourceTeamList[i].state;
                    var checked;
                    if (state == 0) {
                        checked = 'checked="true"';
                    }
                    else {
                        checked = '';
                    }
                    var oLi = '<li><input name="' + teamName + '" type="checkbox" id="' + teamName + '" ' + checked + ' /><label for="' + teamName + '">' + teamName + '</label></li>'
                    $('.special-team-ul').append(oLi);
                }
            }

        }
        function eachUserList(){
            $('.special-user-ul').html('');
            if(sourceState == 0){
                for(var i=0;i<sourceUserList.length;i++){
                    var oLi = '<li><input name="' +sourceUserList[i] + '" type="checkbox" id="' + sourceUserList[i] + '"/><label for="' +sourceUserList[i]+ '">' + sourceUserList[i] + '</label></li>';
                    $('.special-user-ul').append(oLi);
                }
            }else{
                for (var j = 0; j < sourceUserList.length; j++) {
                    var userName = sourceUserList[j].username;
                    var state = sourceUserList[j].state;
                    var checked;
                    if (state == 0) {
                        checked = 'checked="true"';
                    }
                    else {
                        checked = '';
                    }
                    var oLi = '<li><input name="' + userName + '" type="checkbox" id="' + userName + '" ' + checked + ' /><label for="' + userName + '">' + userName+ '</label></li>'
                    $('.special-user-ul').append(oLi);
                }
            }
        }
        //  指定用户部分效果
        $('.special-team-confirm').on('click',function(){
            sourceTeamList=[];
            sourceState=-1;
            var teamList=$('.special-team-ul li');
            for(var i=0;i<teamList.length;i++){
                var isChecked=teamList.eq(i).find('input').attr('checked');
                var name=teamList.eq(i).find('input').attr('name');
                if(!!isChecked){
                    sourceTeamList.push({teamName:name,state:0});
                }else{
                    sourceTeamList.push({teamName:name,state:-1});
                }
            }
            $('.special-box').hide();
        });
        $('.special-team-off').on('click',function(){
            $('.special-box').hide();
        });
        $('.special-user-confirm').on('click',function(){
            sourceUserList=[];
            sourceState=-1;
            var userList=$('.special-user-ul li');
            for(var i=0;i<userList.length;i++){
                var isChecked=userList.eq(i).find('input').attr('checked');
                var name=userList.eq(i).find('input').attr('name');
                if(!!isChecked){
                    sourceUserList.push({username:name,state:0});
                }else{
                    sourceUserList.push({username:name,state:-1});
                }
            }
            $('.special-box').hide();
        });
        $('.special-user-off').on('click',function(){
            $('.special-box').hide();
        });
        //新建数据源弹窗弹出
        $('.uploader-create-source-btn').on('click', function () {
            if(limitNewSource)return;
            $('.new-creatsource-box').show();
            $('.layer-bg').show();
        });
        //新建数据源弹窗关闭
        $(' .box-off,.new-creat-concel').on('click', function () {
            $('#new-source-txt').val('');
            $('.layer-layout').hide();
            $('.layer-bg').hide();
        });
        //重新创建按钮
        $('.recreat-confirm').on('click', function () {
            $('.hints-layer').hide();
            $('.new-creatsource-box').show();
        });
        //新建数据源按钮
        $('.new-creat-confirm').on('click', function () {
            if($('.manage-nav li.on').text()=='上传数据'){
                if(limitNewSource)return;
                var newSource = $('#new-source-txt').val();
                var reg = new RegExp('^[0-9a-z\u4E00-\u9FA5-_\(\)]+$');
                if(newSource.length==0){
                    setLayer(false, '请输入新建数据源名称', true);
                }
                else if(newSource.length>=80){
                    setLayer(false, '数据源名称长度超过上限', true);
                }
                else if(!reg.test(newSource)){
                    alert('数据源名称格式不正确');
                }
                else{
                    $('#new-source-txt').val('');
                    $('.new-creatsource-box').hide();
                    $.ajax({
                        url:'http://'+newIp+'/jwcloud/source/findSource',
                        type : 'POST',
                        sync : 'false',
                        data : {
                            dogId:dogId
                        },
                        xhrFields:{
                            withCredentials:true
                        },
                        success:function(data){
                            if(data.status == 0){
                                var dataArr = data.dataSource;
                                for (var i = 0; i < dataArr.length; i++) {
                                    var arr=dataArr[i].split('-');
                                    if (newSource == arr[arr.length-1]) {
                                        setLayer(false, '您创建的数据源已经存在，请重新创建或者取消', true);
                                        return;
                                    }
                                }
                                limitNewSource=true;
                                $('#source-value').text(newSource).attr('title',newSource);
                                // $('#search-select-allsource .slipe-box').prepend('<p title="'+newSource+'">' + newSource + '</p>');
                                isNewSource();
                                setLayer(true, '数据源创建完成', false);
                                $('.search-source-cover').hide();
                                setTimeout(function () {
                                    $('.hints-layer').fadeOut();
                                    $('.layer-bg').fadeOut();
                                }, 1000);

                                // $.ajax({         //或得用户组
                                //     url:'http://'+newIp+'/jwcloud-interface/Jwcloud?interface=allUserAndAllUserGroup&loginName='+loginName+'&token='+token,
                                //     type:'POST',
                                //     success:function(dataStr){
                                //         var data=eval('('+dataStr+')');
                                //         if(data.status==0){
                                //             sourceTeamList=data.teamList;
                                //             sourceUserList=data.userList;
                                //             sourceState=0;
                                //             eachTeamList();
                                //             eachUserList();
                                //         }else{
                                //             layer(false,'您还没有登录，请点击“确定”返回登录页');
                                //         }
                                //     }
                                // });
                            }else{
                                layer(false,'您还没有登录，请点击“确定”返回登录页');
                            }

                        },
                        error:function(err){
                            console.log(err);
                        }
                    });
                }
            }

        });

        //上传插件代码
        var uploadImg='';
        var uploader = WebUploader.create({
            // swf文件路径
            swf: '../javascripts/Uploader.swf',
            // 文件接收服务端。
            server: './uploaderfile',
            method: 'POST',
            // 选择文件的按钮。可选。
            // 内部根据当前运行是创建，可能是input元素，也可能是flash.
            pick: '#picker',
            auto: true,
            formData: {
                source: $('#source-value').text().replace(/[,.\s]/g,'')
            },
            // chunked:true,
            // threads: 3,
            // 不压缩image, 默认如果是jpeg，文件上传前会压缩一把再上传！
            resize: false
            // accept: {
            //     title: 'xls and csv',
            //     extensions: 'xlsx,xls,csv',
            //     mimeType: 'application/excel,text/comma-separated-values'
            // }
        });

// 显示用户选择
        uploader.on('uploadStart',function(file){

        });
        uploader.on('fileQueued', function (file) {

        });
        uploader.on('filesQueued', function (file) {

        });
// 文件上传进度
        uploader.on('uploadProgress', function (file, percentage) {
            console.log(percentage);
            setCreatLinkHints('no', '添加文件中...');
        });
        //获取唯一字段列表
        var uniqueList;
        function getField(){
            $.ajax({
                url:'http://'+lastIp+'/index.php',  //获得字段列表
                type:'POST',
                async: false,
                data:JSON.stringify({
                    interface:'getField',
                    act:'getUnique',
                    Token:token,
                    username:loginName,
                    filesType:'files'
                }),
                success:function(dataStr){
                    var data=eval('('+dataStr+')');
                    uniqueList=data.Item;
                },
                error:function(err){
                    console.log(err);
                }
            });
        }
        //文件成功失败处理
        uploader.on('uploadSuccess', function (file) {
            $('#' + file.id).find('p.state').text('添加成功，等待上传');
        });
        uploader.on('uploadError', function (file) {
            $('#' + file.id).find('p.state').text('添加出错，请重新添加');

        });
        uploader.on('uploadFinished', function (file) {
//     上传完判断文件格式是否一致
            filesArr=[];
            var files = uploader.getFiles();
            for (var j = 0; j < files.length; j++) {
                filesArr.push(files[j].name);
            }
            getField();
            var fileJson ={};
            fileJson.filesArr =filesArr;
            $.post('./uploader/compareFile', {source: $('#source-value').text(), delFile: JSON.stringify(fileJson)}, function (data) {
                if (data.status === 0) {
                    //如果一致  进入概览 ，弹出上传成功
                    $.ajax({
                        url:'http://' + lastIp + '/index.php',
                        type:'POST',
                        data:JSON.stringify({
                            interface:'getField',
                            act:'getFieldBySource',    //获取数据源的字段
                            sourceName:$('#source-value').text(),
                            typeId:$('#source-second-class-value').attr('data').replace(/^typeId/,''),
                            Token:token,
                            username:loginName
                        }),
                        success:function(dataStr){
                            var data=eval('('+dataStr+')');
                            if(data.status == 0){
                                newSourceB=-1;      //不是新数据源
                                if(!limitAdd){
                                    limitAdd=true;
                                    $('.load-view-table tr:eq(0)').remove();
                                }
                                $('.uploader-source-title tr').html('');
                                $('.uploader-source-title').show();
                                var fieldList=data.Item.field;
                                $('.uploader-data-view-title a').removeClass('uploader-view-change-btn').removeClass('uploader-view-add-btn');
                                $('.uploader-data-view-title a').off('click');
                                $('.uploader-data-view-title a').hover(function(){$('.noEtidHints').show()},function(){$('.noEtidHints').hide()});
                                $('.load-view-table .table-title').parent().removeClass('editTh').find('div:not(".table-title")').remove();
                                $('.load-view-table .table-title').removeClass('table-title');
                                var oDiv='';
                                for(var i=0;i<uniqueList.length;i++){
                                    var oSpan='<span>'+uniqueList[i].uniquefieldname+'</span>';
                                    oDiv+=oSpan;
                                }
                                for (var i = 0; i < fieldList.length; i++) {
                                    var selectTime='';
                                    var unique='';
                                    var star1='';
                                    var star2='';
                                    var star3='';
                                    var positionBox='';
                                    var stationBox='';
                                    var LAC='';
                                    var CID='';
                                    var SID='';
                                    var BID='';
                                    var NID='';
                                    if($('#source-second-class-value').attr('data')=='typeId21'){
                                        var longitudeChecked ='';
                                        var latitudeChecked ='';
                                        if(fieldList[i].isLongitude==0){
                                            longitudeChecked='checked="checked"';
                                        }
                                        if(fieldList[i].isLatitude==0){
                                            latitudeChecked='checked="checked"';
                                        }
                                        if(fieldList[i].isLAC==0){
                                            LAC = 'checked="checked"';
                                        }
                                        if(fieldList[i].isCID==0){
                                            CID = 'checked="checked"';
                                        }
                                        if(fieldList[i].isSID==0){
                                            SID = 'checked="checked"';
                                        }
                                        if(fieldList[i].isBID==0){
                                            BID = 'checked="checked"';
                                        }
                                        if(fieldList[i].isNID==0){
                                            NID = 'checked="checked"';
                                        }
                                        positionBox='<dl class="position-box"><dt>经纬</dt><dd><input type="radio" id="longitude'+i+'" name="position-longitude" '+longitudeChecked+' /><label for="longitude'+i+'">经度</label></dd><dd><input type="radio" id="latitude'+i+'" name="position-latitude" '+latitudeChecked+'/><label for="latitude'+i+'">纬度</label></dd></dl><span></span>';
                                        stationBox='<dl class="station-box"><dt>基站</dt><dd><input type="radio" id="LAC'+i+'" name="LAC" '+LAC+' /><label for="LAC'+i+'">LAC</label></dd><dd><input type="radio" id="CID'+i+'" name="CID" '+CID+'/><label for="CID'+i+'">CID</label></dd>' +
                                            '<dd><input type="radio" id="BID'+i+'" name="BID" '+BID+'/><label for="BID'+i+'">BID</label></dd><dd><input type="radio" id="SID'+i+'" name="SID" '+SID+'/><label for="SID'+i+'">SID</label></dd><dd><input type="radio" id="NID'+i+'" name="NID" '+NID+'/><label for="NID'+i+'">NID</label></dd></dl><span></span>';
                                    }
                                    switch (fieldList[i].level)
                                    {
                                        case '1':
                                            star1='checked';
                                            break;
                                        case '2':
                                            star2='checked';
                                            break;
                                        case '3':
                                            star3='checked';
                                            unique='<div class="search-grade search-only-field fl"><span class="search-grade-title">'+fieldList[i].name+'</span><i></i> <div class="search-grade-list">'+oDiv+ '<span>其他</span><div class="cover"></div></div></div>';
                                            break;
                                    }
                                    if(fieldList[i].isTime==1){
                                        selectTime='checked';
                                    }
                                    // var oTh = '<th class="clearfix"><div class="table-title fl">'+fieldList[i].fieldName+'</div><div class="fl">' +
                                    //     '<div class="search-grade"><span class="search-grade-title search-grade-value">'+level+'</span><i></i> <div class="search-grade-list">'+
                                    //     '<span onclick="hideOnlyField(this)">1星</span><span onclick="hideOnlyField(this)">2星</span><span onclick="showOnlyField(this)">3星</span></div></div><div class="search-grade-radio"><input type="radio" id="time'+i+'" name="select-time" '+selectTime+' value="time'+i+'"><label for="time'+i+'">筛选时间</label></div></div>'+unique+'</th>';

                                    var oTh='<th class="clearfix"><div class="table-title fl">'+fieldList[i].fieldName+'</div><div class="fl add-mark-container"><div class="clearfix add-mark-btn" onclick="showMarkBox(this)"><p class="fl"><span></span><span></span><span></span></p>'+
                                        '<p class="fl">添加标识</p></div>'+
                                        '<div class="add-mark-box clearfix"><dl class="level"><dt>星级</dt><dd><input type="radio" id="start'+i+'_1" name="star'+i+'"  onclick="hideOnlyField(this)" level="1" '+star1+' /><label for="start'+i+'_1" onclick="hideOnlyField(this)">一星</label></dd><dd><input type="radio" id="start'+i+'_2" name="star'+i+'" onclick="hideOnlyField(this)" level="2" '+star2+' /><label for="start'+i+'_2" onclick="hideOnlyField(this)">二星</label></dd><dd><input type="radio" id="start'+i+'_3" name="star'+i+'" onclick="showOnlyField(this)" level="3" '+star3+' /><label for="start'+i+'_3" onclick="showOnlyField(this)">三星</label></dd></dl><span></span>'+positionBox+stationBox+
                                        '<dl><dt>时间</dt><dd><input type="radio" id="screen-time'+i+'" name="screen-time" '+selectTime+'/><label for="screen-time'+i+'" >筛选时间</label></dd></dl><b></b><div class="cover"></div></div></div>'+unique+'</th>';

                                    $('.uploader-source-table tr').append(oTh);
                                }
                                $('.search-grade').off('click');
                                setMarkBoxWidth();
                            }else{
                                isNewSource();
                            }
                        },
                        error:function(err){
                            console.log(err);
                        }
                    });
                    getFilesView();
                } else {
                    setLayer(false, data.message, false);
                    // 插件重置
                    //不一致加以提示
                }
            });
            console.log('重置上传插件');
            uploader.reset();
            console.log(uploader.getFiles());
        });
        var tableTitle;
        uploader.on('uploadFinished', function () {
            if($('#source-class-value').attr('data') == 'typeId6'){
                checkQrCode=true;
                $('.upload-qrcode-box').css({'visibility':'hidden'});
                return;

            }
            $('.upload-qrcode-box').css({'visibility':'visible'});
            uploadImg = WebUploader.create({
                // swf文件路径
                swf: '../javascripts/Uploader.swf',
                auto: true,
                // 文件接收服务端。
                server: './uploadImg',
                method: 'POST',
                pick: {
                    id:"#upload-qrcode",
                    innerHTML:"上传整理人二维码"
                },
                formData: {
                    source: $('#source-value').text().replace(/[,.\s]/g,'')
                },
                resize: false,
                multiple:false,
                accept: {
                    title: 'Images',
                    extensions: 'gif,jpg,jpeg,bmp,png',
                    mimeTypes: 'image/*'
                }
            });
            uploadImg.on( 'fileQueued',function( file ){
                $('#fileList').html('');
                var $li = $(
                        '<div id="' + file.id + '" class="file-item thumbnail">' +
                        '<img>' +
                        '</div>'
                    ),
                    $img = $li.find('img');
                // $list为容器jQuery实例
                $('#fileList').append( $li );

                // 创建缩略图
                // 如果为非图片文件，可以不用调用此方法。
                // thumbnailWidth x thumbnailHeight 为 100 x 100
                uploadImg.makeThumb( file, function( error, src ) {
                    if ( error ) {
                        $img.replaceWith('<span>不能预览</span>');
                        return;
                    }
                    $img.attr( 'src', src );
                }, 154, 154 );

            });
            uploadImg.on( 'uploadProgress', function( file, percentage ) {
                $('.upload-edit').show();
                $('.upload-edit').html(percentage*100+'%');
            });
            uploadImg.on( 'error', function( type ) {
                if(type == 'Q_TYPE_DENIED'){
                    alert('文件格式不正确');
                }
            });
            uploadImg.on('uploadFinished',function(){
                $('.upload-edit').html('');
                $('.upload-edit').css({'text-align':'right'});
                checkQrCode=true;
                var $btn='<div class="fr edit-btn"><span id="uploadImg2">更换</span><b class="fl">|</b><span onclick="initUploadImg();">删除</span></div>';
                $('.upload-edit').append($btn);
                uploadImg.addButton({
                    id:"#uploadImg2"
                });

            });

        });
        var checkQrCode=false;
        function initUploadImg(){
            $('.upload-edit').html('').hide();
            $('.uploader-list').html('');
            if(!!uploadImg)uploadImg.reset();
            checkQrCode=false;
        }
        $('#ctlBtn').on('click', function () {
            uploader.options.formData.source = $('#source-value').text();
            uploader.upload();
            $('#source').css({'color': '#ccc'}).attr('disabled', 'true');
        });
//
        $('#picker').click(function () {
            uploader.options.formData.source = $('#source-value').text();
        });
        var oPicker = document.getElementById('picker');
        oPicker.addEventListener('click', function (e) {
            if($('#source-class-value').text() == '选择上传的类别' ){
                e.preventDefault();
                setLayer(false, '请选择上传信息的类别', false);
            }
            else if($('#source-second-class-value').text() == '选择上传的二级类目' && isShow('.search-second-change')){
                e.preventDefault();
                setLayer(false, '请选择上传信息的二级类目', false);
            }
            else if ($('#source-value').text() == '选择上传的目标数据源') {
                e.preventDefault();
                setLayer(false, '请选择数据源', false);
            }
        }, true);
        function isShow(n){
            if($(n).css('display')=='none'){
                return false;
            }else{
                return true;
            }
        }
// 新数据源获取概览
        function getFilesView() {
            $.ajax({
                url: './uploader/getFile',
                type: 'POST',
                data: {
                    source: $('#source-value').text()
                },
                success: function (data) {
                    if (data.status === 0) {
                        $('.uploader-data-view').off('click','.search-grade');
                        $('.uploader-btn').hide();
                        $('.uploader-data-view').show();

                        $('.layer-bg').hide();
                        $('.creat-link').hide();
                        $('.load-view-table').html('');
                        var dataList = data.data;
                        var specialData=data.specialData;
                        tableTitle = dataList[0].toString();
                        var dataListLength = 0;
                        var arr = [];
                        for (var j = 0; j < dataList.length; j++) {
                            arr.push(dataList[j].length);
                        }
                        dataListLength = arr.sort(function (n1, n2) {
                            return n2 - n1
                        })[0];
                        for (var i = 0; i < dataList.length + 1; i++) {
                            var oTr = '<tr></tr>';
                            $('.load-view-table').append(oTr);

                            for (var j = 0; j < dataListLength; j++) {
                                if (i === 0) {
                                    var tdData;
                                    if (!dataList[i][j] && dataList[i][j] != 0) {
                                        tdData = '';
                                    }else{
                                        tdData=(''+dataList[i][j]).replace(/"/g,'');
                                    }
                                    if(newSourceB == 0){
                                        var positionBox='';
                                        var stationBox='';
                                        if($('#source-second-class-value').attr('data')=='typeId21'){
                                            positionBox='<dl class="position-box"><dt>经纬</dt><dd><input type="radio" id="longitude'+j+'" name="position-longitude" /><label for="longitude'+j+'">经度</label></dd><dd><input type="radio" id="latitude'+j+'" name="position-latitude"/><label for="latitude'+j+'">纬度</label></dd></dl><span></span>';
                                            stationBox='<dl class="station-box"><dt>基站</dt><dd><input type="radio" id="LAC'+j+'" name="LAC"/><label for="LAC'+j+'">LAC</label></dd><dd><input type="radio" id="CID'+j+'" name="CID" /><label for="CID'+j+'">CID</label></dd>' +
                                                '<dd><input type="radio" id="BID'+j+'" name="BID" /><label for="BID'+j+'">BID</label></dd><dd><input type="radio" id="SID'+j+'" name="SID" /><label for="SID'+j+'">SID</label></dd><dd><input type="radio" id="NID'+j+'" name="NID" /><label for="NID'+j+'">NID</label></dd></dl><span></span>';
                                        }
                                        // var oTh = '<th class="clearfix"><div class="table-title fl">' + tdData + '</div><div class="fl">' +
                                        //     '<div class="search-grade"><span class="search-grade-title search-grade-value">1星</span><i></i> <div class="search-grade-list">'+
                                        //     '<span onclick="hideOnlyField(this)">1星</span><span onclick="hideOnlyField(this)">2星</span><span onclick="showOnlyField(this)">3星</span></div></div><div class="search-grade-radio"><input type="radio" id="time'+j+'" name="select-time" value="time'+j+'"><label for="time'+j+'">筛选时间</label></div></div></th>';

                                        var oTh='<th class="clearfix"><div class="table-title fl">'+tdData+'</div><div class="fl add-mark-container"><div class="clearfix add-mark-btn" onclick="showMarkBox(this)"><p class="fl"><span></span><span></span><span></span></p>'+
                                            '<p class="fl">添加标识</p></div>'+
                                            '<div class="add-mark-box clearfix"><dl class="level"><dt>星级</dt><dd><input type="radio" id="start'+j+'_1" name="star'+j+'"  onclick="hideOnlyField(this)" level="1" checked="checked" /><label for="start'+j+'_1" onclick="hideOnlyField(this)">一星</label></dd><dd><input type="radio" id="start'+j+'_2" name="star'+j+'" onclick="hideOnlyField(this)" level="2" /><label for="start'+j+'_2" onclick="hideOnlyField(this)">二星</label></dd><dd><input type="radio" id="start'+j+'_3" name="star'+j+'" onclick="showOnlyField(this)" level="3" /><label for="start'+j+'_3" onclick="showOnlyField(this)">三星</label></dd></dl><span></span>'+positionBox+stationBox+
                                            '<dl><dt>时间</dt><dd><input type="radio" id="screen-time'+j+'" name="screen-time" /><label for="screen-time'+j+'" >筛选时间</label></dd></dl><b></b></div></div></th>';

                                        $('.load-view-table tr:last-child').append(oTh);
                                    }else{
                                        var oTh= '<th><div class="fl">' + tdData + '</div></th>';
                                        $('.load-view-table tr:last-child').append(oTh);
                                    }
                                }
                                else if (i === dataList.length) {
                                    if(!specialData){
                                        var oTd = '<td>...</td>';
                                        $('.load-view-table tr:last-child').append(oTd);
                                    }else{

                                    }
                                }
                                else {
                                    var tdData;
                                    if (!dataList[i][j] && dataList[i][j] != 0) {
                                        tdData = '';
                                    }else{
                                        tdData =(''+dataList[i][j]).replace(/"/g,'');
                                    }
                                    var oTd = '<td>' + tdData + '</td>';
                                    $('.load-view-table tr:last-child').append(oTd);
                                }
                            }
                        }
                        bindGradeClick();
                        setMarkBoxWidth();
                        for(var n=0;n<specialData.length;n++){
                            for(var i=0;i<dataList[0].length;i++){
                                if(i==0){
                                    var oTr = '<tr></tr>';
                                    $('.load-view-table').append(oTr);
                                }
                                var oTd2 = '<td>...</td>';
                                $('.load-view-table tr:last-child').append(oTd2);
                            }
                            for(var k=0;k<dataList[0].length;k++){
                                var tdData;
                                if(k==0){
                                    var oTr = '<tr></tr>';
                                    $('.load-view-table').append(oTr);
                                }
                                if(!specialData[n][k] && specialData[n][k] != 0){
                                    tdData='';
                                }else{
                                    tdData=(''+specialData[n][k]).replace(/"/g,'');
                                }
                                var oTd= '<td>' + tdData+ '</td>';
                                $('.load-view-table tr:last-child').append(oTd);
                            }
                        }
                        for(var i=0;i<dataList[0].length;i++){
                            if(i==0){
                                var oTr = '<tr></tr>';
                                $('.load-view-table').append(oTr);
                            }
                            var oTd2 = '<td>...</td>';
                            $('.load-view-table tr:last-child').append(oTd2);
                        }
                        //新增上传按钮

                        uploader.addButton({
                            id:"#uploader-view-continue"
                        })

                    } else {
                        $('.load-view-table').text(data.message)
                    }
                },
                error: function (err) {
                    setLayer(false, err, false);
                }
            })
        }

        //选择星级部分
        function bindGradeClick(){
            $('.uploader-data-view').on('click','.search-grade',function(e){
                e.stopPropagation();
                console.log('点击上了');
                var list=$(this).find('.search-grade-list');
                console.log(list.css('display'));
                if(list.css('display')=='none'){
                    console.log('展示');
                    list.show();
                }else{
                    console.log('隐藏');
                    list.hide();
                }
            });
        }

        $('.uploader-data-view').on('click','.search-grade-list span',function(e){
            e.stopPropagation();
            console.log($(this).text());
            $(this).parent().parent().find('.search-grade-title').text($(this).text());
            console.log('点击span隐藏');
            $('.search-grade-list').hide();
        });

        function checkOnlyField(){
            if($('.search-only-field').length==0){
                return true;
            }else{
                for(var i=0;i<$('.search-only-field').length;i++){
                    if(!$('.search-only-field').eq(i).find('.search-grade-title').text()){
                        setLayer(false, '3星字段，请指定唯一', false);
                        return false;
                    }
                    return true;
                }
            }
        }
        function checkReadLine(){
            var readLine=$('#read-file-line').val().replace(/\s/g,'');
            if(readLine.length===0){
                setLayer(false, '请填写文件读取起始行', false);
                return false;
            }
            else if(Number(readLine)==0){
                setLayer(false, '读取文件起始行数不能为0', false);
                return false;
            }
            else if( !(/^[0-9]+$/.test(readLine))){
                setLayer(false, '读取文件起始行数请填写数字', false);
                return false;
            }else{
                return true;
            }
        }
        //确定按钮

        function getArrIndex(arr,str){
            for(var i=0;i<arr.length;i++){
                if(arr[i]==str){
                    return i;
                    break;
                }
            }
        }
        var limitClick=true;
        $('.uploader-view-confirm').on('click', function (){
            if(checkOnlyField() && checkReadLine()){
                if(!limitClick)return;
                limitClick=false;
                $('.uploader-view-confirm').css({cursor:'wait'});
                sourceTitle=[];
                var reg = new RegExp('^[0-9a-z\u4E00-\u9FA5-_\(\)]+$');
                var reg2 = new RegExp('^[0-9a-zA-Z\u4E00-\u9FA5-_\(\)]+$');
                var checkPostionSame=true;
                var checkPostionNoLongitude=true;
                var checkPostionNoLatitude=true;

                if(reg.test($('#source-value').text())){
                    var sourceName=$('#source-value').text().replace(/[,.\s]/g,'');
                    var firstClass=$('#source-class-value').text();
                    var firstId=$('#source-class-value').attr('data').replace(/typeId/g,'');
                    var secondClass=$('#source-second-class-value').text();
                    var secondId=$('#source-second-class-value').attr('data').replace(/typeId/g,'');
                    var tableTitleArr = [];
                    var screenTime='';
                    if(newSourceB !=0 || $('.uploader-source-title').css('display')=='block'){
                        var titleLength = $('.uploader-source-table tr:eq(0) th .table-title').length;
                        for (var n = 0; n < titleLength; n++) {
                            tableTitleArr.push($('.uploader-source-table tr th .table-title').eq(n).text().replace(/[,.\s]/g,''));
                        }
                    }else{
                        var titleLength = $('.load-view-table tr:eq(0) th .table-title').length;
                        for (var n = 0; n < titleLength; n++) {
                            tableTitleArr.push($('.load-view-table tr:eq(0) th .table-title').eq(n).text().replace(/[,.\s]/g,''));
                        }
                    }
                    var lac='';
                    var cid='';
                    var bid='';
                    var sid='';
                    var nid='';
                    var lng='';
                    var lat='';
                    var other='';
                    var uniqueWord='';
                    if(!$('#source-class-value').text() == '选择上传类别' || !$('#source-second-class-value').text()=='选择上传二级类目'){
                        firstClass='';
                        secondClass='';
                    }
                    if($('#source-second-class-value').attr('data')=='typeId21'){
                        if($('input:radio[name="position-longitude"]:checked').length===0){
                            checkPostionNoLongitude=false;
                        }
                        if($('input:radio[name="position-latitude"]:checked').length===0){
                            checkPostionNoLatitude=false;
                        }
                        lac=$('input:radio[name=LAC]:checked').length>0?$('input:radio[name=LAC]:checked').parent().parent().parent().parent().parent().find('.table-title').text():'';
                        cid=$('input:radio[name=CID]:checked').length>0?$('input:radio[name=CID]:checked').parent().parent().parent().parent().parent().find('.table-title').text():'';
                        bid=$('input:radio[name=BID]:checked').length>0?$('input:radio[name=BID]:checked').parent().parent().parent().parent().parent().find('.table-title').text():'';
                        sid=$('input:radio[name=SID]:checked').length>0?$('input:radio[name=SID]:checked').parent().parent().parent().parent().parent().find('.table-title').text():'';
                        nid=$('input:radio[name=NID]:checked').length>0?$('input:radio[name=NID]:checked').parent().parent().parent().parent().parent().find('.table-title').text():'';
                        lng=$('input:radio[name=position-longitude]:checked').length>0?$('input:radio[name=position-longitude]:checked').parent().parent().parent().parent().parent().find('.table-title').text():'';
                        lat=$('input:radio[name=position-latitude]:checked').length>0?$('input:radio[name=position-latitude]:checked').parent().parent().parent().parent().parent().find('.table-title').text():'';
                        lac=lac?'LAC#separator#'+getArrIndex(tableTitleArr,lac)+'\n':'';
                        cid=cid?'CID#separator#'+getArrIndex(tableTitleArr,cid)+'\n':'';
                        bid=bid?'BID#separator#'+getArrIndex(tableTitleArr,bid)+'\n':'';
                        sid=sid?'SID#separator#'+getArrIndex(tableTitleArr,sid)+'\n':'';
                        nid=nid?'NID#separator#'+getArrIndex(tableTitleArr,nid)+'\n':'';
                        lng=lng?'经度#separator#'+getArrIndex(tableTitleArr,lng)+'\n':'';
                        lat=lat?'纬度#separator#'+getArrIndex(tableTitleArr,lat)+'\n':'';
                        other=lac+cid+bid+sid+nid+lng+lat;
                    }
                    var uniqueJson={};
                    for(var i=0;i<$('.table-title').length;i++){
                        var json={};
                        json.fieldName=$('.table-title').eq(i).text().replace(/[,.\s]/g,'');

                        json.level=$('.uploader-data-table input:radio[name="star'+i+'"]:checked').attr('level');

                        var onlyField=$('.table-title').eq(i).parent().find('.search-only-field .search-grade-title').text().replace(/[,.\s]/g,'');
                        if(!!onlyField){
                            if(onlyField=='其他'){
                                onlyField=$('.table-title').eq(i).text().replace(/[,.\s]/g,'');
                            }
                            if(!uniqueJson[onlyField]){
                                uniqueJson[onlyField]=new Array();
                            }
                            uniqueJson[onlyField].push(i);
                        }else{
                            onlyField='';
                        }
                        if($('#source-second-class-value').attr('data')=='typeId21'){  //当是地图时判断同意字段  禁止同时选择经度纬度
                            var longitude=$('.uploader-data-table #longitude'+i);
                            var latitude=$('.uploader-data-table #latitude'+i);
                            var isLAC=$('.uploader-data-table #LAC'+i);
                            var isCID=$('.uploader-data-table #CID'+i);
                            var isBID=$('.uploader-data-table #BID'+i);
                            var isSID=$('.uploader-data-table #SID'+i);
                            var isNID=$('.uploader-data-table #NID'+i);
                            json.isLongitude =longitude.attr('checked')?0:-1;
                            json.isLatitude =latitude.attr('checked')?0:-1;
                            json.isLAC =isLAC.attr('checked')?0:-1;
                            json.isCID =isCID.attr('checked')?0:-1;
                            json.isBID =isBID.attr('checked')?0:-1;
                            json.isSID =isSID.attr('checked')?0:-1;
                            json.isNID =isNID.attr('checked')?0:-1;
                            if(json.isLongitude==0 && json.isLatitude==0){
                                checkPostionSame=false;
                            }
                        }else{
                            json.isLongitude=-1;
                            json.isLatitude =-1;
                        }
                        if($('#screen-time'+i).attr('checked') == 'checked'){
                            screenTime='筛选时间#separator#'+i;
                            json.isTime=1;  //选中
                        }else{
                            json.isTime=0;  //未选中
                        }
                        json.name=onlyField;
                        sourceTitle.push(json);
                        if($('#screen-time'+i+':checked').length>0){
                            appointTime=$('#screen-time'+i+':checked').parent().parent().parent().parent().parent().find('.table-title').text();   //指定时间
                        }
                    }
                    for(var i in uniqueJson){
                        uniqueWord+=i+'#separator#'+uniqueJson[i].join(',')+'\n';
                    }

                    if (!checkFalsePos(tableTitleArr)) {   //检查是否有空位
                        if(!checkFindSame(tableTitleArr)){     //检测是否有相同
                            if(checkFields(tableTitleArr)){   //格式符合要求
                                if(!!firstClass || !!secondClass){
                                    if(!!checkPostionSame){
                                        if(!!checkPostionNoLongitude){
                                            if(!!checkPostionNoLatitude){
                                                if(sourceName != '选择上传的目标数据源'){
                                                    if(checkQrCode){
                                                        $.ajax({
                                                            url: './uploader/changeFile',
                                                            type: 'POST',
                                                            data: {
                                                                source: sourceName
                                                            },
                                                            success: function (data) {
                                                                if (data.status === 0) {
                                                                    sendFile(tableTitleArr,uniqueWord,sourceName,firstClass,secondClass,firstId,secondId,screenTime,other);
                                                                }
                                                            },
                                                            error:function(err){
                                                                console.log(err);
                                                            }
                                                        })
                                                    }else{
                                                        noAccordHints('请上传整理人二维码')
                                                    }

                                                }else{
                                                    noAccordHints('请选择数据源');
                                                }
                                            }else{
                                                noAccordHints('您没有选则纬度');
                                            }
                                        }else{
                                            noAccordHints('您没有选择经度');
                                        }

                                    }else{
                                        noAccordHints('同一字段不允许同时选择经度和纬度');
                                    }

                                }else{
                                    noAccordHints('请选择上传类别');
                                }
                            }
                        }else{
                            noAccordHints('上传的文件有重复字段');
                        }
                        //判断是否有修改  没有修改不传参数
                    }else{
                        noAccordHints('编写的字段有空位，请填写后上传');
                    }
                }else{
                    noAccordHints('数据源有特殊字符');
                }
            }else{

            }
        });
        function checkFields(n){
            var reg2 = new RegExp('^[0-9a-zA-Z\u4E00-\u9FA5-_\(\)]+$');
            for(var i=0;i<n.length;i++){
                if(!(reg2.test(n[i]))){
                    setLayer(false, '字段格式不符合要求', false);
                    limitClick=true;
                    $('.uploader-view-confirm').css({cursor:'pointer'});
                    return false;
                }
            }
            return true;
        }
        function noAccordHints(msg){
            limitClick=true;
            setLayer(false, msg, false);
            $('.uploader-view-confirm').css({cursor:'pointer'});
        }

        //建立索引（向es发送文件）
        var sendAllCount={};
        function sendFile(tableTitleArr,uniqueWord,sourceName,firstClass,secondClass,firstId,secondeId,screenTime,other) {
            batch++;
            setCreatLinkHints('no', '已开始上传');
            $('.layer-bg').hide();
            $('.creat-link').hide();
            $('.load-view-table').html('');

            addTitle=$('#read-file-line').val();

            initUploaderPage();
            $('#source-value').text('选择上传的目标数据源');
            limitClick=true;
            $('.uploader-view-confirm').css({cursor:'pointer'});
            $.ajax({
                url:'./uploader/getSendAllCount',
                data:{
                    source: sourceName,
                    batchNum:batch
                },
                type:'POST',
                success:function (data) {
                    if (data.status == 0) {
                        sendAllCount['batch'+batch]=data.all['batch'+batch];
                        //插入进度显示
                        $('.uploader-press-container').show();
                        var oLi='<li class="clearfix fl batch'+batch+'"><h2 class="fl">进程'+batch+'</h2>'+
                            '<div class="fl press-bar-container"><div class="press-box">'+
                            '<span class="press-bar" data="'+sendAllCount['batch'+batch]+'"></span></div><p class="clearfix">'+
                            '<span class="fl press-source-name">'+ sourceName +'</span>'+
                            '<span class="fr press-rate">上传完成</span></p></div>'+
                            '<a href="javascript:void(0);" class="fr press-del" onclick="delPressBox(this)" >关闭</a> </li><span class="press-line fl"></span>';
                            window.onbeforeunload=function(){
                                return '';
                            };
                        $('.press-container ul').append(oLi);
                        checkPressNum();
                        if(batch == 1){
                            runSetInterval();
                        }
                        function toDou(n){
                            return n<10?'0'+n:''+n;
                        }
                        var date = new Date();
                        var y = date.getFullYear();
                        var m = date.getMonth() + 1;
                        var d = date.getDate();
                        var h = date.getHours();
                        var mi = date.getMinutes();
                        var time = y + '-' + toDou(m) + '-' + toDou(d) + '-' + toDou(h) + '-' + toDou(mi);
                        var randomNum=+parseInt(Math.random()*1000);
                        var fieldName=time+'_'+randomNum;       //储存文件夹名称
                        $.ajax({
                            url: './uploader/sendFiles',
                            type: 'POST',
                            data: {
                                source: sourceName,
                                batchNum:batch,
                                sendTitle:tableTitleArr.toString()+'\n'+firstClass+'\n'+secondClass+'\n'+addTitle+'\n'+uniqueWord+other+screenTime,
                                sourceDir:fieldName,
                                firstClass:firstClass
                            },
                            success: function (data) {
                                if (data.status === 0) {
                                    $('.batch'+batch+' .press-bar').css({
                                        width:100+'%'
                                    });
                                    checkPressNum();
                                    // setTimeout(function(){
                                        // setCreatLinkHints(true, '', time);
                                        // $('.manage-nav li:eq(0)').trigger('click');
                                    //     initUploaderPage();
                                    // },500);
                                    var data;
                                    if (!!$('#all-user').attr('checked')) {
                                        data = {
                                            sourceName: $('#source-value').text().replace(/[,.\s]/g,''),
                                            userRights: 'all'
                                        }
                                    } else {
                                        if (!!$('#special-team').attr('checked') && !checkTeamList(sourceUserList)) {
                                            data = {
                                                sourceName: $('#source-value').text().replace(/[,.\s]/g,''),
                                                userRights: 'team',
                                                rightsList: selectTeamChecked()
                                            }
                                        }
                                        else if (!!$('#special-user').attr('checked') && !checkTeamList(sourceTeamList)) {
                                            data = {
                                                sourceName: $('#source-value').text().replace(/[,.\s]/g,''),
                                                userRights: 'user',
                                                rightsLists: selectUserChecked()
                                            }
                                        }
                                        else {
                                            data = {
                                                sourceName: $('#source-value').text().replace(/[,.\s]/g,''),
                                                userRights: 'part',
                                                teamRightsList: selectTeamChecked(),
                                                userRightsList: selectUserChecked()
                                            }
                                        }
                                    }
                                    $.ajax({    //演示完需要解除注释
                                        url:'http://'+lastIp+'/index.php',
                                        type:'POST',
                                        data:JSON.stringify({
                                            interface:'getSource',
                                            act:'setSourceField',
                                            Token:token,
                                            username:loginName,
                                            firstItem:firstId,
                                            secondItem:secondeId,
                                            sourceName:sourceName,
                                            sourceTitle:sourceTitle,
                                            right:data,
                                            sourceDir:sourceName+'_'+fieldName,
                                            addTitle:addTitle,
                                            time:appointTime
                                        }),
                                        success:function(dataStr){
                                            var data = eval('('+dataStr+')');
                                            if(data.status == 0){
                                                limitNewSource=false;
                                            }else{

                                            }
                                        },
                                        error:function(err){
                                            console.log(err);
                                        }
                                    })
                                } else {
                                    clearInterval(timer2);
                                    setCreatLinkHints(false);
                                }
                            },
                            error: function (err) {
                                console.log(err);
                            }
                        });

                    } else {

                    }
                },
                error:function(err){
                    console.log(err);
                }
        });

        }
        function runSetInterval(){
            getSendCount();
            timer2 = setInterval(function () {
                getSendCount();
            }, 1000);
        }
        //实时获取 上传进度
        function getSendCount() {
            var batchLength=$('.press-container ul li').length;
            var completeLength=$('.press-del[style="display: inline;"]').length;
            if(batchLength==completeLength){
                window.onbeforeunload=null;
            }
            var sourceName='';
            if($('.manage-nav ul li.on').text()=='上传数据'){
                sourceName=$('.source-value').text().replace(/[,.\s]/g,'');
            }else{
                sourceName=$('#').contents().find('#m-source-value').text().replace(/[,.\s]/g,'');
            }
            $.ajax({
                url: './uploader/getSendCount',
                type: 'POST',
                data: {
                    source: sourceName,
                    batchNum:batch
                },
                success: function (dataObj) {
                    var data=dataObj.count;
                    if(!!data){
                        $.each(data,function(i,v){
                            if(parseInt(v)>=parseInt($('.'+i+' .press-bar').attr('data'))){
                                v=parseInt($('.'+i+' .press-bar').attr('data'));
                                if( $('.'+i+' .press-del').css('display')=='none'){
                                    $('.'+i+' .press-del').show();
                                    $('.'+i+' .press-rate').show();
                                }
                            }
                            $('.'+i+' .press-bar').css({
                                width:(parseInt(v)/parseInt($('.'+i+' .press-bar').attr('data')))*100+'%'
                            })
                        })
                    }
                }
            })
        }
        $('.uploader-data-table ').on('click','.station-box label',function(){

        });
        function checkTeamList(n){
            for(var i=0;i<n.length;i++){
                if(n[i].state == 0){
                    return true;
                }
            }
            return false;
        }
        function selectTeamChecked(){
            var arr=[];
            for(var i=0;i<sourceTeamList.length;i++){
                if(sourceTeamList[i].state == 0){
                    arr.push(sourceTeamList[i].teamName);
                }
            }
            return arr.toString();
        }
        function selectUserChecked(){
            var arr=[];
            for(var i=0;i<sourceUserList.length;i++){
                if(sourceUserList[i].state == 0){
                    arr.push(sourceUserList[i].username);
                }
            }
            return arr.toString();
        }
        $('.creat-link .creat-link-know').click(function () {
            $('.box-off').trigger('click');
            $('.creat-link .creat-link-know').css('background', '#46a870');
        });
        //取消上传按钮 点击
        $('.uploader-view-cancel').on('click', function () {
            $.ajax({
                url: './uploader/delFiles',
                type: 'POST',
                success: function (data) {
                    initUploaderPage();
                }
            })
        });
        //删除进程
        function delPressBox(n){
            $(n).parent().next().remove();
            $(n).parent().remove();
            checkPressNum();
            if($('.press-bar').length == 0){
                $('.uploader-press-container').hide();
                $('.press-complete-num').text(0);
                $('.press-all-num').text(0);
            }
        }
        //检测进程和完成数
        function checkPressNum(){
            var pressLi=$('.press-container ul li');
            $('.press-all-num').text(pressLi.length);
            var pressNum=0;
            for(var i=0;i<pressLi.length;i++){
                var ele=$('.press-container ul li').eq(i).find('.press-bar');
                var eleWidth=ele.width();
                var parentWidth=ele.parent().width();
                if(!!ele && eleWidth/parentWidth===1){
                    pressNum++;
                    $('.press-container ul li').eq(i).find('.press-del').show();
                    $('.press-container ul li').eq(i).find('.press-rate').show();
                }else{
                    $('.press-container ul li').eq(i).find('.press-del').hide();
                    $('.press-container ul li').eq(i).find('.press-rate').hide();

                }
            }
            $('.press-complete-num').text(pressNum);
        }

        //检查修改的字段是否有空位
        function checkFalsePos(arr) {
            for (var i = 0; i < arr.length; i++) {
                if (!arr[i]) {
                    return true;  //如果有空位返回true
                }
            }
            return false;
        }

        //上传文件页初始化
        var limitAdd = true; //限制添加按钮 只添加一次
        function initUploaderPage() {
            $('.uploader-btn').show();
            $('.load-view-table').html('');
            $('.uploader-data-view').hide();

            limitAdd = true;
            limitNewSource=false;
            uploader.reset();
            $('.read-file-line').val('');
            initUploadImg();
        }
        //查重
        function checkFindSame(arr) {
            for (var i = 0; i < arr.length; i++) {
                for (var j = i + 1; j < arr.length; j++) {
                    if (arr[i] == arr[j]) {
                        return true;
                    }
                }
            }
            return false;
        }

        function hideOnlyField(n){
            $(n).parent().parent().parent().parent().parent().find('.search-only-field').remove();
        }
       function showOnlyField(n){
            $(n).parent().parent().parent().parent().parent().find('.search-only-field').remove();
            var oDiv='';
            for(var i=0;i<uniqueList.length;i++){
                var oSpan='<span>'+uniqueList[i].uniquefieldname+'</span>';
                oDiv+=oSpan;
            }
            oDiv='<div class="search-grade search-only-field fl"><span class="search-grade-title"></span><i></i> <div class="search-grade-list">'+oDiv+ '<span>其他</span></div></div>';
            $(n).parent().parent().parent().parent().parent().append(oDiv);
        }
//弹窗打包
        function setLayer(boolean, value, n) {
            //boolean：正确提示还是错误提示；value：提示内容；n：是否有按钮（true、false）
            $('.hints-layer img').removeAttr();
            $('.hints-layer .hints-txt').text('');
            if (boolean) {
                $('.hints-layer img').attr({
                    src: "../images/icon_upload_complete.png"
                })
            } else {
                $('.hints-layer img').attr({
                    src: "../images/icon_upload_error.png"
                })
            }
            if (n) {
                $('.hints-layer .hints-layer-btn').show();
            } else {
                $('.hints-layer .hints-layer-btn').hide();
            }
            $('.hints-layer .hints-txt').text(value);
            $('.hints-layer').show();
            $('.layer-bg').show();
        }

        //建立索引弹窗打包
        function setCreatLinkHints(boolean, value, date) {
            if (boolean === 'no') {
                $('.creat-link .creat-link-waiting').text(value);
                $('.creat-link .creat-link-waiting').show();
                $('.creat-link .creat-link-hints').hide();
            }
            else if (boolean) {
                $('.creat-link img').attr({
                    src: "../images/icon_upload_complete.png"
                });
                // var data = new Date();
                // var y=data.getFullYear();
                // var month=data.getMonth();
                // var day=data.getDate();
                // var h=data.getHours();
                // var m=data.getMinutes();
                // var time=y+'-'+month+'-'+day+'  '+h+':'+m;
                // $('.creat-link .hints-txt').text('您于' + date + '上传至"' + $('#source-value').text() + '"的数据建立索引完毕，可正常搜索');
                $('.creat-link .creat-link-waiting').hide();
                $('.creat-link .creat-link-hints').show();
            }
            else {
                $('.creat-link img').attr({
                    src: "../images/icon_upload_error.png"
                });
                $('.creat-link .creat-link-know').css('backgound', '#d0433f');
                $('.creat-link .hints-txt').text('创建索引过程中出现异常，请刷新页面重新上传文件');
                $('.creat-link .creat-link-waiting').hide();
                $('.creat-link .creat-link-hints').show();
            }
            $('.creat-link').show();
            $('.layer-bg').show();
        }

        $('.delHints .del-hints-no').on('click', function () {
            $('.box-off').trigger('click');
        });
    }else if(admin==-1){
        $('#skip-login').remove();
        layer(false,'对不起，您没有权限登陆后台');
        setTimeout(function(){
            window.location.href='search';
        },1500)
    }else{
        layer(false,'对不起，您还没有登录');
        setTimeout(function(){
            // window.location.href='login';
        },1000)
    }
    //指定用户组
    $('.special-team-sel,#special-team').on('click',function(e){
        if($('#source-value').text()=='选择上传的目标数据源'){
            e.preventDefault();
            setLayer(false, '请选择数据源', false);
        }else{
            eachTeamList();
            $('.special-team-box').show();
            $('.special-user-box').hide();
        }
    });
    $('.special-user-sel, #special-user').on('click',function(e){
        if($('#source-value').text()=='选择上传的目标数据源'){
            e.preventDefault();
            setLayer(false, '请选择数据源', false);
        }else{
            eachUserList();
            $('.special-user-box').show();
            $('.special-team-box').hide();
        }
    });
    $('.special-all-sel,#all-user').on('click',function(e){
        $('.special-team-box').hide();
        $('.special-user-box').hide();
        if($('#source-value').text()=='选择上传的目标数据源'){
            e.preventDefault();
            setLayer(false, '请选择数据源', false);
        }
    });

//画圆

    function circleProgress(obj,value){
        var canvas = document.getElementById(obj);
        var context = canvas.getContext('2d');
        var _this = $(canvas),
            value= Number(value),// 当前百分比,数值
            color = "#bccc74",// 进度条、文字样式
            maxpercent = 100,//最大百分比，可设置
            c_width = _this.width(),// canvas，宽度
            c_height =_this.height();// canvas,高度
        // 判断设置当前显示颜色

        // 清空画布
        context.clearRect(0, 0, c_width, c_height);
        //画线

        context.stroke();
        // 画初始圆
        context.beginPath();
        // 将起始点移到canvas中心
        context.moveTo(c_width/2, c_height/2);
        // 绘制一个中心点为（c_width/2, c_height/2），半径为c_height/2，起始点0，终止点为Math.PI * 2的 整圆
        context.arc(c_width/2, c_height/2, c_height/2, 0, Math.PI * 2, false);
        context.closePath();
        context.fillStyle = '#619360'; //填充颜色
        context.fill();
        // 绘制内圆
        context.beginPath();
        context.strokeStyle = color;
        // context.lineCap = 'square';
        context.closePath();
        context.fill();
        context.lineWidth = 16;//绘制内圆的线宽度

        function draw(cur){
            // 画内部空白
            context.beginPath();
            context.moveTo(24, 24);
            context.arc(c_width/2, c_height/2, c_height/2-15, 0, Math.PI * 2, true);
            context.closePath();
            context.fillStyle = 'rgba(255,255,255,1)';  // 填充内部颜色
            context.fill();
            // 画内圆
            context.beginPath();
            // 绘制一个中心点为（c_width/2, c_height/2），半径为c_height/2-5不与外圆重叠，
            // 起始点-(Math.PI/2)，终止点为((Math.PI*2)*cur)-Math.PI/2的 整圆cur为每一次绘制的距离
            context.arc(c_width/2, c_height/2, c_height/2-8, -(Math.PI / 2), ((Math.PI * 2) * cur ) - Math.PI / 2, false);
            context.stroke();
            //在中间写字
            context.font = "14px";  // 字体大小，样式
            context.fillStyle = '#999';  // 颜色
            context.textAlign = 'center';  // 位置
            context.textBaseline = 'top';
            context.moveTo(50,50);  // 文字填充位置
            context.fillText('总容量',40 , 24);
            context.fillText('8T',40 , 40);
        }
        draw(value/100);
    }





