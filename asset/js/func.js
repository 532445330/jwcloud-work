/******************************************************************************
Function: validateRegForm
Parameters: none
Return: true or false
Description: Validate the form for emptiness.
******************************************************************************/
function validateRegForm()
{console.log(window.document.RegisterForm);
    var regForm = window.document.RegisterForm;
    var name = regForm.username.value;
    var pwd = regForm.password.value;
    var rPwd = regForm.repassword.value;
    if (isEmpty(name)) {
        reportStatus(907);
        return false;
    }
    else {
        if (name.length < 1 || name.length > 32) {
            reportStatus(914);
            return false;
        }
        if (!isLegalCharacters(name)) {
            reportStatus(903);
            return false;
        }
    }
    if (isEmpty(pwd)) {
        reportStatus(904);
        return false;
    }
    else {
        if (pwd.length < 6 || pwd.length > 16) {
            reportStatus(905);
            return false;
        }
        if (!isLegalCharacters(pwd)) {
            reportStatus(906);
            return false;
        }

    }
    if (isEmpty(rPwd)) {
        reportStatus(908);
        return false;
    }
    else {
        if (rPwd.length < 6 || rPwd.length > 16) {
            reportStatus(909);
            return false;
        }
        if (!isLegalCharacters(rPwd)) {
            reportStatus(910);
            return false;
        }
    }
    if (pwd != rPwd) {
        reportStatus(911);
        regForm.password.value ="";
        regForm.repassword.focus();
        return false;
    }

    return true;
}

/******************************************************************************
Function: validateChangeForm
Parameters: none
Return: true or false
Description: Validate the form for emptiness.
******************************************************************************/
function validateChangeForm() {
    var cForm = window.document.ChangePinForm;
    var oP = cForm.oldPwd.value;
    var nP = cForm.newPwd.value;
    var rP = cForm.retypePwd.value;

    if (isEmpty(oP)) {
        reportStatus(912);
        return false;
    }
    else {
        if (oP.length < 6 || oP.length > 16) {
            reportStatus(905);
            return false;
        }
        if (!isLegalCharacters(oP)) {
            reportStatus(906);
            return false;
        }
    }
    if (isEmpty(nP)) {
        reportStatus(913);
        return false;
    }
    else {
        if (nP.length < 6 || nP.length > 16) {
            reportStatus(905);
            return false;
        }
        if (!isLegalCharacters(nP)) {
            reportStatus(906);
            return false;
        }
    }
    if (isEmpty(rP)) {
        reportStatus(913);
        return false;
    }
    else {
        if (rP.length < 6 || rP.length > 16) {
            reportStatus(905);
            return false;
        }
        if (!isLegalCharacters(rP)) {
            reportStatus(906);
            return false;
        }
    }
    if (nP != rP) {
        reportStatus(911);
        cForm.newPwd.value = "";
        cForm.retypePwd.value = "";
        cForm.newPwd.focus();
        return false;
    }

    return true;
}

/******************************************************************************
Function: isLegalCharacters
Parameters: string
Return: true or false
Description: Judge the string whether it is legal.
******************************************************************************/
function isLegalCharacters(s)
{
    var str;
    var len;
    var i;
    var c;

    str = new String(s);
    len = str.length;

    for(i=0; i < len; i++) {

        c = str.charAt(i);
        if ( (!(c >= '0' && c <= '9'))
            && (!(c >= 'a' && c <= 'z'))
            && (!(c >= 'A' && c <= 'Z'))
        )
        {
            return false;
        }
    }
    return true;
}

/******************************************************************************
Function: isEmpty
Parameters: string
Return: true or false
Description: Judge the string whether it is empty.
******************************************************************************/
function isEmpty(strValue)
{
    var myString = new String(strValue);
    var len = myString.length;
    if ("" == myString)
    {
        return true;
    }
    for (var i=0; i < len; ++i)
    {
        if(myString.charAt(i) != " ")
        {
            return false;
        }
    }
    return true;
}

/******************************************************************************
Function: getChallenge
Parameters: none
Return: challenge
Description: Send XMLHttpRequest get challenge.
******************************************************************************/
function getChallenge()
{
	var challenge = sendRequest("Auth?func=getChallenge");
	return ""+challenge+"";
}

/******************************************************************************
Function: getAuthCode
Parameters: none
Return: authCode
Description: Send XMLHttpRequest get authCode.
******************************************************************************/
function getAuthCode()
{	
	var authCode;
	authCode = sendRequest("/ConfigInfo?data=AuthCode");
	return ""+authCode+"";
}

/**********************************************************************************************
Function: doAuth
Parameters: dogID, result
Return: factor
Description: Send XMLHttpRequest do authenticate.
***********************************************************************************************/
function doAuth(dogid, result)
{
	var ret = sendRequest("Auth?func=Authentication&dogid="+dogid+"&result="+result+"");
	return ret;
}

/******************************************************************************
Function: sendRequest
Parameters: url
Return: response text
Description: Send XMLHttpRequest
******************************************************************************/
function sendRequest(url)
{
	var httpRequest = false;
	
  	if(window.XMLHttpRequest)
  	{
    	httpRequest = new XMLHttpRequest();
	}
	else
	{
		try
		{	
			httpRequest = new ActiveXObject("Msxm12.XMLHTTP");
		}
		catch(e)
		{
			try
			{
				httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch(e)
			{
			}
		}
	}
	if(!httpRequest)
	{
		return false;
	}
	httpRequest.open('POST', url, false);
	httpRequest.send(null);
	var xmlText = httpRequest.responseText;
	var reg = /[^\[][^\]]*[^\]]/;
	var newXmlText = reg.exec(xmlText);
	return newXmlText;
}

/******************************************************************************
Function: getAuthObject
Parameters: none
Return: an object for authentication
Description: get object for authentication
******************************************************************************/
function getAuthObject() {
    var objAuth;
    if (window.ActiveXObject || "ActiveXObject" in window) //IE
    {
        objAuth = document.getElementById("AuthIE");
    }
    else if (navigator.userAgent.indexOf("Chrome") > 0) {
        objAuth = getAuthObjectChrome();
    }
    else {
        objAuth = document.getElementById("AuthNoIE");
    }
    return objAuth;
}

/******************************************************************************
Function: embedTag
Parameters: none
Return: none
Description: embed tag of object for authentication
******************************************************************************/
function embedTag() {
    if (window.ActiveXObject || "ActiveXObject" in window) {//IE
        ;
    }
    else if (navigator.userAgent.indexOf("Chrome") > 0) {//Chrome
        ;
    }
    else {
        var temp = document.body.innerHTML;
        var tag = "<embed id=\"AuthNoIE\" type=\"application/x-dogauth\" width=0 height=0 hidden=\"true\"></embed>";
        document.body.innerHTML = tag + temp;
    }
}

/******************************************************************************
Function: clearInfo
Parameters: none
Return: none
Description: Clear the error info displayed in page.
******************************************************************************/
function clearInfo() {
    document.getElementById("error-hints").innerHTML = "";
}

/******************************************************************************
Function: reportStatus
Parameters: status
Return: Description
Description: Report status's description.
******************************************************************************/
function reportStatus(status) {
    var text = "Unknown status code";
    switch (status) {
        case 0: text = "请求已成功完成";
            break;
        case 1: text = "请求超出数据文件的范围 ";
            break;
        case 3: text = "系统内存不足";
            break;
        case 4: text = "打开的登录会话数目过多";
            break;
        case 5: text = "访问被拒绝";
            break;
        case 7: text = "没有搜索到u-key";
            break;
        case 8: text = "加密/解密的数据长度太短 ";
            break;
        case 9: text = "输入函数的句柄无效";
            break;
        case 10: text = "无法识别文件标识符";
            break;
        case 15: text = "无效的 XML 格式";
            break;
        case 18: text = "未找到待升级的u-key";
            break;
        case 19: text = "数据无效";
            break;
        case 20: text = "该u-key不支持升级请求";
            break;
        case 21: text = "升级计数器设置不正确";
            break;
        case 22: text = "输入的开发商代码无效";
            break;
        case 24: text = "输入的时间值超出被支持的数值范围";
            break;
        case 26: text = "升级要求回执数据，但输入参数 ack_data 为 NULL";
            break;
        case 27: text = "程序在终端服务器上运行 ";
            break;
        case 29: text = "V2C 文件中使用了未知算法";
            break;
        case 30: text = "签名验证失败";
            break;
        case 31: text = "特征不可用 ";
            break;
        case 33: text = "API 和u-key运行环境（License Manager）通讯错误 ";
            break;
        case 34: text = "API 不识别开发商代码";
            break;
        case 35: text = "无效的 XML 格式 ";
            break;
        case 36: text = "无效的 XML 范围 ";
            break;
        case 37: text = "当前连接的u-key数目过多";
            break;
        case 39: text = "会话被中断";
            break;
        case 41: text = "特征已失效 ";
            break;
        case 42: text = "u-key的运行环境版本太旧";
            break;
        case 43: text = "与u-key通讯中出现 USB 通信错误 ";
            break;
        case 45: text = "系统时钟已被篡改 ";
            break;
        case 46: text = "安全通道中发生了通信错误 ";
            break;
        case 50: text = "没有搜索到ukey";
            break;
        case 54: text = "文件中的升级计数器的数值小于u-key 中的升级计数器的数值，不允许安装 V2C 文件";
            break;
        case 55: text = "文件中的升级计数器的数值大于u-key 中的升级计数器的数值，不允许安装 V2C 文件";
            break;
        case 400: text = "未找到 API 的动态库 ";
            break;
        case 401: text = "API 的动态库无效 ";
            break;
        case 500: text = "对象的初始化不正确";
            break;
        case 501: text = "无效的函数参数 ";
            break;
        case 502: text = "两次登录到同一对象";
            break;
        case 503: text = "从同一对象注销两次 ";
            break;
        case 525: text = "系统或平台的使用不正确 ";
            break;
        case 698: text = "未实施所请求的功能 ";
            break;
        case 699: text = "API 中内部错误 ";
            break;
        case 802: text = "参数为空";
            break;
        case 803: text = "认证代码长度不正确 ";
            break;
        case 804: text = "请先登录";
            break;
        case 810: text = "口令长度不正确 ";
            break;
        case 811: text = "参数长度不正确 ";
            break;
        case 812: text = "用户数据长度不正确";
            break;
        case 813: text = "用户名长度不正确";
            break;
        case 814: text = "认证因子长度不正确";
            break;
        case 820: text = "";
            break;
        case 821: text = "请先验证管理员口令";
            break;
        case 822: text = "请先验证用户口令";
            break;
        case 823: text = "缓冲区长度不足";
            break;
        case 824: text = "认证动态库初始化失败 ";
            break;
        case 825: text = "用户口令被锁定 ";
            break;
        case 831: text = "Verify password failed, you still have 14 chances";
            break;
        case 832: text = "Verify password failed, you still have 13 chances";
            break;
        case 833: text = "Verify password failed, you still have 12 chances";
            break;
        case 834: text = "Verify password failed, you still have 11 chances";
            break;
        case 835: text = "Verify password failed, you still have 10 chances";
            break;
        case 836: text = "Verify password failed, you still have 9 chances";
            break;
        case 837: text = "Verify password failed, you still have 8 chances";
            break;
        case 838: text = "Verify password failed, you still have 7 chances";
            break;
        case 839: text = "Verify password failed, you still have 6 chances";
            break;
        case 840: text = "Verify password failed, you still have 5 chances";
            break;
        case 841: text = "Verify password failed, you still have 4 chances";
            break;
        case 842: text = "Verify password failed, you still have 3 chances";
            break;
        case 843: text = "Verify password failed, you still have 2 chances";
            break;
        case 844: text = "Verify password failed, you still have 1 chance";
            break;
        case 845: text = "用户口令被锁定";
            break;
        case 901: text = "认证失败 ";
            break;
        case 902: text = "生成挑战数据失败 ";
            break;
        case 903: text = "用户名包含不支持的字符 ";
            break;
        case 904: text = "请输入口令 ";
            break;
        case 905: text = "密码长度须在 6-16 字节之间";
            break;
        case 906: text = "密码包含不支持的字符";
            break;
        case 907: text = "请输入用户名";
            break;
        case 908: text = "请再次输入口令";
            break;
        case 909: text = "密码长度须在 6-16 字节之间 ";
            break;
        case 910: text = "密码包含不支持的字符 ";
            break;
        case 911: text = "口令和确认口令不一致 ";
            break;
        case 912: text = "请输入当前口令 ";
            break;
        case 913: text = "请输入新口令 ";
            break;
        case 914: text = "用户名长度须在 1-32 字节之间 ";
            break;
        case 915: text = "此u-key已经注册过，不支持再次注册 ";
            break;
        case 916: text = "在 java.library.path 指定的文件夹找不到 dog_auth_srv 库文件 ";
            break;
        case 917: text = "获取挑战数据失败 ";
            break;
        case 918: text = "获取挑战数据失败 ";
            break;
        case 919: text = "找不到会话文件，请确认会话文件夹已 经正确创建和设置";
            break;
        case 920: text = "加载动态库失败：dog_auth_srv_php.dll， 请确认配置文件已经正确设置";
            break;

    }
    errShow($('#error-hints'),true,text);
}

/**********************************************************************************************
Class: AuthObject
Dynamic prototype method
Description: used for Chrome and produced with dynamic prototype method
***********************************************************************************************/
function AuthObject() {

    if (typeof AuthObject._initialized == "undefined") {
        // GetUserNameEx
        AuthObject.prototype.GetUserNameEx = function (scope, authCode) {
            //console.log("enter GetUserNameEx()");
            window.postMessage({ type: "SNTL_FROM_PAGE", text: { "InvokeMethod": "GetUserNameEx", "Scope": scope, "AuthCode": authCode} }, "*");
            return 0;
        };

        // GetDigestEx
        AuthObject.prototype.GetDigestEx = function (scope, authCode, password, challenge) {
            //console.log("enter GetDigestEx()");
            window.postMessage({ type: "SNTL_FROM_PAGE", text: { "InvokeMethod": "GetDigestEx", "Scope": scope, "AuthCode": authCode, "UserPin": password, "Challenge": challenge} }, "*");
            return 0;
        };

        // RegisterUserEx   
        AuthObject.prototype.RegisterUserEx = function (scope, authCode, username, password) {
            //console.log("enter RegisterUserEx()");
            window.postMessage({ type: "SNTL_FROM_PAGE", text: { "InvokeMethod": "RegisterUserEx", "Scope": scope, "AuthCode": authCode, "Name": username, "UserPin": password } }, "*");
            return 0;
        };

        // ChangeUserPinEx
        AuthObject.prototype.ChangeUserPinEx = function (scope, authCode, oldPassword, newPassword) {
            //console.log("enter ChangeUserPinEx()");
            window.postMessage({ type: "SNTL_FROM_PAGE", text: { "InvokeMethod": "ChangeUserPinEx", "Scope": scope, "AuthCode": authCode, "OldPin": oldPassword, "NewPin": newPassword } }, "*");
            return 0;
        };

        // SetUserDataEx
        AuthObject.prototype.SetUserDataEx = function (scope, authCode, password, type, offset, data) {
            //console.log("enter SetUserDataEx()");
            window.postMessage({ type: "SNTL_FROM_PAGE", text: { "InvokeMethod": "SetUserDataEx", "Scope": scope, "AuthCode": authCode, "UserPin": password, "Type": type, "Offset": offset, "Data": data } }, "*");
            return 0;
        };

        // GetUserDataEx
        AuthObject.prototype.GetUserDataEx = function (scope, authCode, type, offset, size) {
            //console.log("enter GetUserDataEx()");
            window.postMessage({ type: "SNTL_FROM_PAGE", text: { "InvokeMethod": "GetUserDataEx", "Scope": scope, "AuthCode": authCode, "Type": type, "Offset": offset, "Size": size } }, "*");
            return 0;
        };

        // EnumDog
        AuthObject.prototype.EnumDog = function (authCode) {
            //console.log("enter EnumDog()");
            window.postMessage({ type: "SNTL_FROM_PAGE", text: { "InvokeMethod": "EnumDog", "AuthCode": authCode} }, "*");
            return 0;
        };

        AuthObject._initialized = true;
    }
}

/**********************************************************************************************
Function: getAuthObjectChrome
Parameters: none
Return: an AuthObject object
Description: get object for authentication 
***********************************************************************************************/
function getAuthObjectChrome() {
    var obj = new AuthObject();
    return obj;
}
//错误提示
function errShow(obj,isShow,message){
    if(isShow){
        obj.css('display','block');
        obj.html('<i></i>'+message);
    }
    else{
        obj.css('display','none');
        obj.html('<i></i>'+message);
    }
}