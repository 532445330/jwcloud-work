/**
 *
 * Created by UPC on 2017/11/16.
 */

const gulp = require('gulp');
const uglify = require('gulp-uglify');
const notify = require('gulp-notify');
const path = require('path');
const watch = require('gulp-watch');
const sourcemaps = require('gulp-sourcemaps');
const plumber = require('gulp-plumber');
const toPath=__dirname+'/../public/javascripts/';

gulp.task('uglify',function(){
   gulp.src('./js/*.js')
       .pipe(sourcemaps.init())
       .pipe(plumber())
       .pipe(uglify())
       .pipe(sourcemaps.write('./maps'))
       .pipe(gulp.dest(toPath))
       .pipe(notify('uglify accountManagejs complete!'));

});

gulp.task('default',['uglify','watchJs'],function(){

});

gulp.task('watchJs',function(){
    gulp.watch('./js/*.js',function(a){
        var filename=a.path.split('\\').slice(-1);
        gulp.src(a.path)
            .pipe(sourcemaps.init())
            .pipe(plumber())
            .pipe(uglify())
            .pipe(sourcemaps.write('./maps'))
            .pipe(gulp.dest(toPath))
            .pipe(notify('uglify '+filename+' complete!'));

    });
});